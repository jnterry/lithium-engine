#UDL - Universal Data Language

##Definitions:
Token: A string of characters begining with a (, comma or whitespace and ending with a ), comma or whitespace

##Introduction
Every type in UDL is broken in fields, each field has 4 attributes:

* Qualifiers
	* fileconst -> const value that identifies a file is for the correct type of data
	* transient -> in code, not in file
	* opt -> optional
* Type Name
* Field Name
* Value

The full syntax for expressing an entire field is:

	qualifiers %type-name field-name = (value)
	
However, it is important to note every attribute of the field is optional - more on this below.
	
##Primatives
UDL has a few built in types which are the foundation for all types in UDL, all types are made up
of these built in primative types and other types.

The full list of primatives is:

* Int8 -> signed 8 bit integer type
* uInt8 -> unsigned 8 bit integer type
* Int16 -> signed 16 bit integer type
* uInt16 -> unsigned 16 bit integer type
* Int32 -> signed 32 bit integer type
* uInt32 -> unsigned 32 bit integer type
* Int64 -> signed 64 bit integer type
* uInt64 -> unsigned 64 bit integer type
* float -> 32 bit floating point type
* double -> 64 bit floating point type
* bool -> 8 bit type which can be one of two states, true and false

##Simple Example
With this knowlege we can represent a simple 3d vector as follows:

	(%float x = (1), %float y = (2), %float z = (3))

As stated above, every attribute of a field is optional, hence the data for the 3d vector could instead by represented as follows:

	((1), (2), (3))
	
Also note that a primative value can be sepecified without brackets, furthur simplifing the 3d vector data to:

	(1,2,3)

This optional nature of field attriutes allows simple types to be expressed very quickly and easily while allowing
larger more complex types to be expressed  with clarity. The extra details also allow for data valiation while loading.

All of the following examples are valid ways of specifing the 3d vector:

	(1,2,3)
	(%float = 1, %float = 2, %float = 3)
	(x = 1, y = 2, z = 3)
	(%float = 1, y = 2, 3)
	
The rules for parsing a field are as follows:

* If the field contains only 1 token it is interpreted to be the fields value
* Else
	* If a % symbol is present
		* Everything before the % is interpreted to be a quallifier
		* Everything after the % symbol but before the first whitespace after it is part of the type-name
	* If a = symbol is present everything after it and before the end of the field (, symbol) is the value of field
	* If there is a seperate token before the = symbol but after the type-name it is the field name, if no % is present then any token before the = is the field name
		
While loading data:

* A field without a specified type-name will have the type inferred depending on how the engine is trying to load the data
* A field without a specified field-name will be loaded into the fields of the type in the order they are found
	* EG: The c++ type:
	
		struct Vector3f{
			float x;
			float y;
			float z;
		}
		
	Being loaded from:
	
		(1,2,3)
			
	Will have the first UDL field (value 1) loaded into the first field in the struct - 'x', 2 into 'y' and 3 into 'z'
			
##A More Complex Example
Lets say we have the following types registered with the MetaType system in c++

	struct Vector2f{
		float x;
		float y;
	}

	struct Transform2d{
		Vector2f position;
		float rotation;
	}

	struct Camera2d{
		Vector2f viewport; //x is the width, y the height
		Transform2d location;
		float fov; //field of view
	}

The data for an entire Camera2d could be written into UDL as follows:

	(
		Vector2f viewport = (1024,768),
		Transform2d location = (
			Vector2d position = (0,0),
			float rotation = 180,
		),
		float fov = 90
	)

Stipping out all the optional details yields the following:

	((1024,768),((0,0),180,),90)
	
This is simply a depth first list of the tree like data structure.

##Defining Types
In order for the above to work the MetaType system must know about the non priamtive types, 
###Compile Time Types
These are types for which there is a class or struct in c++, registering them with the MetaType system allows their type-name to
be used in UDL.
###Runtime Types
The second way is to load a type from UDL.
This can be done in one of two ways, the first is to explicitly define the data for a MetaType which will be loaded and registered with the MetaType system.
EG:

The MetaType for the c++ code:

	struct Vector2f{
		float x;
		float y;
	}
Could instead by loaded from the following UDL:

	%MetaType(
		%std::string name = "Vector2f",
		%uInt16 mFieldCount = 2,
		%MetaTypeField[mFieldCount] mFields = (
			(
				%std::string mName = "x",
				%uInt32 mCount = 1,
				%QualifiedMetaType mType = $float,
				%uInt32 mOffset = 0,
			),
			(
				%std::string mName = "y",
				%uInt32 mCount = 1,
				%QualifiedMetaType mType = $float,
				%uInt32 mOffset = 4,
			)
		)
	)

Or, using a pure data representation:

	("Vector2f",2,(("x",1,$float,0),("y",1,$float,4)))

The first approach is very verbose, while the second is quite confusing as to what is going on.
The second method to define a type is implicitly, simply write UDL as normal but ommit the value attribute of each field, eg:

	%Vector2f(
		%float x,
		%float y,
	)

When the parser comes across this definition, if it does not already know of a type names "Vector2f" it will implicitly
construct the type, automatically calculating byte offsets as it knows the size of each type making up Vector2f.







	
	