LOGGING TUTORIAL

Just as std::cout is an instance of std::ostream inside the namespace std, lie::util::DLog is an instance of the lie::util::Logger class
inside the lie::util namespace.

The usual syntax for logging is:
logger.write(LIE_LOGMSG_TRACE([module-name]) << "message" << "more message");
(where "TRACE" can be replaced with any other default message type)

The default log can also be used in this way, however, so that logging can be disable at compile time the following macros are also provided:
LIE_DLOG_TRACE(std::string moduleName, message)
LIE_DLOG_INFO(std::string moduleName, message)
LIE_DLOG_REPORT(std::string moduleName, message)
LIE_DLOG_WARN(std::string moduleName, message)
LIE_DLOG_ERROR(std::string moduleName, message)
LIE_DLOG_FATAL(std::string moduleName, message)
LIE_DLOG(unsigned char level, std::string moduleName, message)

The message can be treated as a std::ostream and hence the stream operators can be used
eg:
LIE_DLOG_TRACE("module", "Val of i = " << i << ", value of x = " << x);

Internally all logging done by the Lithium Engine uses these macros so you are able to disable logging at compile time by recompiling the engine
Logging is enabled by default, this can be disabled by defining the following:
#define LIE_DISABLE_DLOG_TRACE
#define LIE_DISABLE_DLOG_INFO
#define LIE_DISABLE_DLOG_REPORT
#define LIE_DISABLE_DLOG_WARN
#define LIE_DISABLE_DLOG_ERROR
#define LIE_DISABLE_DLOG_FATAL
#define LIE_DISABLE_DLOG_CUSTOM

alternativly, you can define
LIE_DISABLE_DLOG_ALL to disable all log levels at once

This means that less important or all logging levels can be disabled by defining (or not defining) various things,
this can save performance in production code.
