#include <lie\test.hpp>
#include <lie-test\all.hpp>

int main(int argc, char** argv) {
	lie::test::TestGroup rootTester("Root");
	lie::Test_All(&rootTester);
	if (rootTester.saveReport("Unit Test Report.html")){
		LIE_DLOG_INFO("quicktest", "Unit test report saved!");
	}
	else {
		LIE_DLOG_ERROR("quicktest", "Failed to save unit test report!");
	}
	return 0;
}