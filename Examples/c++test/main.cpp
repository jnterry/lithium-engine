#include <initializer_list>
#include <string>
#include <iostream>
#include <cstddef>
#include <type_traits>
#include <typeinfo>

template<unsigned int T>
class Vector{
public:
    float data[T];

};

template<>
class Vector<2>{
public:
    union{
        float data[2];
        struct {
            float x;
            float y;
        };
    };
};

template<>
class Vector<3>{
public:
    union{
        float data[3];
        struct {
            float x;
            float y;
            float z;
        };
    };
};

template<>
class Vector<4>{
public:
    union{
        float data[4];
        struct {
            float x;
            float y;
            float z;
            float w;
        };
    };
};

int main(int argc, char **argv){
    Vector<3> v;
    v.data[0] = 10;
    v.data[1] = 11;
    v.data[2] = 12;
    std::cout << v.x << " " << v.y << " " << v.z << " " << v.data[0] << " " << v.data[1] << " " << v.data[2] << std::endl;
    v.x = 1;
    v.y = 2;
    v.z = 3;
    std::cout << v.x << " " << v.y << " " << v.z << " " << v.data[0] << " " << v.data[1] << " " << v.data[2] << std::endl;
}
