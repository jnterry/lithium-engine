/*
 * File:   main.cpp
 * Author: Jamie
 *
 * Created on 25 October 2013, 16:44
 */
#define LIE_ENABLE_DLOG_ALL

//#include <SFML\Window.hpp>
//#include <GL\glew.h>
//#include <GL\gl.h>

#include "Globals.hpp"

#include <lie/core.hpp>
#include <lie/test.hpp>
#include <lie/math.hpp>
#include <lie/render.hpp>
#include <lie/game.hpp>
#include <lie-test\all.hpp>

#include <iostream>
#include <map>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <chrono>

#define CAM_MOVE_MULT 10.0
#define CAM_ROT_MULT 1.5

void rdbAddFolder(lie::core::ResourceDatabase& rdb, lie::core::Folder& folder){
    LIE_DLOG_TRACE("quicktest", "Scanning for resource in folder: " << folder.getFullPath());
    auto fileList = folder.getFiles();
    for(auto fileIt = fileList.begin(); fileIt != fileList.end(); ++fileIt){
        if(fileIt->getExtension() == "shader"){
            rdb.addResource(new lie::core::Resource<lie::rend::ShaderProgram>(*fileIt, 0, fileIt->getFullName(), "shader"));
        } else if (fileIt->getExtension() == "jpg"){
            rdb.addResource(new lie::core::Resource<lie::rend::Texture>(*fileIt, 0, fileIt->getFullName(), "jpg"));
        } else if (fileIt->getExtension() == "obj"){
            rdb.addResource(new lie::core::Resource<lie::rend::Mesh>(*fileIt, 0, fileIt->getFullName(), "obj"));
        } else {
            LIE_DLOG_REPORT("quicktest", "Found resource of unknown type: " << fileIt->getFullName() << ", ignoring.");
        }
    }
}

struct Vertex{
    lie::math::Vector3f position;
    lie::math::Vector3f normal;
    float uv[2];
};

int main(int argc, char** argv) {
	
	/*lie::core::Logger* defaultLogger = new lie::core::Logger();
	defaultLogger.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterHtml("Log.html")));
	defaultLogger.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterCout()));

	lie::core::ServiceProvider::register<lie::core::Logger, lie::core::ServiceContextGlobal>(lie::core::Logger);

	lie::core::Logger* = lie::core::ServiceProvider::get < lie::core::Logger, lie::rend::ServiceContext::sub<lie::rend::Mesh>() >();*/
	
    ////////////////////////////////////
    //set up logs
    lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterHtml("Log.html")));
    lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterCout()));

    ////////////////////////////////////
    //init render system
    lie::rend::Device* device = lie::rend::Device::createDevice();
	if (device == nullptr){
		return -1;
	}
    lie::rend::Window* lwin = device->createWindow(lie::rend::VideoMode(1024,768,32), "Title", lie::rend::Window::Default);

    LIE_DLOG_INFO("quicktest", "MaxMeshAttributes: " << device->querryFeature(lie::rend::Device::Feature::MaxMeshAttributes));

    ////////////////////////////////////
    //load resources
	resDb.registerLoader<lie::rend::ShaderProgram>("shader", &lie::rend::resourceLoaderShaderFileList);
	resDb.registerLoader<lie::rend::Texture>("jpg", &lie::rend::resourceLoaderTextureJpg);
    resDb.registerLoader<lie::rend::Mesh>("obj",
        [&device](lie::core::File& file, unsigned int byteOffset) -> lie::rend::Mesh*{
            lie::rend::MeshDataBuffer mdb(lie::rend::MeshVertexLayout({
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3), //pos
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3), //norm
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,2)}));//uv
            if(lie::rend::mesh::loadObj(file, mdb, 0, 1, 2)){
                if(((mdb.getVertex<Vertex>(0)->normal == lie::math::Vector3f(0,0,0)))){
                    //then normals not loaded
                    LIE_DLOG_INFO("quicktest", "Mesh " << file.getFullPath() << " had no normals, calculating them");
                    lie::rend::mesh::calculateSmoothNormals<lie::math::Vector3f, lie::math::Vector3f>(mdb, 0,1);

                }

                LIE_DLOG_TRACE("quicktest", "Loaded obj: " << file.getFullName());
                return mdb.buildMesh(*device);
            } else {
                LIE_DLOG_ERROR("quicktest", "Failed to load obj: " << file.getFullName());
                return nullptr;
            }
        });

    //add all the resources
    rdbAddFolder(resDb, lie::core::Folder::getExecutableFolder().push("data"));


    lie::core::ResourceHandle<lie::rend::ShaderProgram> prog2dBasic(
                                        resDb.getResource<lie::rend::ShaderProgram>("Basic2d.shader"));
    lie::core::ResourceHandle<lie::rend::ShaderProgram> prog3dPhong(
                                        resDb.getResource<lie::rend::ShaderProgram>("Phong3d.shader"));
    lie::core::ResourceHandle<lie::rend::ShaderProgram> prog3dLightless(
                                        resDb.getResource<lie::rend::ShaderProgram>("Lightless3d.shader"));
	lie::core::ResourceHandle<lie::rend::ShaderProgram> prog3dDepthRend(
										resDb.getResource<lie::rend::ShaderProgram>("DepthBuffer3d.shader"));
    lie::core::ResourceHandle<lie::rend::Texture> desertTexture(
                                        resDb.getResource<lie::rend::Texture>("Desert.jpg"));
    lie::core::ResourceHandle<lie::rend::Texture> whiteTexture(
                                        resDb.getResource<lie::rend::Texture>("white.jpg"));
    lie::core::ResourceHandle<lie::rend::Texture> bricksTexture(
                                        resDb.getResource<lie::rend::Texture>("bricks.jpg"));
	lie::core::ResourceHandle<lie::rend::Texture> motherOfPearlTexture(
										resDb.getResource<lie::rend::Texture>("mother-of-pearl.jpg"));
	lie::core::ResourceHandle<lie::rend::Texture> hTexture(
										resDb.getResource<lie::rend::Texture>("h-letter.jpg"));
    lie::core::ResourceHandle<lie::rend::Mesh> monkeyMesh(
                                        resDb.getResource<lie::rend::Mesh>("monkey3.obj"));
    lie::core::ResourceHandle<lie::rend::Mesh> bunnyMesh(
                                        resDb.getResource<lie::rend::Mesh>("bunny.obj"));
    lie::core::ResourceHandle<lie::rend::Mesh> torusMesh(
                                        resDb.getResource<lie::rend::Mesh>("torus.obj"));

    lie::rend::MeshVertexLayout basicMeshVertLayout = lie::rend::MeshVertexLayout({
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3),  //pos
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3),  //norm
                                            lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,2)});//uv

    /////////////////////////////////////////////////////
    //MESH 1 - INDEX MODE
	float  vertBuffData[] = {
	    -0.5f, -0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f,
	    0.5f, -0.5f, 0.0f,
	    0.5f, 0.5f, 0.0f,
	};

	float vertTexCordBuffData[] = {
	    0.0f, 0.0f,
	    0.0f, 1.0f,
	    1.0f, 0.0f,
	    1.0f, 1.0f,
	};

	float vertNormBuffData[] = {
	    0.0f, 0.0f, -1.0f,
	    0.0f, 0.0f, -1.0f,
	    0.0f, 0.0f, -1.0f,
	    0.0f, 0.0f, -1.0f,
	};

	unsigned int mesh1IndexData[] = {
        2,1,0,
        3,1,2,
	};

    lie::rend::MeshDataBuffer lmesh1Dbuf(basicMeshVertLayout);
    lmesh1Dbuf.setVertexCount(4);
    lmesh1Dbuf.setAttribData(0, &vertBuffData, 4);
    lmesh1Dbuf.setAttribData(1, &vertNormBuffData, 4);
    lmesh1Dbuf.setAttribData(2, &vertTexCordBuffData, 4);
    lmesh1Dbuf.appendIndicies(mesh1IndexData, 6);
    //lie::rend::mesh::calculateSmoothNormals<lie::math::Vector3f, lie::math::Vector3f>(lmesh1Dbuf, 0, 1);
    lie::rend::Mesh* lmesh = lmesh1Dbuf.buildMesh(*device);

	lie::rend::Mesh* meshCube = lie::rend::MeshGenerator(*device).genRegularPolygon(3, 6, false);

    lie::rend::Renderer3dImmediate renderer3d;
	lie::rend::Renderer3dImmediate renderer3dFinal;
    lie::rend::Renderer2dImmediate renderer2d(1);

    lie::core::Timer timer;

	const lie::rend::MaterialType* basic3dSurface = device->createMaterialType(1)
		.addAttribute("diffuseColor", lie::math::Vector4f(1,1,1,1))
		.addAttribute("specularColor", lie::math::Vector4f(1,1,1,1))
		.addAttribute("specularIntensity", 1)
		.addAttribute("specularExponent", 30)
		.addAttribute("emissiveColor", lie::math::Vector4f(1,1,1,1))
		.build();

	const lie::rend::MaterialType* depthSurface = device->createMaterialType(1)
		.build();

	lie::rend::RenderTexture* rendTexture = device->createRenderTexture(1024, 768, 1, true, false);

	lie::rend::Material* desertMaterial = basic3dSurface->createMaterial();
	desertMaterial->setTexture(0, desertTexture);

	lie::rend::Material* whiteMaterial = basic3dSurface->createMaterial();
	whiteMaterial->setTexture(0, whiteTexture);
	whiteMaterial->setAttribute("diffuseColor", lie::math::Vector4f(1, 0, 0, 1));
	whiteMaterial->setAttribute("specularIntensity", 1.0f);
	whiteMaterial->setAttribute("specularExponent", 70.0f);

	lie::rend::Material* metalMaterial = basic3dSurface->createMaterial();
	metalMaterial->setTexture(0, whiteTexture);
	metalMaterial->setAttribute("specularIntensity", 1.0f);
	metalMaterial->setAttribute("specularExponent", 500.0f);

	lie::rend::Material* bricksMaterial = basic3dSurface->createMaterial();
	bricksMaterial->setTexture(0, bricksTexture);
	bricksMaterial->setAttribute("specularIntensity", 0.0f);
	bricksMaterial->setAttribute("specularExponent", 50.0f);

	lie::rend::Material* rendTextureMaterial = basic3dSurface->createMaterial();
	rendTextureMaterial->setTexture(0, rendTexture->getColorBuffer(0));

	lie::rend::Material* rendTextureDepthMaterial = depthSurface->createMaterial();
	rendTextureDepthMaterial->setTexture(0, rendTexture->getDepthBuffer());

	lie::rend::Material* hMaterial = basic3dSurface->createMaterial();
	hMaterial->setTexture(0, hTexture);

	lie::rend::Material* hDistMaterial = basic3dSurface->createMaterial();
	hDistMaterial->setTexture(0, hTexture);
	


	lie::rend::Pass lPassFilled(*prog3dPhong);
    lPassFilled.setFaceMode(lie::rend::FaceMode::Filled, lie::rend::FaceMode::Wireframe);
	lPassFilled.setTextureSource(0, lie::rend::Pass::TextureSource::MATERIAL, 0);
	lPassFilled.setParameterSource("diffuseColor", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 0);
	lPassFilled.setParameterSource("specularIntensity", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 2);
	lPassFilled.setParameterSource("specularExponent", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 3);
	lPassFilled.setParameterSource("mvpMatrix", lie::rend::Pass::ParameterSource::MVP_MATRIX);
	lPassFilled.setParameterSource("modelMatrix", lie::rend::Pass::ParameterSource::MODEL_MATRIX);
	lPassFilled.setParameterSource("cameraPosition", lie::rend::Pass::ParameterSource::CAMERA_POSITION);
	lPassFilled.setParameterSource("time", lie::rend::Pass::ParameterSource::TIME);
	lie::rend::Technique lTechFilled(&lPassFilled);
	lie::rend::Methodology methodFilledPhong(*basic3dSurface, lTechFilled);

	lie::rend::Pass lPassFilledLightless(*prog3dLightless);
	lPassFilledLightless.setFaceMode(lie::rend::FaceMode::Filled, lie::rend::FaceMode::Wireframe);
	lPassFilledLightless.setTextureSource(0, lie::rend::Pass::TextureSource::MATERIAL, 0);
	lPassFilledLightless.setParameterSource("diffuseColor", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 0);
	lPassFilledLightless.setParameterSource("specularIntensity", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 2);
	lPassFilledLightless.setParameterSource("specularExponent", lie::rend::Pass::ParameterSource::MATERIAL_ATTRIBUTE, 3);
	lPassFilledLightless.setParameterSource("mvpMatrix", lie::rend::Pass::ParameterSource::MVP_MATRIX);
	lPassFilledLightless.setParameterSource("modelMatrix", lie::rend::Pass::ParameterSource::MODEL_MATRIX);
	lPassFilledLightless.setParameterSource("cameraPosition", lie::rend::Pass::ParameterSource::CAMERA_POSITION);
	lPassFilledLightless.setParameterSource("time", lie::rend::Pass::ParameterSource::TIME);
	lie::rend::Technique lTechFilledLightless(&lPassFilledLightless);
	lie::rend::Methodology methodFilledLightless(*basic3dSurface, lTechFilledLightless);

	lie::rend::Pass lPassDepth(*prog3dDepthRend);
	lPassDepth.setFaceMode(lie::rend::FaceMode::Filled, lie::rend::FaceMode::Cull);
	lPassDepth.setTextureSource(0, lie::rend::Pass::TextureSource::MATERIAL, 0);
	lPassDepth.setParameterSource("mvpMatrix", lie::rend::Pass::ParameterSource::MVP_MATRIX);
	lPassDepth.setParameterSource("modelMatrix", lie::rend::Pass::ParameterSource::MODEL_MATRIX);
	lPassDepth.setParameterSource("camNear", lie::rend::Pass::ParameterSource::CAMERA_NEAR_CUT_OFF_3D);
	lPassDepth.setParameterSource("camFar", lie::rend::Pass::ParameterSource::CAMERA_FAR_CUT_OFF_3D);
	lie::rend::Technique lTechDepth(&lPassDepth);
	lie::rend::Methodology methodDepth(*depthSurface, lTechDepth);


    lie::rend::Pass lPassFilled2d(*prog2dBasic);
    lPassFilled2d.setDepthTestMode(lie::rend::DepthTestMode::None);
    lPassFilled2d.setFaceMode(lie::rend::FaceMode::Filled);
    lie::rend::Technique lTechFilled2d(&lPassFilled2d);
	lie::rend::Methodology lMatFilled2d(*basic3dSurface, lTechFilled2d);

    lie::math::Shape2f testShape(  {lie::math::Vector2f(-5.0f,-5.0f),
                                    lie::math::Vector2f(-5.0f, 5.0f),
                                    lie::math::Vector2f( 5.0f, 5.0f),
                                    lie::math::Vector2f( 5.0f,-5.0f)});
    lie::core::Timer fpsTimer;
    lie::core::Timer frameTimer;
    unsigned int frameCounter = 0;
    LIE_DLOG_TRACE("quicktest", "Entering main loop");
    lie::math::Vector2f lastMousePos = lie::math::Vector2f(0,0);
    while(lwin->isOpen()){
        ++frameCounter;
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::A)){
			renderer3d.getCamera().transform.position.x -= frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::D)){
			renderer3d.getCamera().transform.position.x += frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::W)){
			renderer3d.getCamera().transform.position.z -= frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::S)){
			renderer3d.getCamera().transform.position.z += frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::LShift)){
			renderer3d.getCamera().transform.position.y += frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::LControl)){
			renderer3d.getCamera().transform.position.y -= frameTimer.getElapsedTime().asSeconds()*CAM_MOVE_MULT;
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Up)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitX, lie::math::rad(frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Down)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitX, lie::math::rad(-frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Left)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::rad(frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Right)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::rad(-frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Q)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitZ, lie::math::rad(frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
        if(lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::E)){
			renderer3d.getCamera().transform.rotation *= lie::math::Quaternion3f(lie::math::Vector3f::UnitZ, lie::math::rad(-frameTimer.getElapsedTime().asSeconds()*CAM_ROT_MULT));
        }
		frameTimer.restart();

        const lie::rend::VideoMode vidMode = lwin->getVideoMode();
        renderer2d.setViewport(lie::math::Aabb2i(0,0, vidMode.size.x, vidMode.size.y));
        renderer2d.getCamera().setSize(vidMode.size).setCenter(-100,-300);

		renderer3d.setViewport(lie::math::Aabb2i(0, 0, 1024, 768));
		rendTexture->clear();
		renderer3d.begin(*rendTexture);

		{
			lie::math::Matrix4x4f modelMat;
			modelMat *= lie::math::Matrix4x4f::createRotationX(lie::math::deg(90 + timer.getElapsedTime().asMilliseconds() / 14));
			modelMat *= lie::math::Matrix4x4f::createRotationY(lie::math::deg(30 + timer.getElapsedTime().asMilliseconds() / -17));
			modelMat *= lie::math::Matrix4x4f::createRotationZ(lie::math::deg(12 + timer.getElapsedTime().asMilliseconds() / -20));
			modelMat *= lie::math::Matrix4x4f::createTranslation(7, 0, 0);
			modelMat *= lie::math::Matrix4x4f::createRotationY(lie::math::deg(120 + timer.getElapsedTime().asMilliseconds() / 14));
			modelMat *= lie::math::Matrix4x4f::createTranslation(0, 0, -20);
			renderer3d.draw(methodFilledPhong, *metalMaterial, modelMat, *torusMesh);
		}

		{
			lie::math::Matrix4x4f modelMat;
			modelMat *= lie::math::Matrix4x4f::createRotationY(lie::math::deg(timer.getElapsedTime().asMilliseconds() / 10));
			modelMat *= lie::math::Matrix4x4f::createRotationX(lie::math::deg(90 + timer.getElapsedTime().asMilliseconds() / -17));
			modelMat *= lie::math::Matrix4x4f::createTranslation(4, 0, -20);
			renderer3d.draw(methodFilledPhong, *bricksMaterial, modelMat, *monkeyMesh);
		}

		{
			lie::math::Matrix4x4f modelMat;
			modelMat *= lie::math::Matrix4x4f::createRotationY(lie::math::deg(timer.getElapsedTime().asMilliseconds() / 10));
			modelMat *= lie::math::Matrix4x4f::createRotationX(lie::math::deg(90 + timer.getElapsedTime().asMilliseconds() / -17));
			modelMat *= lie::math::Matrix4x4f::createTranslation(-4, 0, -20);
			renderer3d.draw(methodFilledPhong, *metalMaterial, modelMat, *monkeyMesh);
		}


		{
			lie::math::Transform3f trans;

			lie::math::Vector3f position(0, 3, 0);
			position.rotate(lie::math::Quaternion3f(lie::math::Vector3f::UnitX, lie::math::deg(timer.getElapsedTime().asMilliseconds() / -17)));
			position.rotate(lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::deg(30)));
			position += lie::math::Vector3f(0, 0, -20);
			trans.position = position;
			trans.position = lie::math::Vector3f(0,0,-10);
			trans.scale = lie::math::Vector3f(0.5f, 0.5f, 0.5f);
			trans.rotation = lie::math::Quaternion3f(lie::math::Vector3f::UnitX, lie::math::deg(90 + timer.getElapsedTime().asMilliseconds() / 14));
			//trans.rotation *= (lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::toRadians(30 + timer.getElapsedTime().asMilliseconds() / -17)));
			//trans.rotation *= (lie::math::Quaternion3f(lie::math::Vector3f::UnitZ, lie::math::toRadians(12 + timer.getElapsedTime().asMilliseconds() / -20)));

			renderer3d.draw(methodFilledPhong, *desertMaterial, trans.asMatrix(), *meshCube);
		}

		{
			lie::math::Transform3f trans;
			//trans.setRotation(lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::toRadians(timer.getElapsedTime().asMilliseconds()/10)));
			trans.position = lie::math::Vector3f(4.5, -3.69, -15);
			trans.scale = lie::math::Vector3f(20, 20, 20);
			renderer3d.draw(methodFilledPhong, *whiteMaterial, trans.asMatrix(), *bunnyMesh);
		}

		{
			lie::math::Transform3f trans;
			trans.position = lie::math::Vector3f(0, -3, -15);
			trans.scale = lie::math::Vector3f(20, 20, 20);
			trans.rotation = (lie::math::Quaternion3f(lie::math::Vector3f::UnitX, lie::math::deg(90.0f))
				* lie::math::Quaternion3f(lie::math::Vector3f::UnitY, lie::math::deg(180.0f)));
			renderer3d.draw(methodFilledPhong, *bricksMaterial, trans.asMatrix(), *lmesh);
		}

		renderer3dFinal.setViewport(lie::math::Aabb2i(0, 0, vidMode.size.x, vidMode.size.y));
		lwin->clear();
		renderer3dFinal.begin(*lwin);
		{
			lie::math::Transform3f trans;
			trans.position = lie::math::Vector3f(0, 0, -25);
			trans.scale = lie::math::Vector3f(26.666, 20, 20);
			renderer3dFinal.draw(methodFilledLightless, *rendTextureMaterial, trans.asMatrix(), *lmesh);
		}

		{
			lie::math::Transform3f trans;
			trans.position = lie::math::Vector3f(7.5, 7, -24);
			trans.scale = lie::math::Vector3f(4, 4, 4);
			renderer3dFinal.draw(methodFilledLightless, *hMaterial, trans.asMatrix(), *lmesh);
		}

		{
			lie::math::Transform3f trans;
			trans.position = lie::math::Vector3f(10.5, 7, -24);
			trans.scale = lie::math::Vector3f(4, 4, 4);
			renderer3dFinal.draw(methodFilledLightless, *hDistMaterial , trans.asMatrix(), *lmesh);
		}

		{
			lie::math::Transform3f trans;
			trans.position = lie::math::Vector3f(-9, 7, -24);
			trans.scale = lie::math::Vector3f(8, 5.5, 6);
			renderer3dFinal.draw(methodDepth, *rendTextureDepthMaterial, trans.asMatrix(), *lmesh);
		}

		/*renderer2d.begin();
		renderer2d.draw(lMatFilled2d,
			lie::math::Matrix3x3f::createScale(30) *
			lie::math::Matrix3x3f::createRotation(lie::math::rad(timer.getElapsedTime().asMilliseconds() / 500.0f)),
			testShape);*/
			

        lie::rend::Event e;
        while(lwin->pollEvent(e)){
            if(e.type == lie::rend::Event::Closed){
                lwin->close();
            }else if (e.type == lie::rend::Event::Type::MouseMoved){
                //std::cout << "x: " << e.mouse.x << ", y: " << e.mouse.y << std::endl;
            }else if (e.type == lie::rend::Event::Type::MouseButtonPressed){
                //std::cout << "MouseClicked: " << e.mouse.button << "x: " << e.mouse.x << ", y: " << e.mouse.y << std::endl;
            }else if(e.type == lie::rend::Event::Type::MouseMoved){
                std::cout << "Mouse Moved!" << std::endl;
                lie::math::Vector2f newMousePos = lie::math::Vector2f(e.mouse.x, e.mouse.y);
                lie::math::Vector2f mouseDelta = newMousePos - lastMousePos;
                if(mouseDelta.getLength() < 100){
                    //renderer3d.getCamera().setLookDirection(renderer3d.getCamera().getLookDirection().getRotated(lie::math::Quaternion3f(lie::math::Vector3f::UnitY, 0.01)));
                }
                lastMousePos = newMousePos;
			} else if (e.type == lie::rend::Event::Type::KeyReleased){
				if (e.key.keycode == lie::rend::Keyboard::O){
					lPassFilled.setFaceMode(lie::rend::FaceMode::Filled);
				} else if (e.key.keycode == lie::rend::Keyboard::P){
					lPassFilled.setFaceMode(lie::rend::FaceMode::Wireframe);
				}
			}
        }

        lwin->display();
        if(fpsTimer.getElapsedTime().asMilliseconds() >= 1000){
            std::cout << "FPS: " << frameCounter << std::endl;
            fpsTimer.restart();
            frameCounter = 0;
        }
    }

	std::cout << "Execution done, press enter to end the program..." << std::endl;
	std::cin.sync();
	std::cin.get();

    LIE_DLOG_TRACE("quicktest", "Exited main loop, quitting program.");
    return 0;
}
