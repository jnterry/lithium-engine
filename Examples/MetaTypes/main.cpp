#include <iostream>
#include <lie\core\MetaType.hpp>
#include <lie\core\TypeInfo.hpp>


//helper function, prints tree like structure for a type, its sub types, etc
void printMetaType(lie::core::MetaType type, unsigned int level = 0){
    std::cout << "Type: " << type.getName() << ", field count: " << (size_t)type.getFieldCount() << ", size: " << type.getStaticSize() << std::endl;
    for(int i = 0; i < type.getFieldCount(); ++i){
        for(int j = 0; j < level; ++j){
            std::cout << "    ";
        }
        std::cout << "Field " << i << ", offset: " << type.getField(i).getOffset() << ", name: " << type.getField(i).getName() << ", ";
        printMetaType(type.getField(i).getType().getBaseType(), level+1);
    }
}

//first custom class
class Vec2{
public:
    float x;
    float y;
};
//define the metatype for the custom class Vec2
//doing it the long verbose way, manually constructing the QualifiedMetaType from a MetaType
template<> const lie::core::MetaType lie::core::TypeInfo<Vec2>::type = lie::core::MetaType("Vec2", sizeof(Vec2), {
                    lie::core::MetaTypeField(lie::core::QualifiedMetaType(lie::core::TypeInfo<int>::type),"x", offsetof(Vec2,x)),
                    lie::core::MetaTypeField(lie::core::QualifiedMetaType(lie::core::TypeInfo<int>::type),"y", offsetof(Vec2,y))});

//second custom class
class Transform2{
public:
    Vec2 position;
    float rotation;
};

//define the metatype for the custom class Transform2
//this time  using the helper function to automatically get an instance of QuallifiedMetaType
//obviously you can also pass in quallifiers, eg <Vec2*>, <const float&>, etc
template<> const lie::core::MetaType lie::core::TypeInfo<Transform2>::type = lie::core::MetaType("Transform2", sizeof(Transform2), {
                    lie::core::MetaTypeField(lie::core::typeInfoQualified<Vec2>(),"position", offsetof(Transform2,position)),
                    lie::core::MetaTypeField(lie::core::typeInfoQualified<float>(),"rotation", offsetof(Transform2,rotation))});


//third custom class
class Camera2d{
public:
    Vec2 viewport;
    Transform2 transform;
    unsigned char fov;
};
//define the metatype for the custom class Camera2d
template<> const lie::core::MetaType lie::core::TypeInfo<Camera2d>::type = lie::core::MetaType("Camera2d", sizeof(Camera2d), {
                    lie::core::MetaTypeField(lie::core::typeInfoQualified<Vec2>(),"viewport", offsetof(Camera2d,viewport)),
                    lie::core::MetaTypeField(lie::core::typeInfoQualified<Transform2>(),"transform", offsetof(Camera2d,transform)),
                    lie::core::MetaTypeField(lie::core::typeInfoQualified<unsigned char>(),"fov", offsetof(Camera2d,fov))});

int main(int argc, char** argv) {

    //first print info on all the primative types
    std::cout << "Primative Types:" << std::endl;
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    printMetaType(lie::core::TypeInfo<std::uint8_t>::type);
    printMetaType(lie::core::TypeInfo<std::int8_t>::type);
    printMetaType(lie::core::TypeInfo<std::uint16_t>::type);
    printMetaType(lie::core::TypeInfo<std::int16_t>::type);
    printMetaType(lie::core::TypeInfo<std::uint32_t>::type);
    printMetaType(lie::core::TypeInfo<std::int32_t>::type);
    printMetaType(lie::core::TypeInfo<std::uint64_t>::type);
    printMetaType(lie::core::TypeInfo<std::int64_t>::type);
    printMetaType(lie::core::TypeInfo<float>::type);
    printMetaType(lie::core::TypeInfo<double>::type);
    printMetaType(lie::core::TypeInfo<bool>::type);

    //print out info on vec2
    std::cout << std::endl << "Vec2:" << std::endl;
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    printMetaType(lie::core::TypeInfo<Vec2>::type);

    //print out info on Transform2
    std::cout << std::endl << "Transform2:" << std::endl;
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    printMetaType(lie::core::TypeInfo<Transform2>::type);

    //print out info on Camera2d
    std::cout << std::endl << "Camera2d:" << std::endl;
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    printMetaType(lie::core::TypeInfo<Camera2d>::type);


    return 0;
}
