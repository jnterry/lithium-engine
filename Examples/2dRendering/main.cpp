#define LIE_DEBUG
#include <iostream>
#include <lie/core.hpp>
#include <lie/math.hpp>
#include <lie/render.hpp>

#define CAM_MOVE_MULT 1300
#define CAM_ROT_MULT 200
#define CAM_ZOOM_MULT 0.005

void handleCameraControls(lie::rend::Renderer2d* renderer){
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::A)){
		renderer->getCamera().move(lie::math::Vector2f(-renderer->getFrameTime().asSeconds()*CAM_MOVE_MULT, 0));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::D)){
		renderer->getCamera().move(lie::math::Vector2f(renderer->getFrameTime().asSeconds()*CAM_MOVE_MULT, 0));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::W)){
		renderer->getCamera().move(lie::math::Vector2f(0, renderer->getFrameTime().asSeconds()*CAM_MOVE_MULT));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::S)){
		renderer->getCamera().move(lie::math::Vector2f(0, -renderer->getFrameTime().asSeconds()*CAM_MOVE_MULT));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Q)){
		renderer->getCamera().rotate(lie::math::deg(renderer->getFrameTime().asSeconds()*CAM_ROT_MULT));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::E)){
		renderer->getCamera().rotate(lie::math::deg(-renderer->getFrameTime().asSeconds()*CAM_ROT_MULT));
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::Z)){
		renderer->getCamera().zoom(1 + CAM_ZOOM_MULT);
	}
	if (lie::rend::Keyboard::isPressed(lie::rend::Keyboard::Key::X)){
		renderer->getCamera().zoom(1 - CAM_ZOOM_MULT);
	}
}

int main(int argc, char** argv) {
	lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterHtml("Log.html")));
	lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterCout()));

	lie::rend::Device* device = lie::rend::Device::createDevice();
	lie::rend::Window* app = device->createWindow(lie::rend::VideoMode(1024, 768), "Lithium Engine 2d Rendering Example");
	lie::rend::Renderer2d* renderer = new lie::rend::Renderer2dImmediate(1);
	renderer->setViewport(lie::math::Aabb2i(0, 0, 1024, 768));

	lie::math::Shape2f shapePentagon = lie::math::Shape2f::createRegularPolygon(5, 100);
	lie::math::Shape2f shapeSquare = lie::math::Shape2f::createRegularPolygon(4, 100);

	lie::rend::Methodology* method = lie::rend::Methodology::createDefault2D(*device);
	lie::rend::Material* material = method->getMaterialType().createMaterial();

	int frameCounter = 0;
	lie::core::Timer frameTimer;
	while (app->isOpen()){
		handleCameraControls(renderer);
		app->clear(lie::rend::BufferTypes::All);

		renderer->begin(*app);

		renderer->draw(*method, *material, 
			lie::math::Matrix3x3f::createTranslation(lie::math::Vector2f(0,0)), 
			0, shapePentagon);

		renderer->draw(*method, *material,
			lie::math::Matrix3x3f::createTranslation(lie::math::Vector2f(-500, 0)),
			0, shapeSquare);

		renderer->end();

		app->display();

		lie::rend::Event e;
		while (app->pollEvent(e)){
			switch (e.type){
			case lie::rend::Event::Closed:
				app->close();
				break;
			default:
				break;
			}
		} //end of event loop
		
		++frameCounter;
		if (frameTimer.getElapsedTime().asSeconds() > 0.5){
			std::cout << "FPS: " << frameCounter * 2 << std::endl;
			frameCounter = 0;
			frameTimer.restart();
		}
	} //end of main loop

	delete method;

	std::cout << "Program ended, press enter to close" << std::endl;
	std::cin.sync();
	std::cin.get();
	return 0;
}