#version 330 core

layout(location = 0) in vec3 vertPosition;
layout(location = 1) in vec3 vertNormal;
layout (location = 2) in vec2 vertTexCoord;

out vec2 texCoord0;
out vec3 faceNormal;
out vec3 worldPosition;




//model view projection matrix
//note, the 3 matricies are multiplied together on CPU as otherwise the same
//mpv matrix would have to be calculated on every vertex from 3 input uniform matricies
uniform mat4 mvpMatrix = mat4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
uniform mat4 modelMatrix = mat4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);

void main(){
	gl_Position = mvpMatrix * vec4(vertPosition, 1.0);
	faceNormal = normalize((modelMatrix * vec4(vertNormal, 0.0)).xyz);
	texCoord0 = vertTexCoord;
	worldPosition = (modelMatrix * vec4(vertPosition, 1.0)).xyz;
}
