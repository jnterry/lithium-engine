#version 330 core

uniform sampler2D diffuse;

smooth in vec4 color;
smooth out vec4 outputColor;

//lighting uniforms
uniform vec3 ambientLightColor = vec3(0.6,0.6,0.6);

struct light{
    vec4 position;
    vec3 color;
};

//uniform light lightArray;

void main(){
    //outputColor.w = color.w;
    //outputColor.xyz = color.xyz * ambientLightColor.xyz;
	outputColor = texture2D(diffuse, vec2(0.8,0.8));
}
