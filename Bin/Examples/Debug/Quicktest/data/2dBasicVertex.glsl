#version 330 core
layout (location = 0) in vec2 vertPosition;
layout (location = 1) in vec4 vertColor;
smooth out vec4 color;
uniform mat3 mvpMatrix = mat3(1,0,0, 0,1,0, 0,0,1);
void main(){
	gl_Position = vec4((mvpMatrix * vec3(vertPosition,1)), 1);
	gl_Position.z = 0;
	color = vertColor;
}