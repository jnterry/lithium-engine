#version 330 core
#define MAX_TEXTURE_UNITS 8

in vec2 texCoord0;
in vec3 faceNormal;
in vec3 worldPosition;

layout (location=0) smooth out vec4 outputColor;

///////////////////////////////////////////////
//UNIFORMS
uniform sampler2D diffuseMap;
uniform float camNear;
uniform float camFar;

void main(){
	float z = texture2D(diffuseMap, texCoord0).x;

	//linerize depth buffer, http://www.geeks3d.com/20091216/geexlab-how-to-visualize-the-depth-buffer-in-glsl/
	float grey = (2.0*camNear) / (camFar + camNear - z * (camFar-camNear));
	outputColor = vec4(grey, grey, grey, 1);
}
