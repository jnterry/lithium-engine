#version 330 core
#define MAX_TEXTURE_UNITS 8

in vec2 texCoord0;
in vec3 faceNormal;
in vec3 worldPosition;

layout (location=0) smooth out vec4 outputColor;

struct DirectionalLight{
	vec4 color; //alpha (.w) is intensity
	vec3 direction;
};

struct PointLight{
	vec4 color; //alpha (.w) is intensity
	vec3 attenuation;
	vec3 position;
};

///////////////////////////////////////////////
//UNIFORMS
uniform vec4 ambientLightColor = vec4(0.05,0.05,0.05, 1.0);
uniform sampler2D diffuseMap;
uniform vec3 cameraPosition;
uniform vec4 diffuseColor;
uniform float specularIntensity;
uniform float specularExponent;
uniform float time;

vec4 calcLight(vec4 lightColor, vec3 direction, vec3 normal){
	float diffuseFactor = dot(normal, -direction);
	vec4 resultColor = vec4(0,0,0,0);
	if(diffuseFactor > 0){
		//////////////////////////////////////////////////////////
		//Difuse Lighting
		resultColor = vec4(lightColor.xyz, 1.0) * lightColor.w * diffuseFactor * diffuseColor;
		
		
		//////////////////////////////////////////////////////////
		//Specular Lighting
		//closer these are the more specular you get as eye is closer to the reflected beam
		vec3 dirToCam = normalize(cameraPosition - worldPosition); 
		vec3 reflectionDir = normalize(reflect(direction, normal));
		float specularFactor = dot(dirToCam,reflectionDir); //cosine of the angle between directions, 1 when angle is 0, gets less as angle increases
		specularFactor = pow(specularFactor, specularExponent); //raise to specified power
		if(specularFactor > 0){
			resultColor += vec4(lightColor.xyz, 1.0) * specularFactor * specularIntensity * texture2D(diffuseMap, texCoord0);;
		}
		
	}
	return resultColor;
}

vec4 calcDirectionalLight(DirectionalLight light, vec3 normal){
	return calcLight(light.color, light.direction, normal);
}

vec4 calcPointLight(PointLight light, vec3 normal){
	vec3 lightDir = worldPosition - light.position;
	float distToPoint = length(lightDir);
	lightDir = normalize(lightDir);
	
	vec4 color = calcLight(light.color, lightDir, normal);
	float attenuation = light.attenuation.z +
						light.attenuation.y * distToPoint +
						light.attenuation.x * distToPoint * distToPoint
						+ 0.0001; //dont div by 0
	return color / attenuation; //here
}

DirectionalLight testLight;
PointLight testPoint;

void main(){
	testLight.color = vec4(1,1,1, 0.4);
	testLight.direction = normalize(vec3(1,0,-3));
	
	testPoint.color = vec4(0,1,0, 0.8);
	testPoint.attenuation = vec3(2, 0, 0);
	testPoint.position = vec3(sin(time/200.0)*6,-6,-17+(cos(time/200.0)*6));
	
	vec4 color = texture2D(diffuseMap, texCoord0);
	
	vec4 totalLight = ambientLightColor * diffuseColor * color;
	totalLight += calcDirectionalLight(testLight, faceNormal);
	totalLight += calcPointLight(testPoint, faceNormal);

	outputColor = color * totalLight;
	
	//gamma correct
	outputColor = pow(outputColor, vec4(0.454545));
}
