#version 330 core
#define MAX_TEXTURE_UNITS 8

in vec2 texCoord0;
in vec3 faceNormal;
in vec3 worldPosition;

layout (location=0) smooth out vec4 outputColor;

struct DirectionalLight{
	vec4 color; //alpha (.w) is intensity
	vec3 direction;
};

struct PointLight{
	vec4 color; //alpha (.w) is intensity
	vec3 attenuation;
	vec3 position;
};

///////////////////////////////////////////////
//UNIFORMS
uniform vec4 ambientLightColor = vec4(0.4,0.4,0.4, 1.0);
uniform sampler2D diffuseMap;
uniform vec3 cameraPosition;
uniform vec4 diffuseColor;
uniform float specularIntensity;
uniform float specularExponent;
uniform float time;

void main(){
	vec2 finalTextCoord = texCoord0;
	//finalTextCoord.x += sin(texCoord0.y*35 + time/200) / 60;

	outputColor = texture2D(diffuseMap, finalTextCoord);
}
