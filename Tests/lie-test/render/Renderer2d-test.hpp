#ifndef LIE_RENDER_RENDERER2D_TEST_HPP
#define LIE_RENDER_RENDERER2D_TEST_HPP

#include <lie/Test.hpp>
#include <lie/render/Renderer2d.hpp>
#include <lie/math/Vector2.hpp>
#include <lie/math/OstreamOperators.hpp>

namespace lie{
	namespace rend{
		namespace test{
			//////////////////////////////////////////////////////////////////////////
			/// \brief Renderer2d implementation whose virtual methods do nothing, allows
			/// testing of non-virtual methods in Renderer2d
			//////////////////////////////////////////////////////////////////////////
			class Renderer2dEmpty : public Renderer2d{
			public:
				Renderer2dEmpty(unsigned short layerCount) : Renderer2d(layerCount){}
				void draw(const Methodology& meth, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer, lie::math::Shape2f& shape) override{}
			};
		}


		void Test_Renderer2d(lie::test::TestGroup* group){
			lie::test::TestGroup* grp = group->createSubgroup("Renderer2d");

			test::Renderer2dEmpty& r = test::Renderer2dEmpty(5);
			grp->add(new lie::test::ValueTest<unsigned short>("layer count constructor", 5, r.getLayerCount()));

			for (int i = 0; i < 5; ++i){
				grp->add(new lie::test::ValueTest<lie::math::Vector2f>("default factors " + i, lie::math::Vector2f(1, 1), r.getLayerParrallaxFactor(i)));
				grp->add(new lie::test::ValueTest<float>("default X factor" + i, 1, r.getLayerParrallaxFactorX(i)));
				grp->add(new lie::test::ValueTest<float>("default Y factor" + i, 1, r.getLayerParrallaxFactorY(i)));
			}

			r.setLayerParrallaxFactor(0, 0.8, 0.4);
			r.setLayerParrallaxFactorX(1, 0.2);
			r.setLayerParrallaxFactorY(2, 0.5);
			r.setLayerParrallaxFactor(3, lie::math::Vector2f(-1,10));
			r.setLayerParrallaxFactor(4, 0.7);

			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 0", lie::math::Vector2f(0.8, 0.4), r.getLayerParrallaxFactor(0)));
			grp->add(new lie::test::ValueTest<float>("factor X 0", 0.8, r.getLayerParrallaxFactorX(0)));
			grp->add(new lie::test::ValueTest<float>("factor Y 0", 0.4, r.getLayerParrallaxFactorY(0)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 1", lie::math::Vector2f(0.2, 1.0), r.getLayerParrallaxFactor(1)));
			grp->add(new lie::test::ValueTest<float>("factor X 1", 0.2, r.getLayerParrallaxFactorX(1)));
			grp->add(new lie::test::ValueTest<float>("factor Y 1", 1.0, r.getLayerParrallaxFactorY(1)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 2", lie::math::Vector2f(1.0, 0.5), r.getLayerParrallaxFactor(2)));
			grp->add(new lie::test::ValueTest<float>("factor X 2", 1.0, r.getLayerParrallaxFactorX(2)));
			grp->add(new lie::test::ValueTest<float>("factor Y 2", 0.5, r.getLayerParrallaxFactorY(2)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 3", lie::math::Vector2f(-1, 10), r.getLayerParrallaxFactor(3)));
			grp->add(new lie::test::ValueTest<float>("factor X 3", -1.0, r.getLayerParrallaxFactorX(3)));
			grp->add(new lie::test::ValueTest<float>("factor Y 3", 10, r.getLayerParrallaxFactorY(3)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 4", lie::math::Vector2f(0.7, 0.7), r.getLayerParrallaxFactor(4)));
			grp->add(new lie::test::ValueTest<float>("factor X 4", 0.7, r.getLayerParrallaxFactorX(4)));
			grp->add(new lie::test::ValueTest<float>("factor Y 4", 0.7, r.getLayerParrallaxFactorY(4)));

			grp->add(new lie::test::Comment("Adding two layers"));
			r.setLayerCount(7);
			grp->add(new lie::test::ValueTest<unsigned short>("layer count", 7, r.getLayerCount()));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 0", lie::math::Vector2f(0.8, 0.4), r.getLayerParrallaxFactor(0)));
			grp->add(new lie::test::ValueTest<float>("factor X 0", 0.8, r.getLayerParrallaxFactorX(0)));
			grp->add(new lie::test::ValueTest<float>("factor Y 0", 0.4, r.getLayerParrallaxFactorY(0)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 1", lie::math::Vector2f(0.2, 1.0), r.getLayerParrallaxFactor(1)));
			grp->add(new lie::test::ValueTest<float>("factor X 1", 0.2, r.getLayerParrallaxFactorX(1)));
			grp->add(new lie::test::ValueTest<float>("factor Y 1", 1.0, r.getLayerParrallaxFactorY(1)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 2", lie::math::Vector2f(1.0, 0.5), r.getLayerParrallaxFactor(2)));
			grp->add(new lie::test::ValueTest<float>("factor X 2", 1.0, r.getLayerParrallaxFactorX(2)));
			grp->add(new lie::test::ValueTest<float>("factor Y 2", 0.5, r.getLayerParrallaxFactorY(2)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 3", lie::math::Vector2f(-1, 10), r.getLayerParrallaxFactor(3)));
			grp->add(new lie::test::ValueTest<float>("factor X 3", -1.0, r.getLayerParrallaxFactorX(3)));
			grp->add(new lie::test::ValueTest<float>("factor Y 3", 10, r.getLayerParrallaxFactorY(3)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 4", lie::math::Vector2f(0.7, 0.7), r.getLayerParrallaxFactor(4)));
			grp->add(new lie::test::ValueTest<float>("factor X 4", 0.7, r.getLayerParrallaxFactorX(4)));
			grp->add(new lie::test::ValueTest<float>("factor Y 4", 0.7, r.getLayerParrallaxFactorY(4)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 5", lie::math::Vector2f(1,1), r.getLayerParrallaxFactor(5)));
			grp->add(new lie::test::ValueTest<float>("factor X 5", 1, r.getLayerParrallaxFactorX(5)));
			grp->add(new lie::test::ValueTest<float>("factor Y 5", 1, r.getLayerParrallaxFactorY(5)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 6", lie::math::Vector2f(1, 1), r.getLayerParrallaxFactor(6)));
			grp->add(new lie::test::ValueTest<float>("factor X 6", 1, r.getLayerParrallaxFactorX(6)));
			grp->add(new lie::test::ValueTest<float>("factor Y 6", 1, r.getLayerParrallaxFactorY(6)));

			grp->add(new lie::test::Comment("removing 4 layers"));
			r.setLayerCount(3);
			grp->add(new lie::test::ValueTest<unsigned short>("layer count", 3, r.getLayerCount()));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 0", lie::math::Vector2f(0.8, 0.4), r.getLayerParrallaxFactor(0)));
			grp->add(new lie::test::ValueTest<float>("factor X 0", 0.8, r.getLayerParrallaxFactorX(0)));
			grp->add(new lie::test::ValueTest<float>("factor Y 0", 0.4, r.getLayerParrallaxFactorY(0)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 1", lie::math::Vector2f(0.2, 1.0), r.getLayerParrallaxFactor(1)));
			grp->add(new lie::test::ValueTest<float>("factor X 1", 0.2, r.getLayerParrallaxFactorX(1)));
			grp->add(new lie::test::ValueTest<float>("factor Y 1", 1.0, r.getLayerParrallaxFactorY(1)));
			grp->add(new lie::test::ValueTest<lie::math::Vector2f>("factors 2", lie::math::Vector2f(1.0, 0.5), r.getLayerParrallaxFactor(2)));
			grp->add(new lie::test::ValueTest<float>("factor X 2", 1.0, r.getLayerParrallaxFactorX(2)));
			grp->add(new lie::test::ValueTest<float>("factor Y 2", 0.5, r.getLayerParrallaxFactorY(2)));
		}
			
	}
}

#endif // LIE_RENDER_RENDERER2D_TEST_HPP
