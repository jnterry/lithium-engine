#ifndef LIE_RENDER_MESHDATABUFFER_TEST_HPP
#define LIE_RENDER_MESHDATABUFFER_TEST_HPP

#include <lie/Test.hpp>
#include <lie/render/MeshDataBuffer.hpp>
#include <stdint.h>
#include <cstdint>

namespace lie{
    namespace rend{
        namespace test{
            struct VertexLayoutA{
				VertexLayoutA(float px, float py, float pz, float nx, float ny, float nz, float uvx, float uvy){
					position[0] = px;
					position[1] = py;
					position[2] = pz;
					normal[0] = nx;
					normal[1] = ny;
					normal[2] = nz;
					uv[0] = uvx;
					uv[1] = uvy;
				}
                float position[3];
                float normal[3];
                float uv[2];
            };

            struct VertexLayoutB{
                VertexLayoutB(float px, float py, float pz, std::uint16_t uvx, std::uint16_t uvy){
					position[0] = px;
					position[1] = py;
					position[2] = pz;
					uv[0] = uvx;
					uv[1] = uvy;
				}
                float position[3];
                std::uint16_t uv[2];
            };
        }
        void Test_MeshDataBuffer(lie::test::TestGroup* group){
            lie::test::TestGroup* mdbGroup = group->createSubgroup("MeshDataBuffer");

            {
                lie::test::TestGroup* mdbdGroup = mdbGroup->createSubgroup("Direct Mode");
                mdbdGroup->add(new lie::test::Comment("Testing basic functionality, setting all data at once"));
                MeshDataBuffer mdb(lie::rend::MeshVertexLayout({
                                        lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3), //pos
                                        lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3), //normal
                                        lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float, 2)}));//uv

                float data[] = {
                    0.0f, 1.0f, 2.0f,      3.0f, 4.0f, 5.0f,       6.0f, 7.0f,
                    8.0f, 9.0f, 10.0f,     11.0f, 12.0f, 13.0f,    14.0f, 15.0f,
                    16.0f, 17.0f, 18.0f,   19.0f, 20.0f, 21.0f,    22.0f, 23.0f,

                    24.0f, 25.0f, 26.0f,    27.0f, 28.0f, 29.0f,   30.0f, 31.0f,
                    32.0f, 33.0f, 34.0f,    35.0f, 36.0f, 37.0f,   38.0f, 39.0f,
                    40.0f, 41.0f, 42.0f,    43.0f, 44.0f, 45.0f,   46.0f, 47.0f,
                };

                mdb.appendVerticies(&data, sizeof(data)/mdb.getVertexLayout().getSize());
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 6, mdb.getVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 6, mdb.getDrawnVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("face count", 2, mdb.getFaceCount()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("has index buffer", false, mdb.hasIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(0).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[1]", 1, mdb.getFace<test::VertexLayoutA>(0).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[2]", 2, mdb.getFace<test::VertexLayoutA>(0).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[0]", 3, mdb.getFace<test::VertexLayoutA>(0).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[1]", 4, mdb.getFace<test::VertexLayoutA>(0).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[2]", 5, mdb.getFace<test::VertexLayoutA>(0).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[0]", 6, mdb.getFace<test::VertexLayoutA>(0).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[1]", 7, mdb.getFace<test::VertexLayoutA>(0).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[0]", 8, mdb.getFace<test::VertexLayoutA>(0).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[1]", 9, mdb.getFace<test::VertexLayoutA>(0).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[2]", 10, mdb.getFace<test::VertexLayoutA>(0).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[0]", 11, mdb.getFace<test::VertexLayoutA>(0).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[1]", 12, mdb.getFace<test::VertexLayoutA>(0).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[2]", 13, mdb.getFace<test::VertexLayoutA>(0).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[0]", 14, mdb.getFace<test::VertexLayoutA>(0).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[1]", 15, mdb.getFace<test::VertexLayoutA>(0).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[0]", 16, mdb.getFace<test::VertexLayoutA>(0).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[1]", 17, mdb.getFace<test::VertexLayoutA>(0).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[2]", 18, mdb.getFace<test::VertexLayoutA>(0).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[0]", 19, mdb.getFace<test::VertexLayoutA>(0).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[1]", 20, mdb.getFace<test::VertexLayoutA>(0).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[2]", 21, mdb.getFace<test::VertexLayoutA>(0).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[0]", 22, mdb.getFace<test::VertexLayoutA>(0).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[1]", 23, mdb.getFace<test::VertexLayoutA>(0).v2->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[0]", 24, mdb.getFace<test::VertexLayoutA>(1).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[1]", 25, mdb.getFace<test::VertexLayoutA>(1).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[2]", 26, mdb.getFace<test::VertexLayoutA>(1).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[0]", 27, mdb.getFace<test::VertexLayoutA>(1).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[1]", 28, mdb.getFace<test::VertexLayoutA>(1).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[2]", 29, mdb.getFace<test::VertexLayoutA>(1).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[0]", 30, mdb.getFace<test::VertexLayoutA>(1).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[1]", 31, mdb.getFace<test::VertexLayoutA>(1).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[0]", 32, mdb.getFace<test::VertexLayoutA>(1).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[1]", 33, mdb.getFace<test::VertexLayoutA>(1).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[2]", 34, mdb.getFace<test::VertexLayoutA>(1).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[0]", 35, mdb.getFace<test::VertexLayoutA>(1).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[1]", 36, mdb.getFace<test::VertexLayoutA>(1).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[2]", 37, mdb.getFace<test::VertexLayoutA>(1).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[0]", 38, mdb.getFace<test::VertexLayoutA>(1).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[1]", 39, mdb.getFace<test::VertexLayoutA>(1).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[0]", 40, mdb.getFace<test::VertexLayoutA>(1).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[1]", 41, mdb.getFace<test::VertexLayoutA>(1).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[2]", 42, mdb.getFace<test::VertexLayoutA>(1).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[0]", 43, mdb.getFace<test::VertexLayoutA>(1).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[1]", 44, mdb.getFace<test::VertexLayoutA>(1).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[2]", 45, mdb.getFace<test::VertexLayoutA>(1).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[0]", 46, mdb.getFace<test::VertexLayoutA>(1).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[1]", 47, mdb.getFace<test::VertexLayoutA>(1).v2->uv[1]));

                mdbdGroup->add(new lie::test::Comment("Using FaceIterator, testing prefix ++ and --"));
                auto it1 = mdb.beginFace<test::VertexLayoutA>();
                mdbdGroup->add(new lie::test::ValueTest<bool>("it1 == end", false, it1 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v0->position[0]", 0, (*it1).v0->position[0])); //test with alternative deref
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v1->normal[1]", 12, it1->v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v2->uv[1]", 23, it1->v2->uv[1]));
                ++it1;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it1 == end", false, it1 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v0->position[0]", 24, it1->v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v1->normal[1]", 36, it1->v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v2->uv[1]", 47, it1->v2->uv[1]));
                ++it1;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it1 == end", true, it1 == mdb.endFace<test::VertexLayoutA>()));
                --it1;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it1 == end", false, it1 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v0->position[0]", 24, it1->v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v1->normal[1]", 36, it1->v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v2->uv[1]", 47, it1->v2->uv[1]));
                --it1;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it1 == end", false, it1 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v0->position[0]", 0, it1->v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v1->normal[1]", 12, it1->v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it->v2->uv[1]", 23, it1->v2->uv[1]));

                mdbdGroup->add(new lie::test::Comment("Setting vertex count, increasing from 6 to 9"));
                mdb.setVertexCount(9);
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 9, mdb.getVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 9, mdb.getDrawnVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("face count", 3, mdb.getFaceCount()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("has index buffer", false, mdb.hasIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(0).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[1]", 1, mdb.getFace<test::VertexLayoutA>(0).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[2]", 2, mdb.getFace<test::VertexLayoutA>(0).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[0]", 3, mdb.getFace<test::VertexLayoutA>(0).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[1]", 4, mdb.getFace<test::VertexLayoutA>(0).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[2]", 5, mdb.getFace<test::VertexLayoutA>(0).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[0]", 6, mdb.getFace<test::VertexLayoutA>(0).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[1]", 7, mdb.getFace<test::VertexLayoutA>(0).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[0]", 8, mdb.getFace<test::VertexLayoutA>(0).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[1]", 9, mdb.getFace<test::VertexLayoutA>(0).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[2]", 10, mdb.getFace<test::VertexLayoutA>(0).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[0]", 11, mdb.getFace<test::VertexLayoutA>(0).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[1]", 12, mdb.getFace<test::VertexLayoutA>(0).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[2]", 13, mdb.getFace<test::VertexLayoutA>(0).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[0]", 14, mdb.getFace<test::VertexLayoutA>(0).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[1]", 15, mdb.getFace<test::VertexLayoutA>(0).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[0]", 16, mdb.getFace<test::VertexLayoutA>(0).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[1]", 17, mdb.getFace<test::VertexLayoutA>(0).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[2]", 18, mdb.getFace<test::VertexLayoutA>(0).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[0]", 19, mdb.getFace<test::VertexLayoutA>(0).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[1]", 20, mdb.getFace<test::VertexLayoutA>(0).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[2]", 21, mdb.getFace<test::VertexLayoutA>(0).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[0]", 22, mdb.getFace<test::VertexLayoutA>(0).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[1]", 23, mdb.getFace<test::VertexLayoutA>(0).v2->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[0]", 24, mdb.getFace<test::VertexLayoutA>(1).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[1]", 25, mdb.getFace<test::VertexLayoutA>(1).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[2]", 26, mdb.getFace<test::VertexLayoutA>(1).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[0]", 27, mdb.getFace<test::VertexLayoutA>(1).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[1]", 28, mdb.getFace<test::VertexLayoutA>(1).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[2]", 29, mdb.getFace<test::VertexLayoutA>(1).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[0]", 30, mdb.getFace<test::VertexLayoutA>(1).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[1]", 31, mdb.getFace<test::VertexLayoutA>(1).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[0]", 32, mdb.getFace<test::VertexLayoutA>(1).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[1]", 33, mdb.getFace<test::VertexLayoutA>(1).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[2]", 34, mdb.getFace<test::VertexLayoutA>(1).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[0]", 35, mdb.getFace<test::VertexLayoutA>(1).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[1]", 36, mdb.getFace<test::VertexLayoutA>(1).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[2]", 37, mdb.getFace<test::VertexLayoutA>(1).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[0]", 38, mdb.getFace<test::VertexLayoutA>(1).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[1]", 39, mdb.getFace<test::VertexLayoutA>(1).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[0]", 40, mdb.getFace<test::VertexLayoutA>(1).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[1]", 41, mdb.getFace<test::VertexLayoutA>(1).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[2]", 42, mdb.getFace<test::VertexLayoutA>(1).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[0]", 43, mdb.getFace<test::VertexLayoutA>(1).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[1]", 44, mdb.getFace<test::VertexLayoutA>(1).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[2]", 45, mdb.getFace<test::VertexLayoutA>(1).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[0]", 46, mdb.getFace<test::VertexLayoutA>(1).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[1]", 47, mdb.getFace<test::VertexLayoutA>(1).v2->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->uv[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->uv[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->uv[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->uv[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->uv[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->uv[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->uv[1]));

                mdbdGroup->add(new lie::test::Comment("Setting vertex data through face reference"));
                *mdb.getFace<test::VertexLayoutA>(2).v1 = test::VertexLayoutA(-1,-2,-3, -4,-5,-6, -7,-8);
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 9, mdb.getVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 9, mdb.getDrawnVertexCount()));
                mdbdGroup->add(new lie::test::ValueTest<unsigned int>("face count", 3, mdb.getFaceCount()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("has index buffer", false, mdb.hasIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(0).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[1]", 1, mdb.getFace<test::VertexLayoutA>(0).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->pos[2]", 2, mdb.getFace<test::VertexLayoutA>(0).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[0]", 3, mdb.getFace<test::VertexLayoutA>(0).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[1]", 4, mdb.getFace<test::VertexLayoutA>(0).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->norm[2]", 5, mdb.getFace<test::VertexLayoutA>(0).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[0]", 6, mdb.getFace<test::VertexLayoutA>(0).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v0->uv[1]", 7, mdb.getFace<test::VertexLayoutA>(0).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[0]", 8, mdb.getFace<test::VertexLayoutA>(0).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[1]", 9, mdb.getFace<test::VertexLayoutA>(0).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->pos[2]", 10, mdb.getFace<test::VertexLayoutA>(0).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[0]", 11, mdb.getFace<test::VertexLayoutA>(0).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[1]", 12, mdb.getFace<test::VertexLayoutA>(0).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->norm[2]", 13, mdb.getFace<test::VertexLayoutA>(0).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[0]", 14, mdb.getFace<test::VertexLayoutA>(0).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v1->uv[1]", 15, mdb.getFace<test::VertexLayoutA>(0).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[0]", 16, mdb.getFace<test::VertexLayoutA>(0).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[1]", 17, mdb.getFace<test::VertexLayoutA>(0).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->pos[2]", 18, mdb.getFace<test::VertexLayoutA>(0).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[0]", 19, mdb.getFace<test::VertexLayoutA>(0).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[1]", 20, mdb.getFace<test::VertexLayoutA>(0).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->norm[2]", 21, mdb.getFace<test::VertexLayoutA>(0).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[0]", 22, mdb.getFace<test::VertexLayoutA>(0).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face0.v2->uv[1]", 23, mdb.getFace<test::VertexLayoutA>(0).v2->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[0]", 24, mdb.getFace<test::VertexLayoutA>(1).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[1]", 25, mdb.getFace<test::VertexLayoutA>(1).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->pos[2]", 26, mdb.getFace<test::VertexLayoutA>(1).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[0]", 27, mdb.getFace<test::VertexLayoutA>(1).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[1]", 28, mdb.getFace<test::VertexLayoutA>(1).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->norm[2]", 29, mdb.getFace<test::VertexLayoutA>(1).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[0]", 30, mdb.getFace<test::VertexLayoutA>(1).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v0->uv[1]", 31, mdb.getFace<test::VertexLayoutA>(1).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[0]", 32, mdb.getFace<test::VertexLayoutA>(1).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[1]", 33, mdb.getFace<test::VertexLayoutA>(1).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->pos[2]", 34, mdb.getFace<test::VertexLayoutA>(1).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[0]", 35, mdb.getFace<test::VertexLayoutA>(1).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[1]", 36, mdb.getFace<test::VertexLayoutA>(1).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->norm[2]", 37, mdb.getFace<test::VertexLayoutA>(1).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[0]", 38, mdb.getFace<test::VertexLayoutA>(1).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v1->uv[1]", 39, mdb.getFace<test::VertexLayoutA>(1).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[0]", 40, mdb.getFace<test::VertexLayoutA>(1).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[1]", 41, mdb.getFace<test::VertexLayoutA>(1).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->pos[2]", 42, mdb.getFace<test::VertexLayoutA>(1).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[0]", 43, mdb.getFace<test::VertexLayoutA>(1).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[1]", 44, mdb.getFace<test::VertexLayoutA>(1).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->norm[2]", 45, mdb.getFace<test::VertexLayoutA>(1).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[0]", 46, mdb.getFace<test::VertexLayoutA>(1).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face1.v2->uv[1]", 47, mdb.getFace<test::VertexLayoutA>(1).v2->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->pos[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->norm[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->uv[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v0->uv[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v0->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[0]", -1, mdb.getFace<test::VertexLayoutA>(2).v1->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[1]", -2, mdb.getFace<test::VertexLayoutA>(2).v1->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->pos[2]", -3, mdb.getFace<test::VertexLayoutA>(2).v1->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[0]", -4, mdb.getFace<test::VertexLayoutA>(2).v1->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[1]", -5, mdb.getFace<test::VertexLayoutA>(2).v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->norm[2]", -6, mdb.getFace<test::VertexLayoutA>(2).v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->uv[0]", -7, mdb.getFace<test::VertexLayoutA>(2).v1->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v1->uv[1]", -8, mdb.getFace<test::VertexLayoutA>(2).v1->uv[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->pos[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->position[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->norm[2]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->uv[0]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("face2.v2->uv[1]", 0, mdb.getFace<test::VertexLayoutA>(2).v2->uv[1]));

                mdbdGroup->add(new lie::test::Comment("Testing iterator, using postfix ++ and --"));
                auto it2 = mdb.beginFace<test::VertexLayoutA>();
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutA>()));
                it2++;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutA>()));
                it2++;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutA>()));
                it2++;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", true, it2 == mdb.endFace<test::VertexLayoutA>()));
                it2--;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v0->position[0]", 0, it2->v0->position[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v1->normal[1]", -5, it2->v1->normal[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v2->uv[1]", 0, it2->v2->uv[1]));
                it2--;
                mdbdGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutA>()));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v0->position[1]", 25, it2->v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v1->normal[2]", 37, it2->v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2->v2->uv[0]", 46, it2->v2->uv[0]));

                mdbdGroup->add(new lie::test::Comment("Testing FaceIterator[]"));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[-1].v0->position[1]", 1, it2[-1].v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[-1].v1->normal[2]", 13, it2[-1].v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[-1].v2->uv[0]", 22, it2[-1].v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[0].v0->position[1]", 25, it2[0].v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[0].v1->normal[2]", 37, it2[0].v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[0].v2->uv[0]", 46, it2[0].v2->uv[0]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[1].v0->position[1]", 0, it2[1].v0->position[1]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[1].v1->normal[2]", -6, it2[1].v1->normal[2]));
                mdbdGroup->add(new lie::test::ValueTest<float>("it2[1].v2->uv[0]", 0, it2[1].v2->uv[0]));
            }

            {
                lie::test::TestGroup* mdbiGroup = mdbGroup->createSubgroup("Indexed Mode");

                mdbiGroup->add(new lie::test::Comment("Testing basic functionality, setting all data at once"));
                MeshDataBuffer mdb(lie::rend::MeshVertexLayout({
                                        lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float,3), //pos
                                        lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::uInt16, 2)}));//uv

                test::VertexLayoutB data[] = {
                    test::VertexLayoutB(0,1,2,    3,4),
                    test::VertexLayoutB(5,6,7,    8,9),
                    test::VertexLayoutB(10,11,12, 13,14),
                    test::VertexLayoutB(15,16,17, 18,19),
                };
                mdb.appendVerticies(&data, sizeof(data)/mdb.getVertexLayout().getSize());
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 4, mdb.getVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 3, mdb.getDrawnVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("face count", 1, mdb.getFaceCount()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("has index buffer", false, mdb.hasIndexBuffer()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));

                mdbiGroup->add(new lie::test::Comment("Testing face iterator with 4 vertices"));
                auto it1 = mdb.beginFace<test::VertexLayoutB>();
                mdbiGroup->add(new lie::test::ValueTest<bool>("it1 == end", false, it1 == mdb.endFace<test::VertexLayoutB>()));
                ++it1;
                mdbiGroup->add(new lie::test::ValueTest<bool>("it1 == end", true, it1 == mdb.endFace<test::VertexLayoutB>()));

                unsigned int indicies[] = {
                    0,1,2,
                    1,2,3,
                };
                mdbiGroup->add(new lie::test::Comment("Adding valid indices"));
                mdb.appendIndicies(indicies, 6);
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 4, mdb.getVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 6, mdb.getDrawnVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("face count", 2, mdb.getFaceCount()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("has index buffer", true, mdb.hasIndexBuffer()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));
                auto it2 = mdb.beginFace<test::VertexLayoutB>();
                mdbiGroup->add(new lie::test::ValueTest<bool>("it2 == end", false, it2 == mdb.endFace<test::VertexLayoutB>()));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[0]", 0, it2->v0->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[1]", 1, it2->v0->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[2]", 2, it2->v0->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v0->uv[0]", 3, it2->v0->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v0->uv[1]", 4, it2->v0->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[0]", 5, it2->v1->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[1]", 6, it2->v1->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[2]", 7, it2->v1->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v1->uv[0]", 8, it2->v1->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v1->uv[1]", 9, it2->v1->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[0]", 10, it2->v2->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[1]", 11, it2->v2->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[2]", 12, it2->v2->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v2->uv[0]", 13, it2->v2->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v2->uv[1]", 14, it2->v2->uv[1]));
                ++it2;
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[0]", 5, it2->v0->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[1]", 6, it2->v0->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v0->position[2]", 7, it2->v0->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v0->uv[0]", 8, it2->v0->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v0->uv[1]", 9, it2->v0->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[0]", 10, it2->v1->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[1]", 11, it2->v1->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v1->position[2]", 12, it2->v1->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v1->uv[0]", 13, it2->v1->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v1->uv[1]", 14, it2->v1->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[0]", 15, it2->v2->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[1]", 16, it2->v2->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it2->v2->position[2]", 17, it2->v2->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v2->uv[0]", 18, it2->v2->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it2->v2->uv[1]", 19, it2->v2->uv[1]));
                ++it2;
                mdbiGroup->add(new lie::test::ValueTest<bool>("it2 == end", true, it2 == mdb.endFace<test::VertexLayoutB>()));

                mdbiGroup->add(new lie::test::Comment("Setting uv data seperately"));
                std::uint16_t uvData[] = {
                    100,101,
                    102,103,
                    104,105,
                    106,107,
                };
                mdb.setAttribData(1,uvData,4);
                auto it3 = mdb.beginFace<test::VertexLayoutB>();
                mdbiGroup->add(new lie::test::ValueTest<bool>("it3 == end", false, it3 == mdb.endFace<test::VertexLayoutB>()));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[0]", 0, it3->v0->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[1]", 1, it3->v0->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[2]", 2, it3->v0->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v0->uv[0]", 100, it3->v0->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v0->uv[1]", 101, it3->v0->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[0]", 5, it3->v1->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[1]", 6, it3->v1->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[2]", 7, it3->v1->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v1->uv[0]", 102, it3->v1->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v1->uv[1]", 103, it3->v1->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[0]", 10, it3->v2->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[1]", 11, it3->v2->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[2]", 12, it3->v2->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v2->uv[0]", 104, it3->v2->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v2->uv[1]", 105, it3->v2->uv[1]));
                ++it3;
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[0]", 5, it3->v0->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[1]", 6, it3->v0->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v0->position[2]", 7, it3->v0->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v0->uv[0]", 102, it3->v0->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v0->uv[1]", 103, it3->v0->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[0]", 10, it3->v1->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[1]", 11, it3->v1->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v1->position[2]", 12, it3->v1->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v1->uv[0]", 104, it3->v1->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v1->uv[1]", 105, it3->v1->uv[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[0]", 15, it3->v2->position[0]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[1]", 16, it3->v2->position[1]));
                mdbiGroup->add(new lie::test::ValueTest<float>("it3->v2->position[2]", 17, it3->v2->position[2]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v2->uv[0]", 106, it3->v2->uv[0]));
                mdbiGroup->add(new lie::test::ValueTest<std::uint16_t>("it3->v2->uv[1]", 107, it3->v2->uv[1]));


                mdbiGroup->add(new lie::test::Comment("Clearing index data"));
                mdb.clearIndexBuffer();
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 4, mdb.getVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 3, mdb.getDrawnVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("face count", 1, mdb.getFaceCount()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("has index buffer", false, mdb.hasIndexBuffer()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("validate index buffer", true, mdb.validateIndexBuffer()));

                unsigned int badIndexData[] = {
                    0,1,2,
                    10,1,3,
                    4,0,1,
                };

                mdbiGroup->add(new lie::test::Comment("Adding invalid index data"));
                mdb.appendIndicies(badIndexData, 9);
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("vertex count", 4, mdb.getVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("drawn vertex count", 9, mdb.getDrawnVertexCount()));
                mdbiGroup->add(new lie::test::ValueTest<unsigned int>("face count", 3, mdb.getFaceCount()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("has index buffer", true, mdb.hasIndexBuffer()));
                mdbiGroup->add(new lie::test::ValueTest<bool>("validate index buffer", false, mdb.validateIndexBuffer()));
            }
        }
    }
}

#endif // LIE_RENDER_MESHDATABUFFER_TEST_HPP
