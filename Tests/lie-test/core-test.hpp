#ifndef LIE_CORE_TEST_HPP
#define LIE_CORE_TEST_HPP

#include <lie/Test.hpp>

#include <lie-test\core\Convert-test.hpp>
#include <lie-test\core\Time-test.hpp>
#include <lie-test\core\RTimeMsg-test.hpp>
#include <lie-test\core\MetaType-test.hpp>
#include <lie-test\core\Folder-test.hpp>
#include <lie-test\core\File-test.hpp>
#include <lie-test\core\ByteBuffer-test.hpp>
#include <lie-test\core\ByteIterator-test.hpp>
#include "core/TypeInfo-test.hpp"
#include "core/TypeTraits-test.hpp"
#include "core/StringUtils-test.hpp"


namespace lie{
    void Test_core(lie::test::TestGroup* group){
        lie::test::TestGroup* coreGroup = new lie::test::TestGroup("core");
        group->add(coreGroup);

		lie::core::Test_ConvertFuncs(coreGroup);
		lie::core::Test_Time(coreGroup);
		lie::core::Test_RTimeMsg(coreGroup);
		lie::core::Test_MetaType(coreGroup);
		lie::core::Test_Folder(coreGroup);
		lie::core::Test_File(coreGroup);
		lie::core::Test_ByteBuffer(coreGroup);
		lie::core::Test_ByteIterator(coreGroup);
		lie::core::Test_TypeInfo(coreGroup);
		lie::core::Test_TypeTraits(coreGroup);
		lie::core::Test_StringUtils(coreGroup);
    }
}


#endif // LIE_GAME_TEST_HPP
