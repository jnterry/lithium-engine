#ifndef LIE_CORE_METATYPE_TEST_HPP
#define LIE_CORE_METATYPE_TEST_HPP

#include <lie/Test.hpp>

namespace lie{
    namespace core{
        void Test_MetaType(lie::test::TestGroup* group){
			lie::test::TestGroup* metaGroup = new lie::test::TestGroup("MetaType");
			group->add(metaGroup);

        }
    }
}

#endif // LIE_CORE_METATYPE_TEST_HPP
