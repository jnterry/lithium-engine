#ifndef LIE_CORE_TEST_TIME_H
#define LIE_CORE_TEST_TIME_H

#include <lie/Test.hpp>
#include <lie/core\Time.hpp>


namespace lie{
    namespace core{
		void Test_Time(lie::test::TestGroup* group){
			lie::test::TestGroup* timeGroup = new lie::test::TestGroup("Time");
			group->add(timeGroup);


			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//First test Microseconds <-> Milliseconds <-> Seconds <-> Days <-> etc
			//Note: Some of these tests are commeneted out, this is due to them reporting as fail due to floating point truncation
            lie::test::TestGroup* timeConvGroup = timeGroup->createSubgroup("Converting Time Units");
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asMicroseconds()",	1, lie::core::Time::Microseconds(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asMicroseconds()",	1000, lie::core::Time::Milliseconds(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asMicroseconds()",		1000000, lie::core::Time::Seconds(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asMicroseconds()",		60000000, lie::core::Time::Minutes(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asMicroseconds()",		3600000000, lie::core::Time::Hours(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asMicroseconds()",			8.64e+10, lie::core::Time::Days(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asMicroseconds()",		6.048e+11, lie::core::Time::Weeks(1).asMicroseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asMicroseconds()",		3.1536e+13, lie::core::Time::Years(1).asMicroseconds()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asMilliseconds()",	 0.001, lie::core::Time::Microseconds(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asMilliseconds()",	 1, lie::core::Time::Milliseconds(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asMilliseconds()",		 1000, lie::core::Time::Seconds(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asMilliseconds()",		 60000, lie::core::Time::Minutes(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asMilliseconds()",		 3600000, lie::core::Time::Hours(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asMilliseconds()",			 86400000, lie::core::Time::Days(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asMilliseconds()",		 604800000, lie::core::Time::Weeks(1).asMilliseconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asMilliseconds()",		 3.1536e+10, lie::core::Time::Years(1).asMilliseconds()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asSeconds()",	0.000001f, lie::core::Time::Microseconds(1).asSeconds()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asSeconds()",	0.001f, lie::core::Time::Milliseconds(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asSeconds()",		1.0f, lie::core::Time::Seconds(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asSeconds()",		60.0f, lie::core::Time::Minutes(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asSeconds()",			3600.0f, lie::core::Time::Hours(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asSeconds()",			86400.0f, lie::core::Time::Days(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asSeconds()",			604800.0f, lie::core::Time::Weeks(1).asSeconds()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asSeconds()",			31536000.0f, lie::core::Time::Years(1).asSeconds()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asMinutes()",	0.000000017f, lie::core::Time::Microseconds(1).asMinutes()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asMinutes()",	0.000016667f, lie::core::Time::Milliseconds(1).asMinutes()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asMinutes()",		0.016666667f, lie::core::Time::Seconds(1).asMinutes()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asMinutes()",		1.0f, lie::core::Time::Minutes(1).asMinutes()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asMinutes()",			60.0f, lie::core::Time::Hours(1).asMinutes()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asMinutes()",			1440.0f, lie::core::Time::Days(1).asMinutes()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asMinutes()",			10080.0f, lie::core::Time::Weeks(1).asMinutes()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asMinutes()",			525600.0f, lie::core::Time::Years(1).asMinutes()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asHours()",	2.777777778e-10f, lie::core::Time::Microseconds(1).asHours()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asHours()",	0.000000278f, lie::core::Time::Milliseconds(1).asHours()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asHours()",		0.000277778f, lie::core::Time::Seconds(1).asHours()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asHours()",		0.016666667f, lie::core::Time::Minutes(1).asHours()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asHours()",			1.0f, lie::core::Time::Hours(1).asHours()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asHours()",			24.0f, lie::core::Time::Days(1).asHours()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asHours()",			168.0f, lie::core::Time::Weeks(1).asHours()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asHours()",			8760.0f, lie::core::Time::Years(1).asHours()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asDays()", 	1.157407407e-11f, lie::core::Time::Microseconds(1).asDays()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asDays()",	0.000000012f, lie::core::Time::Milliseconds(1).asDays()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asDays()",		0.000011574f, lie::core::Time::Seconds(1).asDays()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asDays()",		0.000694444f, lie::core::Time::Minutes(1).asDays()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asDays()",			0.041666667f, lie::core::Time::Hours(1).asDays()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asDays()",			1.0f, lie::core::Time::Days(1).asDays()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asDays()",			7.0f, lie::core::Time::Weeks(1).asDays()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asDays()",			365.0f, lie::core::Time::Years(1).asDays()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asWeeks()",	1.653439153e-12f, lie::core::Time::Microseconds(1).asWeeks()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asWeeks()",	0.000000002f, lie::core::Time::Milliseconds(1).asWeeks()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asWeeks()",		0.000001653f, lie::core::Time::Seconds(1).asWeeks()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asWeeks()",		0.000099206f, lie::core::Time::Minutes(1).asWeeks()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asWeeks()",			0.005952381f, lie::core::Time::Hours(1).asWeeks()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asWeeks()",			0.142857143f, lie::core::Time::Days(1).asWeeks()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asWeeks()",			1.0f, lie::core::Time::Weeks(1).asWeeks()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asWeeks()",			365.0f/7.0f, lie::core::Time::Years(1).asWeeks()));

			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Microseconds(1).asYears()",	3.170979198e-14f, lie::core::Time::Microseconds(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Milliseconds(1).asYears()",	3.170979198e-11f, lie::core::Time::Milliseconds(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Seconds(1).asYears()",		3.17098e-008f, lie::core::Time::Seconds(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Minutes(1).asYears()",		1.90259e-006f, lie::core::Time::Minutes(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Hours(1).asYears()",			0.000114155f, lie::core::Time::Hours(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Days(1).asYears()",			0.002739726f, lie::core::Time::Days(1).asYears()));
			//timeConvGroup->add(new lie::test::ValueTest<double>("Time::Weeks(1).asYears()",			0.019178082f, lie::core::Time::Weeks(1).asYears()));
			timeConvGroup->add(new lie::test::ValueTest<double>("Time::Years(1).asYears()",			1.0f, lie::core::Time::Years(1).asYears()));


			//Finished converting time units
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//Test getting date
			//Note: Dates got from here: http://www.epochconverter.com/
			lie::test::TestGroup* timeT1Group = timeGroup->createSubgroup("Testing Date functions for time since epoch -> 0 milliseconds");
			lie::core::Time t1;
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getDayOfMonth()", (short)1, t1.getDayOfMonth()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getHour24()", (short)0, t1.getHour24()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getHour12()", (short)12, t1.getHour12()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.isAm()", true, t1.isAm()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getAmPm()", (std::string)"AM", t1.getAmPm()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getMonth()", (short)lie::core::Time::Months::January, t1.getMonth()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getMinute()", (short)0, t1.getMinute()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getSecond()", (short)0, t1.getSecond()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getYear()", (short)1970, t1.getYear()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getDayOfWeek()", (short)5, t1.getDayOfWeek()));
			timeT1Group->add(new lie::test::ValueTest<short>("t1.getDayOfYear()", (short)0, t1.getDayOfYear()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDayName()", (std::string)"Thursday", t1.getDayName()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getMonthName()", (std::string)"January", t1.getMonthName()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDayNameShort()", (std::string)"Thu", t1.getDayNameShort()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getMonthNameShort()", (std::string)"Jan", t1.getMonthNameShort()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDateDDMMYYYY()", (std::string)"01/01/1970", t1.getDateDDMMYYYY()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDateMMDDYYYY()", (std::string)"01/01/1970", t1.getDateMMDDYYYY()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDateDDMMYY()", (std::string)"01/01/70", t1.getDateDDMMYY()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDateMMDDYY()", (std::string)"01/01/70", t1.getDateMMDDYY()));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getDateYYYYMMDD("")", (std::string)"19700101", t1.getDateYYYYMMDD("")));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getTimeHHMMSS(true)", (std::string)"00:00:00", t1.getTimeHHMMSS(true)));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getTimeHHMMSS(false)", (std::string)"12:00:00", t1.getTimeHHMMSS(false)));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getTimeHHMM(true)", (std::string)"00:00", t1.getTimeHHMM(true)));
			timeT1Group->add(new lie::test::ValueTest<std::string>("t1.getTimeHHMM(false)", (std::string)"12:00", t1.getTimeHHMM(false)));

			lie::test::TestGroup* timeT2Group = timeGroup->createSubgroup("Testing Date functions for time since epoch -> 564365433 milliseconds");
			lie::core::Time t2 = lie::core::Time::Seconds(564365433L);
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getDayOfMonth()", (short)20, t2.getDayOfMonth()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getHour24()", (short)0, t2.getHour24()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getHour12()", (short)12, t2.getHour12()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.isAm()", true, t2.isAm()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getAmPm()", (std::string)"AM", t2.getAmPm()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getMonth()", (short)lie::core::Time::Months::November, t2.getMonth()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getMinute()", (short)10, t2.getMinute()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getSecond()", (short)33, t2.getSecond()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getYear()", (short)1987, t2.getYear()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getDayOfWeek()", (short)6, t2.getDayOfWeek()));
			timeT2Group->add(new lie::test::ValueTest<short>("t2.getDayOfYear()", (short)323, t2.getDayOfYear()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDayName()", (std::string)"Friday", t2.getDayName()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getMonthName()", (std::string)"November", t2.getMonthName()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDayNameShort()", (std::string)"Fri", t2.getDayNameShort()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getMonthNameShort()", (std::string)"Nov", t2.getMonthNameShort()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDateDDMMYYYY()", (std::string)"20/11/1987", t2.getDateDDMMYYYY()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDateMMDDYYYY()", (std::string)"11/20/1987", t2.getDateMMDDYYYY()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDateDDMMYY()", (std::string)"20/11/87", t2.getDateDDMMYY()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDateMMDDYY()", (std::string)"11/20/87", t2.getDateMMDDYY()));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getDateYYYYMMDD("")", (std::string)"19871120", t2.getDateYYYYMMDD("")));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getTimeHHMMSS(true)", (std::string)"00:10:33", t2.getTimeHHMMSS(true)));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getTimeHHMMSS(false)", (std::string)"12:10:33", t2.getTimeHHMMSS(false)));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getTimeHHMM(true)", (std::string)"00:10", t2.getTimeHHMM(true)));
			timeT2Group->add(new lie::test::ValueTest<std::string>("t2.getTimeHHMM(false)", (std::string)"12:10", t2.getTimeHHMM(false)));

            /*:TODO:
            This crashes the program if the time is after 32bit epoch time end
            under certian compilers (gcc it crahses, vs2010 it doesnt
            http://en.wikipedia.org/wiki/Year_2038_problem
            should add test case for post 2038 if ever fixed!
            */
		}
    }

}


#endif
