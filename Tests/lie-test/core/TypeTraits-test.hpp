#ifndef LIE_CORE_TYPETRAITS_TEST_HPP
#define LIE_CORE_TYPETRAITS_TEST_HP

#include <lie/core/TypeTraits.hpp>
#include <lie/Test.hpp>

namespace lie{
    namespace core{
        void Test_TypeTraits(lie::test::TestGroup* group){
			lie::test::TestGroup* ttGroup = group->createSubgroup("TypeTraits");

            {
                lie::test::TestGroup* ttPtrCountGroup = ttGroup->createSubgroup("CountPointerLevel");
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int", 0, lie::core::CountPointerLevel<int>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int*", 1, lie::core::CountPointerLevel<int*>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("const int*", 1, lie::core::CountPointerLevel<const int*>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int**", 2, lie::core::CountPointerLevel<int**>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int***", 3, lie::core::CountPointerLevel<int***>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int****", 4, lie::core::CountPointerLevel<int****>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("const int****", 4, lie::core::CountPointerLevel<const int****>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("int&", 0, lie::core::CountPointerLevel<int&>::value));
                ttPtrCountGroup->add(new lie::test::ValueTest<unsigned int>("const int&", 0, lie::core::CountPointerLevel<const int&>::value));
            }

            {

            }
        }
    }
}

#endif // LIE_CORE_TYPETRAITS_TEST_HPP
