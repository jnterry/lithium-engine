#ifndef LIE_CORE_TYPEINFO_TEST_HPP
#define LIE_CORE_TYPEINFO_TEST_HPP

#include <lie/core/TypeInfo.hpp>
#include <lie/Test.hpp>
#include <stdint.h>
#include <cstdint>

namespace lie{
    namespace core{

        void Test_TypeInfo(lie::test::TestGroup* group){
			lie::test::TestGroup* tiGroup = group->createSubgroup("TypeInfo");

            {
                lie::test::TestGroup* tiprimGroup = tiGroup->createSubgroup("Primitives");
                tiprimGroup->add(new lie::test::Comment("Testing TypeInfo<> for primitive types, should be defined by engine"));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<uInt8> name", "uInt8", lie::core::TypeInfo<std::uint8_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt8> isAtomic", true, lie::core::TypeInfo<std::uint8_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt8> isConcrete", true, lie::core::TypeInfo<std::uint8_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<uInt8> size", 1, lie::core::TypeInfo<std::uint8_t>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<Int8> name", "Int8", lie::core::TypeInfo<std::int8_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int8> isAtomic", true, lie::core::TypeInfo<std::int8_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int8> isConcrete", true, lie::core::TypeInfo<std::int8_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<Int8> size", 1, lie::core::TypeInfo<std::int8_t>::type.getStaticSize()));


                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<uInt16> name", "uInt16", lie::core::TypeInfo<std::uint16_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt16> isAtomic", true, lie::core::TypeInfo<std::uint16_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt16> isConcrete", true, lie::core::TypeInfo<std::uint16_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<uInt16> size", 2, lie::core::TypeInfo<std::uint16_t>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<Int16> name", "Int16", lie::core::TypeInfo<std::int16_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int16> isAtomic", true, lie::core::TypeInfo<std::int16_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int16> isConcrete", true, lie::core::TypeInfo<std::int16_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<Int16> size", 2, lie::core::TypeInfo<std::int16_t>::type.getStaticSize()));


                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<uInt32> name", "uInt32", lie::core::TypeInfo<std::uint32_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt32> isAtomic", true, lie::core::TypeInfo<std::uint32_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt32> isConcrete", true, lie::core::TypeInfo<std::uint32_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<uInt32> size", 4, lie::core::TypeInfo<std::uint32_t>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<Int32> name", "Int32", lie::core::TypeInfo<std::int32_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int32> isAtomic", true, lie::core::TypeInfo<std::int32_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int32> isConcrete", true, lie::core::TypeInfo<std::int32_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<Int32> size", 4, lie::core::TypeInfo<std::int32_t>::type.getStaticSize()));


                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<uInt64> name", "uInt64", lie::core::TypeInfo<std::uint64_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt64> isAtomic", true, lie::core::TypeInfo<std::uint64_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<uInt64> isConcrete", true, lie::core::TypeInfo<std::uint64_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<uInt64> size", 8, lie::core::TypeInfo<std::uint64_t>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<Int64> name", "Int64", lie::core::TypeInfo<std::int64_t>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int64> isAtomic", true, lie::core::TypeInfo<std::int64_t>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<Int64> isConcrete", true, lie::core::TypeInfo<std::int64_t>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<Int64> size", 8, lie::core::TypeInfo<std::int64_t>::type.getStaticSize()));


                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<float> name", "float", lie::core::TypeInfo<float>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<float> isAtomic", true, lie::core::TypeInfo<float>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<float> isConcrete", true, lie::core::TypeInfo<float>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<float> size", sizeof(float), lie::core::TypeInfo<float>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<double> name", "double", lie::core::TypeInfo<double>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<double> isAtomic", true, lie::core::TypeInfo<double>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<double> isConcrete", true, lie::core::TypeInfo<double>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<double> size", sizeof(double), lie::core::TypeInfo<double>::type.getStaticSize()));

                tiprimGroup->add(new lie::test::ValueTest<std::string>("TypeInfo<bool> name", "bool", lie::core::TypeInfo<bool>::type.getName()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<bool> isAtomic", true, lie::core::TypeInfo<bool>::type.isAtomic()));
                tiprimGroup->add(new lie::test::ValueTest<bool>("TypeInfo<bool> isConcrete", true, lie::core::TypeInfo<bool>::type.is(lie::core::MetaType::Concrete)));
                tiprimGroup->add(new lie::test::ValueTest<int>("TypeInfo<bool> size", sizeof(bool), lie::core::TypeInfo<bool>::type.getStaticSize()));

            }

            {
                tiGroup->add(new lie::test::Comment("Checking Compaision of MetaTypes work - only the exact same instances should equal one another!"));
                tiGroup->add(new lie::test::ValueTest<bool>("int type == int type", true, lie::core::TypeInfo<std::int32_t>::type == lie::core::TypeInfo<std::int32_t>::type));
                tiGroup->add(new lie::test::ValueTest<bool>("uint type == int type", false, lie::core::TypeInfo<std::uint32_t>::type == lie::core::TypeInfo<std::int32_t>::type));
            }
        }
    }
}


#endif // LIE_CORE_TYPEINFO_TEST_HPP
