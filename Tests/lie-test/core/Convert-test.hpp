#ifndef LIE_CORE_CONVERT_TEST_HPP
#define LIE_CORE_CONVERT_TEST_HPP

#include <lie/Test.hpp>
#include <lie/core/Convert.hpp>
#include <lie/core/ExcepParseError.hpp>
#include <limits>

namespace lie{
    namespace core{

        void Test_ConvertFuncs(lie::test::TestGroup* group){
			lie::test::TestGroup* convertGroup = group->createSubgroup("convert");

			lie::test::TestGroup* toStringGroup  = convertGroup->createSubgroup("toString");
            toStringGroup->add(new lie::test::ValueTest<std::string>("1", (std::string)"1", lie::core::toString(1)));
            toStringGroup->add(new lie::test::ValueTest<std::string>("1.0", (std::string)"1", lie::core::toString(1.0)));
            toStringGroup->add(new lie::test::ValueTest<std::string>("-1", (std::string)"-1", lie::core::toString(-1)));
            toStringGroup->add(new lie::test::ValueTest<std::string>("3.14", (std::string)"3.14", lie::core::toString(3.14)));

            lie::test::TestGroup* parseGroup  = convertGroup->createSubgroup("Parsing Numbers from Strings");
            parseGroup->add(new lie::test::ValueTest<int>("Int(1)", 1, lie::core::parseInt("1")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(-1)", -1, lie::core::parseInt("-1")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(+1)", 1, lie::core::parseInt("+1")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(100)", 100, lie::core::parseInt("100")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(-987)", -987, lie::core::parseInt("-987")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(+1000000)", 1000000, lie::core::parseInt("+1000000")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(1000000000000)", std::numeric_limits<int>::max(), lie::core::parseInt("1000000000000")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(+1000000000000)", std::numeric_limits<int>::max(), lie::core::parseInt("+1000000000000")));
			parseGroup->add(new lie::test::ValueTest<int>("Int(-1000000000000)", std::numeric_limits<int>::min(), lie::core::parseInt("-1000000000000")));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(one)", &lie::core::parseInt, "one"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(a1)", &lie::core::parseInt, "a1"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(1a)", &lie::core::parseInt, "1a"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(2.)", &lie::core::parseInt, "2."));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(5.0)", &lie::core::parseInt, "5.0"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(5.1)", &lie::core::parseInt, "5.1"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(--1)", &lie::core::parseInt, "--1"));
			parseGroup->add(new lie::test::ExceptionTest<lie::core::ExcepParseError, int, std::string>(
				"Int(++6)", &lie::core::parseInt, "++6"));

            parseGroup->add(new lie::test::ValueTest<int>("Float(1)", 1.0f, lie::core::parseFloat("1")));
            parseGroup->add(new lie::test::ValueTest<int>("Float(-1)", -1.0f, lie::core::parseFloat("-1")));
            parseGroup->add(new lie::test::ValueTest<int>("Float(100)", 100.0f, lie::core::parseFloat("100")));
            parseGroup->add(new lie::test::ValueTest<int>("Float(-100)", -100.0f, lie::core::parseFloat("-100")));
            parseGroup->add(new lie::test::ValueTest<int>("Float(1.5)", 1.5f, lie::core::parseFloat("1.5")));
            parseGroup->add(new lie::test::ValueTest<int>("Float(-1.5)", -1.5f, lie::core::parseFloat("-1.5")));

            parseGroup->add(new lie::test::ValueTest<int>("Double(1)", 1.0, lie::core::parseDouble("1")));
            parseGroup->add(new lie::test::ValueTest<int>("Double(-1)", -1.0, lie::core::parseDouble("-1")));
            parseGroup->add(new lie::test::ValueTest<int>("Double(100)", 100.0, lie::core::parseDouble("100")));
            parseGroup->add(new lie::test::ValueTest<int>("Double(-100)", -100.0, lie::core::parseDouble("-100")));
            parseGroup->add(new lie::test::ValueTest<int>("Double(1.5)", 1.5, lie::core::parseDouble("1.5")));
            parseGroup->add(new lie::test::ValueTest<int>("Double(-1.5)", -1.5, lie::core::parseDouble("-1.5")));

            lie::test::TestGroup* cmpGroup  = convertGroup->createSubgroup("Comparing Floats with Tolerance");
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(10.0f, 1.0f)", false, compareFloats(10.0f, 1.0f)));
            cmpGroup->add(new lie::test::ValueTest<float>("copareFloats(1.0f, 2.0f)", false, compareFloats(1.0f, 2.0f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.1f, 1.2f)", false, compareFloats(1.1f, 1.2f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.01f, 1.02f)", false, compareFloats(1.01f, 1.02f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.001f, 1.002f)", false, compareFloats(1.001f, 1.002f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.0001f, 1.0002f)", false, compareFloats(1.0001f, 1.0002f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.00001f, 1.00002f)", false, compareFloats(1.00001f, 1.00002f)));
            //cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.000001f, 1.000002f)", false, compareFloats(1.000001f, 1.000002f))); //these all fail
            //cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.0000001f, 1.0000002f)", false, compareFloats(1.0000001f, 1.0000002f)));
            //cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.00000001f, 1.00000002f)", false, compareFloats(1.00000001f, 1.00000002f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.0f, 1.0f)", true, compareFloats(1.0f, 1.0f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(0.5f+0.5f, 1.0f)", true, compareFloats(0.5f+0.5f, 1.0f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(1.0f/3.0f, 0.333...f)", true, compareFloats(1.0f/3.0f, 0.3333333333333f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(2.0f/3.0f, 0.666...f)", true, compareFloats(2.0f/3.0f, 0.6666666666666f)));
            cmpGroup->add(new lie::test::ValueTest<float>("compareFloats(2.0f-1.1f, 0.9f)", true, compareFloats(2.0f-1.1f, 0.9f)));

        }
    }
}
#endif // LIE_CORE_CONVERT_TEST_HPP
