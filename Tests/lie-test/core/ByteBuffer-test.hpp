#ifndef LIE_CORE_BYTEBUFFER_TEST_HPP
#define LIE_CORE_BYTEBUFFER_TEST_HPP

#include <lie/core/ByteBuffer.hpp>
#include <lie/test.hpp>
#include <lie/math/Vector3.hpp>


namespace lie{
    namespace core{

		void Test_ByteBuffer(lie::test::TestGroup* group){
		    lie::test::TestGroup* bbGroup = group->createSubgroup("Byte Buffer");

		    {
		        lie::core::ByteBuffer bb;
		        bbGroup->add(new lie::test::Comment("New empty buffer"));
		        bbGroup->add(new lie::test::ValueTest<size_t>(".size", 0, bb.size()));

		        bbGroup->add(new lie::test::Comment("push_back-ing a char"));
		        bb.push_back('c');
                bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(char), bb.size()));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(0)", 'c', bb.at<char>(0)));

                bbGroup->add(new lie::test::Comment("clearing"));
                bb.clear();
                bbGroup->add(new lie::test::ValueTest<size_t>(".size", 0, bb.size()));

                bbGroup->add(new lie::test::Comment("push_back-ing many chars"));
                bb.push_back('h');
                bb.push_back('e');
                bb.push_back('l');
                bb.push_back('l');
                bb.push_back('o');
                bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(char)*5, bb.size()));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(0)", 'h', bb.at<char>(0)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(1)", 'e', bb.at<char>(1)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(2)", 'l', bb.at<char>(2)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(3)", 'l', bb.at<char>(3)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(4)", 'o', bb.at<char>(4)));

                ///////////////////////////////////////////////////////////
                //manual tests:
                //throws will only occur if LIE_DEBUG is defined
                //bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(5)", 'o', bb.at<char>(5))); //should throw lie::core::OutOfRangeException
                //bbGroup->add(new lie::test::ValueTest<int>("bb.at<int>(1)", 'o', bb.at<int>(1))); //should not throw anything, test will fail though
                //bbGroup->add(new lie::test::ValueTest<int>("bb.at<int>(2)", 'o', bb.at<int>(2))); //should throw lie::core::OutOfRangeException
		    }

		    {
                lie::core::ByteBuffer bb;
		        bbGroup->add(new lie::test::Comment("push_back-ing some primative types, reading them back"));
		        bb.push_back<int>(123456789);
		        bb.push_back<float>(1.23f);
		        bb.push_back<short>(987);
		        bb.push_back<double>(123.98765);

		        bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(int)+sizeof(float)+sizeof(short)+sizeof(double), bb.size()));
		        bbGroup->add(new lie::test::ValueTest<int>("bb.at<int>(...)", 123456789, bb.at<int>(0)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 1.23f, bb.at<float>(sizeof(int))));
		        bbGroup->add(new lie::test::ValueTest<short>("bb.at<short>(...)", 987, bb.at<short>(sizeof(int)+sizeof(float))));
		        bbGroup->add(new lie::test::ValueTest<double>("bb.at<double>(...)", 123.98765, bb.at<double>(sizeof(int)+sizeof(float)+sizeof(short))));
		    }

            {
                lie::core::ByteBuffer bb;

		        bbGroup->add(new lie::test::Comment("push_back-ing same primatives but with operator <<"));
		        bb << 123456789 << 1.23f << (short)987 << 123.98765;

		        bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(int)+sizeof(float)+sizeof(short)+sizeof(double), bb.size()));
		        bbGroup->add(new lie::test::ValueTest<int>("bb.at<int>(...)", 123456789, bb.at<int>(0)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 1.23f, bb.at<float>(sizeof(int))));
		        bbGroup->add(new lie::test::ValueTest<short>("bb.at<short>(...)", 987, bb.at<short>(sizeof(int)+sizeof(float))));
		        bbGroup->add(new lie::test::ValueTest<double>("bb.at<double>(...)", 123.98765, bb.at<double>(sizeof(int)+sizeof(float)+sizeof(short))));

                bbGroup->add(new lie::test::Comment("setting bytes retrieved with .at, re-reading"));
                bb.at<int>(0) = 987654321;
                bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(int)+sizeof(float)+sizeof(short)+sizeof(double), bb.size()));
		        bbGroup->add(new lie::test::ValueTest<int>("bb.at<int>(...)", 987654321, bb.at<int>(0)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 1.23f, bb.at<float>(sizeof(int))));
		        bbGroup->add(new lie::test::ValueTest<short>("bb.at<short>(...)", 987, bb.at<short>(sizeof(int)+sizeof(float))));
		        bbGroup->add(new lie::test::ValueTest<double>("bb.at<double>(...)", 123.98765, bb.at<double>(sizeof(int)+sizeof(float)+sizeof(short))));
		    }

            {
                lie::core::ByteBuffer bb;
		        bbGroup->add(new lie::test::Comment("push_back-ing lie::math::Vector3<float>"));
		        lie::math::Vector3f vec(1.23, -5.43, 7.98);
		        bb << vec;

		        bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(float)*3, bb.size()));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 1.23, bb.at<float>(sizeof(float)*0)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", -5.43, bb.at<float>(sizeof(float)*1)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 7.98, bb.at<float>(sizeof(float)*2)));

		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.x(...)", 1.23, bb.at<lie::math::Vector3f>(0).x));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.y(...)", -5.43, bb.at<lie::math::Vector3f>(0).y));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.z(...)", 7.98, bb.at<lie::math::Vector3f>(0).z));

		        bbGroup->add(new lie::test::ValueTest<bool>("bb.at<Vec3>(...) == vec",
                                true, bb.at<lie::math::Vector3f>(0) == vec));

                bbGroup->add(new lie::test::Comment("testing deep copy was added to ByteBuffer"));
                vec.x = 0;
                vec.y = 1;
                vec.z = 2;

                bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 1.23, bb.at<float>(sizeof(float)*0)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", -5.43, bb.at<float>(sizeof(float)*1)));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<float>(...)", 7.98, bb.at<float>(sizeof(float)*2)));

                bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.x(...)", 1.23, bb.at<lie::math::Vector3f>(0).x));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.y(...)", -5.43, bb.at<lie::math::Vector3f>(0).y));
		        bbGroup->add(new lie::test::ValueTest<float>("bb.at<Vec3>.z(...)", 7.98, bb.at<lie::math::Vector3f>(0).z));

		        bbGroup->add(new lie::test::ValueTest<bool>("bb.at<Vec3>(...) == vec",
                                false, bb.at<lie::math::Vector3f>(0) == vec));
		    }

		    {
		        lie::core::ByteBuffer bb;
		        bbGroup->add(new lie::test::Comment("push_back-ing a std::string"));
		        bb.push_back(std::string("hello"));
                bbGroup->add(new lie::test::ValueTest<size_t>(".size", sizeof(char)*6, bb.size()));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(0)", 'h', bb.at<char>(0)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(1)", 'e', bb.at<char>(1)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(2)", 'l', bb.at<char>(2)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(3)", 'l', bb.at<char>(3)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(4)", 'o', bb.at<char>(4)));
                bbGroup->add(new lie::test::ValueTest<char>("bb.at<char>(5)", '\0', bb.at<char>(5)));
		    }
		}
    }
}


#endif // LIE_CORE_BYTEBUFFER_TEST_HPP
