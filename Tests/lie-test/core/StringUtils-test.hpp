/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file StringUtils-test.hpp
/// \author Jamie Terry
/// \date 2015/08/21
/// \brief Contains unit tests for code in StringUtils.hpp
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_STRINGUTILS_HPP
#define LIE_TEST_STRINGUTILS_HPP

#include <lie\test.hpp>
#include <lie\core\StringUtils.hpp>

namespace lie{
	namespace core{
		void Test_StringUtils(lie::test::TestGroup* group){
			lie::test::TestGroup* strUtilGrp = group->createSubgroup("String Utilities");

			{
				lie::test::TestGroup* eicg = strUtilGrp->createSubgroup("Equals Ignore Case (chars)");
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'a'", true, equalsIgnoreCase('a', 'a')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'a'", true, equalsIgnoreCase('A', 'a')));
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'A'", true, equalsIgnoreCase('a', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'A'", true, equalsIgnoreCase('A', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'j' == 'j'", true, equalsIgnoreCase('j', 'j')));
				eicg->add(new lie::test::ValueTest<bool>("'J' == 'j'", true, equalsIgnoreCase('J', 'j')));
				eicg->add(new lie::test::ValueTest<bool>("'j' == 'J'", true, equalsIgnoreCase('j', 'J')));
				eicg->add(new lie::test::ValueTest<bool>("'J' == 'J'", true, equalsIgnoreCase('J', 'J')));
				eicg->add(new lie::test::ValueTest<bool>("'z' == 'z'", true, equalsIgnoreCase('z', 'z')));
				eicg->add(new lie::test::ValueTest<bool>("'Z' == 'z'", true, equalsIgnoreCase('Z', 'z')));
				eicg->add(new lie::test::ValueTest<bool>("'z' == 'Z'", true, equalsIgnoreCase('z', 'Z')));
				eicg->add(new lie::test::ValueTest<bool>("'Z' == 'Z'", true, equalsIgnoreCase('Z', 'Z')));

				//these pairs have the same ascii offset as lower + upper case letters, hence if only 
				//offset is compared these tests will fail
				eicg->add(new lie::test::ValueTest<bool>("'@' == '@'", true, equalsIgnoreCase('@', '@')));
				eicg->add(new lie::test::ValueTest<bool>("'\'' == '@'", false, equalsIgnoreCase('\'', '@')));
				eicg->add(new lie::test::ValueTest<bool>("'@' == '\''", false, equalsIgnoreCase('@', '\'')));
				eicg->add(new lie::test::ValueTest<bool>("'\'' == '\''", true, equalsIgnoreCase('\'', '\'')));
				eicg->add(new lie::test::ValueTest<bool>("'[' == '['", true, equalsIgnoreCase('[', '[')));
				eicg->add(new lie::test::ValueTest<bool>("'{' == '['", false, equalsIgnoreCase('{', '[')));
				eicg->add(new lie::test::ValueTest<bool>("'[' == '{'", false, equalsIgnoreCase('[', '{')));
				eicg->add(new lie::test::ValueTest<bool>("'{' == '{'", true, equalsIgnoreCase('{', '{')));
				eicg->add(new lie::test::ValueTest<bool>("'!' == '!'", true, equalsIgnoreCase('!', '!')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == '!'", false, equalsIgnoreCase('A', '!')));
				eicg->add(new lie::test::ValueTest<bool>("'!' == 'A'", false, equalsIgnoreCase('!', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'A'", true, equalsIgnoreCase('A', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'=' == '='", true, equalsIgnoreCase('=', '=')));
				eicg->add(new lie::test::ValueTest<bool>("']' == '='", false, equalsIgnoreCase(']', '=')));
				eicg->add(new lie::test::ValueTest<bool>("'=' == ']'", false, equalsIgnoreCase('=', ']')));
				eicg->add(new lie::test::ValueTest<bool>("']' == ']'", true, equalsIgnoreCase(']', ']')));

				//different letter pairs
				eicg->add(new lie::test::ValueTest<bool>("'b' == 'a'", false, equalsIgnoreCase('b', 'a')));
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'b'", false, equalsIgnoreCase('a', 'b')));
				eicg->add(new lie::test::ValueTest<bool>("'B' == 'a'", false, equalsIgnoreCase('B', 'a')));
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'B'", false, equalsIgnoreCase('a', 'B')));
				eicg->add(new lie::test::ValueTest<bool>("'b' == 'A'", false, equalsIgnoreCase('b', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'b'", false, equalsIgnoreCase('A', 'b')));
				eicg->add(new lie::test::ValueTest<bool>("'B' == 'A'", false, equalsIgnoreCase('B', 'A')));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'B'", false, equalsIgnoreCase('A', 'B')));

				//random pairs
				eicg->add(new lie::test::ValueTest<bool>("'f' == '8'", false, equalsIgnoreCase('f', '8')));
				eicg->add(new lie::test::ValueTest<bool>("'\n' == '!'", false, equalsIgnoreCase('\n', '!')));
				eicg->add(new lie::test::ValueTest<bool>("'1' == '2", false, equalsIgnoreCase('1', '2')));
				eicg->add(new lie::test::ValueTest<bool>("'a' == '*", false, equalsIgnoreCase('a', '*')));
				eicg->add(new lie::test::ValueTest<bool>("'8' == '*", false, equalsIgnoreCase('8', '*')));
			}

			{
				lie::test::TestGroup* eicg = strUtilGrp->createSubgroup("Equals Ignore Case (strings)");
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'a'", true, equalsIgnoreCase("a", "a")));
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'A'", true, equalsIgnoreCase("a", "A")));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'a'", true, equalsIgnoreCase("A", "a")));
				eicg->add(new lie::test::ValueTest<bool>("'A' == 'A'", true, equalsIgnoreCase("A", "A")));

				eicg->add(new lie::test::ValueTest<bool>("'a1z' == 'a1z'", true, equalsIgnoreCase("a1z", "a1z")));
				eicg->add(new lie::test::ValueTest<bool>("'a1z' == 'A1z'", true, equalsIgnoreCase("a1z", "A1z")));
				eicg->add(new lie::test::ValueTest<bool>("'a1Z' == 'A1z'", true, equalsIgnoreCase("a1Z", "A1z")));
				eicg->add(new lie::test::ValueTest<bool>("'A1Z' == 'a1z'", true, equalsIgnoreCase("A1Z", "a1z")));
				eicg->add(new lie::test::ValueTest<bool>("'a1z' == 'A1Z'", true, equalsIgnoreCase("a1z", "A1Z")));
				eicg->add(new lie::test::ValueTest<bool>("'a1z' == 'AQZ'", false, equalsIgnoreCase("a1z", "AQZ")));
				eicg->add(new lie::test::ValueTest<bool>("'a' == 'aa", false, equalsIgnoreCase("a", "aa")));
				eicg->add(new lie::test::ValueTest<bool>("'aA' == 'aa", true, equalsIgnoreCase("aA", "aa")));
				eicg->add(new lie::test::ValueTest<bool>("'123' == '123", true, equalsIgnoreCase("123", "123")));
				eicg->add(new lie::test::ValueTest<bool>("'12g' == '123", false, equalsIgnoreCase("12g", "123")));
			}

			{
				lie::test::TestGroup* grp = strUtilGrp->createSubgroup("stringSplit (by char)");
				std::vector<std::string> r;
				grp->add(new lie::test::ValueTest<int>("Split empty string by ' ' - result", 0, stringSplit(r, "", ' ')));
				grp->add(new lie::test::ValueTest<int>("Split empty string by ' ' - vec.size", 0, r.size()));

				grp->add(new lie::test::ValueTest<int>("Split \"a/b/c\" by '/' - result", 3, stringSplit(r, "a/b/c", '/')));
				grp->add(new lie::test::ValueTest<int>("Split \"a/b/c\" by '/' - vec.size", 3, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"a/b/c\" by '/' - vec[0]", "a", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"a/b/c\" by '/' - vec[1]", "b", r[1]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"a/b/c\" by '/' - vec[2]", "c", r[2]));

				grp->add(new lie::test::ValueTest<int>("Append - Split \"a/b/c\" by '/' - result", 3, stringSplit(r, "a/b/c", '/')));
				grp->add(new lie::test::ValueTest<int>("Append - Split \"a/b/c\" by '/' - vec.size", 6, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[0]", "a", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[1]", "b", r[1]));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[2]", "c", r[2]));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[3]", "a", r[3]));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[4]", "b", r[4]));
				grp->add(new lie::test::ValueTest<std::string>("Append - Split \"a/b/c\" by '/' - vec[5]", "c", r[5]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"hello world\" by ' ' - result", 2, stringSplit(r, "hello world", ' ')));
				grp->add(new lie::test::ValueTest<int>("Split \"hello world\" by ' ' - vec.size", 2, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"hello world\" by ' ' - vec[0]", "hello", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"hello world\" by ' ' - vec[1]", "world", r[1]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \":test\" by ':' - result", 1, stringSplit(r, ":test", ':')));
				grp->add(new lie::test::ValueTest<int>("Split \":test\" by ':' - vec.size", 1, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \":test\" by ':' - vec[0]", "test", r[0]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"thing::a:\" by ':' - result", 3, stringSplit(r, "thing::a:", ':')));
				grp->add(new lie::test::ValueTest<int>("Split \"thing::a:\" by ':' - vec.size", 3, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a:\" by ':' - vec[0]", "thing", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a:\" by ':' - vec[1]", "", r[1]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a:\" by ':' - vec[2]", "a", r[2]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"thing::a: \" by ':' - result", 4, stringSplit(r, "thing::a: ", ':')));
				grp->add(new lie::test::ValueTest<int>("Split \"thing::a: \" by ':' - vec.size", 4, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a: \" by ':' - vec[0]", "thing", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a: \" by ':' - vec[1]", "", r[1]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a: \" by ':' - vec[2]", "a", r[2]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"thing::a: \" by ':' - vec[2]", " ", r[3]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"...\" by '.' - result", 2, stringSplit(r, "...", '.')));
				grp->add(new lie::test::ValueTest<int>("Split \"...\"  by '.' - vec.size", 2, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"...\" by '.' - vec[0]", "", r[0]));
				grp->add(new lie::test::ValueTest<std::string>("Split \"...\" by '.' - vec[1]", "", r[1]));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"|\" by '|' - result", 0, stringSplit(r, "|", '|')));
				grp->add(new lie::test::ValueTest<int>("Split \"|\"  by '|' - vec.size", 0, r.size()));

				r.clear();
				grp->add(new lie::test::ValueTest<int>("Split \"hello world\" by '|' - result", 1, stringSplit(r, "hello world", '|')));
				grp->add(new lie::test::ValueTest<int>("Split \"hello world\"  by '|' - vec.size", 1, r.size()));
				grp->add(new lie::test::ValueTest<std::string>("Split \"hello world\" by '|' - vec[0]", "hello world", r[0]));
			}
		}
	}
}

#endif