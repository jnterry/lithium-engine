#ifndef LIE_CORE_RTimeMsg_TEST_HPP
#define LIE_CORE_RTimeMsg_TEST_HPP

#include <lie/Test.hpp>
#include <lie/core\RTimeMsg.hpp>

namespace lie{
    namespace core{

        void Test_RTimeMsg(lie::test::TestGroup* group){
			lie::test::TestGroup* rtmGroup = new lie::test::TestGroup("RTimeMsg");
			group->add(rtmGroup);

            {
                lie::test::TestGroup* rtmConstGroup = rtmGroup->createSubgroup("Making runtime message with constuctor");

                lie::core::RTimeMsg rt(lie::core::RTimeMsg::Info, "testing", "test.cpp", 100);
                rtmConstGroup->add(new lie::test::ValueTest<unsigned char>("level", lie::core::RTimeMsg::Info, rt.getLevel()));
                rtmConstGroup->add(new lie::test::ValueTest<std::string>("module", std::string("testing"), rt.getModule()));
                rtmConstGroup->add(new lie::test::ValueTest<std::string>("filename", std::string("test.cpp"), rt.getFileName()));
                rtmConstGroup->add(new lie::test::ValueTest<unsigned int>("line num", 100, rt.getLineNumber()));
                rtmConstGroup->add(new lie::test::ValueTest<std::string>("empty message", std::string(""), rt.getMessage()));
                rt << "Test string! Val: " << 10;
                rtmConstGroup->add(new lie::test::ValueTest<std::string>("message", std::string("Test string! Val: 10"), rt.getMessage()));
                rt << " a little extra";
                rtmConstGroup->add(new lie::test::ValueTest<std::string>("message appended", std::string("Test string! Val: 10 a little extra"), rt.getMessage()));
            }

            {
                lie::test::TestGroup* rtmMacroGroup = rtmGroup->createSubgroup("Making runtime message with macro");
                lie::core::RTimeMsg rt = LIE_RTMSG(5, std::string("macro mod"));
                rtmMacroGroup->add(new lie::test::ValueTest<unsigned char>("level", 5, rt.getLevel()));
                rtmMacroGroup->add(new lie::test::ValueTest<std::string>("module", std::string("macro mod"), rt.getModule()));
                rtmMacroGroup->add(new lie::test::ValueTest<unsigned int>("line num", (__LINE__ - 3), rt.getLineNumber()));
                rtmMacroGroup->add(new lie::test::ValueTest<std::string>("filename", std::string(__FILE__), rt.getFileName()));
                rtmMacroGroup->add(new lie::test::ValueTest<std::string>("empty message", std::string(""), rt.getMessage()));
                rt << "pushing text";
                rtmMacroGroup->add(new lie::test::ValueTest<std::string>("message", std::string("pushing text"), rt.getMessage()));
            }

        }

    }
}


#endif // LIE_CORE_RTimeMsg_TEST_HPP
