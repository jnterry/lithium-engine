#ifndef LIE_CORE_FOLDER_TEST_HPP
#define LIE_CORE_FOLDER_TEST_HPP

#include <lie/test.hpp>
#include <lie/core\Folder.hpp>

namespace lie{
    namespace core{

        void Test_Folder(lie::test::TestGroup* group){
			lie::test::TestGroup* folderGroup = new lie::test::TestGroup("Folder");
			group->add(folderGroup);

            folderGroup->add(new lie::test::Comment("Construct Folder from path string"));
            Folder dir("C:\\folder1\\folder2\\folder3");
            folderGroup->add(new lie::test::ValueTest<std::string>("Name", std::string("folder3"), dir.getName()));
            folderGroup->add(new lie::test::ValueTest<std::string>("Fullpath", std::string("C:\\folder1\\folder2\\folder3"), dir.getFullPath()));
            folderGroup->add(new lie::test::ValueTest<std::string>("Volume", std::string("C:"), dir.getVolume()));

            dir.push("folder4");
            folderGroup->add(new lie::test::ValueTest<std::string>("pushing dir - name", std::string("folder4"), dir.getName()));
            folderGroup->add(new lie::test::ValueTest<std::string>("pushing dir - Fullpath", std::string("C:\\folder1\\folder2\\folder3\\folder4"), dir.getFullPath()));
            folderGroup->add(new lie::test::ValueTest<std::string>("pushing dir - Volume", std::string("C:"), dir.getVolume()));

            dir.setVolume("D:");
            folderGroup->add(new lie::test::ValueTest<std::string>("setting volume - name", std::string("folder4"), dir.getName()));
            folderGroup->add(new lie::test::ValueTest<std::string>("setting volume - Fullpath", std::string("D:\\folder1\\folder2\\folder3\\folder4"), dir.getFullPath()));
            folderGroup->add(new lie::test::ValueTest<std::string>("setting volume - Volume", std::string("D:"), dir.getVolume()));


            folderGroup->add(new lie::test::Comment("Comparison Operators"));
            Folder dir2("C:\\folder1\\folder2\\folder3\\folder4");
            folderGroup->add(new lie::test::ValueTest<bool>("==", false, dir == dir2));
            folderGroup->add(new lie::test::ValueTest<bool>("!=", true, dir != dir2));

            dir2.setVolume("D:");
            folderGroup->add(new lie::test::ValueTest<bool>("==", true, dir == dir2));
            folderGroup->add(new lie::test::ValueTest<bool>("!=", false, dir != dir2));
        }
    }
}

#endif // LIE_CORE_Folder_TEST_HPP
