#ifndef LIE_CORE_FILE_TEST_HPP
#define LIE_CORE_FILE_TEST_HPP

#include <lie/Test.hpp>
#include <lie/core\File.hpp>

namespace lie{
    namespace core{

        void Test_File(lie::test::TestGroup* group){
			lie::test::TestGroup* fileGroup = new lie::test::TestGroup("File");
			group->add(fileGroup);

            fileGroup->add(new lie::test::Comment("Construct file from path string"));
            File file("C:\\folder1\\folder2\\test.txt");
            fileGroup->add(new lie::test::ValueTest<std::string>("Name", std::string("test"), file.getName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Extension", std::string("txt"), file.getExtension()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Fullname", std::string("test.txt"), file.getFullName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Folder", std::string("C:\\folder1\\folder2"), file.getFolder().getFullPath(lie::core::PathStyle::Windows)));

            fileGroup->add(new lie::test::Comment("Set functions"));
            file.setName("cabbage");
            fileGroup->add(new lie::test::ValueTest<std::string>("Name", std::string("cabbage"), file.getName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Extension", std::string("txt"), file.getExtension()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Fullname", std::string("cabbage.txt"), file.getFullName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Folder", std::string("C:\\folder1\\folder2"), file.getFolder().getFullPath(lie::core::PathStyle::Windows)));

            lie::core::Folder dir("C:\\folder");
            file.setFolder(dir);
            fileGroup->add(new lie::test::ValueTest<std::string>("Name", "cabbage", file.getName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Extension", std::string("txt"), file.getExtension()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Fullname", std::string("cabbage.txt"), file.getFullName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Folder", std::string("C:\\folder"), file.getFolder().getFullPath(lie::core::PathStyle::Windows)));

            dir.push("hi");//lets check the setDir is a deep copy...
            fileGroup->add(new lie::test::Comment("Checking a deep copy was made..."));

            file.setFullName("file.ext");
            fileGroup->add(new lie::test::ValueTest<std::string>("Name", std::string("file"), file.getName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Extension", std::string("ext"), file.getExtension()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Fullname", std::string("file.ext"), file.getFullName()));
            fileGroup->add(new lie::test::ValueTest<std::string>("Folder", std::string("C:\\folder"), file.getFolder().getFullPath(lie::core::PathStyle::Windows)));
        }
    }
}

#endif // LIE_CORE_FILE_TEST_HPP
