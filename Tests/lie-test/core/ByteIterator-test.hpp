#ifndef LIE_CORE_BYTEITERATOR_TEST_HPP
#define LIE_CORE_BYTEITERATOR_TEST_HPP

#include <lie/core/ByteBuffer.hpp>
#include <lie/core/ByteIterator.hpp>
#include <lie/test.hpp>
#include <lie/math/Vector3.hpp>


namespace lie{
    namespace core{

		void Test_ByteIterator(lie::test::TestGroup* group){
		    lie::test::TestGroup* biGroup = group->createSubgroup("Byte Iterator");

		    {
		        biGroup->add(new lie::test::Comment("Reading all ints from array"));

                int ints[] = {1, 3, 5, 7, 9, -10, 12, 15, -12345}; //9 ints
                lie::core::ByteIterator<int> iter1((lie::core::byte_t*)&ints);

                biGroup->add(new lie::test::ValueTest<int>("int index 0", 1, iter1[0]));
                biGroup->add(new lie::test::ValueTest<int>("int index 1", 3, iter1[1]));
                biGroup->add(new lie::test::ValueTest<int>("int index 2", 5, iter1[2]));
                biGroup->add(new lie::test::ValueTest<int>("int index 3", 7, iter1[3]));
                biGroup->add(new lie::test::ValueTest<int>("int index 4", 9, iter1[4]));
                biGroup->add(new lie::test::ValueTest<int>("int index 5", -10, iter1[5]));
                biGroup->add(new lie::test::ValueTest<int>("int index 6", 12, iter1[6]));
                biGroup->add(new lie::test::ValueTest<int>("int index 7", 15, iter1[7]));
                biGroup->add(new lie::test::ValueTest<int>("int index 8", -12345, iter1[8]));

                biGroup->add(new lie::test::ValueTest<int>("int index 0 using ++", 1, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 1 using ++", 3, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 2 using ++", 5, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 3 using ++", 7, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 4 using ++", 9, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 5 using ++", -10, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using ++", 12, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 7 using ++", 15, *iter1));
                iter1++;
                biGroup->add(new lie::test::ValueTest<int>("int index 8 using ++", -12345, *iter1));
                iter1--;
                biGroup->add(new lie::test::ValueTest<int>("int index 7 using --", 15, *iter1));
                biGroup->add(new lie::test::ValueTest<int>("int index 5 using [-2]", -10, iter1[-2]));
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using [-1]", 12, iter1[-1]));
                biGroup->add(new lie::test::ValueTest<int>("int index 7 using [0]", 15, iter1[0]));
                biGroup->add(new lie::test::ValueTest<int>("int index 8 using [1]", -12345, iter1[1]));

                biGroup->add(new lie::test::Comment("Same again, using prefix ++ and -- rather than postfix"));
                lie::core::ByteIterator<int> iter2((lie::core::byte_t*)&ints);
                biGroup->add(new lie::test::ValueTest<int>("int index 0 using ++", 1, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 1 using ++", 3, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 2 using ++", 5, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 3 using ++", 7, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 4 using ++", 9, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 5 using ++", -10, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using ++", 12, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 7 using ++", 15, *iter2));
                ++iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 8 using ++", -12345, *iter2));
                --iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 7 using --", 15, *iter2));
                --iter2;
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using --", 12, *iter2));

                biGroup->add(new lie::test::Comment("Reading every other int with stride"));
                lie::core::ByteIterator<int, sizeof(int)*2> iter3((lie::core::byte_t*)&ints);
                biGroup->add(new lie::test::ValueTest<int>("int index 0", 1, iter3[0]));
                biGroup->add(new lie::test::ValueTest<int>("int index 2", 5, iter3[1]));
                biGroup->add(new lie::test::ValueTest<int>("int index 4", 9, iter3[2]));
                biGroup->add(new lie::test::ValueTest<int>("int index 6", 12, iter3[3]));
                biGroup->add(new lie::test::ValueTest<int>("int index 8", -12345, iter3[4]));

                biGroup->add(new lie::test::ValueTest<int>("int index 0 using ++", 1, *iter3));
                iter3++;
                biGroup->add(new lie::test::ValueTest<int>("int index 2 using ++", 5, *iter3));
                iter3++;
                biGroup->add(new lie::test::ValueTest<int>("int index 4 using ++", 9, *iter3));
                iter3++;
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using ++", 12, *iter3));
                iter3++;
                biGroup->add(new lie::test::ValueTest<int>("int index 8 using ++", -12345, *iter3));
                iter3--;
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using --", 12, *iter3));
                biGroup->add(new lie::test::ValueTest<int>("int index 4 using [-1]", 9, iter3[-1]));
                biGroup->add(new lie::test::ValueTest<int>("int index 6 using [0]", 12, iter3[0]));
                biGroup->add(new lie::test::ValueTest<int>("int index 8 using [1]", -12345, iter3[1]));
		    }
		}
    }
}


#endif // LIE_CORE_BYTEITERATOR_TEST_HPP

