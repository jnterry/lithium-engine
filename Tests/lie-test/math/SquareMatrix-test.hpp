#ifndef LIE_MATH_SQUAREMATRIX_TEST_HPP
#define LIE_MATH_SQUAREMATRIX_TEST_HPP

#include <lie/test.hpp>
#include <lie/math\SquareMatrix.hpp>

namespace lie{
    namespace math{

        void Test_SquareMatrix(lie::test::TestGroup* group){
            lie::test::TestGroup* matGroup = group->createSubgroup("SquareMatrix<Type, Dimension>");

            matGroup->add(new lie::test::Comment("Default constructor 2x2"));
            lie::math::SquareMatrix<int, 2> mat1;
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][0]", 0, mat1[0][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][1]", 0, mat1[0][1]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][0]", 0, mat1[1][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][1]", 0, mat1[1][1]));

            matGroup->add(new lie::test::Comment("Operator=  brace initializer list"));
            mat1 = {1,2,3,4};
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][0]", 1, mat1[0][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][1]", 2, mat1[0][1]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][0]", 3, mat1[1][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][1]", 4, mat1[1][1]));

            matGroup->add(new lie::test::Comment(".transpose (2x2 matix)"));
            mat1.transpose();
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][0]", 1, mat1[0][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[0][1]", 3, mat1[0][1]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][0]", 2, mat1[1][0]));
            matGroup->add(new lie::test::ValueTest<int>("mat1[1][1]", 4, mat1[1][1]));

            matGroup->add(new lie::test::Comment("Brace initlializer list contructor"));
            lie::math::SquareMatrix<float, 3> mat2({1,2,3, 4,5,6, 7,8,9});
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][0]", 1, mat2[0][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][1]", 2, mat2[0][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][2]", 3, mat2[0][2]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][0]", 4, mat2[1][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][1]", 5, mat2[1][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][2]", 6, mat2[1][2]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][0]", 7, mat2[2][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][1]", 8, mat2[2][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][2]", 9, mat2[2][2]));

            matGroup->add(new lie::test::Comment(".transpose (3x3 matix)"));
            mat2.transpose();
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][0]", 1, mat2[0][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][1]", 4, mat2[0][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[0][2]", 7, mat2[0][2]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][0]", 2, mat2[1][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][1]", 5, mat2[1][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[1][2]", 8, mat2[1][2]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][0]", 3, mat2[2][0]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][1]", 6, mat2[2][1]));
            matGroup->add(new lie::test::ValueTest<float>("mat2[2][2]", 9, mat2[2][2]));





        }

    }
}

#endif // LIE_MATH_SQUAREMATRIX_TEST_HPP
