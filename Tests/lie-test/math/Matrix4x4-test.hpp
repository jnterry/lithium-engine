#ifndef LIE_MATH_MAT4_TEST_H
#define LIE_MATH_MAT4_TEST_H

#include <lie/test.hpp>
#include <lie/math/Matrix4x4.hpp>
#include <lie/math/Angle.hpp>

namespace lie{
    namespace math{

        void Test_Matrix4x4(lie::test::TestGroup* group){
			lie::test::TestGroup* mat4Group = new lie::test::TestGroup("Matrix4x4f (matrix 4x4)");
			group->add(mat4Group);


            lie::test::TestGroup* idenGroup = mat4Group->createSubgroup("Identity Matrix Values");
            idenGroup->add(new lie::test::ValueTest<float>("Identity[0][0]", 1.0f, lie::math::Matrix4x4f::Identity.at(0,0)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[0][1]", 0.0f, lie::math::Matrix4x4f::Identity.at(0,1)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[0][2]", 0.0f, lie::math::Matrix4x4f::Identity.at(0,2)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[0][3]", 0.0f, lie::math::Matrix4x4f::Identity.at(0,3)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[1][0]", 0.0f, lie::math::Matrix4x4f::Identity.at(1,0)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[1][1]", 1.0f, lie::math::Matrix4x4f::Identity.at(1,1)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[1][2]", 0.0f, lie::math::Matrix4x4f::Identity.at(1,2)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[1][3]", 0.0f, lie::math::Matrix4x4f::Identity.at(1,3)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[2][0]", 0.0f, lie::math::Matrix4x4f::Identity.at(2,0)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[2][1]", 0.0f, lie::math::Matrix4x4f::Identity.at(2,1)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[2][2]", 1.0f, lie::math::Matrix4x4f::Identity.at(2,2)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[2][3]", 0.0f, lie::math::Matrix4x4f::Identity.at(2,3)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[3][0]", 0.0f, lie::math::Matrix4x4f::Identity.at(3,0)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[3][1]", 0.0f, lie::math::Matrix4x4f::Identity.at(3,1)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[3][2]", 0.0f, lie::math::Matrix4x4f::Identity.at(3,2)));
            idenGroup->add(new lie::test::ValueTest<float>("Identity[3][3]", 1.0f, lie::math::Matrix4x4f::Identity.at(3,3)));

            {
                lie::test::TestGroup* conVoidGroup = mat4Group->createSubgroup("Constuctor(void) - expecting idenity");
                Matrix4x4f mat;
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.0f, mat[0][0]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 0.0f, mat[0][1]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 0.0f, mat[0][2]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 0.0f, mat[0][3]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 0.0f, mat[1][0]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 1.0f, mat[1][1]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 0.0f, mat[1][2]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 0.0f, mat[1][3]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 0.0f, mat[2][0]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 0.0f, mat[2][1]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 1.0f, mat[2][2]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 0.0f, mat[2][3]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 0.0f, mat[3][0]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 0.0f, mat[3][1]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 0.0f, mat[3][2]));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 1.0f, mat[3][3]));

                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(0,0)", 1.0f, mat.at(0,0)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(0,1)", 0.0f, mat.at(0,1)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(0,2)", 0.0f, mat.at(0,2)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(0,3)", 0.0f, mat.at(0,3)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(1,0)", 0.0f, mat.at(1,0)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(1,1)", 1.0f, mat.at(1,1)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(1,2)", 0.0f, mat.at(1,2)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(1,3)", 0.0f, mat.at(1,3)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(2,0)", 0.0f, mat.at(2,0)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(2,1)", 0.0f, mat.at(2,1)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(2,2)", 1.0f, mat.at(2,2)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(2,3)", 0.0f, mat.at(2,3)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(3,0)", 0.0f, mat.at(3,0)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(3,1)", 0.0f, mat.at(3,1)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(3,2)", 0.0f, mat.at(3,2)));
                conVoidGroup->add(new lie::test::ValueTest<float>("mat.at(3,3)", 1.0f, mat.at(3,3)));
            }

            {

                lie::test::TestGroup* conArrayGroup = mat4Group->createSubgroup("Constructor ({f1, f2, f3, ...})");
                Matrix4x4f mat({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16});
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.0f, mat[0][0]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 2.0f, mat[0][1]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 3.0f, mat[0][2]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 4.0f, mat[0][3]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 5.0f, mat[1][0]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 6.0f, mat[1][1]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 7.0f, mat[1][2]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 8.0f, mat[1][3]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 9.0f, mat[2][0]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 10.0f, mat[2][1]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 11.0f, mat[2][2]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 12.0f, mat[2][3]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 13.0f, mat[3][0]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 14.0f, mat[3][1]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 15.0f, mat[3][2]));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 16.0f, mat[3][3]));

                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,0)", 1.0f, mat.at(0,0)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,1)", 2.0f, mat.at(0,1)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,2)", 3.0f, mat.at(0,2)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,3)", 4.0f, mat.at(0,3)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,0)", 5.0f, mat.at(1,0)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,1)", 6.0f, mat.at(1,1)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,2)", 7.0f, mat.at(1,2)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,3)", 8.0f, mat.at(1,3)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,0)", 9.0f, mat.at(2,0)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,1)", 10.0f, mat.at(2,1)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,2)", 11.0f, mat.at(2,2)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,3)", 12.0f, mat.at(2,3)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,0)", 13.0f, mat.at(3,0)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,1)", 14.0f, mat.at(3,1)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,2)", 15.0f, mat.at(3,2)));
                conArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,3)", 16.0f, mat.at(3,3)));
            }

            {
                lie::test::TestGroup* setSqrGroup = mat4Group->createSubgroup("Setting values with [][]");
                lie::math::Matrix4x4f mat;
                mat[0][0] = 1;
                mat[0][1] = 2;
                mat[0][2] = 3;
                mat[0][3] = 4;
                mat[1][0] = 5;
                mat[1][1] = 6;
                mat[1][2] = 7;
                mat[1][3] = 8;
                mat[2][0] = 9;
                mat[2][1] = 10;
                mat[2][2] = 11;
                mat[2][3] = 12;
                mat[3][0] = 13;
                mat[3][1] = 14;
                mat[3][2] = 15;
                mat[3][3] = 16;

                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(0,0)", 1.0f, mat.at(0,0)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(0,1)", 2.0f, mat.at(0,1)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(0,2)", 3.0f, mat.at(0,2)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(0,3)", 4.0f, mat.at(0,3)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(1,0)", 5.0f, mat.at(1,0)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(1,1)", 6.0f, mat.at(1,1)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(1,2)", 7.0f, mat.at(1,2)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(1,3)", 8.0f, mat.at(1,3)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(2,0)", 9.0f, mat.at(2,0)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(2,1)", 10.0f, mat.at(2,1)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(2,2)", 11.0f, mat.at(2,2)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(2,3)", 12.0f, mat.at(2,3)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(3,0)", 13.0f, mat.at(3,0)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(3,1)", 14.0f, mat.at(3,1)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(3,2)", 15.0f, mat.at(3,2)));
                setSqrGroup->add(new lie::test::ValueTest<float>("mat.at(3,3)", 16.0f, mat.at(3,3)));

                lie::test::TestGroup* opEqMatGroup = mat4Group->createSubgroup("operator=(Matrix4x4f)");
                mat = Matrix4x4f::Identity;

                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.0f, mat[0][0]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 0.0f, mat[0][1]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 0.0f, mat[0][2]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 0.0f, mat[0][3]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 0.0f, mat[1][0]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 1.0f, mat[1][1]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 0.0f, mat[1][2]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 0.0f, mat[1][3]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 0.0f, mat[2][0]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 0.0f, mat[2][1]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 1.0f, mat[2][2]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 0.0f, mat[2][3]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 0.0f, mat[3][0]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 0.0f, mat[3][1]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 0.0f, mat[3][2]));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 1.0f, mat[3][3]));

                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(0,0)", 1.0f, mat.at(0,0)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(0,1)", 0.0f, mat.at(0,1)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(0,2)", 0.0f, mat.at(0,2)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(0,3)", 0.0f, mat.at(0,3)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(1,0)", 0.0f, mat.at(1,0)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(1,1)", 1.0f, mat.at(1,1)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(1,2)", 0.0f, mat.at(1,2)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(1,3)", 0.0f, mat.at(1,3)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(2,0)", 0.0f, mat.at(2,0)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(2,1)", 0.0f, mat.at(2,1)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(2,2)", 1.0f, mat.at(2,2)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(2,3)", 0.0f, mat.at(2,3)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(3,0)", 0.0f, mat.at(3,0)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(3,1)", 0.0f, mat.at(3,1)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(3,2)", 0.0f, mat.at(3,2)));
                opEqMatGroup->add(new lie::test::ValueTest<float>("mat.at(3,3)", 1.0f, mat.at(3,3)));

                lie::test::TestGroup* opEqArrayGroup = mat4Group->createSubgroup("operator=({10,11,12,...})");
                mat = {10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};

                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 10.0f, mat[0][0]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 11.0f, mat[0][1]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 12.0f, mat[0][2]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 13.0f, mat[0][3]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 14.0f, mat[1][0]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 15.0f, mat[1][1]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 16.0f, mat[1][2]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 17.0f, mat[1][3]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 18.0f, mat[2][0]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 19.0f, mat[2][1]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 20.0f, mat[2][2]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 21.0f, mat[2][3]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 22.0f, mat[3][0]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 23.0f, mat[3][1]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 24.0f, mat[3][2]));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 25.0f, mat[3][3]));

                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,0)", 10.0f, mat.at(0,0)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,1)", 11.0f, mat.at(0,1)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,2)", 12.0f, mat.at(0,2)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(0,3)", 13.0f, mat.at(0,3)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,0)", 14.0f, mat.at(1,0)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,1)", 15.0f, mat.at(1,1)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,2)", 16.0f, mat.at(1,2)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(1,3)", 17.0f, mat.at(1,3)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,0)", 18.0f, mat.at(2,0)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,1)", 19.0f, mat.at(2,1)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,2)", 20.0f, mat.at(2,2)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(2,3)", 21.0f, mat.at(2,3)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,0)", 22.0f, mat.at(3,0)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,1)", 23.0f, mat.at(3,1)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,2)", 24.0f, mat.at(3,2)));
                opEqArrayGroup->add(new lie::test::ValueTest<float>("mat.at(3,3)", 25.0f, mat.at(3,3)));
            }

            ///////////////////////////////////////////////////
            {
                lie::test::TestGroup* opAddEqGroup = mat4Group->createSubgroup("operator+=()");

                lie::math::Matrix4x4f mat = {1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16};
                lie::math::Matrix4x4f mat1( {2,3,4,5, 6,7,8,9, 10,11,12,13, 14,15,16,17});

                mat += mat1;

                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 3.0f, mat[0][0]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 5.0f, mat[0][1]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 7.0f, mat[0][2]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 9.0f, mat[0][3]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 11.0f, mat[1][0]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 13.0f, mat[1][1]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 15.0f, mat[1][2]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 17.0f, mat[1][3]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 19.0f, mat[2][0]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 21.0f, mat[2][1]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 23.0f, mat[2][2]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 25.0f, mat[2][3]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 27.0f, mat[3][0]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 29.0f, mat[3][1]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 31.0f, mat[3][2]));
                opAddEqGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 33.0f, mat[3][3]));

                lie::test::TestGroup* opSubEqGroup = mat4Group->createSubgroup("operator-=()");
                mat -= mat1;
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.0f, mat[0][0]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 2.0f, mat[0][1]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 3.0f, mat[0][2]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 4.0f, mat[0][3]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 5.0f, mat[1][0]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 6.0f, mat[1][1]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 7.0f, mat[1][2]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 8.0f, mat[1][3]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 9.0f, mat[2][0]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 10.0f, mat[2][1]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 11.0f, mat[2][2]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 12.0f, mat[2][3]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 13.0f, mat[3][0]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 14.0f, mat[3][1]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 15.0f, mat[3][2]));
                opSubEqGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 16.0f, mat[3][3]));

            }

            {
                lie::test::TestGroup* copyConGroup = mat4Group->createSubgroup("Copy Constructor");
                lie::math::Matrix4x4f mat({16,15,14,13, 12,11,10,9, 8,7,6,5, 4,3,2,1});
                lie::math::Matrix4x4f matCopy(mat);

                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 16.0f, mat[0][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 15.0f, mat[0][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 14.0f, mat[0][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 13.0f, mat[0][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 12.0f, mat[1][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 11.0f, mat[1][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 10.0f, mat[1][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 9.0f, mat[1][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 8.0f, mat[2][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 7.0f, mat[2][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 6.0f, mat[2][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 5.0f, mat[2][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 4.0f, mat[3][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 3.0f, mat[3][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 2.0f, mat[3][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 1.0f, mat[3][3]));

                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][0]", 16.0f, matCopy[0][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][1]", 15.0f, matCopy[0][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][2]", 14.0f, matCopy[0][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][3]", 13.0f, matCopy[0][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][0]", 12.0f, matCopy[1][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][1]", 11.0f, matCopy[1][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][2]", 10.0f, matCopy[1][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][3]", 9.0f, matCopy[1][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][0]", 8.0f, matCopy[2][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][1]", 7.0f, matCopy[2][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][2]", 6.0f, matCopy[2][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][3]", 5.0f, matCopy[2][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][0]", 4.0f, matCopy[3][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][1]", 3.0f, matCopy[3][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][2]", 2.0f, matCopy[3][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][3]", 1.0f, matCopy[3][3]));

                matCopy = lie::math::Matrix4x4f::Identity;
                copyConGroup->add(new lie::test::Comment("Ensuring deep copy..."));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 16.0f, mat[0][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 15.0f, mat[0][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 14.0f, mat[0][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 13.0f, mat[0][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 12.0f, mat[1][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 11.0f, mat[1][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 10.0f, mat[1][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 9.0f, mat[1][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 8.0f, mat[2][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 7.0f, mat[2][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 6.0f, mat[2][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 5.0f, mat[2][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 4.0f, mat[3][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 3.0f, mat[3][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 2.0f, mat[3][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 1.0f, mat[3][3]));

                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][0]", 1.0f, matCopy[0][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][1]", 0.0f, matCopy[0][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][2]", 0.0f, matCopy[0][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[0][3]", 0.0f, matCopy[0][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][0]", 0.0f, matCopy[1][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][1]", 1.0f, matCopy[1][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][2]", 0.0f, matCopy[1][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[1][3]", 0.0f, matCopy[1][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][0]", 0.0f, matCopy[2][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][1]", 0.0f, matCopy[2][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][2]", 1.0f, matCopy[2][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[2][3]", 0.0f, matCopy[2][3]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][0]", 0.0f, matCopy[3][0]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][1]", 0.0f, matCopy[3][1]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][2]", 0.0f, matCopy[3][2]));
                copyConGroup->add(new lie::test::ValueTest<float>("matCopy[3][3]", 1.0f, matCopy[3][3]));
            }

            {
                lie::test::TestGroup* matMultGroup = mat4Group->createSubgroup("Matrix4x4f*=Matrix4x4f mutliplication");
                lie::math::Matrix4x4f mat({2,4,6,8, 10,12,14,16, 18,20,22,24, 26,28,30,32});
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 2.0f, mat[0][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 4.0f, mat[0][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 6.0f, mat[0][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 8.0f, mat[0][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 10.0f, mat[1][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 12.0f, mat[1][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 14.0f, mat[1][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 16.0f, mat[1][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 18.0f, mat[2][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 20.0f, mat[2][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 22.0f, mat[2][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 24.0f, mat[2][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 26.0f, mat[3][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 28.0f, mat[3][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 30.0f, mat[3][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 32.0f, mat[3][3]));

                mat *= lie::math::Matrix4x4f::Identity;
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 2.0f, mat[0][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 4.0f, mat[0][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 6.0f, mat[0][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 8.0f, mat[0][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 10.0f, mat[1][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 12.0f, mat[1][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 14.0f, mat[1][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 16.0f, mat[1][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 18.0f, mat[2][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 20.0f, mat[2][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 22.0f, mat[2][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 24.0f, mat[2][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 26.0f, mat[3][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 28.0f, mat[3][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 30.0f, mat[3][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 32.0f, mat[3][3]));

                mat *= lie::math::Matrix4x4f({1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16});

                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 180.0f, mat[0][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 200.0f, mat[0][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 220.0f, mat[0][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 240.0f, mat[0][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 404.0f, mat[1][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 456.0f, mat[1][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 508.0f, mat[1][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 560.0f, mat[1][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 628.0f, mat[2][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 712.0f, mat[2][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 796.0f, mat[2][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 880.0f, mat[2][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 852.0f, mat[3][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 968.0f, mat[3][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 1084.0f, mat[3][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 1200.0f, mat[3][3]));
            }

            {
                lie::test::TestGroup* matMultGroup = mat4Group->createSubgroup("Matrix4x4f*Matrix4x4f mutliplication");
                lie::math::Matrix4x4f mat = lie::math::Matrix4x4f({1,1,2,2, 3,3,4,4, 5,5,6,6, 7,7,8,8}) * lie::math::Matrix4x4f({0,0,1,1, 1,1,0,0, 3,3,1,1, 2,2,2,2});
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 11.0f, mat[0][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 11.0f, mat[0][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 7.0f, mat[0][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 7.0f, mat[0][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 23.0f, mat[1][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 23.0f, mat[1][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 15.0f, mat[1][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 15.0f, mat[1][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 35.0f, mat[2][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 35.0f, mat[2][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][2]", 23.0f, mat[2][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[2][3]", 23.0f, mat[2][3]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 47.0f, mat[3][0]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 47.0f, mat[3][1]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][2]", 31.0f, mat[3][2]));
                matMultGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 31.0f, mat[3][3]));
            }


            lie::test::TestGroup* matVecMultGroup = mat4Group->createSubgroup("Matrix4x4f*Vector3 mutliplication");
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::Identity * lie::math::Vector3f(1,2,3);
                matVecMultGroup->add(new lie::test::Comment("Group 1"));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.x", 1.0f, vec.x));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.y", 2.0f, vec.y));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.z", 3.0f, vec.z));
            }
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f({1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16}) * lie::math::Vector3f(1,2,3);
                matVecMultGroup->add(new lie::test::Comment("Group 2"));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.x", 0.17647058823f, vec.x));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.y", 0.45098039215f, vec.y));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.z", 0.72549019607f, vec.z));
            }
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f({1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16}) * lie::math::Vector3f(30,60,90);
                matVecMultGroup->add(new lie::test::Comment("Group 3"));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.x", 0.16332819722f, vec.x));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.y", 0.44221879815f, vec.y));
                matVecMultGroup->add(new lie::test::ValueTest<float>("vec.z", 0.72110939907f, vec.z));
            }


            {
                lie::test::TestGroup* mat4intGroup = mat4Group->createSubgroup("Matrix4x4i");
                lie::math::Matrix4x4i mati({1,2,3,4, 5,6,7,8, 9,8,7,6, 1,3,5,7});
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][0]", 1, mati[0][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][1]", 2, mati[0][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][2]", 3, mati[0][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][3]", 4, mati[0][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][0]", 5, mati[1][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][1]", 6, mati[1][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][2]", 7, mati[1][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][3]", 8, mati[1][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][0]", 9, mati[2][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][1]", 8, mati[2][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][2]", 7, mati[2][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][3]", 6, mati[2][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][0]", 1, mati[3][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][1]", 3, mati[3][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][2]", 5, mati[3][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][3]", 7, mati[3][3]));

                mat4intGroup->add(new lie::test::Comment("multipling by int scalar 2"));
                mati *= 2;
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][0]", 2, mati[0][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][0]", 2, mati[0][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][1]", 4, mati[0][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][2]", 6, mati[0][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[0][3]", 8, mati[0][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][0]", 10, mati[1][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][1]", 12, mati[1][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][2]", 14, mati[1][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[1][3]", 16, mati[1][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][0]", 18, mati[2][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][1]", 16, mati[2][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][2]", 14, mati[2][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[2][3]", 12, mati[2][3]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][0]", 2, mati[3][0]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][1]", 6, mati[3][1]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][2]", 10, mati[3][2]));
                mat4intGroup->add(new lie::test::ValueTest<int>("mati[3][3]", 14, mati[3][3]));

                /*mat4intGroup->add(new lie::test::Comment("multipling by int matrix by float scalar 0.5f"));
                lie::math::Matrix4x4f matif = mati * 0.5f;
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[0][0]", 1, matif[0][0]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[0][1]", 2, matif[0][1]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[0][2]", 3, matif[0][2]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[0][3]", 4, matif[0][3]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[1][0]", 5, matif[1][0]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[1][1]", 6, matif[1][1]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[1][2]", 7, matif[1][2]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[1][3]", 8, matif[1][3]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[2][0]", 9, matif[2][0]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[2][1]", 8, matif[2][1]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[2][2]", 7, matif[2][2]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[2][3]", 6, matif[2][3]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[3][0]", 1, matif[3][0]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[3][1]", 3, matif[3][1]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[3][2]", 5, matif[3][2]));
                mat4intGroup->add(new lie::test::ValueTest<float>("matif[3][3]", 7, matif[3][3]));*/



            }

            {
                lie::test::TestGroup* scaleVecGroup = mat4Group->createSubgroup("Matrix4x4f::createScale(float uniformScale) then * Vector3");
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createScale(1) * lie::math::Vector3f(1,2,3);
                    scaleVecGroup->add(new lie::test::Comment("Group 1"));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.x", 1.0f, vec.x));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.y", 2.0f, vec.y));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.z", 3.0f, vec.z));
                }
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createScale(2) * lie::math::Vector3f(1,2,3);
                    scaleVecGroup->add(new lie::test::Comment("Group 2"));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.x", 2.0f, vec.x));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.y", 4.0f, vec.y));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.z", 6.0f, vec.z));
                }
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createScale(0.5) * lie::math::Vector3f(1,2,3);
                    scaleVecGroup->add(new lie::test::Comment("Group 3"));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.x", 0.5f, vec.x));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.y", 1.0f, vec.y));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.z", 1.5f, vec.z));
                }
            }
            {
                lie::test::TestGroup* scaleVecGroup = mat4Group->createSubgroup("Matrix4x4f::createScale(float x, float y, float z)");
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createScale(1,2,3) * lie::math::Vector3f(1,2,3);
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.x", 1.0f, vec.x));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.y", 4.0f, vec.y));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.z", 9.0f, vec.z));
                }
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createScale(5,1,0.1) * lie::math::Vector3f(1,2,3);
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.x", 5.0f, vec.x));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.y", 2.0f, vec.y));
                    scaleVecGroup->add(new lie::test::ValueTest<float>("vec.z", 0.3f, vec.z));
                }
            }

            {
                lie::test::TestGroup* transFloatGroup = mat4Group->createSubgroup("Matrix4x4f::createTranslation(float x, float y, float z)");
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createTranslation(1,2,3) * lie::math::Vector3f(1,2,3);
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.x", 2.0f, vec.x));
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.y", 4.0f, vec.y));
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.z", 6.0f, vec.z));
                }
                {
                    lie::math::Vector3f vec = lie::math::Matrix4x4f::createTranslation(-1,0.1,1) * lie::math::Vector3f(1,2,3);
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.x", 0.0f, vec.x));
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.y", 2.1f, vec.y));
                    transFloatGroup->add(new lie::test::ValueTest<float>("vec.z", 4.0f, vec.z));
                }
            }

            {
                lie::test::TestGroup* transVecGroup = mat4Group->createSubgroup("Matrix4x4f::createTranslation(Vector3f offset)");
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createTranslation(Vector3f(5,-2,1)) * lie::math::Vector3f(1,2,3);
                transVecGroup->add(new lie::test::ValueTest<float>("vec.x", 6.0f, vec.x));
                transVecGroup->add(new lie::test::ValueTest<float>("vec.y", 0.0f, vec.y));
                transVecGroup->add(new lie::test::ValueTest<float>("vec.z", 4.0f, vec.z));
            }

            {
                lie::test::TestGroup* perProjGroup = mat4Group->createSubgroup("Matrix4x4f::createPerspectiveProjection(...)");
                {
                    lie::math::Matrix4x4f mat = lie::math::Matrix4x4f::createPerspectiveProjection(lie::math::deg(45.0f), 1024, 768, 0.1f, 200.0f);
                    perProjGroup->add(new lie::test::Comment("Param Set: 45.0f, 1024, 768, 0.1f, 200.0f"));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.81066f, mat[0][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 0.0f, mat[0][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 0.0f, mat[0][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 0.0f, mat[0][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 0.0f, mat[1][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 2.41421f, mat[1][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 0.0f, mat[1][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 0.0f, mat[1][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 0.0f, mat[2][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 0.0f, mat[2][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][2]", -1.001f, mat[2][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][3]", -1.0f, mat[2][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 0.0f, mat[3][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 0.0f, mat[3][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][2]", -0.2001f, mat[3][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 0.0f, mat[3][3]));
                }
                {
					lie::math::Matrix4x4f mat = lie::math::Matrix4x4f::createPerspectiveProjection(lie::math::deg(60.0f), 500, 500, 1.0f, 500.0f);
                    perProjGroup->add(new lie::test::Comment("Param Set: 60.0f, 500, 500, 1.0f, 500.0f"));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][0]", 1.73205f, mat[0][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][1]", 0.0f, mat[0][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][2]", 0.0f, mat[0][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[0][3]", 0.0f, mat[0][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][0]", 0.0f, mat[1][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][1]", 1.73205f, mat[1][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][2]", 0.0f, mat[1][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[1][3]", 0.0f, mat[1][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][0]", 0.0f, mat[2][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][1]", 0.0f, mat[2][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][2]", -1.00401f, mat[2][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[2][3]", -1.0f, mat[2][3]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][0]", 0.0f, mat[3][0]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][1]", 0.0f, mat[3][1]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][2]", -2.00401f, mat[3][2]));
                    perProjGroup->add(new lie::test::ValueTest<float>("mat[3][3]", 0.0f, mat[3][3]));
                }
            }

            lie::test::TestGroup* createRotGroup = mat4Group->createSubgroup("Matrix4x4f::createRotationXXXXXX(...)");
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationX(lie::math::deg(100.0f)) * lie::math::Vector3f(0,0,0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin x .x", 0.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin x .y", 0.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin x .z", 0.0f, vec.z));
            }
            {
				lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationY(lie::math::rad(5)) * lie::math::Vector3f(0, 0, 0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin y .x", 0.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin y .y", 0.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin y .z", 0.0f, vec.z));
            }
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationZ(lie::math::deg(200.0f)) * lie::math::Vector3f(0,0,0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin z .x", 0.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin z .y", 0.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating origin z .z", 0.0f, vec.z));
            }

            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationX(lie::math::deg(90.0f)) * lie::math::Vector3f(1,0,0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) x .x", 1.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) x .y", 0.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) x .z", 0.0f, vec.z));
            }
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationY(lie::math::deg(90.0f)) * lie::math::Vector3f(1,0,0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) y .x", 0.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) y .y", 0.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) y .z", 1.0f, vec.z));
            }
            {
                lie::math::Vector3f vec = lie::math::Matrix4x4f::createRotationZ(lie::math::deg(90.0f)) * lie::math::Vector3f(1,0,0);
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) z .x", 0.0f, vec.x));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) z .y", -1.0f, vec.y));
                createRotGroup->add(new lie::test::ValueTest<float>("rotating (1,0,0) z .z", 0.0f, vec.z));
            }




        }
    }
}

#endif // LIE_MATH_VEC3_TEST_H
