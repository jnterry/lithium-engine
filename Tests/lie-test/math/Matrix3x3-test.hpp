#ifndef LIE_MATH_MATRIX3X3_TEST_HPP
#define LIE_MATH_MATRIX3X3_TEST_HPP

#include <lie/test.hpp>
#include <lie/math\Matrix3x3.hpp>

namespace lie{
    namespace math{

        void Test_Matrix3x3(lie::test::TestGroup* group){
            lie::test::TestGroup* matGroup = group->createSubgroup("Matrix3x3<Type>");

            {
                matGroup->add(new lie::test::Comment("Defualt constructor - expecting identity to be returned"));
                lie::math::Matrix3x3f mat1;
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 1, mat1[0][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 0, mat1[0][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][2]", 0, mat1[0][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 0, mat1[1][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 1, mat1[1][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][2]", 0, mat1[1][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 0, mat1[2][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 0, mat1[2][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][2]", 1, mat1[2][2]));
            }

            {
                matGroup->add(new lie::test::Comment("Brace initializer constructor"));
                lie::math::Matrix3x3f mat1({1,2,3, 4,5,6, 7,8,9});
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 1, mat1[0][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 2, mat1[0][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][2]", 3, mat1[0][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 4, mat1[1][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 5, mat1[1][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][2]", 6, mat1[1][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 7, mat1[2][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 8, mat1[2][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][2]", 9, mat1[2][2]));
                matGroup->add(new lie::test::Comment("Transposing previous matrix"));
                mat1.transpose();
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 1, mat1[0][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 4, mat1[0][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][2]", 7, mat1[0][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 2, mat1[1][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 5, mat1[1][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][2]", 8, mat1[1][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 3, mat1[2][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 6, mat1[2][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][2]", 9, mat1[2][2]));
            }

            {
                matGroup->add(new lie::test::Comment("operator (0,1,2, 3,4,5, 6,7,8) * Identity"));
                auto mat1 = lie::math::Matrix3x3f({0,1,2, 3,4,5, 6,7,8}) * lie::math::Matrix3x3f::Identity;
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 0, mat1[0][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 1, mat1[0][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][2]", 2, mat1[0][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 3, mat1[1][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 4, mat1[1][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][2]", 5, mat1[1][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 6, mat1[2][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 7, mat1[2][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][2]", 8, mat1[2][2]));
            }

            {
                matGroup->add(new lie::test::Comment("operator (9,8,7, 6,5,4, 3,2,1) * (0.0,0.5,1.0, 1.5,2.0,2.5, 3.0,3.5,4.0)"));
                auto mat1 = lie::math::Matrix3x3f({9,8,7, 6,5,4, 3,2,1}) * lie::math::Matrix3x3f({0.0,0.5,1.0, 1.5,2.0,2.5, 3.0,3.5,4.0});
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 33, mat1[0][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 45, mat1[0][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[0][2]", 57, mat1[0][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 19.5, mat1[1][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 27, mat1[1][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[1][2]", 34.5, mat1[1][2]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 6, mat1[2][0]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 9, mat1[2][1]));
                matGroup->add(new lie::test::ValueTest<float>("mat1[2][2]", 12, mat1[2][2]));
            }

            {
                matGroup->add(new lie::test::Comment("Create scale factor 2, scaling (1,2)"));
                Vector2f vec = lie::math::Matrix3x3f::createScale(2) * lie::math::Vector2f(1,2);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", 2, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 4, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create scale factor 3,0.5, scaling (3,6)"));
                Vector2f vec = lie::math::Matrix3x3f::createScale(3,0.5) * lie::math::Vector2f(3,6);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", 9, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 3, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create translation (1,2), translating(0,0)"));
                Vector2f vec = lie::math::Matrix3x3f::createTranslation(1,2) * lie::math::Vector2f(0,0);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", 1, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 2, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create translation (-5,10), translating(2,3)"));
                Vector2f vec = lie::math::Matrix3x3f::createTranslation(lie::math::Vector2f(-5,10)) * lie::math::Vector2f(2,3);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", -3, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 13, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create rotation 90deg, rotating(1,0)"));
                Vector2f vec = lie::math::Matrix3x3f::createRotation(lie::math::deg(90)) * lie::math::Vector2f(1,0);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", 0, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 1, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create rotation -90deg, rotating(0,1)"));
                Vector2f vec = lie::math::Matrix3x3f::createRotation(lie::math::deg(-90)) * lie::math::Vector2f(0,1);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", 1, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", 0, vec.y));
            }

            {
                matGroup->add(new lie::test::Comment("Create rotation 180deg, rotating(1,2)"));
                Vector2f vec = lie::math::Matrix3x3f::createRotation(lie::math::deg(90)) * lie::math::Vector2f(1,2);
                matGroup->add(new lie::test::ValueTest<float>("vec.x", -1, vec.x));
                matGroup->add(new lie::test::ValueTest<float>("vec.y", -2, vec.y));
            }


        }

    }
}

#endif // LIE_MATH_MATRIX3X3_TEST_HPP
