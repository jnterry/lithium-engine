#ifndef LIE_MATH_MATRIX_TEST_HPP
#define LIE_MATH_MATRIX_TEST_HPP

#include <lie/test.hpp>
#include <lie/math\Matrix.hpp>

namespace lie{
    namespace math{

        void Test_Matrix(lie::test::TestGroup* group){
            lie::test::TestGroup* matGroup = group->createSubgroup("Matrix<Type, Rows, Cols>");
            {
                lie::test::TestGroup* matBasicGroup = matGroup->createSubgroup("Basic Functions - constuctors, ==, =, !=, etc");
                matBasicGroup->add(new lie::test::Comment("Default constructor"));
                lie::math::Matrix<int, 3,2> mat1;
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat1.getRowCount()", 3, mat1.getRowCount()));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat1.getColumnCount()", 2, mat1.getColumnCount()));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[0][0]", 0, mat1[0][0]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[0][1]", 0, mat1[0][1]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[0][2]", 0, mat1[0][2]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[1][0]", 0, mat1[1][0]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[1][1]", 0, mat1[1][1]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1[1][2]", 0, mat1[1][2]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[0]", 0, mat1.begin()[0]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[1]", 0, mat1.begin()[1]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[2]", 0, mat1.begin()[2]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[3]", 0, mat1.begin()[3]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[4]", 0, mat1.begin()[4]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[5]", 0, mat1.begin()[5]));

                matBasicGroup->add(new lie::test::Comment("Setting via [][], read with at"));
                mat1[0][0] = 1;
                mat1[0][1] = 2;
                mat1[0][2] = 3;
                mat1[1][0] = 4;
                mat1[1][1] = 5;
                mat1[1][2] = 6;
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,0)", 1, mat1.at(0,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,1)", 2, mat1.at(0,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,2)", 3, mat1.at(0,2)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,0)", 4, mat1.at(1,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,1)", 5, mat1.at(1,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,2)", 6, mat1.at(1,2)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[0]", 1, mat1.begin()[0]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[1]", 2, mat1.begin()[1]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[2]", 3, mat1.begin()[2]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[3]", 4, mat1.begin()[3]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[4]", 5, mat1.begin()[4]));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.begin()[5]", 6, mat1.begin()[5]));

                matBasicGroup->add(new lie::test::Comment("Brace initilializer list constructor"));
                lie::math::Matrix<unsigned int, 3,2> mat2({6,5,4,3,2,1});
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][0]", 6, mat2[0][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][1]", 5, mat2[0][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][2]", 4, mat2[0][2]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][0]", 3, mat2[1][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][1]", 2, mat2[1][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][2]", 1, mat2[1][2]));

                matBasicGroup->add(new lie::test::Comment("operator==, !="));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 == mat1", false, mat2 == mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 == mat2", false, mat1 == mat2));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 != mat1", true, mat2 != mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 != mat2", true, mat1 != mat2));

                matBasicGroup->add(new lie::test::Comment("Setting values with begin()[]"));
                mat2.begin()[0] = 10;
                mat2.begin()[1] = 11;
                mat2.begin()[2] = 12;
                mat2.begin()[3] = 13;
                mat2.begin()[4] = 14;
                mat2.begin()[5] = 15;
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][0]", 10, mat2[0][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][1]", 11, mat2[0][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][2]", 12, mat2[0][2]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][0]", 13, mat2[1][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][1]", 14, mat2[1][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][2]", 15, mat2[1][2]));

                matBasicGroup->add(new lie::test::Comment("operator="));
                mat2 = mat1;
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,0)", 1, mat1.at(0,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,1)", 2, mat1.at(0,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,2)", 3, mat1.at(0,2)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,0)", 4, mat1.at(1,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,1)", 5, mat1.at(1,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,2)", 6, mat1.at(1,2)));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][0]", 1, mat2[0][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][1]", 2, mat2[0][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][2]", 3, mat2[0][2]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][0]", 4, mat2[1][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][1]", 5, mat2[1][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][2]", 6, mat2[1][2]));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 == mat1", true, mat2 == mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 == mat2", true, mat1 == mat2));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 != mat1", false, mat2 != mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 != mat2", false, mat1 != mat2));

                matBasicGroup->add(new lie::test::Comment("Ensuring Deep Copy"));
                mat1.at(0,0) = 0;
                mat1.at(1,2) = 0;
                mat2.at(1,1) = 10;
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,0)", 0, mat1.at(0,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,1)", 2, mat1.at(0,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,2)", 3, mat1.at(0,2)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,0)", 4, mat1.at(1,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,1)", 5, mat1.at(1,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,2)", 0, mat1.at(1,2)));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][0]", 1, mat2[0][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][1]", 2, mat2[0][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[0][2]", 3, mat2[0][2]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][0]", 4, mat2[1][0]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][1]", 10, mat2[1][1]));
                matBasicGroup->add(new lie::test::ValueTest<unsigned int>("mat2[1][2]", 6, mat2[1][2]));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 == mat1", false, mat2 == mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 == mat2", false, mat1 == mat2));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat2 != mat1", true, mat2 != mat1));
                matBasicGroup->add(new lie::test::ValueTest<bool>("mat1 != mat2", true, mat1 != mat2));

                matBasicGroup->add(new lie::test::Comment("Testing for breaking on self-assignment"));
                mat1 = mat1;
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,0)", 0, mat1.at(0,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,1)", 2, mat1.at(0,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(0,2)", 3, mat1.at(0,2)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,0)", 4, mat1.at(1,0)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,1)", 5, mat1.at(1,1)));
                matBasicGroup->add(new lie::test::ValueTest<int>("mat1.at(1,2)", 0, mat1.at(1,2)));
            }

            {
                lie::test::TestGroup* matArthGroup = matGroup->createSubgroup("Aritmetic Operators");
                lie::math::Matrix<float, 2,4> mat1({0.1f,0.2f, 1.1f,1.2f, 2.1f,2.2f, 3.1f,3.2f});
                matArthGroup->add(new lie::test::ValueTest<unsigned int>("mat1.getRowCount()", 2, mat1.getRowCount()));
                matArthGroup->add(new lie::test::ValueTest<unsigned int>("mat1.getColumnCount()", 4, mat1.getColumnCount()));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 0.1, mat1[0][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 0.2, mat1[0][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 1.1, mat1[1][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 1.2, mat1[1][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 2.1, mat1[2][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 2.2, mat1[2][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[3][0]", 3.1, mat1[3][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[3][1]", 3.2, mat1[3][1]));

                matArthGroup->add(new lie::test::Comment("Multiply by scalar"));
                mat1 *= 2;
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[0][0]", 0.2, mat1[0][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[0][1]", 0.4, mat1[0][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[1][0]", 2.2, mat1[1][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[1][1]", 2.4, mat1[1][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[2][0]", 4.2, mat1[2][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[2][1]", 4.4, mat1[2][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[3][0]", 6.2, mat1[3][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat1[3][1]", 6.4, mat1[3][1]));

                matArthGroup->add(new lie::test::Comment("Get Transpose + copy constructor"));
                //0.1 1.1 2.1 3.1
                //0.2 1.2 2.2 3.2
                //becomes
                //0.1 0.2
                //1.1 1.2
                //2.1 2.2
                //3.1 3.2
                lie::math::Matrix<float, 4,2> mat2(mat1.getTranspose());
                matArthGroup->add(new lie::test::ValueTest<unsigned int>("mat2.getRowCount()", 4, mat2.getRowCount()));
                matArthGroup->add(new lie::test::ValueTest<unsigned int>("mat2.getColumnCount()", 2, mat2.getColumnCount()));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[0][0]", 0.2, mat2[0][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[1][0]", 0.4, mat2[1][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[0][1]", 2.2, mat2[0][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[1][1]", 2.4, mat2[1][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[0][2]", 4.2, mat2[0][2]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[1][2]", 4.4, mat2[1][2]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[0][3]", 6.2, mat2[0][3]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat2[1][3]", 6.4, mat2[1][3]));

                matArthGroup->add(new lie::test::Comment("Operator mat + mat"));
                lie::math::Matrix<int, 2,2> mat3 = lie::math::Matrix<int, 2,2>({1,2, 3,4}) + lie::math::Matrix<int, 2,2>({5,6, 7,8});
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[0][0]", 6, mat3[0][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[0][1]", 8, mat3[0][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[1][0]", 10, mat3[1][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[1][1]", 12, mat3[1][1]));

                matArthGroup->add(new lie::test::Comment("Operator mat<int> -= mat<float>"));
                mat3 -= lie::math::Matrix<float, 2,2>({1.9f,2.1f, 3.6f,4.8f});
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[0][0]", 4, mat3[0][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[0][1]", 5, mat3[0][1]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[1][0]", 6, mat3[1][0]));
                matArthGroup->add(new lie::test::ValueTest<float>("mat3[1][1]", 7, mat3[1][1]));
            }

        }

    }
}

#endif // LIE_MATH_MATRIX_TEST_HPP
