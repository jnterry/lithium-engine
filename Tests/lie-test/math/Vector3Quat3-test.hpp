//////////////////////////////////////////////////////////////////
//		      Part of Lithium Engine, by Jamie Terry            //
//////////////////////////////////////////////////////////////////
/// \file Vector3Quat3-test.hpp
/// \brief This file tests the interaction between the Vector3 class and the Quaternion 3 class
///
/// \ingroup unit-tests
//////////////////////////////////////////////////////////////////
#ifndef LIE_MATH_VECTOR3QUAT3_TEST_HPP
#define LIE_MATH_VECTOR3QUAT3_TEST_HPP

#include <lie/test.hpp>
#include <lie/math/Vector3-impl.hpp>
#include <lie/math/Quaternion3.hpp>
#include <lie/math/Angle.hpp>

namespace lie{
    namespace math{

        void Test_Vec3Quat3(lie::test::TestGroup* group){
            lie::test::TestGroup* vqGroup = group->createSubgroup("Quaternion3 Vector3 Interactions");

            {
                vqGroup->add(new lie::test::Comment("Rotating (1,0,0) 90 deg anticlockwise around z"));
                lie::math::Vector3f vec(1,0,0);
				lie::math::Quaternion3<float> quat(lie::math::Vector3<float>::UnitZ, lie::math::deg(90.0f));
                vec.rotate(quat);
                vqGroup->add(new lie::test::ValueTest<float>("vec.x", 0.0f, vec.x));
                vqGroup->add(new lie::test::ValueTest<float>("vec.y", 1.0f, vec.y));
                vqGroup->add(new lie::test::ValueTest<float>("vec.z", 0.0f, vec.z));
            }

            {
                vqGroup->add(new lie::test::Comment("Rotating (1,0,0) -90 deg anticlockwise around z"));
                lie::math::Vector3f vec(1,0,0);
				lie::math::Quaternion3f quat(lie::math::Vector3f::UnitZ, lie::math::deg(-90.0f));
                vec.rotate(quat);
                vqGroup->add(new lie::test::ValueTest<float>("vec.x", 0.0f, vec.x));
                vqGroup->add(new lie::test::ValueTest<float>("vec.y", -1.0f, vec.y));
                vqGroup->add(new lie::test::ValueTest<float>("vec.z", 0.0f, vec.z));
            }

            {
                vqGroup->add(new lie::test::Comment("Rotating (1,0,0) 180 deg anticlockwise around z"));
                lie::math::Vector3f vec(1,0,0);
				lie::math::Quaternion3f quat(lie::math::Vector3f::UnitZ, lie::math::deg(180.0f));
                vec.rotate(quat);
                vqGroup->add(new lie::test::ValueTest<float>("vec.x", -1.0f, vec.x));
                vqGroup->add(new lie::test::ValueTest<float>("vec.y", 0.0f, vec.y));
                vqGroup->add(new lie::test::ValueTest<float>("vec.z", 0.0f, vec.z));
            }

            {
                vqGroup->add(new lie::test::Comment("Rotating (0,1,0) 180 deg anticlockwise around y"));
                lie::math::Vector3f vec(0,1,0);
				lie::math::Quaternion3f quat(lie::math::Vector3f::UnitY, lie::math::deg(180.0f));
                vec.rotate(quat);
                vqGroup->add(new lie::test::ValueTest<float>("vec.x", 0.0f, vec.x));
                vqGroup->add(new lie::test::ValueTest<float>("vec.y", 1.0f, vec.y));
                vqGroup->add(new lie::test::ValueTest<float>("vec.z", 0.0f, vec.z));
            }

            {
                vqGroup->add(new lie::test::Comment("Rotating (0,0,1) 180 deg anticlockwise around y"));
                lie::math::Vector3f vec(0,0,1);
				lie::math::Quaternion3f quat(lie::math::Vector3f::UnitY, lie::math::deg(180.0f));
                vec.rotate(quat);
                vqGroup->add(new lie::test::ValueTest<float>("vec.x", 0.0f, vec.x));
                vqGroup->add(new lie::test::ValueTest<float>("vec.y", 0.0f, vec.y));
                vqGroup->add(new lie::test::ValueTest<float>("vec.z", -1.0f, vec.z));
            }

        }

    }
}

#endif // LIE_MATH_VECTOR3QUAT3_TEST_HPP
