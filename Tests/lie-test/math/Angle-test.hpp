/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Angle-test.hpp
/// \author Jamie Terry
/// \date 2015/06/17
/// \brief Contains unit tests for the lie::math::Angle class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_MATH_ANGLE_TEST_HPP
#define	LIE_MATH_ANGLE_TEST_HPP

#include <lie/math/Angle.hpp>
#include <lie/test.hpp>

namespace lie{
	namespace math{

		void Test_Angle(lie::test::TestGroup* group){
			lie::test::TestGroup* angleGroup = group->createSubgroup("Angle");
			{
				lie::test::TestGroup* unitConvGroup = angleGroup->createSubgroup("Freestanding Conversion Functions");
				unitConvGroup->add(new lie::test::ValueTest<float>("1 rad to deg", 57.2957795, lie::math::toDegrees(1)));
				unitConvGroup->add(new lie::test::ValueTest<float>("1pi rad to deg", 180.0, lie::math::toDegrees(lie::math::PI_d)));
				unitConvGroup->add(new lie::test::ValueTest<float>("2pi rad to deg", 360.0, lie::math::toDegrees(2 * lie::math::PI_d)));
				unitConvGroup->add(new lie::test::ValueTest<float>("3pi rad to deg", 540.0, lie::math::toDegrees(3 * lie::math::PI_d)));
				unitConvGroup->add(new lie::test::ValueTest<float>("1 deg to rad", 0.0174532925, lie::math::toRadians(1)));
				unitConvGroup->add(new lie::test::ValueTest<float>("180 deg to rad", lie::math::PI_f, lie::math::toRadians(180)));
				unitConvGroup->add(new lie::test::ValueTest<float>("360 deg to rad", 2.f*lie::math::PI_f, lie::math::toRadians(360)));
				unitConvGroup->add(new lie::test::ValueTest<float>("540 deg to rad", 3.f*lie::math::PI_f, lie::math::toRadians(540)));
			}

			{
				lie::test::TestGroup* grp = angleGroup->createSubgroup("named constructors and getters");
				lie::math::Anglef a = lie::math::Anglef::degrees(50);
				grp->add(new lie::test::ValueTest<float>("::degrees(50) as deg", 50, a.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("::degrees(50) as rev", 50.f/360.f, a.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("::degrees(50) as rad", 0.872664626, a.asRadians()));

				lie::math::Angled b = lie::math::Angled::revolutions(-100);
				grp->add(new lie::test::ValueTest<float>("::revolutions(-100) as deg", -36000, b.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("::revolutions(-100) as rev", -100, b.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("::revolutions(-100) as rad", -628.318530718, b.asRadians()));

				lie::math::Angled c = lie::math::Angled::radians(8 * PI_d);
				grp->add(new lie::test::ValueTest<float>("::radians(8pi) as deg", 360*4, c.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("::radians(8pi) as rev", 4, c.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("::radians(8pi) as rad", 8 * PI_d, c.asRadians()));

			}

			{
				lie::test::TestGroup* unitConvGroup = angleGroup->createSubgroup("deg(), rad(), rev() helper functions");
				unitConvGroup->add(new lie::test::ValueTest<float>("deg(30) as deg", 30, lie::math::deg(30.f).asDegrees()));
				unitConvGroup->add(new lie::test::ValueTest<float>("deg(30) as rev", 1.f/12.f, lie::math::deg(30.f).asRevolutions()));
				unitConvGroup->add(new lie::test::ValueTest<float>("deg(30) as rad", 0.523598776, lie::math::deg(30.f).asRadians()));

				unitConvGroup->add(new lie::test::ValueTest<float>("deg(-60) as deg", -60, lie::math::deg(-60.f).asDegrees()));
				unitConvGroup->add(new lie::test::ValueTest<float>("deg(-60) as rev", -0.16666666666, lie::math::deg(-60.f).asRevolutions()));
				unitConvGroup->add(new lie::test::ValueTest<float>("deg(-60) as rad", -1.04719755, lie::math::deg(-60.f).asRadians()));
			}

			{
				lie::test::TestGroup* grp = angleGroup->createSubgroup("Getter/Setter/Constructor unit conversions");
				lie::math::Anglef a1 = lie::math::Anglef::degrees(720);
				grp->add(new lie::test::ValueTest<float>("constructed 720deg as deg", 720, a1.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("constructed 720deg as rad", 4.f*lie::math::PI_f, a1.asRadians()));
				grp->add(new lie::test::ValueTest<float>("constructed 720deg as rev", 2, a1.asRevolutions()));
				a1.setDegrees(30);
				grp->add(new lie::test::ValueTest<float>("set 30deg as deg", 30, a1.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("set 30deg as rad", 0.523598776f, a1.asRadians()));
				grp->add(new lie::test::ValueTest<float>("set 30deg as rev", 30.f/360.f, a1.asRevolutions()));

				lie::math::Anglef a2 = lie::math::Anglef::radians(-lie::math::PI_f);
				grp->add(new lie::test::ValueTest<float>("constructed -PI rad as deg", -180.f, a2.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("constructed -PI rad as rad", -lie::math::PI_f, a2.asRadians()));
				grp->add(new lie::test::ValueTest<float>("constructed -PI rad as rev", -0.5f, a2.asRevolutions()));
				a2.setRadians(100);
				grp->add(new lie::test::ValueTest<float>("set 100rad as deg", 5729.57795, a2.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("set 100rad as rad", 100, a2.asRadians()));
				grp->add(new lie::test::ValueTest<float>("set 100rad as rev", 15.9154943092, a2.asRevolutions()));

				lie::math::Anglef a3 = lie::math::Anglef::revolutions(-10);
				grp->add(new lie::test::ValueTest<float>("constructed -10 rev as deg", -3600.f, a3.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("constructed -10 rev as rad", -20.f*lie::math::PI_f, a3.asRadians()));
				grp->add(new lie::test::ValueTest<float>("constructed -10 rev as rev", -10, a3.asRevolutions()));
				a3.setRevolutions(0.1);
				grp->add(new lie::test::ValueTest<float>("set 0.1 rev as deg", 36, a3.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("set 0.1 rev rad", 0.62831853071, a3.asRadians()));
				grp->add(new lie::test::ValueTest<float>("set 0.1 rev as rev", 0.1, a3.asRevolutions()));
			}

			{
				lie::test::TestGroup* grp = angleGroup->createSubgroup("Comparisons");

				lie::math::Angled a1 = lie::math::Angled::degrees(36);
				lie::math::Angled a2 = lie::math::Angled::degrees(360);
				grp->add(new lie::test::ValueTest<bool>("36deg == 360deg", false, a1 == a2));
				grp->add(new lie::test::ValueTest<bool>("36deg != 360deg", true, a1 != a2));
				grp->add(new lie::test::ValueTest<bool>("36deg <  360deg", true, a1 <  a2));
				grp->add(new lie::test::ValueTest<bool>("36deg <= 360deg", true, a1 <= a2));
				grp->add(new lie::test::ValueTest<bool>("36deg >  360deg", false, a1 >  a2));
				grp->add(new lie::test::ValueTest<bool>("36deg >= 360deg", false, a1 >= a2));

				a1 = lie::math::Angled::degrees(72);
				a2 = lie::math::Angled::revolutions(0.2);
				grp->add(new lie::test::ValueTest<bool>("72deg == 0.2 turns", true, a1 == a2));
				grp->add(new lie::test::ValueTest<bool>("72deg != 0.2 turns", false, a1 != a2));
				grp->add(new lie::test::ValueTest<bool>("72deg <  0.2 rev", false, a1 < a2));
				grp->add(new lie::test::ValueTest<bool>("72deg <= 0.2 rev", true, a1 <= a2));
				grp->add(new lie::test::ValueTest<bool>("72deg >  0.2 rev", false, a1 > a2));
				grp->add(new lie::test::ValueTest<bool>("72deg >= 0.2 rev", true, a1 >= a2));

				a1 = lie::math::Angled::degrees(365);
				a2 = lie::math::Angled::degrees(5);
				grp->add(new lie::test::ValueTest<bool>("365deg == 5deg", false, a1 == a2));
				grp->add(new lie::test::ValueTest<bool>("365deg != 5deg", true, a1 != a2));
				grp->add(new lie::test::ValueTest<bool>("365deg <  5deg", false, a1 < a2));
				grp->add(new lie::test::ValueTest<bool>("365deg <= 5deg", false, a1 <= a2));
				grp->add(new lie::test::ValueTest<bool>("365deg >  5deg", true, a1 > a2));
				grp->add(new lie::test::ValueTest<bool>("365deg >= 5deg", true, a1 >= a2));
			}

			{
				lie::test::TestGroup* grp = angleGroup->createSubgroup("Arithmetic");

				lie::math::Anglef result = lie::math::Anglef::degrees(360) / 10;
				grp->add(new lie::test::ValueTest<float>("360deg/10", 36, result.asDegrees()));

				result = lie::math::Anglef::degrees(50);
				result += lie::math::Anglef::degrees(400);
				grp->add(new lie::test::ValueTest<float>("50deg += 400 deg", 450, result.asDegrees()));

				result = lie::math::Anglef::revolutions(6) - lie::math::Anglef::revolutions(6.5);
				grp->add(new lie::test::ValueTest<float>("6 rev - 6.5 rev", -180, result.asDegrees()));

				result = lie::math::Anglef::radians(40);
				grp->add(new lie::test::ValueTest<float>("= 40 rad", 40, result.asRadians()));

				result *= 2;
				grp->add(new lie::test::ValueTest<float>("40rad *= 2", 80, result.asRadians()));

				result = lie::math::Anglef::revolutions(2) * 3;
				grp->add(new lie::test::ValueTest<float>("2 rev * 3", 6, result.asRevolutions()));

				result = result;
				grp->add(new lie::test::ValueTest<float>("a = a", 6, result.asRevolutions()));

				lie::math::Anglef a = (lie::math::deg(90) + lie::math::rev(1));
				grp->add(new lie::test::ValueTest<float>("deg(90) + rev(1) as deg", 450, a.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("deg(90) + rev(1) as rev", 1.25, a.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("deg(90) + rev(1) as rad", 7.85398163, a.asRadians()));

				lie::math::Angled b = lie::math::Angled::revolutions(1);
				b += lie::math::deg(90);
				grp->add(new lie::test::ValueTest<float>("rev(1) += deg(90) as deg", 450, b.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("rev(1) += deg(90) as rev", 1.25, b.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("rev(1) += deg(90) as rad", 7.85398163, b.asRadians()));

				lie::math::Anglef c = (lie::math::Anglef::radians(1) - lie::math::rev(1));
				grp->add(new lie::test::ValueTest<float>("rad(1) - rev(1) as deg", -302.70422, c.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("rad(1) - rev(1) as rev", -0.84084505555, c.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("rad(1) - rev(1) as rad", -5.28318530718, c.asRadians()));
			
				lie::math::Anglef d = lie::math::Anglef::degrees(360);
				d -= lie::math::deg(90);
				grp->add(new lie::test::ValueTest<float>("deg(360) -= deg(90) as deg", 270, d.asDegrees()));
				grp->add(new lie::test::ValueTest<float>("deg(360) -= deg(90) as rev", 0.75, d.asRevolutions()));
				grp->add(new lie::test::ValueTest<float>("deg(360) -= deg(90) as rad", 4.71238898, d.asRadians()));
			}

			{
				lie::test::TestGroup* grp = angleGroup->createSubgroup("Clamping");

				lie::math::Angled a = lie::math::Angled::revolutions(0.5);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 0.5 rev", 0.5, a.asRevolutions()));

				a.setRevolutions(1.3);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 1.3 rev", 0.3, a.asRevolutions()));

				a.setRevolutions(100.01);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 100.01 rev", 0.01, a.asRevolutions()));

				a.setRevolutions(-0.4);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp -0.4 rev", 0.6, a.asRevolutions()));

				a.setRevolutions(-8.6);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp -8.6 rev", 0.4, a.asRevolutions()));

				a.setRevolutions(1);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 1 rev", 0, a.asRevolutions()));

				a.setRevolutions(0);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 0 rev", 0, a.asRevolutions()));

				a.setDegrees(365);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 365 degrees", 5.0, a.asDegrees()));

				a.setDegrees(540);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 540 degrees", 180, a.asDegrees()));

				a.setDegrees(360.1);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp 360.1 degrees", 0.1, a.asDegrees()));

				a.setDegrees(-90);
				a.clamp();
				grp->add(new lie::test::ValueTest<double>("clamp -90 degrees", 270, a.asDegrees()));


			}
		}

	}
	
}

#endif