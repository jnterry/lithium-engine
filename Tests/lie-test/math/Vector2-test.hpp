#ifndef LIE_MATH_VEC2_TEST_HPP
#define LIE_MATH_VEC2_TEST_HPP

#include <lie/math/Vector2.hpp>
#include <lie/Test.hpp>

namespace lie{
    namespace math{

        void Test_Vec2(lie::test::TestGroup* group){
			lie::test::TestGroup* vec2Group = group->createSubgroup("Vector2");

            {
                Vector2f vec1(5.0f, 0.0f);
                vec2Group->add(new lie::test::Comment("constructor -> vec1(5.0, 0.0)"));
                vec2Group->add(new lie::test::ValueTest<float>("vec1.x", 5.0f, vec1.x));
                vec2Group->add(new lie::test::ValueTest<float>("vec1.y", 0.0f, vec1.y));

                vec2Group->add(new lie::test::Comment("operator= {1.0f, -9.2f}"));
                vec1 = {3.0f, -9.2f};
                vec2Group->add(new lie::test::ValueTest<float>("vec1.x", 3.0f, vec1.x));
                vec2Group->add(new lie::test::ValueTest<float>("vec1.y", -9.2f, vec1.y));

                vec2Group->add(new lie::test::Comment("copy constructor, testing + test deep copy"));
                Vector2<double> vec2(vec1);
                vec1 = {1.0f, 2.0f};
                vec2Group->add(new lie::test::ValueTest<float>("vec1.x", 1.0f, vec1.x));
                vec2Group->add(new lie::test::ValueTest<float>("vec1.y", 2.0f, vec1.y));
                vec2Group->add(new lie::test::ValueTest<float>("vec2.x", 3.0, vec2.x));
                vec2Group->add(new lie::test::ValueTest<float>("vec2.y", -9.2, vec2.y));

                vec2Group->add(new lie::test::Comment("comparison operators"));
                vec2Group->add(new lie::test::ValueTest<float>("vec2 == vec1", false, vec2 == vec1));
                vec2Group->add(new lie::test::ValueTest<float>("vec1 == vec2", false, vec1 == vec2));
                vec2Group->add(new lie::test::ValueTest<float>("vec2 != vec1", true, vec2 != vec1));
                vec2Group->add(new lie::test::ValueTest<float>("vec1 != vec2", true, vec1 != vec2));

            }

            lie::test::TestGroup* distGroup = vec2Group->createSubgroup("Distance Functions");
            {
                Vector2f vec1(10.0f, 0.0f);
                Vector2f vec2(0.0f, 7.0f);
                Vector2f vec3(3.0f, 4.0f);
                Vector2f vec4(6.0f, -10.0f);

                distGroup->add(new lie::test::ValueTest<float>("(10.0f, 0.0f) length", 10.0f, vec1.getLength()));
                distGroup->add(new lie::test::ValueTest<float>("(10.0f, 0.0f) lengthSquared", 100.0f, vec1.getLengthSquared()));
                distGroup->add(new lie::test::ValueTest<float>("(10.0f, 0.0f) distTo(0,0)", 10.0f, vec1.getDistanceTo(Vector2f::Origin)));
                distGroup->add(new lie::test::ValueTest<float>("(10.0f, 0.0f) distToSquared(0,0)", 100.0f, vec1.getDistanceToSquared(Vector2f::Origin)));

                distGroup->add(new lie::test::ValueTest<float>("(0.0f, 7.0f) length", 7.0f, vec2.getLength()));
                distGroup->add(new lie::test::ValueTest<float>("(0.0f, 7.0f) lengthSquared", 49.0f, vec2.getLengthSquared()));
                distGroup->add(new lie::test::ValueTest<float>("(0.0f, 7.0f) distTo(0,0)", 7.0f, vec2.getDistanceTo(Vector2f::Origin)));
                distGroup->add(new lie::test::ValueTest<float>("(0.0f, 7.0f) distToSquared(0,0)", 49.0f, vec2.getDistanceToSquared(Vector2f::Origin)));

                distGroup->add(new lie::test::ValueTest<float>("(3.0f, 4.0f) length", 5.0f, vec3.getLength()));
                distGroup->add(new lie::test::ValueTest<float>("(4.0f, 3.0f) lengthSquared", 25.0f, vec3.getLengthSquared()));
                distGroup->add(new lie::test::ValueTest<float>("(3.0f, 4.0f) distTo(0,0)", 5.0f, vec3.getDistanceTo(Vector2f::Origin)));
                distGroup->add(new lie::test::ValueTest<float>("((3.0f, 4.0f) distToSquared(0,0)", 25.0f, vec3.getDistanceToSquared(Vector2f::Origin)));

            }
        }

    }
}

#endif
