/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Transform2-test.hpp
/// \author Jamie Terry
/// \date 2015/07/02
/// \brief Contains unit tests for the lie::math::Transform2 class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_MATH_TRANSFORM2_TEST_HPP
#define LIE_MATH_TRANSFORM2_TEST_HPP

#include <lie/math/Vector2.hpp>
#include <lie/math/Matrix3x3.hpp>
#include <lie/math/Transform2.hpp>
#include <lie/Test.hpp>

namespace lie{
	namespace math{

		void Test_Transform2(lie::test::TestGroup* group){
			lie::test::TestGroup* grp = group->createSubgroup("Transform2");

			grp->add(new lie::test::Comment("Default Constructor"));
			Transform2f trans;
			grp->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grp->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f::Origin, trans.getTranslation()));
			grp->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(1,1), trans.getScale()));
			grp->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(0), trans.getRotation()));
			grp->add(new lie::test::ValueTest<Matrix3x3f>("matrix", Matrix3x3f::Identity, trans.asMatrix()));

			lie::test::TestGroup* grpMod = grp->createSubgroup("Modify Operations");
			trans.move(10, -6);
			grpMod->add(new lie::test::Comment("move(10, -6)"));
			grpMod->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f(10,-6), trans.getTranslation()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(1, 1), trans.getScale()));
			grpMod->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(0), trans.getRotation()));

			trans.rotate(lie::math::deg(90));
			grpMod->add(new lie::test::Comment("rotate(deg(90))"));
			grpMod->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f(10, -6), trans.getTranslation()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(1, 1), trans.getScale()));
			grpMod->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(90), trans.getRotation()));
		
			trans.scale(1, 5);
			grpMod->add(new lie::test::Comment("scale(1 ,5)"));
			grpMod->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f(10, -6), trans.getTranslation()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(1, 5), trans.getScale()));
			grpMod->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(90), trans.getRotation()));

			trans.scale(lie::math::Vector2f(0.1, 2));
			grpMod->add(new lie::test::Comment("scale(vec(0.1, 2))"));
			grpMod->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f(10, -6), trans.getTranslation()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(0.1, 10), trans.getScale()));
			grpMod->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(90), trans.getRotation()));

			trans.move(lie::math::Vector2f(-1, 10));
			grpMod->add(new lie::test::Comment("scale(vec(-1, 10))"));
			grpMod->add(new lie::test::ValueTest<Vector2f>("origin", Vector2f::Origin, trans.getOrigin()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("translation", Vector2f(9, 4), trans.getTranslation()));
			grpMod->add(new lie::test::ValueTest<Vector2f>("scale", Vector2f(0.1, 10), trans.getScale()));
			grpMod->add(new lie::test::ValueTest<Anglef>("rotation", Anglef::degrees(90), trans.getRotation()));
		}

	}
}

#endif LIE_MATH_TRANSFORM2_TEST_HPP