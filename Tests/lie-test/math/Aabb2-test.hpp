#ifndef LIE_MATH_AABB2_TEST_HPP
#define LIE_MATH_AABB2_TEST_HPP

#include <lie/math\Aabb2.hpp>
#include <lie/test.hpp>

namespace lie{
    namespace math{

        void Test_Aabb2(lie::test::TestGroup* group){
			lie::test::TestGroup* aabbGroup = group->createSubgroup("Aabb2");

            {
                aabbGroup->add(new lie::test::Comment("constructor<int> (left, bottom, width, height)"));
                Aabb2i aabb(5,10,6,12);
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getLeft()", 5, aabb.getLeft()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getRight()", 11, aabb.getRight()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getBottom()", 10, aabb.getBottom()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getTop()", 22, aabb.getTop()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getWidth()", 6, aabb.getWidth()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getHeight()", 12, aabb.getHeight()));
            }

            {
                aabbGroup->add(new lie::test::Comment("constructor<float> ({list of Vector2<float>})"));
                Aabb2f aabb({lie::math::Vector2f(-5.0f,  6.0f),
                             lie::math::Vector2f( 2.3f,  1.0f),
                             lie::math::Vector2f(-2.4f,  7.0f),
                             lie::math::Vector2f( 5.6f, -9.0f),
                             lie::math::Vector2f( 3.2f,  4.2f)});

                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getLeft()", -5.0f, aabb.getLeft()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getRight()", 5.6f, aabb.getRight()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getBottom()", -9.0f, aabb.getBottom()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getTop()", 7.0f, aabb.getTop()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getWidth()", 10.6f, aabb.getWidth()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getHeight()", 16.0f, aabb.getHeight()));
            }

            {
                aabbGroup->add(new lie::test::Comment("constructor<float> ({single Vector2})"));
                Aabb2f aabb({lie::math::Vector2f(-5.0f,  6.0f)});

                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getLeft()", -5.0f, aabb.getLeft()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getRight()", -5.0f, aabb.getRight()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getBottom()", 6.0f, aabb.getBottom()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getTop()", 6.0, aabb.getTop()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getWidth()", 0.0f, aabb.getWidth()));
                aabbGroup->add(new lie::test::ValueTest<int>("aabb.getHeight()", 0.0f, aabb.getHeight()));
            }



        }
    }

}


#endif
