#ifndef LIE_MATH_QUATERNION3_TEST_HPP
#define LIE_MATH_QUATERNION3_TEST_HPP

#include <lie/test.hpp>
#include <lie/math\Quaternion3.hpp>

namespace lie{
    namespace math{

        void Test_Quaternion3(lie::test::TestGroup* group){
            lie::test::TestGroup* qGroup = group->createSubgroup("Quaternion3");

            {
                qGroup->add(new lie::test::Comment("Default constructor"));
                lie::math::Quaternion3f quat;
                qGroup->add(new lie::test::ValueTest<float>("quat.x()", 0.0f, quat.x()));
                qGroup->add(new lie::test::ValueTest<float>("quat.y()", 0.0f, quat.y()));
                qGroup->add(new lie::test::ValueTest<float>("quat.z()", 0.0f, quat.z()));
                qGroup->add(new lie::test::ValueTest<float>("quat.w()", 1.0f, quat.w()));
            }

            {
                qGroup->add(new lie::test::Comment("Constructor taking each value"));
                lie::math::Quaternion3i quat1(1,2,3,4);
                qGroup->add(new lie::test::ValueTest<int>("quat1.x()", 1, quat1.x()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.y()", 2, quat1.y()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.z()", 3, quat1.z()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.w()", 4, quat1.w()));

                qGroup->add(new lie::test::Comment("Copy Constructor"));
                lie::math::Quaternion3i quat2(quat1);
                qGroup->add(new lie::test::ValueTest<int>("quat1.x()", 1, quat1.x()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.y()", 2, quat1.y()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.z()", 3, quat1.z()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.w()", 4, quat1.w()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.x()", 1, quat2.x()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.y()", 2, quat2.y()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.z()", 3, quat2.z()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.w()", 4, quat2.w()));
                qGroup->add(new lie::test::ValueTest<bool>("quat2 == quat1", true, quat2 == quat1));
                qGroup->add(new lie::test::ValueTest<bool>("quat1 == quat2", true, quat1 == quat2));
                qGroup->add(new lie::test::ValueTest<bool>("quat2 != quat1", false, quat2 != quat1));
                qGroup->add(new lie::test::ValueTest<bool>("quat1 != quat2", false, quat1 != quat2));

                qGroup->add(new lie::test::Comment("Ensuring deep copy"));
                quat1.x() = 5;
                quat2.z() = 9;
                qGroup->add(new lie::test::ValueTest<int>("quat1.x()", 5, quat1.x()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.y()", 2, quat1.y()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.z()", 3, quat1.z()));
                qGroup->add(new lie::test::ValueTest<int>("quat1.w()", 4, quat1.w()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.x()", 1, quat2.x()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.y()", 2, quat2.y()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.z()", 9, quat2.z()));
                qGroup->add(new lie::test::ValueTest<int>("quat2.w()", 4, quat2.w()));
                qGroup->add(new lie::test::ValueTest<bool>("quat2 == quat1", false, quat2 == quat1));
                qGroup->add(new lie::test::ValueTest<bool>("quat1 == quat2", false, quat1 == quat2));
                qGroup->add(new lie::test::ValueTest<bool>("quat2 != quat1", true, quat2 != quat1));
                qGroup->add(new lie::test::ValueTest<bool>("quat1 != quat2", true, quat1 != quat2));
            }

            {
                //answers obtained from http://www.bluetulip.org/programs/quaternions.html
                qGroup->add(new lie::test::Comment("Quaternion*Quaternion Multiplication (1+ 2i+3j+4k) * (2+ 5i+-1j+-2k)"));
                Quaternion3f quat = Quaternion3f(2,3,4, 1) * Quaternion3f(5,-1,-2, 2);
                qGroup->add(new lie::test::ValueTest<float>("quat.x()", 7.0f, quat.x()));
                qGroup->add(new lie::test::ValueTest<float>("quat.y()", 29.0f, quat.y()));
                qGroup->add(new lie::test::ValueTest<float>("quat.z()", -11.0f, quat.z()));
                qGroup->add(new lie::test::ValueTest<float>("quat.w()", 3.0f, quat.w()));
            }

            {
                qGroup->add(new lie::test::Comment("Quaternion*=Quaternion Multiplication (0.1+ 6i+-7j+8k) * (3+ 0.5i+-6j+-1.5k)"));
                Quaternion3f quat(6,-7,8, 0.1);
                quat *= Quaternion3f(0.5f,-6,-1.5, 3);

                qGroup->add(new lie::test::ValueTest<float>("quat.x()", 76.55f, quat.x()));
                qGroup->add(new lie::test::ValueTest<float>("quat.y()", -8.6f, quat.y()));
                qGroup->add(new lie::test::ValueTest<float>("quat.z()", -8.6499999f, quat.z()));
                qGroup->add(new lie::test::ValueTest<float>("quat.w()", -32.7f, quat.w()));
            }

            {
                qGroup->add(new lie::test::Comment("Quaternion toConjugate"));
                Quaternion3f quat(1,2,4,8);
                quat.toConjugate();

                qGroup->add(new lie::test::ValueTest<float>("quat.x()", -1, quat.x()));
                qGroup->add(new lie::test::ValueTest<float>("quat.y()", -2, quat.y()));
                qGroup->add(new lie::test::ValueTest<float>("quat.z()", -4, quat.z()));
                qGroup->add(new lie::test::ValueTest<float>("quat.w()", 8, quat.w()));
            }

            {
                qGroup->add(new lie::test::Comment("Quaternion getConjugate"));
                Quaternion3f quatOrig(1.1, -0.6, -0.2, -3);
                Quaternion3f quat = quatOrig.getConjugate();

                qGroup->add(new lie::test::ValueTest<float>("quat.x()", -1.1f, quat.x()));
                qGroup->add(new lie::test::ValueTest<float>("quat.y()", 0.6f, quat.y()));
                qGroup->add(new lie::test::ValueTest<float>("quat.z()", 0.2f, quat.z()));
                qGroup->add(new lie::test::ValueTest<float>("quat.w()", -3.0f, quat.w()));
            }
        }

    }
}

#endif // LIE_MATH_QUATERNION3_TEST_HPP
