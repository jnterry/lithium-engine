#ifndef LIE_MATH_VECTOR3_TEST_HPP
#define LIE_MATH_VECTOR3_TEST_HPP

#include <lie/Test.hpp>
#include <lie/math\Vector3.hpp>

namespace lie{
    namespace math{

        void Test_Vec3(lie::test::TestGroup* group){
			lie::test::TestGroup* vec3Group = new lie::test::TestGroup("Vector3");
			group->add(vec3Group);


            vec3Group->add(new lie::test::ValueTest<float>("Origin.x", 0.0f, lie::math::Vector3f::Origin.x));
            vec3Group->add(new lie::test::ValueTest<float>("Origin.y", 0.0f, lie::math::Vector3f::Origin.y));
            vec3Group->add(new lie::test::ValueTest<float>("Origin.z", 0.0f, lie::math::Vector3f::Origin.z));

            vec3Group->add(new lie::test::Comment("static const UnitX"));
            vec3Group->add(new lie::test::ValueTest<float>("UnitX.x", 1.0f, lie::math::Vector3f::UnitX.x));
            vec3Group->add(new lie::test::ValueTest<float>("UnitX.y", 0.0f, lie::math::Vector3f::UnitX.y));
            vec3Group->add(new lie::test::ValueTest<float>("UnitX.z", 0.0f, lie::math::Vector3f::UnitX.z));

            vec3Group->add(new lie::test::Comment("static const UnitY"));
            vec3Group->add(new lie::test::ValueTest<float>("UnitY.x", 0.0f, lie::math::Vector3f::UnitY.x));
            vec3Group->add(new lie::test::ValueTest<float>("UnitY.y", 1.0f, lie::math::Vector3f::UnitY.y));
            vec3Group->add(new lie::test::ValueTest<float>("UnitY.z", 0.0f, lie::math::Vector3f::UnitY.z));

            vec3Group->add(new lie::test::Comment("static const UnitZ"));
            vec3Group->add(new lie::test::ValueTest<float>("UnitZ.x", 0.0f, lie::math::Vector3f::UnitZ.x));
            vec3Group->add(new lie::test::ValueTest<float>("UnitZ.y", 0.0f, lie::math::Vector3f::UnitZ.y));
            vec3Group->add(new lie::test::ValueTest<float>("UnitZ.z", 1.0f, lie::math::Vector3f::UnitZ.z));

            lie::math::Vector3f vec1(5.0, 0.0, 0.0);
            lie::math::Vector3f vec2(0.0, 5.0, 0.0);
            lie::math::Vector3f vec3(0.0, 0.0, 5.0);
            vec3Group->add(new lie::test::Comment("constructor -> vec1(5.0, 0.0, 0.0)"));
            vec3Group->add(new lie::test::ValueTest<float>("vec1.x", 5.0f, vec1.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec1.y", 0.0f, vec1.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec1.z", 0.0f, vec1.z));

            vec3Group->add(new lie::test::Comment("constructor -> vec2(0.0, 5.0, 0.0)"));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.x", 0.0f, vec2.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.y", 5.0f, vec2.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.z", 0.0f, vec2.z));

            vec3Group->add(new lie::test::Comment("constructor -> vec3(0.0, 0.0, 5.0)"));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.x", 0.0f, vec3.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.y", 0.0f, vec3.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.z", 5.0f, vec3.z));


            vec3Group->add(new lie::test::ValueTest<float>("vec1.getLength()", 5.0f, vec1.getLength()));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.getLength()", 5.0f, vec2.getLength()));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.getLength()", 5.0f, vec3.getLength()));

            vec3Group->add(new lie::test::ValueTest<float>("vec1.getLengthSquared()", 25.0f, vec1.getLengthSquared()));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.getLengthSquared()", 25.0f, vec2.getLengthSquared()));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.getLengthSquared()", 25.0f, vec3.getLengthSquared()));

            vec3Group->add(new lie::test::ValueTest<float>("vec1.getDistanceTo(Origin)", 5.0f, vec1.getDistanceTo(lie::math::Vector3f::Origin)));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.getDistanceTo(Origin)", 5.0f, vec2.getDistanceTo(lie::math::Vector3f::Origin)));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.getDistanceTo(Origin)", 5.0f, vec3.getDistanceTo(lie::math::Vector3f::Origin)));

            vec3Group->add(new lie::test::ValueTest<float>("vec1.getDistanceToSquared(Origin)", 25.0f, vec1.getDistanceToSquared(lie::math::Vector3f::Origin)));
            vec3Group->add(new lie::test::ValueTest<float>("vec2.getDistanceToSquared(Origin)", 25.0f, vec2.getDistanceToSquared(lie::math::Vector3f::Origin)));
            vec3Group->add(new lie::test::ValueTest<float>("vec3.getDistanceToSquared(Origin)", 25.0f, vec3.getDistanceToSquared(lie::math::Vector3f::Origin)));

            lie::math::Vector3f vec4 = lie::math::Vector3f::Origin;
            vec3Group->add(new lie::test::Comment("vec4 = Origin"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 0.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 0.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 0.0f, vec4.z));

            lie::math::Vector3f vec5(15.0, 16.0, 17.0);
            vec3Group->add(new lie::test::Comment("vec4 = vec5(15,16,17)"));
            vec4 = vec5;
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 15.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 16.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 17.0f, vec4.z));

            vec4 -= vec4;
            vec3Group->add(new lie::test::Comment("vec4 -= vec4"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.vec3Group->add(new lie::test::ValueTest<float>x", 0.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 0.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 0.0f, vec4.z));

            vec4 = vec1 + vec2 + vec3;
            vec3Group->add(new lie::test::Comment("vec4 = vec1 + vec2 + vec3"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 5.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 5.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 5.0f, vec4.z));

            vec4 += vec4;
            vec3Group->add(new lie::test::Comment("vec4 += vec4"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 10.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 10.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 10.0f, vec4.z));


            vec4 -= vec1;
            vec3Group->add(new lie::test::Comment("vec4 -= vec1"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 5.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 10.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 10.0f, vec4.z));

            vec4 -= (vec2+vec3)*2;
            vec3Group->add(new lie::test::Comment("vec4 -= (vec2+vec3)*2"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 5.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 0.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 0.0f, vec4.z));

            vec4.x *= 0;
            vec3Group->add(new lie::test::Comment("vec4.x *= 0"));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.x", 0.0f, vec4.x));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.y", 0.0f, vec4.y));
            vec3Group->add(new lie::test::ValueTest<float>("vec4.z", 0.0f, vec4.z));

            vec3Group->add(new lie::test::Comment("op=="));
            vec3Group->add(new lie::test::ValueTest<float>("vec4 == Origin", true, vec4 == lie::math::Vector3f::Origin));
            vec3Group->add(new lie::test::ValueTest<float>("vec4 != Origin", false, vec4 != lie::math::Vector3f::Origin));

        }
    }
}

#endif // LIE_MATH_VECOR3_TEST_H
