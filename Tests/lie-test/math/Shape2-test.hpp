#ifndef LIE_MATH_SHAPE2_TEST_HPP
#define LIE_MATH_SHAPE2_TEST_HPP

#include <lie/math\Shape2.hpp>
#include <lie/test.hpp>
#include <lie/core\ExcepOutOfRange.hpp>

namespace lie{
    namespace math{

        void Test_Shape2(lie::test::TestGroup* group){
			lie::test::TestGroup* shapeGroup = group->createSubgroup("Shape2");

            lie::math::Shape2<float> shape1(3);
            shapeGroup->add(new lie::test::Comment("Made new shape with 3 verticies, checking default data"));
            shapeGroup->add(new lie::test::ValueTest<unsigned int>("size", 3, shape1.size()));
            shapeGroup->add(new lie::test::ValueTest<float>("op[0].x", 0, shape1[0].x));
            shapeGroup->add(new lie::test::ValueTest<float>("op[0].y", 0, shape1[0].y));
            shapeGroup->add(new lie::test::ValueTest<float>("op[1].x", 0, shape1[1].x));
            shapeGroup->add(new lie::test::ValueTest<float>("op[1].y", 0, shape1[1].y));
            shapeGroup->add(new lie::test::ValueTest<float>("op[2].x", 0, shape1[2].x));
            shapeGroup->add(new lie::test::ValueTest<float>("op[2].y", 0, shape1[2].y));

            shapeGroup->add(new lie::test::ValueTest<float>("at(0).x", 0, shape1.at(0).x));
            shapeGroup->add(new lie::test::ValueTest<float>("at(0).y", 0, shape1.at(0).y));
            shapeGroup->add(new lie::test::ValueTest<float>("at(1).x", 0, shape1.at(1).x));
            shapeGroup->add(new lie::test::ValueTest<float>("at(1).y", 0, shape1.at(1).y));
            shapeGroup->add(new lie::test::ValueTest<float>("at(2).x", 0, shape1.at(2).x));
            shapeGroup->add(new lie::test::ValueTest<float>("at(2).y", 0, shape1.at(2).y));

            shapeGroup->add(new lie::test::Comment("Making unit square with constructor taking array"));
            Vector2i unitSquareData[] = {lie::math::Vector2i(0,0), lie::math::Vector2i(0,1), lie::math::Vector2i(1,0), lie::math::Vector2i(1,1)};
            lie::math::Shape2<int> shapeUSq(4, unitSquareData);
            shapeGroup->add(new lie::test::ValueTest<int>("op[0].x", 0, shapeUSq[0].x));
            shapeGroup->add(new lie::test::ValueTest<int>("op[0].y", 0, shapeUSq[0].y));
            shapeGroup->add(new lie::test::ValueTest<int>("op[1].x", 0, shapeUSq[1].x));
            shapeGroup->add(new lie::test::ValueTest<int>("op[1].y", 1, shapeUSq[1].y));
            shapeGroup->add(new lie::test::ValueTest<int>("op[2].x", 1, shapeUSq[2].x));
            shapeGroup->add(new lie::test::ValueTest<int>("op[2].y", 0, shapeUSq[2].y));
            shapeGroup->add(new lie::test::ValueTest<int>("op[3].x", 1, shapeUSq[3].x));
            shapeGroup->add(new lie::test::ValueTest<int>("op[3].y", 1, shapeUSq[3].y));

            shapeGroup->add(new lie::test::Comment("Testing brace initializer list constructor"));
            lie::math::Shape2<double> shape2({  lie::math::Vector2<double>(0.0, 0.0),
                                                lie::math::Vector2<double>(0.0, 1.8),
                                                lie::math::Vector2<double>(1.3, 0.0),
                                                lie::math::Vector2<double>(1.9, 1.5)});

            shapeGroup->add(new lie::test::ValueTest<double>("op[0].x", 0.0, shape2[0].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[0].y", 0.0, shape2[0].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[1].x", 0.0, shape2[1].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[1].y", 1.8, shape2[1].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[2].x", 1.3, shape2[2].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[2].y", 0.0, shape2[2].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[3].x", 1.9, shape2[3].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[3].y", 1.5, shape2[3].y));

            shapeGroup->add(new lie::test::Comment("Testing copy constructor - shape2double -> shape2double"));
            lie::math::Shape2<double> shape3(shape2);
            shapeGroup->add(new lie::test::ValueTest<double>("op[0].x", 0.0, shape3[0].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[0].y", 0.0, shape3[0].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[1].x", 0.0, shape3[1].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[1].y", 1.8, shape3[1].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[2].x", 1.3, shape3[2].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[2].y", 0.0, shape3[2].y));
            shapeGroup->add(new lie::test::ValueTest<double>("op[3].x", 1.9, shape3[3].x));
            shapeGroup->add(new lie::test::ValueTest<double>("op[3].y", 1.5, shape3[3].y));

            shapeGroup->add(new lie::test::Comment("operator== and operator!="));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape3 == shape2", true, shape3 == shape2));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape2 == shape3", true, shape2 == shape3));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape3 != shape2", false, shape3 != shape2));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape2 != shape3", false, shape2 != shape3));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape3 == shape3", true, shape3 == shape3));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape3 != shape3", false, shape3 != shape3));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape1 == shape3", false, shape1 == shape3));
            shapeGroup->add(new lie::test::ValueTest<bool>("shape1 != shape3", true, shape1 != shape3));

            shapeGroup->add(new lie::test::Comment("Ensuring deep copy was made"));
            shape3[1] = lie::math::Vector2<double>(3.1, 3.2);
            shape2[2] = lie::math::Vector2<double>(2.1, 2.2);
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[0].x", 0.0, shape2[0].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[0].x", 0.0, shape2[0].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[1].x", 0.0, shape2[1].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[1].y", 1.8, shape2[1].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[2].x", 2.1, shape2[2].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[2].y", 2.2, shape2[2].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[3].x", 1.9, shape2[3].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape2 op[3].y", 1.5, shape2[3].y));

            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[0].x", 0.0, shape3[0].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[0].y", 0.0, shape3[0].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[1].x", 3.1, shape3[1].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[1].y", 3.2, shape3[1].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[2].x", 1.3, shape3[2].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[2].y", 0.0, shape3[2].y));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[3].x", 1.9, shape3[3].x));
            shapeGroup->add(new lie::test::ValueTest<double>("shape3 op[3].y", 1.5, shape3[3].y));

            shapeGroup->add(new lie::test::Comment("Testing copy constructor - shape2double -> shape2int"));
            lie::math::Shape2<int> shape4(shape3);
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[0].x", 0, shape4[0].x));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[0].y", 0, shape4[0].y));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[1].x", 3, shape4[1].x));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[1].y", 3, shape4[1].y));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[2].x", 1, shape4[2].x));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[2].y", 0, shape4[2].y));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[3].x", 1, shape4[3].x));
            shapeGroup->add(new lie::test::ValueTest<int>("shape4 op[3].y", 1, shape4[3].y));




        }


    }
}

#endif // LIE_MATH_SHAPE2_TEST_HPP
