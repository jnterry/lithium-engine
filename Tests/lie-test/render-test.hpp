#ifndef LIE_RENDER_TEST_HPP
#define LIE_RENDER_TEST_HPP

#include <lie/Test.hpp>
#include "render/MeshDataBuffer-test.hpp"
#include "render/Renderer2d-test.hpp"

namespace lie{
    void Test_render(lie::test::TestGroup* group){
        lie::test::TestGroup* renderGroup = group->createSubgroup("render");
        lie::rend::Test_MeshDataBuffer(renderGroup);
		lie::rend::Test_Renderer2d(renderGroup);

	}
}

#endif // LIE_RENDER_TEST_HPP
