//////////////////////////////////////////////////////////////////
//		      Part of Lithium Engine, by Jamie Terry            //
//////////////////////////////////////////////////////////////////
/// \file all.h
/// \brief
/// This file includes all module unit-test files which include all unit
/// tests for the lithium engine
/// Therefore the ENTIRE test suite can be run be simply calling
/// the funcion in this file, lie::Test_All();
///
/// \defgroup unit-tests Unit Tests
/// \brief Contains all the engine's unit tests. Not to be confused with the Testing module
/// which has classes for making, running and generating reports
//////////////////////////////////////////////////////////////////


#include <lie/Test.hpp>
#include "math-test.hpp"
#include "core-test.hpp"
#include "render-test.hpp"

namespace lie{
	void Test_All(lie::test::TestGroup* group){
        lie::test::TestGroup* lieGroup = new lie::test::TestGroup("Lithium Engine");
        group->add(lieGroup);

		lie::Test_core(lieGroup);
        lie::Test_math(lieGroup);
        lie::Test_render(lieGroup);

	}

}
