#ifndef LIE_MATH_TEST_H
#define LIE_MATH_TEST_H

#include <lie/Test.hpp>

#include "math\Vector2-test.hpp"
#include "math\Vector3-test.hpp"
#include "math\Quaternion3-test.hpp"
#include "math\Vector3Quat3-test.hpp"
#include "math\Matrix-test.hpp"
#include "math\SquareMatrix-test.hpp"
#include "math\Matrix3x3-test.hpp"
#include "math\Matrix4x4-test.hpp"
#include "math\Shape2-test.hpp"
#include "math\Aabb2-test.hpp"
#include "math\Angle-test.hpp"
#include "math\Transform2-test.hpp"


namespace lie{
    void Test_math(lie::test::TestGroup* group){
        lie::test::TestGroup* mathGroup = group->createSubgroup("math");

		lie::math::Test_Vec2(mathGroup);
		lie::math::Test_Vec3(mathGroup);
		lie::math::Test_Quaternion3(mathGroup);
		lie::math::Test_Vec3Quat3(mathGroup);
		lie::math::Test_Matrix(mathGroup);
		lie::math::Test_SquareMatrix(mathGroup);
		lie::math::Test_Matrix3x3(mathGroup);
		lie::math::Test_Matrix4x4(mathGroup);
		lie::math::Test_Shape2(mathGroup);
		lie::math::Test_Aabb2(mathGroup);
		lie::math::Test_Angle(mathGroup);
		lie::math::Test_Transform2(mathGroup);

	}

}


#endif // LIE_MATH_TEST_H
