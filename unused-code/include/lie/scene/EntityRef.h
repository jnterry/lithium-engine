#ifndef LIE_GAME_ENTITYREF_H
#define LIE_GAME_ENTITYREF_H


namespace lie{
    namespace scene{
        typedef unsigned int EntityId;

        //forward decleration
        class EntityScene;

        class EntityRef{
            friend class EntityScene;
            EntityId mId;
        private:



            //private constructor, avalible only to entity scene
            EntityRef(EntityId id){mId = id;}


        };
    }
}

#endif // LIE_GAME_ENTITYREF_H
