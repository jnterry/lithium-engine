#ifndef LIE_GAME_ENTITYSCENE_H
#define LIE_GAME_ENTITYSCENE_H

#include <vector>
#include <map>
#include <string>
#include <typeinfo>

#include <lie\scene\EntityProperty.h>
#include <lie\scene\EntityRef.h>
#include <lie\core\Logger.hpp>

namespace lie{
    namespace scene{

        class EntityScene{
        public:
            /////////////////////////////////////////////////
            /// \brief Default constructor
            /// Sets up internal data storage
            /// You MUST register all components you wish to use
            /// BEFORE creating any entity scene! This is because the entity
            /// scene can't set up its internal data structures until all component
            /// types are known
            /////////////////////////////////////////////////
            EntityScene(){
                msSceneCreated = true;
            }

            EntityRef createEntity(){
                //this means calling register property will from now on fail
                //dont bother branching, quicker to just make it equal true

                return EntityRef(0);

            }

            bool deleteEntity(EntityRef ent){return this->deleteEntity(ent.mId);}
            bool deleteEntity(EntityId ent);


            //c style entity modifiers
            //see EntityRef for oop wrapper

            /////////////////////////////////////////////////
            /// \brief Returns reference to the property of the specified type from the specified
            /// entity
            ///
            /// \param
            /// \param
            /// \return
            ///
            /////////////////////////////////////////////////

            template <typename T>
            T& getEntityProperty(EntityId target){

            }

            /////////////////////////////////////////////////
            /// \brief Creates new instance of specified property and adds it
            /// to specified entity (unless entity already owns instance of that property)
            /// \tparam T -> the type of property to create
            /// \param EntityId target -> the entity to add it to
            /// \return reference to created property, or , if entity already had that property,
            /// reference to the entities existing copy of that property
            /////////////////////////////////////////////////

            template <typename T>
            T& createEntityProperty(EntityId target){


            }

            /////////////////////////////////////////////////
            /// \brief Deletes a specified entity's instance of specified property type
            /// \tparam T -> the property type to delete
            /// \param  EntityId target -> the entity whose property you wish to delete
            /// \return bool -> true if entity had instance of specified property and it has
            /// been deleted, if entity did not have that property, returns false and leaves
            /// entity unchanged
            /////////////////////////////////////////////////

            template <typename T>
            bool deleteEntityProperty(EntityId target){
                return false;
            }

            /////////////////////////////////////////////////
            /// \brief Querries whether a specific entity has an instance of the
            /// property type specified
            /// \tparam T -> the type of property to querry
            /// \param EntityId target -> the Id of the entity you wish to querry
            /// \return true if entity "target" has instance of that property, else false
            /// will always return false if there is no existing entity with the id "target"
            /////////////////////////////////////////////////
            template <typename T>
            bool hasEntityGotProperty(EntityId target){
                return false;
            }


            /////////////////////////////////////////////////
            /// \brief Returns a reference to a vector containing all properties of the type requested
            /// Use this function in EntitySystems to update entities
            /// \tparam T -> the type of property to get
            /// \return referece to std::vector
            /////////////////////////////////////////////////
            template <typename T>
            std::vector<T>& getAllPropertiesOfType(){
                return this->mProperties[T::Id];
            }

            /////////////////////////////////////////////////
            /// \brief Registers a properry type for use with any EntityScene
            /// All properties MUST be registered before creating a scene!
            /// \tparam T -> the type of property to register
            /// \param std::string -> the name to use for the property
            /// (name is used for debugging and saving/loading entities, MUST be unique!
            /// changing a property name will cause old save files to be unloadable)
            /// \return true if porperty was succsessfully registered, false if not
            /// could fail due to a scene already being created or name being a duplicate
            ///
            /// Note: Registing properties is a pain, it could have been resolved at compile time
            /// HOWEVER registering them at program start up allows for modding/patch support
            /// properties can be defined in data files loaded by the engine instead of being defined
            /// in code, this is why they can't sort themselves out at compile time!
            /////////////////////////////////////////////////
            template <typename T>
            static bool registerPropertyType(std::string name){
                if(msSceneCreated){
                    //then an entity has been created in some system somewhere
                    //can't now change data structure and add new property type
                    LIE_DLOG_ERROR("lie::scene::EntityScene", "Attempted to register the property type \"" << name
                                 << "\", however a scene has already been created. Properties must be registered before any scene is made!");
                    return false;
                }


                //count number of elements with key of typeid(T)
                //if it is not 0, that property type is already registered!
                if(msPropertyTypeToIdMapper.count(&typeid(T)) != 0){
                    LIE_DLOG_WARN("lie::scene::EntityScene", "Attempted to register the property type \"" << name
                                 << "\", however it has already been registered!");
                    return false;
                } else {
                    //add this prop type to the map
                    T::Id = msPropertyTypeToIdMapper.size();
                    msPropertyTypeToIdMapper[&typeid(T)] = std::pair<std::string, EntityPropertyId>(name, T::Id);

                    //return true
                    return true;
                }
            }
        private:
            //Maps an EntityId (used externally) to the index of the mEntsPropertyList
            //vector where the properties the entity owns are listed
            std::vector<EntityId> mIdToIndexMapper;


            //Vector of vector, otuer vector index is entity index, inner vector contains components in entity
            std::vector< std::vector<EntityId> > mEntityPropertyLinker;

            //pointer to array of vectors, array index is property type, contents of vector at that index is all
            //instances of that property in this array;
            std::vector<EntityProperty>* mProperties;

            //stores map of property types to a pair of the prop types name (string) and PropId
            static std::map<const std::type_info*, std::pair<std::string,EntityPropertyId> > msPropertyTypeToIdMapper;

            //records if an enetiy has been made yet, if so, don't allow "registerPropertyType()" to do anything!!!
            static bool msSceneCreated;
        };
    }
}

#endif // LIE_GAME_ENTITYSCENE_H
