#ifndef LIE_CORE_GENERICTREE_TEST_HPP
#define LIE_CORE_GENERICTREE_TEST_HPP

#include <lie/Test.hpp>
#include <lie/core\GenericTree.hpp>

namespace lie{
    namespace core{
        void Test_GenericTree(lie::test::TestGroup* group){
			lie::test::TestGroup* treeGroup = group->createSubgroup("GenericTree");
			lie::test::TestGroup* intTreeGroup = treeGroup->createSubgroup("Int Tree Tests");

            lie::core::GenericTree<int> intRoot(1);
            auto intNode1 = intRoot.add(2);
            auto intNode2 = intNode1->add(3);
            intNode1->add(4);
            auto intNode3 = intRoot.add(5);

            intTreeGroup->add(new lie::test::ValueTest<bool>("root is root", true, intRoot.isRootNode()));
            intTreeGroup->add(new lie::test::ValueTest<GenericTree<int>*>("root parent", (GenericTree<int>*)nullptr, intRoot.getParent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root depth", (unsigned int)0, intRoot.getDepth()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root contents", 1, intRoot.getContent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root decendent count", (unsigned int)4, intRoot.getDecendentCount()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root child count", (unsigned int)2, intRoot.getChildCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(2)", true, intRoot.hasChild(2)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(3)", false, intRoot.hasChild(3)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(5)", true, intRoot.hasChild(5)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(9)", true, intRoot.hasChild(5)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root getChild(2) == intNode2", true, intRoot.getChild(2) == intNode1));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root getChild(100) == root.end()", true, intRoot.getChild(100) == intRoot.end()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root getChild(100) == root.begin()", false, intRoot.getChild(100) == intRoot.begin()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root getChild(100) == intNode2", false, intRoot.getChild(100) == intNode2));

            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 is root", false, intNode1->isRootNode()));
            intTreeGroup->add(new lie::test::ValueTest<GenericTree<int>*>("node1 parent", &intRoot, intNode1->getParent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node1 depth", (unsigned int)1, intNode1->getDepth()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 content", 2, intNode1->getContent()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 decendent count", (unsigned int)2, intNode1->getDecendentCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 child count", (unsigned int)2, intNode1->getChildCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(3)", true, intNode1->hasChild(3)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(4)", true, intNode1->hasChild(4)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(5)", false, intNode1->hasChild(5)));

            intTreeGroup->add(new lie::test::ValueTest<bool>("node2 is root", false, intNode2->isRootNode()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node2 depth", (unsigned int)2, intNode2->getDepth()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node2 content", 3, intNode2->getContent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node2 decendent count", (unsigned int)0, intNode2->getDecendentCount()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node2 child count", (unsigned int)0, intNode2->getChildCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node2 hasChild(10)", false, intNode2->hasChild(10)));

            intTreeGroup->add(new lie::test::ValueTest<bool>("node3 is root", false, intNode3->isRootNode()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node3 depth", (unsigned int)1, intNode3->getDepth()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node3 content", 5, intNode3->getContent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node3 decendent count", (unsigned int)0, intNode3->getDecendentCount()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("node3 child count", (unsigned int)0, intNode3->getChildCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("node3 hasChild(10)", false, intNode3->hasChild(10)));


            lie::core::GenericTree<int> intRoot2(6);
            intRoot2.add(8);
            intRoot2.add(9);
            intTreeGroup->add(new lie::test::ValueTest<GenericTree<int>*>("root2 parent", (GenericTree<int>*)nullptr, intRoot2.getParent()));

            intRoot.add(intRoot2);
            intTreeGroup->add(new lie::test::ValueTest<GenericTree<int>*>("root2 parent", &intRoot, intRoot2.getParent()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root is root", true, intRoot.isRootNode()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root depth", (unsigned int)0, intRoot.getDepth()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root contents", 1, intRoot.getContent()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root decendent count", (unsigned int)7, intRoot.getDecendentCount()));
            intTreeGroup->add(new lie::test::ValueTest<unsigned int>("root child count", (unsigned int)3, intRoot.getChildCount()));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(2)", true, intRoot.hasChild(2)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(3)", false, intRoot.hasChild(3)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(5)", true, intRoot.hasChild(5)));
            intTreeGroup->add(new lie::test::ValueTest<bool>("root hasChild(6)", true, intRoot.hasChild(6)));

            lie::core::GenericTree<int> intRoot3(2);
            intRoot3.add(3)->add(7);
            intRoot3.add(12);
            intRoot3.add(10)->add(11);


            intRoot.merge(intRoot3);

            lie::test::TestGroup* treeMergeGroup = intTreeGroup->createSubgroup("Testing Merge");

            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("root decendent count", (unsigned int)11, intRoot.getDecendentCount()));
            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("root child count", (unsigned int)3, intRoot.getChildCount()));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("root hasChild(2)", true, intRoot.hasChild(2)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("root hasChild(3)", false, intRoot.hasChild(3)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("root hasChild(5)", true, intRoot.hasChild(5)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("root hasChild(6)", true, intRoot.hasChild(6)));

            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 is root", false, intNode1->isRootNode()));
            treeMergeGroup->add(new lie::test::ValueTest<GenericTree<int>*>("node1 parent", &intRoot, intNode1->getParent()));
            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("node1 depth", (unsigned int)1, intNode1->getDepth()));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 content", 2, intNode1->getContent()));
            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("node1 decendent count", (unsigned int)6, intNode1->getDecendentCount()));
            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("node1 child count", (unsigned int)4, intNode1->getChildCount()));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(3)", true, intNode1->hasChild(3)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(4)", true, intNode1->hasChild(4)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(5)", false, intNode1->hasChild(5)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(10)", true, intNode1->hasChild(10)));
            treeMergeGroup->add(new lie::test::ValueTest<bool>("node1 hasChild(12)", true, intNode1->hasChild(12)));
            treeMergeGroup->add(new lie::test::ValueTest<unsigned int>("node1 getChild(10)->getChildCount()", (unsigned int)1, intNode1->getChild(10)->getChildCount()));

        }
    }
}

#endif // LIE_CORE_GENERICTREE_TEST_HPP
