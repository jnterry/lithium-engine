#ifndef LIE_CORE_ALLOCATOR_H
#define LIE_CORE_ALLOCATOR_H

#include <lie\core\Handle.hpp>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Policy based memory allocator, all dynamically allocated memory in lie should be allocated through one of these
        ///
        /// \tparam T_ALLOCTYPE Defines the type of allocator this memory arena uses
        ///
        /// \tparam T_THREAD Defines the thread locking primative that should be used by this allocator when allocating or freeing memory
        ///
        /// \tparam T_DEALLOCMARKER Defines what to do to bytes that are deallocated -> leave as they are, fill with magic number, set to 0, etc
        ///
        /// \tparam T_BYTEGUARDER Defines the method in which the allocator should guard its bytes, this is normally done by adding a padding byte(s)
        /// after each allocated region and filling it with a magic value, then upon deallocating it is checked to ensure the value has not been modified
        ///
        ///
        ///
        /// Memory will be structured as follows:
        ///     - (Total Number of Bytes Allocated)
        ///         - Added by T_ALLOCTYPE -> most allocator types will need to store this, some (eg: pool allocators) will not
        ///     - (Guard Bytes)
        ///         - T_GUARDBYTES -> some byte guarders will only add bytes at the start, some will only add at the end, others at both and some at neither
        ///     - Data
        ///         - The actual data being stored
        ///     - (Guard Bytes)
        ///         - Again, may or may not be present
        ///
        ///
        /////////////////////////////////////////////////
        /*template <typename T_ALLOCTYPE, typename T_THREAD, typename T_DEALLOCMARKER, typename T_BYTEGUARDER, typename T_TRACKER>
        class Allocator{
        public:
            Allocator(T_ALLOCTYPE allocer, T_THREAD threadPrim, T_DEALLOCMARKER deallocMarker, T_BYTEGUARDER byteGuarder, T_TRACKER memTracker){
                this->mAllocer = allocer;
                this->mThreadPrim = threadPrim;
                this->mDeallocMarker = deallocMarker;
                this->mByteGuarder = byteGuarder;
                this->mTracker = memTracker;
            }

            /////////////////////////////////////////////////
            /// \brief Allocates a new region of raw uninitiallised memory of the specified
            /// size and with the specified alignment for the offset pointer
            /// Eg:
            /// \return void* pointing to the allocated memory
            /////////////////////////////////////////////////
            void* allocate(size_t datasize, size_t align, size_t offset){
                //Adjust the size value so it can also store the guard bytes
                //(the T_ALLOCTYPE will handle adding even more if it needs to store the size of the data being allocated)
                int allocsize = datasize + T_BYTEGUARDER::TOTAL_GUARD_SIZE;

                //adjust offset so it points at the data being allocated, eg if guard size is 1 byte, then the data starts
                //1 byte into the allocated data
                //eg: allocating 2 bytes, allign 0, size 0 with guard byte on either end
                //data will be: guard data data guard, therefore the data starts on the second byte
                 //(the T_ALLOCTYPE will handle adding even more if it needs to store the size of the data being allocated)
                offset += T_BYTEGUARDER::FRONT_GUARD_SIZE;

                 this->mThreadPrim.enter(); //enter critical section/mutex/nothing/etc

                //actually do the allocation with new size and offset, will return pointer to the data dictated by "offset"
                void* result = this->mAllocer.allocate(allocsize, align, offset);

                this->mByteGuarder.addGuards(result, datasize); //add guard values to the allocated memory
                this->mTracker.add(result, T_BYTEGUARDER::FRONT_GUARD_SIZE, T_BYTEGUARDER::BACK_GUARD_SIZE, datasize);//start tracking the newly allocated memory

                this->mThreadPrim.leave();//leave the critical section

                return result;
            }

            /////////////////////////////////////////////////
            /// \brief deallocates a chunck of memory that is pointed to by the passed in pointer.
            /// Note, the memory MUST have been allocated from the Allocator that this method is
            /// called on
            /// \return void
            /////////////////////////////////////////////////
            void deallocate(void* mem){
                this->mThreadPrim.enter();
                int size = this->mAllocer.getSize(mem, T_BYTEGUARDER::FRONT_GUARD_SIZE);
                this->mByteGuarder.checkGuards(mem, size);
                this->mTracker.remove(mem);
                this->mAllocer.deallocate(mem-=T_BYTEGUARDER::FRONT_GUARD_SIZE, size+=T_BYTEGUARDER::TOTAL_GUARD_SIZE);
                this->mThreadPrim.leave();
            }

            /////////////////////////////////////////////////
            /// \brief Returns the data size of the region pointed to
            /// does not include any guard bytes, bytes to store allocated size, etc
            /// \return size_t -> size in bytes
            /////////////////////////////////////////////////
            size_t getSize(void* mem){
                return this->mAllocer.getSize(mem, T_BYTEGUARDER::FRONT_GUARD_SIZE);
            }

            /////////////////////////////////////////////////
            /// \brief Uses this allocators "allocate" function to retrieve memory and
            /// then constructors an object of the specified type in that location using
            /// the passed in parameters for the constructor
            /// \return pointer to the new object
            /////////////////////////////////////////////////
            template <typename T, typename... argType>
            Handle<T> create(argType... args){
                //allocate new raw region of memory that has the size and alignment of type T using this.allocate()
                //then use placment new operator to create a new instance of type T in the memory region
                //finally, give the arguments passed in to the constructor of the type
                return new (this->allocate(sizeof(T), alignof(T), 0)) T(args...);
            }

            /////////////////////////////////////////////////
            /// \brief correctly deconstructs the passed in object and then deallocates
            /// the memory it used
            /////////////////////////////////////////////////
            template <typename T>
            void destroy(T* obj){
                if(obj){ //check pointer is valid
                    obj->~T(); //if so, explicitly call the destructor
                    this->deallocate(obj);//and then deallocate the memory
                }
            }

        private:
            T_ALLOCTYPE mAllocer;
            T_THREAD mThreadPrim;
            T_DEALLOCMARKER mDeallocMarker;
            T_BYTEGUARDER mByteGuarder;
            T_TRACKER mTracker;
        };*/
    }//end of core::
}//end of lie::

#endif // LIE_CORE_ALLOCATOR_H
