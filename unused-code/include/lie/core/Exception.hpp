#ifndef LIE_CORE_EXCEPTION_HPP
#define LIE_CORE_EXCEPTION_HPP

#include <string>

namespace lie{
    namespace core{

        class Exception{
        public:
            /////////////////////////////////////////////////
            /// \brief Virtual function to be overidden by base classes
            /// returns the type of exception (class name)
            /////////////////////////////////////////////////
            virtual std::string type() const {return "Exception";}
        private:
            const std::string mMsg;
            const std::string mFile;
            const unsigned int mLine;
        };

    }
}

#endif // LIE_CORE_EXCEPTION_HPP
