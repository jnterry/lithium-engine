#ifndef LIE_CORE_HANDLE_H
#define LIE_CORE_HANDLE_H

#include <type_traits>

namespace lie{
    namespace core{
    /////////////////////////////////////////////////
    /// \brief The handle class must be used instead of pointers for any object allocated by a memory arena
    /// (memory arenas will automaticly return handles)
    ///
    /// The handle class is a replacement for normal pointers that give
    /// more power to the allocators.
    /// Some allocators will need to internally rearange thier memory and the objects
    /// they have allocated to prevent fragmentation, however this can not be done if
    /// client code knows the exact location in memory of the objects allocated, as any pointers
    /// to that exact location in memory will be invalidated if the object is moved
    ///
    /// One way to solve this is to have a large array containing pointers to all the heap allocated
    /// objects in the program, then the clients store the index of the array the pointer to their
    /// object is in. This has a number of problems:
    /// 1) The array must be big enough to hold a pointer to every heap allocated object in the program
    ///   -> if we make it very big it will waste memory and worse not fit in the cache
    ///   -> if we make it too small we will not be able to allocate more than X objects on the heap
    ///   -> if we make it dynamically resizable we will waste time dealing with the resizing
    ///
    /// Therefore the handle class is used instead
    /// It provides a common interface to simple pointers, pointer to pointers or arrays of pointers
    ///
    /// Allocators that need to rearange data can return handles that are effectivly pointers to pointers
    /// When moving data the allocator can update the second pointer level and all handles will continue to be valid
    /// Moreover, the allocator manages how it stores the pointers so it can do so in a huge array whose size is determined at compile time,
    /// store pointers as headers to the actual objects, allocate new pointer objects when needed, etc
    ///
    /// However, allocators that never rearange their data can instead return handles that are simple pointers
    /// these will not use the extra memory that pointer to pointers will, nor will it suffer the increased indirection and possible cache
    /// misses of pointer to pointers
    ///
    /// This is achieved by template speciallisation which means there is no runtime overhead to having a common interface that an abstract base
    /// handle would have
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////
    /// \brief The basic handle class, acts as simple pointer
    /// Single layer of indirection
    /////////////////////////////////////////////////
    /*template <typename T>
    class Handle{
    public:
        typedef T TYPE;
        T* operator->(){return obj;}
        T* operator&(){return obj;}
    private:
        T* obj;
    };

    /////////////////////////////////////////////////
    /// \brief Pointer speciallisation of the handle class, acts as a pointer to pointer
    /// allowing allocators to rearange their data
    /////////////////////////////////////////////////
    template <typename T>
    class Handle<T *>{
    public:
        typedef T* TYPE;
        T* operator->(){return *obj;};
        T* operator&(){return *obj;}
    private:
        T** obj;
    };*/

    /////////////////////////////////////////////////
    /// \brief The handle class is a pointer to pointer which allows memory allocators
    /// to rearange their memory to avoid fragmentation
    /////////////////////////////////////////////////
    template <typename T>
    class Handle{
    public:
        typedef T TYPE;
        T* operator->(){return *mObj;}
        T& operator*(){return **mObj;}
    private:
        T** mObj;
    };




    }
}


#endif // LIE_CORE_HANDLE_H
