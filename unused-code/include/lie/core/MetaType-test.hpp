#ifndef LIE_CORE_METATYPE_TEST_HPP
#define LIE_CORE_METATYPE_TEST_HPP

#include <lie\core\UnitTest.hpp>
#include <lie\core\Convert.hpp>

namespace lie{
    namespace core{
        void Test_MetaType(){
            lie::core::UnitTestClass utg("MetaType");

            MetaType* intType = new MetaType("int", 0, nullptr, sizeof(int),
                MetaType::Flags::Signed | MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem);

            MetaType* ucharType = new MetaType("uchar", 0, nullptr, sizeof(unsigned char),
                MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem);


            MetaTypeField randTypeFields[2] = {MetaTypeField(QualifiedMetaType(intType), "intField",0), MetaTypeField(QualifiedMetaType(ucharType), "ucharField",4)};
            MetaType* randType = new MetaType("rand", 2, randTypeFields, 5, MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::StaticMem);

            MetaTypeField rand2TypeFields[1] = {MetaTypeField(QualifiedMetaType(intType,1) ,"intPtr",0)};
            MetaType* rand2Type = new MetaType("rand2", 1, rand2TypeFields, 4);



            lie::core::UnitTest::setResultGroup("manual MetaType creation int, reading params");
            lie::core::UnitTest::addTest("intType->getName()", std::string("int"), intType->getName());
            lie::core::UnitTest::addTest("intType->getStaticSize()", sizeof(int), intType->getStaticSize());
            lie::core::UnitTest::addTest("intType->getFieldCount()", 0, intType->getFieldCount());
            lie::core::UnitTest::addTest("intType->is(MetaType::Primative)", true, intType->is(MetaType::Flags::Primative));
            lie::core::UnitTest::addTest("intType->is(MetaType::Signed)", true, intType->is(MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("intType->is(MetaType::Pod)", true, intType->is(MetaType::Flags::Pod));
            lie::core::UnitTest::addTest("intType->is(MetaType::Copyable)", true, intType->is(MetaType::Flags::Copyable));
            lie::core::UnitTest::addTest("intType->is(MetaType::Concrete)", true, intType->is(MetaType::Flags::Concrete));
            lie::core::UnitTest::addTest("intType->is(MetaType::StaticMem)", true, intType->is(MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("intType->is(Everything)", true, intType->is(MetaType::Flags::Signed | MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("intType->is(Primative | Signed)", true, intType->is(MetaType::Flags::Primative | MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("intType->is(Copyable | Pod)", true, intType->is(MetaType::Flags::Copyable | MetaType::Flags::Pod));

            lie::core::UnitTest::setResultGroup("manual MetaType creation uchar, reading params");
            lie::core::UnitTest::addTest("ucharType->getName()", std::string("uchar"), ucharType->getName());
            lie::core::UnitTest::addTest("ucharType->getStaticSize()", sizeof(unsigned char), ucharType->getStaticSize());
            lie::core::UnitTest::addTest("ucharType->getFieldCount()", 0, ucharType->getFieldCount());
            lie::core::UnitTest::addTest("ucharType->is(MetaType::Primative)", true, ucharType->is(MetaType::Flags::Primative));
            lie::core::UnitTest::addTest("ucharType->is(MetaType::Signed)", false, ucharType->is(MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("ucharType->is(MetaType::Pod)", true, ucharType->is(MetaType::Flags::Pod));
            lie::core::UnitTest::addTest("ucharType->is(MetaType::Copyable)", true, ucharType->is(MetaType::Flags::Copyable));
            lie::core::UnitTest::addTest("ucharType->is(MetaType::Concrete)", true, ucharType->is(MetaType::Flags::Concrete));
            lie::core::UnitTest::addTest("ucharType->is(MetaType::StaticMem)", true, ucharType->is(MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("ucharType->is(Everything)", false, ucharType->is(MetaType::Flags::Signed | MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("ucharType->is(Primative | Signed)", false, ucharType->is(MetaType::Flags::Primative | MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("ucharType->is(Copyable | Pod)", true, ucharType->is(MetaType::Flags::Copyable | MetaType::Flags::Pod));

            lie::core::UnitTest::setResultGroup("manual MetaType creation rand, reading params");
            lie::core::UnitTest::addTest("randType->getName()", std::string("rand"), randType->getName());
            lie::core::UnitTest::addTest("randType->getStaticSize()", 5, randType->getStaticSize());
            lie::core::UnitTest::addTest("randType->getFieldCount()", 2, randType->getFieldCount());
            lie::core::UnitTest::addTest("randType->is(MetaType::Primative)", false, randType->is(MetaType::Flags::Primative));
            lie::core::UnitTest::addTest("randType->is(MetaType::Signed)", false, randType->is(MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("randType->is(MetaType::Pod)", true, randType->is(MetaType::Flags::Pod));
            lie::core::UnitTest::addTest("randType->is(MetaType::Copyable)", true, randType->is(MetaType::Flags::Copyable));
            lie::core::UnitTest::addTest("randType->is(MetaType::Concrete)", false, randType->is(MetaType::Flags::Concrete));
            lie::core::UnitTest::addTest("randType->is(MetaType::StaticMem)", true, randType->is(MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("randType->is(Everything)", false, randType->is(MetaType::Flags::Signed | MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("randType->is(Primative | Signed)", false, randType->is(MetaType::Flags::Primative | MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("randType->is(Copyable | Pod)", true, randType->is(MetaType::Flags::Copyable | MetaType::Flags::Pod));

            lie::core::UnitTest::setResultGroup("manual MetaType creation rand2, reading params");
            lie::core::UnitTest::addTest("rand2Type->getName()", std::string("rand2"), rand2Type->getName());
            lie::core::UnitTest::addTest("rand2Type->getStaticSize()", 4, rand2Type->getStaticSize());
            lie::core::UnitTest::addTest("rand2Type->getFieldCount()", 1, rand2Type->getFieldCount());
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::Primative)", false, rand2Type->is(MetaType::Flags::Primative));
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::Signed)", false, rand2Type->is(MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::Pod)", false, rand2Type->is(MetaType::Flags::Pod));
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::Copyable)", false, rand2Type->is(MetaType::Flags::Copyable));
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::Concrete)", false, rand2Type->is(MetaType::Flags::Concrete));
            lie::core::UnitTest::addTest("rand2Type->is(MetaType::StaticMem)", false, rand2Type->is(MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("rand2Type->is(Everything)", false, rand2Type->is(MetaType::Flags::Signed | MetaType::Flags::Primative | MetaType::Flags::Pod | MetaType::Flags::Copyable | MetaType::Flags::Concrete | MetaType::Flags::StaticMem));
            lie::core::UnitTest::addTest("rand2Type->is(Primative | Signed)", false, rand2Type->is(MetaType::Flags::Primative | MetaType::Flags::Signed));
            lie::core::UnitTest::addTest("rand2Type->is(Copyable | Pod)", false, rand2Type->is(MetaType::Flags::Copyable | MetaType::Flags::Pod));

            delete intType;
            delete ucharType;
            delete randType;
            delete rand2Type;
        }
    }
}

#endif // LIE_CORE_METATYPE_TEST_HPP
