#ifndef LIE_CORE_ALLOCATORMANAGED_HPP
#define LIE_CORE_ALLOCATORMANAGED_HPP

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief compile time configurable object which manages useage of underlying allocator
        ///
        ///
        /// \param
        /// \param
        /// \return
        ///
        /////////////////////////////////////////////////

        class AllocatorManaged{

        };

    }
}


#endif // LIE_CORE_ALLOCATORMANAGED_HPP
