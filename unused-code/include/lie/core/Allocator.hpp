/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Allocator.hpp
///
///
/// \addtogroup core
/////////////////////////////////////////////////
#ifndef LIE_ALLOCATOR_HPP
#define LIE_ALLOCATOR_HPP

#include <lie\core\Handle.hpp>

namespace lie{
    namespace core{
        /////////////////////////////////////////////////
        /// \brief Virtual allocator class specifing the interface for all allocators
        /// in lithium engine
        /// Also provides helper functions for creating and destroying actual types rather than
        /// just raw chucks of memory
        /////////////////////////////////////////////////
        class Allocator{
        public:
            /////////////////////////////////////////////////
            /// \brief Returns raw chunck of memory with specified size, alignment and offset
            /// returns nullptr if allocation failed
            ///
            /// \param size The size in bytes of the block to allocate
            ///
            /// \param alignment Specifies which byte the first byte of the data should be aligned to,
            /// addressOfFirstByte % alignment == 0
            /// May be ignored by some allocators, eg the pool allocator as each region in the pool is already aligned
            ///
            /// \return void* to the allocated region, or nullptr if the allocation failed
            /////////////////////////////////////////////////
            virtual void* allocate(size_t size, char alignment = 4) = 0;

            /////////////////////////////////////////////////
            /// \brief Deallocated the chuck of memory pointed to by the passed in ptr
            /// NOTE: the memory pointed to MUST have  been allocated by THIS allocator
            /// \return void
            /////////////////////////////////////////////////
            virtual void deallocate(void* mem) = 0;

            /////////////////////////////////////////////////
            /// \brief Gets the size of the pointed at data block
            ///
            /// Will return actual size of the data region, not size of the object in it
            /// Eg, if we allocate a 4 byte structure in a pool allocator which allocated blocks of
            /// 8 bytes, then this function would return 8, not 4
            ///
            /// Does not include any extra bookeeping data added by the allocator
            /// Eg: some allocators store the size of the region in the byte before the memory
            /// allocating a 4 byte object would look like this:
            /// bookkeping | data | data | data | data
            /// this function would return 4 in this instance, not 5
            /////////////////////////////////////////////////
            virtual size_t getSize(void* mem) = 0;

            /////////////////////////////////////////////////
            /// \brief Returns the total amount of RAM in bytes that is in use by the allocator
            /// Eg: A fixed 1024 byte allocator that has a single 8 byte structure in it would return 1024
            /////////////////////////////////////////////////
            //virtual size_t getTotalPhysicalSize() = 0;

            /////////////////////////////////////////////////
            /// \brief Returns the total amount of bytes in the allocator that are currently in use for
            ///storing data or bookeeping data
            /// Eg: A fixed 1204 byte allocator that has a single 8 byte structure in it would return 8
            /////////////////////////////////////////////////
            //virtual size_t getTotalAllocatedSize() = 0;

            virtual ~Allocator() = 0;

            /////////////////////////////////////////////////
            /// \brief Uses this allocators "allocate" function to retrieve memory and
            /// then constructors an object of the specified type in that location using
            /// the passed in parameters for the constructor
            /// \return pointer to the new object
            /////////////////////////////////////////////////
            template <typename T, typename... argType>
            Handle<T> create(argType... args){
                //allocate new raw region of memory that has the size and alignment of type T using this.allocate()
                //then use placment new operator to create a new instance of type T in the memory region
                //finally, give the arguments passed in to the constructor of the type
                return new (this->allocate(sizeof(T), alignof(T))) T(args...);
            }

            /////////////////////////////////////////////////
            /// \brief correctly deconstructs the passed in object and then deallocates
            /// the memory it used
            /// Note: the object passed in MUST have been create()-ed by THIS allocator
            /////////////////////////////////////////////////
            template <typename T>
            void destroy(T* obj){
                if(obj){ //check pointer is valid
                    obj->~T(); //if so, explicitly call the destructor
                    this->deallocate(obj);//and then deallocate the memory
                }
            }
        };

    }
}

#endif // LIE_ALLOCATORMALLOC_HPP
