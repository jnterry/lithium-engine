#ifndef LIE_ALLOCATORHEAP_HPP
#define LIE_ALLOCATORHEAP_HPP

#include<stddef.h>

#include <lie\core\Allocator.hpp>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Simplest type of allocator which simply forwards its calls
        /// to standard functions
        /// Will only run out of memory when the system runs out, will not deal with fragmentation etc
        /////////////////////////////////////////////////
        class AllocatorHeap : public Allocator{
        public:
            void* allocate(size_t size, char alignment = 4);
            void deallocate(void* mem);
            size_t getSize(void* mem);
        };
    }
}

#endif // LIE_ALLOCATORMALLOC_HPP
