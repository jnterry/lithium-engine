/////////////////////////////////////////////////
/// \brief New c++11 class enums are great in the way they provide type saftey for enums, avoid name clashes, etc
/// however they cannot be used as bit flags without first defining | and & operators for them
/// This is alot of extra typing :(
/// Instead you can now use this macro
/// \code
/// namespace lie{
///     enum class BitFlags{
///         FlagA = 1,
///         FlagB = 2,
///         FlagC = 4,
///         FlagD = 8,
///         etc...
///     }
/// }
/// LIE_EC_BITFLAGMAKER(lie::BitFlags)
/// \endcode
///
/// \addtogroup core
/////////////////////////////////////////////////

#ifndef LIE_EC_BITFLAGMAKER

    #define LIE_EC_BITFLAGMAKER(ECname) \
    inline ECname operator|(ECname a, ECname b){\
        return static_cast<ECname>(static_cast<int>(a) | static_cast<int>(b));\
    }\
    inline ECname operator&(ECname a, ECname b){\
        return static_cast<ECname>(static_cast<int>(a) & static_cast<int>(b));\
    }\
    inline bool operator==(ECname a, ECname b){\
        return (static_cast<int>(a) == static_cast<int>(b));\
    }\
    inline bool operator!=(ECname a, ECname b){\
        return !operator==(a,b);\
    }\
    inline bool operator==(ECname a, int b){\
        (static_cast<int>(a) == b);\
    }\
    inline bool operator!=(ECname a, int b){\
        return !operator==(a,b);\
    }\
    inline ECname operator|=(ECname a, ECname b){\
        return operator|(a,b);\
    }\
    inline ECname operator&=(ECname a, ECname b){\
        return operator&(a,b);\
    }

#endif
