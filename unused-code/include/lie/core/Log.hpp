#ifndef LIE_CORE_LOG_HPP
#define LIE_CORE_LOG_HPP

#include <lie\core\RTimeMsg.hpp>
#include <functional>
namespace lie{
    namespace core{
        typedef std::function<bool(const RTimeMsg& msg)> RtmFilter;
        typedef std::function<std::string(const RTimeMsg& msg)> RtmFormatter;
        typedef std::function<void(std::string)> RtmWriter;

        /////////////////////////////////////////////////
        /// \brief Policy based LogDevice class
        /// A log device specifies the filter, formatting and output target of log messages
        /// They should  be attached to a lie::core::Log to be used
        ///
        /// \tparam MSGFILTER_T Specifies the filter that is applied when deciding if a message should be logged
        /// Expected interface:
        /// bool operator()(const RTimeMsg& msg) -> should return true if the msg is to be written
        ///
        /// \tparam MSGFORMATER_T Specifies how the RTimeMsg should be converted into a string to be logged
        /// Expected Interface:
        /// std::string operator()(const RTimeMsg& msg) -> should return an std::string which is to be written to the writer
        ///
        /// \tparam WRITER_T Specifies what should be done with the formated string
        /// Eg: written to cout, dialog box, file, over network, etc
        /// Expected Interface: <any-return-type> operator<<(string) -> this means you can directly pass in a ostream!
        /////////////////////////////////////////////////
        class Log{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs new instance of log class with passed in filter, formater and writer.
            /////////////////////////////////////////////////
            Log(RtmFilter* filter, RtmFormatter* formatter, RtmWriter* writer) : mFilter(filter), mFormatter(formatter), mWriter(writer){}

            void write(RTimeMsg msg) const {if(*mFilter(msg)){*mWriter(*mFormatter(msg));}}

        private:
            RtmFilter* mFilter;
            RtmFormatter* mFormatter;
            RtmWriter* mWriter;
        };

    }
}

#endif // LIE_CORE_LOG_HPP
