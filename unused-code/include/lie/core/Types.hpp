#ifndef LIE_CORE_TYPES_H
#define LIE_CORE_TYPES_H

#include <string>
#include <vector>
namespace lie{
    namespace core{
        namespace detail{
            //forward declerations
            template <typename T>
            class TypeDescriptorCreator;
        }

        class TypeDescriptor;

        /////////////////////////////////////////////////
        /// \brief Class which represents a field in a composite type, stores
        /// data about the field's name, offset into the type and the field's type
        /////////////////////////////////////////////////
        class Field{
        public:
            const std::string name;
            const unsigned int offset;
            const TypeDescriptor* type;
            Field(std::string newName, unsigned int newOffset, TypeDescriptor* newType) :
                name(newName), offset(newOffset), type(newType) {}
        };

        /////////////////////////////////////////////////
        /// \brief A TypeDescriptor describes a composite type (ie: a c++ class or struct)
        /// Made up of "fields" that have a specified name and type, field may be
        /// another TypeDescriptor allowing a class to contain instances of another (or the same)
        /// class
        /////////////////////////////////////////////////
        class TypeDescriptor{
        public:
            template <typename T>
            friend class detail::TypeDescriptorCreator;

            /////////////////////////////////////////////////
            /// \brief Returns the instance of TypeDesciptor for the specified type name
            /// This has runtime overhead and should be avoided where possible by using
            /// getByType<TYPE>() instead
            /// Returns nullptr if type does not exist
            /////////////////////////////////////////////////
            //static TypeDescriptor getByName(std::string name);

            /////////////////////////////////////////////////
            /// \brief Returns the instance of TypeDesciptior for the specified type
            /// This has very little (likely no) runtime overhead
            /// Note: If the type has not been defined an empty TypeDesciptor will be returned
            /////////////////////////////////////////////////
            template <typename T>
            static TypeDescriptor* getByType(){
                return detail::TypeDescriptorCreator<T>::get();
            }

            std::string getName() const{return mName;}
            unsigned int getFieldCount(){return mFields.size();}
             std::vector<Field> mFields;
        private:
            std::string mName;

            void addField(lie::core::Field field){
                mFields.push_back(field);
            }

        };

        /////////////////////////////////////////////////
        /// \brief Central repository for accsessing TypeDescriptors
        /// Can acsess by type using templates, this reduces runtime overhead
        /// Alternativly can get TypeDesciptors from a string containing the types name, this
        /// has higher runtime overheads but is can be used to achieve otherwise impossible things
        /////////////////////////////////////////////////
        /*class TypeDatabase{


        };



        /////////////////////////////////////////////////
        /// \brief As MetaTypes are immutable a TypeBuilder must be used
        /// to make a new type so that fields can be added in subsequent function
        /// calls
        /////////////////////////////////////////////////
        class TypeBuilder{
        public:
            TypeBuilder(std::string name);
            TypeBuilder& field();
            MetaType getMetaType();
        private:
            std::vector<Field> mFields;
            std::string mName;
        };*/



        namespace detail{
            /////////////////////////////////////////////////
            /// \brief Class that is to be used to generate TypeDescriptors and ensure only one is created
            /// for each conreate type
            /// Note: You should use the macros to define a type rather than directly calling methods of this class
            /////////////////////////////////////////////////
            template <typename T>
            class TypeDescriptorCreator{
            public:

                TypeDescriptorCreator(std::string name){
                    mTd.mName = name;
                    this->define();//call define to fill in the field  infomation
                }

                void addField(std::string name, unsigned int offset, lie::core::TypeDescriptor type){
                    mTd.addField(Field(name, offset, type));
                }

                static void define();

                typedef T Type;

                static TypeDescriptor* get(){
                    return &mTd;
                }
            private:
                static TypeDescriptor mTd;
            };



            //definition of the static member, prevents undefined references
            template<typename T>
            TypeDescriptor TypeDescriptorCreator<T>::mTd;
        }
    }
}

#define PASTE( _, __ )  _##__
#define GENERATE_LINE( _ ) PASTE( GENERATED_TOKEN_, _ )
#define GENERATE_FILE( _ ) PASTE( __FILE__, _ )
#define NAME_GENERATOR( ) GENERATE_FILE( __LINE__ )

#define LIE_DEFINE_TYPE(TYPE, BODY) \
LIE_DEFINE_TYPE_DATA(TYPE, BODY) \
LIE_MK_TYPE(TYPE)

#define LIE_DEFINE_TYPE_DATA(TYPE, BODY) \
namespace lie{\
    namespace core{\
        namespace detail{\
            template<> \
            void lie::core::detail::TypeDescriptorCreator<TYPE>::define(){\
                BODY\
            }\
        }\
    }\
}

#define LIE_MK_TYPE(TYPE) lie::core::detail::TypeDescriptorCreator<TYPE> tdesc_ ## TYPE (#TYPE);

#define LIE_ADD_FIELD(NAME) \
    addField(#NAME, ((unsigned long) &((Type*) 0)->NAME), lie::core::TypeDescriptor::getByType<decltype(NAME)>());

#endif // LIE_CORE_TYPES_H
