#ifndef LIE_CORE_MEMORY_H
#define LIE_CORE_MEMORY_H

namespace lie{
    namespace core{

        Allocator* RootAllocator;
        Allocator* TempAllocator;

        /*template <typename T_ROOTTYPE, T_TEMPTYPE>
        void initMemory(Allocator* RootAllocator){
            RootAllocator = (T_ROOTTYPE*)malloc(sizeof(T_ROOTTYPE));
            new (RootAllocator) T_ROOTTYPE();
        }*/

    }
}

#endif // LIE_CORE_MEMORY_H
