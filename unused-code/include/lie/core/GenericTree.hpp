#ifndef LIE_CORE_GENERICTREE_HPP
#define LIE_CORE_GENERICTREE_HPP

#include <unordered_map>
#include <vector>
#include <sstream>
#include "ExcepInvalidOperationInState.hpp"

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A GenericTree represents a tree structure in which a node can have any number of
        /// child nodes, each node has a "content" of type T. Each instance of GenericTree is a node
        /// which simerally to a container for its children.
        ///
        /// \note All children of a single node must have unique content, however the content does not need to be
        /// unique across an entire level of the tree, nor across the whole tree.
        /// Hence, the following tree is valid:
        /// \code
        ///        1
        ///      /   \
        ///     1     2
        ///   /  \   / \
        ///  1    2 1   2
        /// \endcode
        ///
        /// \note The order in which child nodes are added to a parent is not preserved and cannot be querried
        ///
        /// \tparam T must support: assignment operator (T varName = someOtherTInstance) and the comparision operator
        ///
        /// :TODO: MEMORY LEAKS!!! Who owns the nodes? Should be the parent, hence the parent should delete child
        /// nodes in its destructor!
        /////////////////////////////////////////////////
        template<typename T>
        class GenericTree{
            typedef typename std::unordered_map<T, GenericTree<T>* > ContainerType;
        public:
            /////////////////////////////////////////////////
            /// \brief A GenericTree iterator allow you to iterate through all the children of a tree node
            /// Supports both forward and reverse iteration
            /////////////////////////////////////////////////
            class iterator{
                friend class GenericTree<T>;
                //This simply wraps an iterator of the internal map, we don't want clients to see the map,
                //so we wrap it, also map iterators have a ->first and ->second, we only want to provide accesss
                //to the nodes, not the key in the map, hence we return ->second
            public:
                bool operator!=(iterator otherIt){
                    return otherIt.mIter != this->mIter;
                }
                bool operator==(iterator otherIt){
					return false;//:TODO:
                    // return otherIt.mIter == this->mIter;
                }
                void operator++(){
                    this->mIter++;
                }
                void operator--(){
                    this->mIter--;
                }
                GenericTree<T>* operator->(){
                    return (this->mIter->second);
                }
                GenericTree<T>& operator*(){
                    return *(this->mIter->second);
                }
            private:
                typedef typename ContainerType::iterator InternalIterType;
                InternalIterType  mIter;
            protected:
                iterator(InternalIterType iter) : mIter(iter){
                    //empty body
                }
            };

            /////////////////////////////////////////////////
            /// \brief Creates a new root level node with the specified content
            /////////////////////////////////////////////////
            GenericTree(T obj) : mParent(nullptr), mContent(obj), mDecendentCount(0){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Returns the parent of this node, if this node is a root node
            /// returns nullptr
            /////////////////////////////////////////////////
            GenericTree<T>* getParent(){
                return this->mParent;
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the begining of the child container for this node
            /////////////////////////////////////////////////
            iterator begin(){
                return iterator(this->mChildren.begin());
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the end of the child container for this node
            /////////////////////////////////////////////////
            iterator end(){
                return iterator(this->mChildren.end());
            }

            /////////////////////////////////////////////////
            /// \brief Constructs a new instance of GenericTree<T> with the passed in content
            /// and adds it to this node as a child.
            ///
            /// The construction and subsequent addition as a child only occurs if this node has
            /// no child with the same content as the passed in "obj" parameter
            ///
            /// \param obj This will be the contents of the new GenericTree<T> instance
            ///
            /// \return An iterator to the new child node, or, if the a node with that content already existed,
            /// an iterator to the pre-existing child
            /////////////////////////////////////////////////
            iterator add(T obj){
                auto child = this->mChildren.find(obj);
                if(child == this->mChildren.end()){
                    //then the child does not exist
                    GenericTree<T>* newNode = new GenericTree<T>(obj);
                    return newNode->setParent(this);
                } else {
                    //child exists
                    return iterator(child);
                }
            }

            /////////////////////////////////////////////////
            /// \brief Makes an existing GenericTree<T> node a child of this ones,
            /// inserting it into the child container of this node.
            /// Any decendents of the existing node remain its decendents
            /// and hence become decendents of this node
            /// eg:
            /// \code
            /// Adding the node 4 from this tree
            /// 4
            ///  \
            ///   5
            /// as a child of node 1 in this tree:
            ///     1
            ///   /   \
            ///  2     3
            /// will result in:
            ///     1
            ///   / | \
            ///  2  3  4
            ///         \
            ///          5
            ///
            /// \endcode
            ///
            /// \note If the specified node was not a root node then it will be removed from its old parent
            ///
            ///
            /// \throw ExcepInvalidOperationInState Thrown if this node already has a child with the
            /// same content as the passed in node. In this case the both this node and the passed in node
            /// will be unchanged. The "merge" function will insteady merge the trees together if a duplicate
            /// child exists
            ///
            /// \param node - the existing node which will be added as a child to this node
            ///
            /// \return iterator to the node in this node's child container
            ///
            /// \see merge
            /////////////////////////////////////////////////
            iterator add(GenericTree<T>& node) throw (lie::core::ExcepInvalidOperationInState){
                auto childIter = this->mChildren.find(node.getContent());
                if(childIter != this->mChildren.end()){
                    //child with that content already exists
                    throw lie::core::ExcepInvalidOperationInState("Attempted to added existing node to GenericTree however "
                                "prospective parent already has child with the same content as the passed in node. No modifications "
                                "have been made to any involved node.");
                } else {
                    //then we can do the insert
                    return node.setParent(this);
                }
            }

            /////////////////////////////////////////////////
            /// \brief The specified node will be added as a child of this node
            /// If this node already has a child with the same content as the passed in node then the trees
            /// will be merged together recursivly. EG:
            /// \code
            /// If the node 1 as follows:
            ///     1
            ///   /   \
            ///  2     3
            ///         \
            ///          4
            /// is added as a child to node 6 in the tree:
            ///     6
            ///   /   \
            ///  7     1
            ///       / \
            ///      8   3
            /// then the following tree will be produced:
            ///       6
            ///     /   \
            ///   7      1
            ///        / | \
            ///       8  2  3
            ///              \
            ///               4
            /// \endcode
            ///
            /// \note The tree that was passed in IS modified in this merge.
            /// Nodes are NOT copied, instead their parents are changed.
            /// The only nodes whose parents are not changed are those that already existed in both the
            /// destination tree AND tree being merged into it (ie: the one passed as an argument).
            /// Eg, for the above example the 1 node passed as an argument will now look like this:
            /// \code
            /// 1
            ///  \
            ///   3
            /// \endcode
            /// The 1 was already a child of 6 so its parent was not changed, simerally the 1 in the destination tree
            /// already had a child with the content 3, hence the 3 node was not moved.
            /// If there are no common parts to the tree (eg: the 6 did have a 1 as a child)
            /// then the passed in node will become part of the destination tree. In this case this function is equivilent to
            /// calling add
            /// \see add
            /////////////////////////////////////////////////
            iterator merge(GenericTree<T>& newChild){
                auto curChildIter = this->mChildren.find(newChild.getContent());
                if(curChildIter != this->mChildren.end()){
                    //then we need to do the merge

                    //this is as simple as it might seem...
                    //my first solution was simply to iterate through all chilren of the new child and merge them
                    //with the existing node in this tree, this does not work...
                    /*for(auto newChildIter = newChild.begin(); newChildIter != newChild.end(); ++newChildIter){
                        curChildIter->second->merge(*newChildIter);
                    }*/

                    //why? as the calling merge can change the parent of nodes, hence erase()
                    //is called on the old parent's child container, this invalidates any iterator pointing to the erased element
                    //see: http://www.cplusplus.com/reference/unordered_map/unordered_map/erase/

                    auto newChildIter = newChild.begin();
                    auto newChildIterCopy = newChild.begin();
                    while(newChildIter != newChild.end()){
                        newChildIterCopy = newChildIter;                //so instead we make a copy of the iterator
                        ++newChildIter;                                 //advance the original iterator past the soon to be erased element
                        curChildIter->second->merge(*newChildIterCopy); //and now (potentially) invalidate the copy of the iterator
                        //but not to worry, the original is still valid :)
                    }

                    //return iter to the existing node that this parent had
                    return curChildIter;
                } else {
                    //then this node does not have a child with the same content as the content of the passed in node
                    //we dont need to do the merge, nice and easy, simply change parent :)
                    return newChild.setParent(this);
                }
            }


            /////////////////////////////////////////////////
            /// \brief Returns an iterator to this node's child with the specified content
            /// If this node does not have a child with the specified content returns an
            /// iterator to the end of the child container, ie: the iterator returned by .end()
            /////////////////////////////////////////////////
            iterator getChild(T obj){
                return iterator(this->mChildren.find(obj));
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if this node has a child node with the specified content
            /////////////////////////////////////////////////
            bool hasChild(T obj){
                return (this->mChildren.find(obj) != this->mChildren.end());
            }

            /////////////////////////////////////////////////
            /// \brief Provides access to the content of this node
            /////////////////////////////////////////////////
            T* operator->(){
                return &mContent;
            }

            /////////////////////////////////////////////////
            /// \brief Provides acces to the content of this node
            /////////////////////////////////////////////////
            T& operator*(){
                return mContent;
            }

            /////////////////////////////////////////////////
            /// \brief returns a reference to the content of this node
            /////////////////////////////////////////////////
            T& getContent(){
                return mContent;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the number of decendents this node has (ie:
            /// how many children + each of theirs child count + etc)
            /////////////////////////////////////////////////
            unsigned int getDecendentCount(){
                return this->mDecendentCount;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the number of children this node has
            /////////////////////////////////////////////////
            unsigned int getChildCount(){
                return this->mChildren.size();
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if this node is a root node, ie it has no parents
            /////////////////////////////////////////////////
            bool isRootNode(){
                return (mParent == nullptr);
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if this node is a leaf node, ie it has no children
            /////////////////////////////////////////////////
            bool isLeafNode(){
                return this->getChildCount() == 0;
            }

            /////////////////////////////////////////////////
            /// \brief Returns this nodes depth in the tree.
            /// Root nodes have a depth of 0, their children a depth of 1, theirs
            /// a depth of 2, etc.
            /// In other words, it is the number of nodes between this node and the tree's root
            /////////////////////////////////////////////////
            unsigned int getDepth(){
                //could do recursivly but dont want stack overflows on large trees
                GenericTree<T>* curLevel = this;
                unsigned int result = 0;
                while(curLevel->mParent != nullptr){
                    ++result;
                    curLevel = curLevel->mParent;
                }
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief DEBUGING FUNCTION
            /// Returns a string formated to show the tree structure
            /// eg:
            /// \code
            ///       6
            ///     /   \
            ///   7      1
            ///        / | \
            ///       8  2  3
            ///              \
            ///               4
            /// \endcode
            /// would return the string (including new lines):
            /// \code
            /// 6
            /// ---1
            /// ------8
            /// ------3
            /// ---------4
            /// ------2
            /// ---7
            /// \endcode
            ///
            /// \note The order of nodes is not guarentied, the above tree could just as easily return
            /// \code
            /// 6
            /// ---7
            /// ---1
            /// ------8
            /// ------2
            /// ------3
            /// ---------4
            /////////////////////////////////////////////////
            std::string toString(int depth = 0){
                std::stringstream ss;
                for(int i = 0; i < depth; ++i){
                    ss << "---";
                }
                ss << this->mContent << "<br>" << std::endl;
                for(auto iter = this->begin(); iter != this->end(); ++iter){
                    ss << iter->toString(depth+1);
                }
                return ss.str();
            }
        private:
            /////////////////////////////////////////////////
            /// \brief Changes the parent of this node to the specified one
            /// Will automatically remove this from the parents child list
            /// Can pass in nullptr to make this a root node, also a node which was
            /// a root node can be given a parent with this function
            ///
            /// Returns an iterator to the node
            /////////////////////////////////////////////////
            iterator setParent(GenericTree<T>* newParent){
                if(this->mParent == newParent){
                    //then do not change parents, instead return iterator to the child without making any changes
                    return newParent->mChildren.find(this->mContent);
                }

                //parent already has a child with that content, cannot transfere
                if((newParent != nullptr)){
                    if(newParent->hasChild(this->mContent)){
                        //cant be transfered as the new parent already has a child with the same content as this
                        throw lie::core::ExcepInvalidOperationInState(std::string("Tried to change the parent of a GenericTree<") + typeid(T).name() +
                                                                  "> node but new parent already has a child with this nodes content. Parent unchanged");
                    }
                }
                //we are going ahead with the transfere if still running
                if(this->mParent != nullptr){
                    //as this is not a root node we need to decrease the old parents decendent count
                    this->mParent->changeDecendentCount(-((int)(this->mDecendentCount+1))); //+1 to include this node as well
                    this->mParent->mChildren.erase(this->mParent->mChildren.find(this->mContent));
                }

                //set the new parent
                this->mParent = newParent;

                //if new parent is not null we need to increase its decendent count and add it to its child map
                if(this->mParent != nullptr){
                    this->mParent->changeDecendentCount(this->mDecendentCount+1); //+1 to include this node as well
                    return (this->mParent->mChildren.insert(std::make_pair(this->mContent, this))).first;
                }
            }

            /////////////////////////////////////////////////
            /// \brief Modifies this node and its ancestor node's decendent count by the set amount
            /////////////////////////////////////////////////
            void changeDecendentCount(int modAmount){
                GenericTree<T>* curLevel = this;
                while(curLevel != nullptr){
                    curLevel->mDecendentCount+=modAmount;
                    curLevel = curLevel->mParent;
                }
            }
            GenericTree<T>* mParent;
            T mContent;
            ContainerType mChildren;
            unsigned int mDecendentCount;
        };

    }
}

#endif // LIE_CORE_GENERICTREE_HPP
