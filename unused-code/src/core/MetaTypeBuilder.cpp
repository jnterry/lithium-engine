#include <lie\core\MetaTypeBuilder.hpp>
#include <lie\core\MetaType.hpp>
#include <lie\core\MetaTypeField.hpp>



namespace lie{
    namespace core{

        class TypeBuilder{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructor which creates a new type builder
            /// Must specify the name of the type to create
            /// Can optinally specify which variations of the type you wish to make
            /// Eg: raw, ptr, reference, etc
            ///
            /// \param typeName - the name of the type you wish to build
            /// \param  makeRaw - defaults to true, specifies whether to create a normal type upon building
            /// \param makePtr - defaults to true, specifies whether to create a pointer variation of the type, its name will be typeName*
            /// \param makeRef - defualts to true, specifies whether to create a reference variation of the type, its name will be typeName&
            /// \return new TypeBuilder instance
            /////////////////////////////////////////////////
            TypeBuilder(std::string typeName, bool makeRaw = true, bool makePtr = true, bool makeRef = true);

            /////////////////////////////////////////////////
            /// \brief Adds a new field with the specified type, name, offset and optinally count
            /// \return TypeBuilder& for operator chaining
            /////////////////////////////////////////////////
            TypeBuilder& field(QualifiedMetaType type, std::string name, void* location, unsigned int count = 1){
                return this->field(MetaTypeField(type, name, (int)location, count));
            }

            TypeBuilder& field(MetaTypeField field);
        private:

        };

    }
}
