#include <lie\core\AllocatorHeap.hpp>
#include <cstdlib>

namespace lie{
    namespace core{


        void* AllocatorHeap::allocate(size_t size, char alignment){
            //get the raw memory chunk
            char* result = (char*)malloc(size+alignment);

            //align the memory
            /*while((result+offset % alignment != 0)){
                *result = 0xEE; //pad it with useless data
            }*/

            return result;
        }

        void AllocatorHeap::deallocate(void* mem){


        }

        size_t AllocatorHeap::getSize(void* mem){
            return 0;

        }

    }
}
