#include <lie\render\Window.hpp>
#include <windows.h>
#include <lie\core\Logger.hpp>
#include "_Win32ErrorToString.hpp"

//http://bobobobo.wordpress.com/2008/01/31/how-to-create-a-basic-window-in-c/

namespace {
    //you must register a class with the windows api before you can create windows,
    //however this should only be done once
    bool registeredClass = false;
    WNDCLASS LieWindClass;

    //the function that will be called whenever any event occurs with any window created by lie
    LRESULT CALLBACK globalOnEvent(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
        //list of what various msg numbers mean
        //http://wiki.winehq.org/List_Of_Windows_Messages
        LIE_DLOG_TRACE("", "globalOnEvent got" << msg);
        switch(msg){
        case WM_CREATE:{
            Beep( 50, 100 );
            break;
        }
        case WM_CLOSE: {
            //DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            //PostQuitMessage(0);
            break;
        }
        //case WM_PAINT: {
            //PAINTSTRUCT ps;
            //BeginPaint(hwnd, &ps);
            //EndPaint(hwnd, &ps);
        //}
        // Set cursor event
        /*case WM_SETCURSOR: {
            // The mouse has moved, if the cursor is in our window we must refresh the cursor
            //if (LOWORD(lParam) == HTCLIENT)
               // SetCursor(m_cursor);

            break;
        }
        // Resize event
        case WM_SIZE: {
            break;
        }

        // Start resizing
        case WM_ENTERSIZEMOVE: {
            break;
        }

        // Stop resizing
        case WM_EXITSIZEMOVE: {
            break;
        }

        // Gain focus event
        case WM_SETFOCUS: {
            break;
        }

        // Lost focus event
        case WM_KILLFOCUS: {
            break;
        }

        // Text event
        case WM_CHAR: {
            break;
        }

        // Keydown event
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN: {
            break;
        }

        // Keyup event
        case WM_KEYUP:
        case WM_SYSKEYUP: {
            break;
        }

        // Mouse wheel event
        case WM_MOUSEWHEEL: {
            break;
        }

        // Mouse left button down event
        case WM_LBUTTONDOWN: {
            break;
        }

        // Mouse left button up event
        case WM_LBUTTONUP: {
            break;
        }

        // Mouse right button down event
        case WM_RBUTTONDOWN: {
            break;
        }

        // Mouse right button up event
        case WM_RBUTTONUP: {
            break;
        }

        // Mouse wheel button down event
        case WM_MBUTTONDOWN: {
            break;
        }

        // Mouse wheel button up event
        case WM_MBUTTONUP: {
            break;
        }

        // Mouse X button down event
        //case WM_XBUTTONDOWN: {
            //break;
        //}

        // Mouse X button up event
        //case WM_XBUTTONUP: {
            //break;
        //}

        // Mouse move event
        case WM_MOUSEMOVE: {
            break;
        }

        // Mouse leave event
        case WM_MOUSELEAVE: {
            break;
        }*/

        default:{
            //return DefWindowProc(hwnd, msg, wParam, lParam);
            return 0;
        }

        }
        return 0;
    }


}

namespace lie{
    namespace rend{
        namespace detail{

            class WindowImpl{
            public:
                void create(int style, std::string caption){
                    LIE_DLOG_TRACE("Win32WindowImpl", "Creating new window...");

                    HINSTANCE hInstance = GetModuleHandle(NULL);//get hinstance for current exe

                    // A.  Create WNDCLASS structure and initialize it
                    // The WNDCLASS structure tells Windows WHAT KIND OF
                    // WINDOW we dream of creating.
                    static WNDCLASS wc;
                    if(!registeredClass){
                        wc.cbClsExtra = 0;  // ignore for now
                        wc.cbWndExtra = 0;  // ignore for now
                        wc.hbrBackground = (HBRUSH)GetStockObject( WHITE_BRUSH );   // I want the window to have a white background
                        wc.hCursor = LoadCursor( NULL, IDC_ARROW );            // I want it to have an arrow for a cursor
                        wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );        // I want it to have that envelope like icon
                        wc.hInstance = hInstance;           // INSTANCE HANDLE -- see the GLOSSARY PART of this file for an explanation of what HINSTANCE is
                        wc.lpfnWndProc = &globalOnEvent;     // Give name of WndProc function here.
                        wc.lpszClassName = TEXT("LieWind"); // name for when you call CreateWindow()
                        wc.lpszMenuName = 0;    // no menu - ignore
                        wc.style = CS_HREDRAW | CS_VREDRAW; // Redraw the window
                        // on BOTH horizontal resizes (CS_HREDRAW) and
                        // vertical resizes (CS_VREDRAW).  There are
                        // many more window class styles!


                        // B.  Register the WNDCLASS with Windows, THEN
                        //     create the window.
                        RegisterClass( &wc );   // This kind of "plants" the information
                                                // about the Window we "dream"
                                                // of creating somewhere inside the Windows O/S...
                        registeredClass = true;
                    }

                    DWORD winStyle = WS_POPUP;
                    if(style & lie::rend::Window::Fullscreen){
                        LIE_DLOG_TRACE("Win32WindowImpl", "Creating new fullscreen window");
                    } else {
                        LIE_DLOG_TRACE("Win32WindowImpl", "Creating new non-fullscreen window");
                        if(style & lie::rend::Window::Borders){winStyle |= WS_SIZEBOX;}
                        if(style & lie::rend::Window::Titlebar){winStyle |= WS_CAPTION;}
                        if(style & lie::rend::Window::CloseButton){winStyle |= (WS_CAPTION | WS_SYSMENU);}
                        if(style & lie::rend::Window::MinimiseButton){winStyle |= (WS_CAPTION | WS_MINIMIZEBOX);}
                        if(style & lie::rend::Window::MaximiseButton){winStyle |= (WS_CAPTION | WS_MAXIMIZEBOX);}
                        if(style & lie::rend::Window::Resizeable){winStyle |= WS_SIZEBOX;}
                    }




                    // NOW, at this next stage, WE ACTUALLY CREATE
                    // the window.  The previous lines of code
                    // until this point have only been PREPARATION.

                    // When we call the CreateWindow() function, we will
                    // make a reference to this WNDCLASS structure (BY ITS
                    // NAME -- "Philip"), and the Windows government
                    // will grant us our wish and a REAL LIVE WINDOW,
                    // with the properties that we specified in the
                    // WNDCLASS structure!

                    // NOTICE that the value that is returned from
                    // CreateWindow is a variable of type HWND.

                    // HWND is a "handle to a window" - its a programmatic
                    // reference variable to the Window.
                    // The HWND is OUR MEANS BY WHICH to manipulate
                    // our Window.

                    // Read the schpeal near the top of this file if
                    // not sure about HANDLES.

                    // Most of the time, you would want to save
                    // this HWND into a global variable,
                    // so you wouldn't lose it later.
                    HWND hwnd = CreateWindow(
                        TEXT("LieWind"),         // THIS IS THE LINK
                                                // to the WNDCLASS structure that
                                                // we created earlier.

                        caption.c_str(),        // appears in title of window

                        winStyle,               // the style we just made from the Style enum
                        10, 10,                 // x, y start coordinates of window
                        200, 200,               // width, height of window
                        NULL, NULL,             // nothing and nothing (ignore to start out)
                        hInstance, NULL );      // hInstance -- (see glossary), nothing

                    // Next, SHOW and PAINT the window!
                    // You won't see the window if you DO NOT
                    // call ShowWindow();
                    ShowWindow(hwnd, SW_SHOW);
                    UpdateWindow(hwnd);
                    mIsOpen = true;
                    LIE_DLOG_TRACE("Win32WindowImpl", "Window created");
                }

                void close(){
                    DestroyWindow(this->mHwndHandle);
                    this->mHwndHandle = 0;
                    mIsOpen = false;
                }

                void minimise(){
                    //windows api naming conventions... since when did close = minimise?
                    //CloseWindow(this->mHwndHandle);

                    ShowWindow(this->mHwndHandle, SW_MINIMIZE);
                }

                void restore(){
                    ShowWindow(this->mHwndHandle, SW_RESTORE);
                }

                void maximise(){
                    ShowWindow(this->mHwndHandle, SW_MAXIMIZE);
                }

                bool isMinimised(){
                    WINDOWPLACEMENT info;
                    if(GetWindowPlacement(this->mHwndHandle, &info)){
                        return info.showCmd == SW_MINIMIZE;
                    } else {
                         DWORD err = GetLastError();
                        LIE_DLOG_WARN("Win32WindowImpl","Failed to retrieve minimised state of window, returning false (not minmised). Msg: " <<
                            lie::rend::detail::Win32ErrorToString(err) << "(code: " << err << ")");
                        return false;
                    }
                }
                bool isMaximised(){
                    WINDOWPLACEMENT info;
                    if(GetWindowPlacement(this->mHwndHandle, &info)){
                        return info.showCmd == SW_MINIMIZE;
                    } else {
                        DWORD err = GetLastError();
                        LIE_DLOG_WARN("Win32WindowImpl","Failed to retrieve maximised state of window, returning false (not minmised). Msg: " <<
                            lie::rend::detail::Win32ErrorToString(err) << "(code: " << err << ")");
                        return false;
                    }
                }
                bool isOpen(){
                    return this->mIsOpen;
                }
                void setPosition(int x, int y){

                }
                int getXPosition(){
                    WINDOWPLACEMENT info;
                    if(GetWindowPlacement(this->mHwndHandle, &info)){
                        if(info.showCmd == SW_MINIMIZE){
                            return info.ptMaxPosition.x;
                        } else if(info.showCmd == SW_MAXIMIZE){
                            return info.ptMaxPosition.x;
                        } else if(info.showCmd == SW_RESTORE){
                            return info.rcNormalPosition.left;
                        } else {
                            LIE_DLOG_WARN("Win32WindowImpl","Tried to find window x position but window is in unknown state, code: " << info.showCmd);
                            return 0;
                        }
                    } else {
                        DWORD err = GetLastError();
                        LIE_DLOG_WARN("Win32WindowImpl","Failed to retrieve maximised state of window, returning false (not minmised). Msg: " <<
                            lie::rend::detail::Win32ErrorToString(err) << "(code: " << err << ")");
                        return 0;
                    }
                }
                int getYPosition(){
                    WINDOWPLACEMENT info;
                    if(GetWindowPlacement(this->mHwndHandle, &info)){
                        if(info.showCmd == SW_MINIMIZE){
                            return info.ptMaxPosition.y;
                        } else if(info.showCmd == SW_MAXIMIZE){
                            return info.ptMaxPosition.y;
                        } else if(info.showCmd == SW_RESTORE){
                            return info.rcNormalPosition.top;
                        } else {
                            LIE_DLOG_WARN("Win32WindowImpl","Tried to find window x position but window is in unknown state, code: " << info.showCmd);
                            return 0;
                        }
                    } else {
                        DWORD err = GetLastError();
                        LIE_DLOG_WARN("Win32WindowImpl","Failed to retrieve maximised state of window, returning false (not minmised). Msg: " <<
                            lie::rend::detail::Win32ErrorToString(err) << "(code: " << err << ")");
                        return 0;
                    }
                }
                Monitor getCurrentMonitor(){
                    return lie::rend::Monitor::getPrimaryMonitor();
                }
                bool hasEvent(){
                    //checks with windows if there is any "MSG" avalible and returns true if one is
                    MSG msg;
                    return PeekMessage(&msg, this->mHwndHandle, 0,0, PM_NOREMOVE);
                }

                Event getEvent(bool popFromQueue){
                    //LIE_DLOG_TRACE("", "getting event from window");
                    MSG msg;
                    if(PeekMessage(&msg, this->mHwndHandle, 0, 0, PM_REMOVE)){
                        //LIE_DLOG_TRACE("", "handling event from window");
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }

                    return winMsgToEvent(msg);
                }

                Event waitForEvent(bool popFromQueue){
                    MSG msg;
                    //LIE_DLOG_TRACE("", "waiting 4 event");
                    if(GetMessage(&msg, this->mHwndHandle,0,0)){
                        //LIE_DLOG_TRACE("", "handling event from window");
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }

                    return winMsgToEvent(msg);
                }

            private:
                HWND mHwndHandle;
                bool mIsOpen;

                //translates a windows "MSG" structure into a lie::rend::Event
                Event winMsgToEvent(MSG msg){
                    Event e;
                    return e;
                }
            };

        }
    }
}
