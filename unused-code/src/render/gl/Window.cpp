#include <lie\core\Platform.hpp>
#include <lie\render\Window.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "_Win32WindowImpl.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxWindowImpl.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXWindowImpl.hpp"
#else
    #error No window implementation for this platform
#endif

namespace lie{
    namespace rend{

            RenderContext  Window::getRenderContext(){
                //return this->mRendContext;
            }

            std::string  Window::getCaption(){
                return this->mCaption;
            }

            void Window::setCaption(std::string cap){
                this->mCaption = cap;
            }

            int Window::getStyle(){
                return this->mStyle;
            }

            void  Window::setStyle(int style){
                this->mStyle = style;
            }

            void  Window::create(){
                this->mImpl->create(this->mStyle, this->mCaption);
            }

            void  Window::close(){
                this->mImpl->close();
            }

            void  Window::minimise(){
                this->mImpl->minimise();
            }

            void  Window::restore(){
                this->mImpl->restore();
            }

            void  Window::maximise(){
                this->mImpl->maximise();
            }

            bool  Window::isMinimised(){
                return this->mImpl->isMinimised();
            }

            bool  Window::isMaximised(){
                return this->mImpl->isMaximised();
            }

            bool Window::isOpen(){
                return this->mImpl->isOpen();
            }

            void Window::setPosition(int x, int y, Monitor monitor){
                this->mImpl->setPosition(x,y);
            }

            int  Window::getXPosition(){
                return this->mImpl->getXPosition();
            }

            int  Window::getYPosition(){
                return this->mImpl->getYPosition();
            }

            Monitor Window::getCurrentMonitor(){
                return this->mImpl->getCurrentMonitor();
            }

            Window::Window(std::string caption, int style, bool autocreate) :
                mStyle(style), mCaption(caption){
                mImpl = new lie::rend::detail::WindowImpl;
                if(autocreate){
                    this->create();
                }
            }

            bool Window::hasEvent(){
                return this->mImpl->hasEvent();
            }

            Event Window::getEvent(bool popFromQueue){
                return this->mImpl->getEvent(popFromQueue);
            }

            Event Window::waitForEvent(bool popFromQueue){
                return this->mImpl->waitForEvent(popFromQueue);
            }
    }
}
