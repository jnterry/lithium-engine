/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file math.hpp
/// \author Jamie Terry
/// \brief Contains various mathmatical constants used by the engine
///
/// \note For each constant three versions exist, a floating type with the suffix _f
/// and a double type with the suffix _d.
/// The third version has no suffix and will be a double type if the macro LIE_MATH_DOUBLE_CONSTANTS
/// is defined before including this file. Otherwise they will be of type float.
///
///
///
/// \ingroup math
/////////////////////////////////////////////////
#ifndef LIE_MATH_CONSTANTS_HPP
#define LIE_MATH_CONSTANTS_HPP

namespace lie{
    namespace math{


        const float PI_f =  3.14159265358979323846264338327950288419716939937510;

        const double PI_d = 3.14159265358979323846264338327950288419716939937510;

        #ifdef LIE_MATH_DOUBLE_CONSTANTS
            const double PI = PI_d;
        #else
            const float PI = PI_f;
        #endif

    }
}

#endif // LIE_MATH_CONSTANTS_HPP
