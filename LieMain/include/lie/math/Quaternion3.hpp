#ifndef LIE_MATH_QUATERNION3_HPP
#define LIE_MATH_QUATERNION3_HPP

#include <math.h>

#include "Angle.hpp"
#include "Vector3.hpp"
#include "Matrix4x4.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Templated quaternion class used for representing any
        /// 3d rotation, rotates around some arbitrary axis by some arbitrary angle
        /////////////////////////////////////////////////
        template<typename T_Type>
        class Quaternion3{
        public:
            /// @{
            /// @name Constructors and Assignment
            /// \brief

            /////////////////////////////////////////////////
            /// \brief Creates a quaternion from an axis around which to rotate and
            /// an angle specifying the anticlockwise rotation amount
            ///
            /// \note The creates quaternion will actually represent an angle
            /// of half the specified one, however when using the Vector3::rotate
            /// function the vector is rotated by multiplying by this, then its conjugate,
            /// this has the effect of removing the imaginary part, but applied the rotation
            /// twice, hence the quaternion should store half the needed rotation
            /////////////////////////////////////////////////
            Quaternion3(const lie::math::Vector3<T_Type> axis, Angle<T_Type> angle){
                lie::math::Vector3<T_Type> an = axis.getNormalized();
				T_Type ang = angle.asRadians() / 2.0;
				T_Type s = sin(ang);

                this->mX = an.x * s;
                this->mY = an.y * s;
                this->mZ = an.z * s;
				this->mW = cos(ang);
            }

            /////////////////////////////////////////////////
            /// \brief Creates a new quaternion with the specified x,y,z and w
            /// components
            /////////////////////////////////////////////////
            Quaternion3(T_Type x, T_Type y, T_Type z, T_Type w)
            : mX(x), mY(y), mZ(z), mW(w){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Creates a default quaternion representing a rotation of 0 degrees
            /// around the z axis
            /////////////////////////////////////////////////
            Quaternion3() : Quaternion3(Vector3<T_Type>(0,0,1), rad(0)){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Sets all values of this Quaterion equal to the others
            /////////////////////////////////////////////////
            Quaternion3<T_Type>& operator=(const Quaternion3<T_Type>& other){
                this->mX = other.mX;
                this->mY = other.mY;
                this->mZ = other.mZ;
                this->mW = other.mW;
				return *this;
            }

            /// @} //end of constructors and assignment

            /////////////////////////////////////////////////
            /// \brief Returns true if all aspects of two Quaternion3 instances are equal,
            /// else returns false
            /////////////////////////////////////////////////
            bool operator==(Quaternion3<T_Type>& other){
                return (this->mX == other.mX &&
                        this->mY == other.mY &&
                        this->mZ == other.mZ &&
                        this->mW == other.mW);
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if any aspect of two Quaternion3 instances differ,
            /// else returns false
            /////////////////////////////////////////////////
            bool operator!=(Quaternion3<T_Type>& other){
                return !(*this == other);
            }


            T_Type getLengthSquared(){
                return mX*mX + mY*mY + mZ*mZ + mW*mW;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the length of this Quaternion
            /////////////////////////////////////////////////
            T_Type getLength(){
                return sqrt(getLengthSquared());
            }

            /////////////////////////////////////////////////
            /// \brief Normalizes this quaternion and returns a reference to it
            /// for operator chaining
            /////////////////////////////////////////////////
            Quaternion3<T_Type>& normalize(){
                T_Type length = this->getLength();
                mX /= length;
                mY /= length;
                mZ /= length;
                mW /= length;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Returns a new quaternion that is the normalized version of
            /// this one, does not modify this quaternion
            /////////////////////////////////////////////////
            Quaternion3<T_Type> getNormalized() const{
                Quaternion3 result = *this;
                result.normalize();
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Modifies this Quaternion3 so that it is made into its conjugate
            /// Equivalent of:
            /// \code
            /// quat = quat.getConjugate()
            /// \endcode
            /// \return Reference to this Quaternion3 for operator chaining
            ///
            /// \note The conjugate of the quaternion(x,y,z, w) is defined as the quaternion(-x,-y,-z, w)
            /////////////////////////////////////////////////
            Quaternion3<T_Type>& toConjugate(){
                this->mX = -this->mX;
                this->mY = -this->mY;
                this->mZ = -this->mZ;
				return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Constructs and returns a new Quaternion3 that is
            /// the conjugate of this Quaternion3
            ///
            /// \note The conjugate of the quaternion(x,y,z, w) is defined as the quaternion(-x,-y,-z, w)
            /////////////////////////////////////////////////

            Quaternion3<T_Type> getConjugate() const{
                return Quaternion3<T_Type>(-this->mX, -this->mY, -this->mZ, this->mW);
            }

            Quaternion3<T_Type> getInverse() const{
                T_Type denominator = this->mX*this->mX + this->mY*this->mY + this->mZ*this->mZ + this->mW*this->mW;
                return Quaternion3<T_Type>(this->mX / denominator, this->mY / denominator, this->mZ / denominator, this->mW / denominator);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a new quaternion that is the result of multipling two quaternions
            /// Does not modify either operand quaternion
            /////////////////////////////////////////////////
            Quaternion3<T_Type> operator*(const Quaternion3<T_Type>& rhs) const{
                return Quaternion3<T_Type>(
                        (this->mX * rhs.mW) + (this->mW * rhs.mX) + (this->mY * rhs.mZ) - (this->mZ * rhs.mY), //new quat x
                        (this->mY * rhs.mW) + (this->mW * rhs.mY) + (this->mZ * rhs.mX) - (this->mX * rhs.mZ), //         y
                        (this->mZ * rhs.mW) + (this->mW * rhs.mZ) + (this->mX * rhs.mY) - (this->mY * rhs.mX), //         z
                        (this->mW * rhs.mW) - (this->mX * rhs.mX) - (this->mY * rhs.mY) - (this->mZ * rhs.mZ));//         w
            }

            /////////////////////////////////////////////////
            /// \brief Modifies this quaternion by multiplying it but the specified one
            /////////////////////////////////////////////////
            Quaternion3<T_Type> operator*=(const Quaternion3<T_Type>& rhs){
                Quaternion3<T_Type> result = this->operator*(rhs);
                (*this) = result;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies this Quaternion by the specifed vector and returns the result
            /// as a new quaternion, does not modify either operand quaternion
            /////////////////////////////////////////////////
            Quaternion3<T_Type> operator*(const Vector3<T_Type>& vec) const{
                return Quaternion3<T_Type>( (this->mW * vec.x) + (this->mY * vec.z) - (this->mZ * vec.y),
                                            (this->mW * vec.y) + (this->mZ * vec.x) - (this->mX * vec.z),
                                            (this->mW * vec.z) + (this->mX * vec.y) - (this->mY * vec.x),
                                           -(this->mX * vec.x) - (this->mY * vec.y) - (this->mZ * vec.z));
            }

            /////////////////////////////////////////////////
            /// \brief Calculates and returns a 4x4 matrix which represents the rotation
            /// this quaternion represents
            /////////////////////////////////////////////////
            Matrix4x4<T_Type> asMatrix(){
                return Matrix4x4<T_Type>({  (T_Type)(1.0 - 2.0*this->mY*this->mY - 2.0*this->mZ*this->mZ),
											(T_Type)(2.0*this->mX*this->mY - 2.0*this->mZ*this->mW),
											(T_Type)(2.0*this->mX*this->mZ + 2.0*this->mY*this->mW),
											0,

											(T_Type)(2.0*this->mX*this->mY + 2.0*this->mZ*this->mW),
											(T_Type)(1.0 - 2.0*this->mX*this->mX - 2.0*this->mZ*this->mZ),
											(T_Type)(2.0*this->mY*this->mZ - 2.0*this->mX*this->mW),
											0,

											(T_Type)(2.0*this->mX*this->mZ - 2.0*this->mY*this->mW),
											(T_Type)(2.0*this->mY*this->mZ + 2.0*this->mX*this->mW),
											(T_Type)(1.0 - 2.0*this->mX*this->mX - 2.0*this->mY*this->mY),
											0,

                                            0,0,0,1});
            }

            /// @{
            /// @name Getters
            /// \brief

            /////////////////////////////////////////////////
            /// \brief Returns reference to the x component of the quaternion
            /////////////////////////////////////////////////
            T_Type& x(){
                return mX;
            }

            /////////////////////////////////////////////////
            /// \brief Returns reference to the y component of the quaternion
            /////////////////////////////////////////////////
            T_Type& y(){
                return mY;
            }

            /////////////////////////////////////////////////
            /// \brief Returns reference to the z component of the quaternion
            /////////////////////////////////////////////////
            T_Type& z(){
                return mZ;
            }
            /////////////////////////////////////////////////
            /// \brief Returns reference to the w component of the quaternion
            /////////////////////////////////////////////////
            T_Type& w(){
                return mW;
            }

            /////////////////////////////////////////////////
            /// \brief Returns const reference to the x component of the quaternion
            /////////////////////////////////////////////////
            const T_Type& x() const{
                return mX;
            }

            /////////////////////////////////////////////////
            /// \brief Returns const reference to the y component of the quaternion
            /////////////////////////////////////////////////
            const T_Type& y() const{
                return mY;
            }

            /////////////////////////////////////////////////
            /// \brief Returns const reference to the z component of the quaternion
            /////////////////////////////////////////////////
            const T_Type& z() const{
                return mZ;
            }
            /////////////////////////////////////////////////
            /// \brief Returns const reference to the w component of the quaternion
            /////////////////////////////////////////////////
            const T_Type& w() const{
                return mW;
            }
        private:
            T_Type mX;
            T_Type mY;
            T_Type mZ;
            T_Type mW;
        };

        typedef Quaternion3<int> Quaternion3i;
        typedef Quaternion3<float> Quaternion3f;
        typedef Quaternion3<double> Quaternion3d;

    }
}

#endif // LIE_MATH_QUATERNION3_HPP
