#ifndef LIE_MATH_VECTOR_HPP
#define LIE_MATH_VECTOR_HPP

#include <initializer_list>

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief A vector holds a fixed compile time determined number of elements
        /// of a specified type.
        ///
        /// It includes functions used to manipulate the vector, eg vector addition,
        /// subtraction, length, distance, etc
        /////////////////////////////////////////////////
        template<unsigned int COUNT_T, typename TYPE_T>
        class Vector{
        public:
            ///< The number of elements in this vector
            static const unsigned int ElementCount = COUNT_T;

            ///< The data in this vector
            TYPE_T data[COUNT_T];

			Vector(std::initializer_list<TYPE_T> dataArray){
				for (int i = 0; i < COUNT_T; ++i){
					data[i] = dataArray.begin()[i];
				}
			}
        };

        template<typename TYPE_T>
        class Vector<1, TYPE_T>{
        public:
            union{
                TYPE_T data[1];
                struct{
                    TYPE_T x;
                };
            };
        };

        template<typename TYPE_T>
        class Vector<2, TYPE_T>{
        public:
            union{
                TYPE_T data[2];
                struct{
                    TYPE_T x;
                    TYPE_T y;
                };
            };
        };

        template<typename TYPE_T>
        class Vector<3, TYPE_T>{
        public:
            union{
                TYPE_T data[3];
                struct{
                    TYPE_T x;
                    TYPE_T y;
                    TYPE_T z;
                };
            };
        };

        template<typename TYPE_T>
        class Vector<4, TYPE_T>{
        public:
            union{
                TYPE_T data[4];
                struct{
                    TYPE_T x;
                    TYPE_T y;
                    TYPE_T z;
                    TYPE_T w;
                };
            };
        };

    }
}

#endif // LIE_MATH_VECTOR_HPP
