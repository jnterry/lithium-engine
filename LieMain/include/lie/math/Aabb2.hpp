#ifndef LIE_MATH_AABB2_HPP
#define LIE_MATH_AABB2_HPP

#include "Vector2.hpp"
#include <initializer_list>

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Represents an axis aligned bounding box in 2d
        /// \tparam T_Type the type of primative which defines the bounding box,
        /// normally int, float or double
        /////////////////////////////////////////////////
        template<typename T_Type>
        class Aabb2{
        public:

            /////////////////////////////////////////////////
            /// \brief Constructs an aabb from an array of points automatically
            /// finds both the min and max x and y coordinates
            /////////////////////////////////////////////////
            Aabb2(std::initializer_list<Vector2<T_Type> > points)
            : mMinX(points.begin()->x), mMaxX(points.begin()->x), mMinY(points.begin()->y), mMaxY(points.begin()->y){
                //begin at the second point, the first ones values were used to init the values of this aabb
                for(unsigned int i = 1; i < points.size(); ++i){
                    if(points.begin()[i].x > mMaxX){mMaxX = points.begin()[i].x;}
                    if(points.begin()[i].x < mMinX){mMinX = points.begin()[i].x;}
                    if(points.begin()[i].y > mMaxY){mMaxY = points.begin()[i].y;}
                    if(points.begin()[i].y < mMinY){mMinY = points.begin()[i].y;}
                }
            }

            /////////////////////////////////////////////////
            /// \brief Constructs a new Aabb2 from a left and bottom coordinate
            /// along with the bounding box's width and height
            /////////////////////////////////////////////////
            Aabb2(T_Type left, T_Type bottom, T_Type width, T_Type height)
            : mMinX(left), mMinY(bottom), mMaxX(left+width), mMaxY(bottom+height){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Default constructor, all values set to 0
            /////////////////////////////////////////////////
            Aabb2() : mMinX(0), mMinY(0), mMaxX(0), mMaxY(0)
            {/*empty body*/}


            /////////////////////////////////////////////////
            /// \briefRenderer3d Returns the width of the bounding box, equivilent to
            /// \code
            /// getRight() - getLeft()
            /// \endcode
            /////////////////////////////////////////////////
            T_Type getWidth(){
                return mMaxX - mMinX;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the height of the bounding box, equivilent to
            /// \code
            /// getTop() - getBottom()
            /// \endcode
            /////////////////////////////////////////////////
            T_Type getHeight(){
                return mMaxY - mMinY;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the top cordinate of the aabb, ie: the largest
            /// y value
            /////////////////////////////////////////////////
            T_Type getTop(){
                return this->mMaxY;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the bottom cordinate of the aabb, ie: the smallest
            /// y value
            ///////////Renderer3d//////////////////////////////////////
            T_Type getBottom(){
                return this->mMinY;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the left cordinate of the aabb, ie: the smallest
            /// x value
            /////////////////////////////////////////////////
            T_Type getLeft(){
                return this->mMinX;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the right cordinate of the aabb, ie: the largest
            /// x value
            /////////////////////////////////////////////////
            T_Type getRight(){
                return this->mMaxX;
            }

            /////////////////////////////////////////////////
            /// \brief Returns a Vector2 where the x component represents this Aabb's width
            /// and the y component represents the height
            /////////////////////////////////////////////////
            Vector2<T_Type> getSize(){
                return Vector2<T_Type>(this->getWidth(), this->getHeight());
            }



        private:
            T_Type mMaxY;
            T_Type mMinY;
            T_Type mMaxX;
            T_Type mMinX;

        };

        typedef Aabb2<float> Aabb2f;
        typedef Aabb2<int> Aabb2i;
        typedef Aabb2<double> Aabb2d;

    }
}

#endif // LIE_MATH_AABB2_HPP
