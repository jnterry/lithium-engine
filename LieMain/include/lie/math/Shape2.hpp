#ifndef LIE_MATH_SHAPE2_HPP
#define LIE_MATH_SHAPE2_HPP

#include <lie/core\ArrayIterator.hpp>
#include <lie/core\ExcepOutOfRange.hpp>
#include <lie/core\Convert.hpp>

#include <typeinfo>
#include <cmath>

#include "Vector2.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief A Shape2 represents any 2d convex or concave shape, except those
        /// with self-intersections (ie: one edge of the shape intersects another edge
        /// of the shape) or shapes  which contain holes in them.
        ///
        /// It is made up of a collection of lie::math::Vector2<T> where
        /// T is the tparam of this class
        /////////////////////////////////////////////////
        template<typename T>
        class Shape2{

            //we want Shape2 of any T to be friends of Shape2 with any other T
            //this is because we can do op==, !=, =, etc on Shape2 with different tparams,
            //but they need to access each others internals to work
            template<typename T2>
            friend class Shape2;

            /////////////////////////////////////////////////
            /// \brief The type of vertex that this Shape2 stores
            /// Will be Vector2<T> where T is the tparam of Shape2
            /////////////////////////////////////////////////
            typedef Vector2<T> vertex_type;

            /////////////////////////////////////////////////
            /// \brief A bidirectional iterator to Vector2<T> used for accessing the verticies
            /// in a Shape2<T>
            /////////////////////////////////////////////////
            typedef lie::core::ArrayIterator<Vector2<T> > iterator;

            /////////////////////////////////////////////////
            /// \brief A bidirectional iterator to const Vector2<T> used for accessing the verticies
            /// in a Shape2<T>
            /////////////////////////////////////////////////
            typedef lie::core::ArrayIterator<const Vector2<T> > const_iterator;
        public:
            /////////////////////////////////////////////////
            /// \brief Basic Constructor, creates a new Shape2 instance with space to store
            /// the specified number of verticies
            /// Can optionally pass in a pointer to the first element of an array storing the verticies
            /// that you want the shape to have, if no pointer is passed in all the verticies will be initilised
            /// to (0,0)
            /////////////////////////////////////////////////
            Shape2(unsigned short vertexCount, Vector2<T>* verticies = nullptr)
            : mVertexArray(new Vector2<T>[vertexCount]), mVertexCount(vertexCount){
                //empty body
                if(verticies != nullptr){
                    copyArray(this->mVertexArray, verticies, vertexCount);
                }
            }

            /////////////////////////////////////////////////
            /// \brief Copy constructor, makes deep copy of passed in Shape2 object
            ///
            /// \note It is possible to make a copy where the types are not equal, eg:
            /// \code
            /// Shape2<float> floatShape(3);
            /// Shape2<int> shapeInt(floatShape);
            /// \endcode
            /// The effects would be eqivilent to
            /// \code
            /// float f = 1.1;
            /// int i = f;
            /// \endcode
            /////////////////////////////////////////////////
            template<typename T2>
            Shape2(const Shape2<T2>& other)
            : mVertexArray(new Vector2<T>[other.mVertexCount]), mVertexCount(other.mVertexCount){
                for(int i = 0; i < this->mVertexCount; ++i){
                    this->mVertexArray[i] = other.mVertexArray[i];
                }
            }

            /////////////////////////////////////////////////
            /// \brief Creates a Shape2 from a brace enclosed initializer list.
            /// Allows the following syntax:
            /// \code
            /// lie::math::Shape2<float> shape({lie::math::Vector2<float>(0.0f, 0.0f),
            ///                                 lie::math::Vector2<float>(1.0f, 0.0f),
            ///                                 lie::math::Vector2<float>(0.0f, 1.0f)});
            /// \endcode
            /////////////////////////////////////////////////
            Shape2(std::initializer_list<Vector2<T>> verticies)
            : mVertexCount(verticies.size()), mVertexArray(new Vector2<T>[verticies.size()]){
                for(int i = 0; i < this->mVertexCount; ++i){
                    this->mVertexArray[i] = verticies.begin()[i];
                }
            }

            /////////////////////////////////////////////////
            /// \brief Creates deep copy of passed in shape object
            /////////////////////////////////////////////////
            Shape2(const Shape2<T>& other)
            : mVertexArray(new Vector2<T>[other.mVertexCount]), mVertexCount(other.mVertexCount){
                this->copyArray(this->mVertexArray, other.mVertexArray, other.mVertexCount);
            }

            /////////////////////////////////////////////////
            /// \brief Changes the number of vertices that this Shape2 can store. </br>
            ///
            /// If the size is decreased then the last n vertices will be deleted
            /// where n = (newSize - oldSize). All other vertices will be kept and
            /// keep the same index </br>
            ///
            /// If the size is increased all then all existing vertices will be kept and
            /// all new vertices will be initialised to (0,0) </br>
            ///
            /// \note This is a relativly expensive operation, where possible you should aim to
            /// keep the number of vertices a Shape2 stores constant
            /////////////////////////////////////////////////
            void resize(unsigned int newVertexCount){
                Vector2<T>* oldVertices = this->mVertexArray;
                this->mVertexArray = new Vector2<T>[newVertexCount];

                this->copyArray(this->mVertexArray, oldVertices, std::min(this->mVertexCount, newVertexCount));

                this->mVertexCount = newVertexCount;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the number of verticies held by this Shape object
            /////////////////////////////////////////////////
            unsigned int size() const{
                return this->mVertexCount;
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the first vertex of this shape
            /////////////////////////////////////////////////
            iterator begin(){
                return iterator(mVertexArray, 0);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const_iterator to the first vertex of this shape
            /////////////////////////////////////////////////
            const_iterator cbegin(){
                return const_iterator(mVertexArray, 0);
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the vertex after the last vertex
            /// of this shape
            /// \note An iterator returned by this function will produce garbage data if dereferenced.
            /// Dereferencing an iterator returned by this function caused undefined behavior,
            /// it should be used in comparisons only. Eg:
            /// \code
            /// for(auto it = shape.begin(); it != shape.end(); ++it){
            ///     //do something with the iterator here
            ///     it->x += 5;
            /// }
            /// \endcode
            /////////////////////////////////////////////////
            iterator end(){
                return iterator(mVertexArray, mVertexCount);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const_iterator to the vertex after the last vertex
            /// of this shape
            /// \note An iterator returned by this function will produce garbage data if dereferenced.
            /// Dereferencing an iterator returned by this function caused undefined behavior,
            /// it should be used in comparisons only. Eg:
            /// \code
            /// for(auto it = shape.begin(); it != shape.end(); ++it){
            ///     //do something with the iterator here
            ///     it->x += 5;
            /// }
            /// \endcode
            /////////////////////////////////////////////////
            const_iterator cend(){
                return const_iterator(mVertexArray, mVertexCount);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the vertex of this shape object
            /// as the specified index
            /////////////////////////////////////////////////
            Vector2<T>& operator[](unsigned short index){
                return this->mVertexArray[index];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const reference to the vertex of this shape object
            /// as the specified index
            /////////////////////////////////////////////////
            const Vector2<T>& operator[](unsigned short index) const{
                return this->mVertexArray[index];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the vertex of this shape object
            /// as the specified index
            /// \note This function checks the index is in the correct bounds,
            /// if not it throws a lie::core::ExcepOutOfRange exception
            /// This is in contrast with member operator[] which does no range check
            /////////////////////////////////////////////////
            Vector2<T>& at(unsigned short index) throw (lie::core::ExcepOutOfRange<unsigned short>){
                if(index >= this->mVertexCount){
					throw lie::core::ExcepOutOfRange<unsigned short>(__FUNCTION__, "index", index, 0, this->mVertexCount - 1,
						"Attempted to access a shape vertex by index.");
                }
                return this->mVertexArray[index];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const reference to the vertex of this shape object
            /// as the specified index
            /// \note This function checks the index is in the correct bounds,
            /// if not it throws a lie::core::ExcepOutOfRange exception
            /// This is in contrast with member operator[] which does no range check
            /////////////////////////////////////////////////
            const Vector2<T>& at(unsigned short index) const throw (lie::core::ExcepOutOfRange<unsigned short>){
                if(index >= this->mVertexCount){
					throw lie::core::ExcepOutOfRange<unsigned short>(__FUNCTION__, "index", index, 0, this->mVertexCount - 1,
						"Attempted to access a shape vertex by index.");
                }
                return this->mVertexArray[index];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the first Vertex in this shape
            /////////////////////////////////////////////////
            Vector2<T>& front() noexcept{
                return this->mVertexArray[0];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const reference to the first Vertex in this shape
            /////////////////////////////////////////////////
            const Vector2<T>& front() const noexcept{
                return this->mVertexArray[0];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the last Vertex2 in this shape
            /////////////////////////////////////////////////
            Vector2<T>& back() noexcept{
                return this->mVertexArray[this->mVertexCount-1];
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const reference to the last Vertex2 in this shape
            /////////////////////////////////////////////////
            const Vector2<T>& back() const noexcept{
                return this->mVertexArray[this->mVertexCount-1];
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if both Shape2 objects have the same number of vertices and
            /// all the vertices are equal to the corrosponding vertex in the shape (ie: equal to
            /// the vertex with the same index in the other shape)
            ///
            /// \note Two shapes can seem to be equivilent but still return false, eg: </br>
            /// shape1 has the vertices: (0,0), (0,1) and (1,0) </br>
            /// shape2 has the vertices : (1,0), (0,0) and (0,1) </br>
            /// These shapes will return false as the indicies are in different orders
            /////////////////////////////////////////////////
            template<typename T2>
            bool operator==(const Shape2<T2>& other) const{
                if(this->mVertexCount != other.mVertexCount){
                    return false;
                }

                //same vertex count, check each one...
                for(int i = 0; i < this->mVertexCount; ++i){
                    if(this->mVertexArray[i] != other.mVertexArray[i]){
                        return false;
                    }
                }
                //then all vertices must be the same
                return true;
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if two Shape2 instances have a different
            /// number of vertices or any of their vertices are not the same as the
            /// corresponding vertex in the other Shape2
            /////////////////////////////////////////////////
            template<typename T2>
            bool operator!=(const Shape2<T2>& other) const{
                return !(this->operator==(other));
            }

			//////////////////////////////////////////////////////////////////////////
			/// \brief Named constructor that creates a regular polygon with the specified 
			/// number of sides and specified radius. The polygon's center will be at (0,0)
			/// and all verticies of the polygon will lie on the circle with radius 'r' center
			/// (0,0)
			/// \param numSides The number of sides the created polygon will have
			/// \param r The radius of the circle that the verticies will lie on
			//////////////////////////////////////////////////////////////////////////
			static Shape2<T> createRegularPolygon(int numSides, T r){
				Shape2<T> result(numSides);
				T curAngle = 0;
				T deltaAngle = (2.0 * PI_d) / ((double)numSides);
				for (auto vert = result.begin(); vert != result.end(); ++vert){
					vert->set(sin(curAngle)*r, cos(curAngle)*r);
					curAngle += deltaAngle;
				}
				return result;
			}
        private:
            /////////////////////////////////////////////////
            /// \brief copies the data in source into target for specified
            /// count
            /////////////////////////////////////////////////
            void copyArray(Vector2<T>* target, Vector2<T>* source, unsigned int count){
                for(int i = 0; i < count; ++i){
                    target[i] = source[i];
                }
            }

            Vector2<T>* mVertexArray;
            unsigned short mVertexCount;
        };

        typedef Shape2<float> Shape2f;
        typedef Shape2<int> Shape2i;
        typedef Shape2<double> Shape2d;

    }
}

#endif // LIE_MATH_SHAPE2_HPP
