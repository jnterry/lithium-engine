/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Vector3-impl.hpp
/// \author Jamie Terry
/// \brief This file contains the implementations for member functions of Vector3 which
/// use the Quaternion3 class.
///
/// These function create a circular dependency between Vector3 and Quaternion3,
/// hence they must be defined in a seperate file so the  full definition
/// of both the Vector3 class and the Quaterion3 class is visible.
/// Normally this would be done by pulling in the full Quaternion3.hpp in the
/// cpp file and forward declering Quaternion3 in the hpp, but both are template class so
/// cannot be in a cpp, hence it is defined in this file where both headers are included
/// To use these member functions of Vector you must include this file
///
/// \ingroup math
/////////////////////////////////////////////////
#ifndef LIE_MATH_VECTOR3IMPL_HPP
#define LIE_MATH_VECTOR3IMPL_HPP

namespace lie{
    namespace math{

        template<typename T_Type>
        Vector3<T_Type>& Vector3<T_Type>::rotate(const Quaternion3<T_Type>& quat){
            Quaternion3<T_Type> r = (quat * (*this)) * quat.getConjugate();
            this->x = r.x();
            this->y = r.y();
            this->z = r.z();
            return *this;
        }

        template<typename T_Type>
        Vector3<T_Type> Vector3<T_Type>::getRotated(const Quaternion3<T_Type>& quat) const{
            Vector3<T_Type> result = *this;
            result.rotate(quat);
            return result;
        }

    }
}

#endif // LIE_MATH_VECTOR3IMPL_HPP
