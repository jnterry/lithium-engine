/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Angle.hpp
/// Contains the lie::math::Angle class
/// 
/// \ingroup math
/////////////////////////////////////////////////

#ifndef	LIE_MATH_ANGLE_HPP
#define LIE_MATH_ANGLE_HPP

#include "Constants.hpp"

namespace lie{
	namespace math{
		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper function that converts an angle in degrees to an angle in radians
		/// \param degrees The angle in degrees
		/// \return The same angle represented in radians
		//////////////////////////////////////////////////////////////////////////
		inline double toRadians(double degrees){
			return  (degrees * lie::math::PI_d) / 180.0;
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper function that converts an angle in radians to an angle in degrees
		/// \param radians The angle in radians
		/// \return The same angle represented in degrees
		//////////////////////////////////////////////////////////////////////////
		inline double toDegrees(double radians){
			return (radians * 180.0) / lie::math::PI_d;
		}


		//////////////////////////////////////////////////////////////////////////
		/// \brief The Angle class is used to represent an angle in some abstract way and provide methods
		/// for setting and getting it in degrees, radians and some number of full revolutions.
		/// 
		/// The unit that the angle is stored as internally depends on whether the 
		/// LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS or LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
		/// is defined at compile time. Conversions are only done when setting or getting
		/// the value in some specified unit and only when needed.
		/// For example, if LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS is defined then the 
		/// performance of using this class is the same as using a float privative
		/// to store radians (assuming the compiler inlines everything). <br>
		/// The benefit of this class is that functions can take an Angle instance and then
		/// use the desired unit internally. Therefore if they need to call some other library function
		/// (eg: the trig functions in cmath) they can use the appropriate unit and callers of 
		/// lithium engine functions can use whatever unit they are comfortable with. This greatly improves
		/// readability and there is no need to look up or remember whether each function expects 
		/// degrees or radians, nor is there any need to define some global standard which will no
		/// doubt be forgotten somewhere and cause bugs. <br>
		/// Since the internal unit type can be changed with compile time #defines it is possible to change 
		/// the underlying unit for the entire application at once, which may lead to slight performance
		/// improvements if the number of conversions is decreased.
		/// 
		/// \note This class will not wrap in arithmetic, eg, adding 300 degrees and 200 degrees will
		/// produce 500 degrees. You may maually wrap by calling clamp()
		/// 
		/// \note Some commonly used operators are not provided, for example increment and decrement
		/// This is because the meaning of these depends on the unit used internally, hence to add
		/// one you will need to do the following:
		/// \code
		///		angle += lie::math::Angle::degrees(1);
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		template <typename T>
		class Angle{
			template<class T_TYPE> friend class Angle; //friends with all other types of angle
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new angle instance that represents the same angle as the
			/// specified one
			//////////////////////////////////////////////////////////////////////////
			Angle(const Angle& other) : mValue(other.mValue){ /*empty body*/ }

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a new instance of angle that represents the same rotation as
			/// this instances after it has been clamped to between 0 and 1 revolutions, does NOT
			/// modify this instance
			/// eg, if storing 365 degrees the value would become 5 degrees.
			/// Negative values are also converted to a positive value, eg, -10 degrees 
			/// would become 350 degrees after being clamped
			//////////////////////////////////////////////////////////////////////////
			Angle& getClamped() const{
				Angle result = *this;
				result.clamp();
				return return;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds another angle instance to this one, modifies this instance
			/// \return Reference to this instance for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& operator+=(const Angle& other){
				this->mValue += other.mValue;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds another angle instance to this one, modifies this instance
			/// \return Reference to this instance for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& operator-=(const Angle& other){
				this->mValue -= other.mValue;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Multiplies this angle by some factor, modifies this instance
			/// \return Reference to this instance for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& operator*=(T factor){
				this->mValue *= factor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds another angle instance to this one, modifies this instance
			/// \return Reference to this instance for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& operator/=(T factor){
				this->mValue /= factor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds another angle to this one and returns the result
			/// \return New Angle instance storing the result of the addition
			//////////////////////////////////////////////////////////////////////////
			Angle operator+(const Angle& rhs) const{
				return Angle(this->mValue + rhs.mValue);
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Subtracts another angle from this one and returns the result
			/// \return New Angle instance storing the result of the subtraction
			//////////////////////////////////////////////////////////////////////////
			Angle operator-(const Angle& rhs) const{
				return Angle(this->mValue - rhs.mValue);
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Multiplies this angle by some scaling factor
			/// \return New Angle instance storing the result of the multiplication
			//////////////////////////////////////////////////////////////////////////
			Angle operator*(T factor) const{
				Angle result = *this;
				result *= factor;
				return result;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Divides this angle by some scaling factor
			/// \return New Angle instance storing the result of the division
			//////////////////////////////////////////////////////////////////////////
			Angle operator/(T factor) const{
				Angle result = *this;
				result /= factor;
				return result;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Unary minus operator, returns a new Angle instance that is the 
			/// negative of this one, eg:
			/// \code
			/// angleB = -angleA;
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			Angle operator-() const{
				return Angle<T>(-this->mValue);
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Unary plus operator, returns a clone of this angle, eg
			/// \code
			/// angleB = +angleA;
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			Angle operator+() const{
				return Angle<T>(+this->mValue);
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets this angle equal to another
			//////////////////////////////////////////////////////////////////////////
			Angle& operator=(const Angle& other){
				if (&other == this){ return *this; }
				this->mValue = other.mValue;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two angle instances represent the EXACT same angle
			/// \note 40 degrees and 400 degrees, etc, are NOT considered to be the same as each other
			/// If you wish a comparison such as this to return true do the following:
			/// \code
			/// angleA.getClamped() == angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator==(const Angle& other) const{
				return this->mValue == other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two angle instances represent different angles,
			/// \note 40 degrees and 400 degrees, etc, are considered to be different angles
			/// If this is not the desired behavior do the following:
			/// \code
			/// angleA.getClamped() != angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator!=(const Angle& other) const{
				return this->mValue != other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if this angle's value is less than another's
			/// \note 365 degrees (ie: 5 degrees) is NOT considered to be less than 10 degrees
			/// If this is not the desired behavior do the following:
			/// \code
			/// angleA.getClamped() < angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator<(const Angle& other) const{
				return this->mValue < other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if this angle's value is less than or equal to another's
			/// \note 365 degrees (ie: 5 degrees) is NOT considered to be less than 10 degrees
			/// If this is not the desired behavior do the following:
			/// \code
			/// angleA.getClamped() < angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator<=(const Angle& other) const{
				return this->mValue <= other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if this angle's value is less greater another's
			/// \note 370 degrees (ie: 10 degrees) is NOT considered to be greater than 5 degrees
			/// If this is not the desired behavior do the following:
			/// \code
			/// angleA.getClamped() < angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator>(const Angle& other) const{
				return this->mValue > other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if this angle's value is greater than or equal to another's
			/// \note 370 degrees (ie: 10 degrees) is NOT considered to be greater than 5 degrees
			/// If this is not the desired behavior do the following:
			/// \code
			/// angleA.getClamped() < angleB.getClamped()
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			bool operator>=(const Angle& other) const{
				return this->mValue >= other.mValue;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Casting operator, allows Angle<T> to be implicitly cast to
			/// Angle<T2>
			/// \return new Angle<T2> instance representing the closest representable
			/// angle to this one, depending on the original and final type
			//////////////////////////////////////////////////////////////////////////
			template<typename T_TYPE>
			operator Angle<T_TYPE>(){
				return Angle<T_TYPE>(this->mValue);
			}

			//////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////// 
			////////////////////////////////////////////////////////////////////////// 
			////////////////////////////////////////////////////////////////////////// 
			//////////////////////////////////////////////////////////////////////////
			// Internal unit specific functions below
			// These functions have their definitions in Angle-ImplDeg.hpp
			// and Angle-ImplRad.hpp

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new Angle instance from some number of radians
			//////////////////////////////////////////////////////////////////////////
			static Angle radians(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new Angle instance from some number of degrees
			//////////////////////////////////////////////////////////////////////////
			static Angle degrees(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new angle instance from some number of revolutions, eg
			/// revolutions(0.5) would be equivalent to degrees(180) or radians(PI)
			//////////////////////////////////////////////////////////////////////////
			static Angle revolutions(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of this angle to the specified number of degrees
			/// \return reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& setDegrees(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of this angle to the specified number of radians
			/// \return reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& setRadians(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of this angle to the specified number of revolutions
			/// (ie: setRevolutions(0.5) is equivalent to setDegrees(180))
			/// \return reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Angle& setRevolutions(T value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the angle in radians
			//////////////////////////////////////////////////////////////////////////
			T asRadians() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the angle in degrees
			//////////////////////////////////////////////////////////////////////////
			T asDegrees() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the angle represented as some number of full revolutions
			/// eg, an angle of 180 degrees or PI radians would return 0.5
			//////////////////////////////////////////////////////////////////////////
			T asRevolutions() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Clamps the angle between 0 and 1 revolutions, eg, if storing 365 degrees
			/// and .clamp() is called then the value would become 5 degrees.
			/// Negative values are also converted to a positive value, eg, -10 degrees 
			/// would become 350 degrees after being clamped
			/// \warning Modifies THIS instances, use .getClamped() if you wish to retrieve
			/// a new instance and leave this one untouched
			//////////////////////////////////////////////////////////////////////////
			Angle& clamp();
		private:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use one of the named constructors to create
			/// this class, eg <br>
			/// \code
			///		auto rad = lie::math::Angle::radians(lie::math::PI);
			///		auto deg = lie::math::Angle::degrees(180);
			/// \endcode
			//////////////////////////////////////////////////////////////////////////
			Angle(T value) : mValue(value){ /* empty body */ }

			///< The angle stored by this instance
			T mValue;
		};

		///< Alias for Angle<float>
		typedef Angle<float> Anglef;

		///< Alias for Angle<double>
		typedef Angle<double> Angled;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper function for creating an instance of Angle<float> for an angle
		/// expressed in radians
		/// \param radians The angle in radians
		/// \return Angle<float> instance representing the angle
		//////////////////////////////////////////////////////////////////////////
		inline Angle<float> rad(float radians){
			return lie::math::Angle<float>::radians(radians);
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper function for creating an instance of Angle<float for an angle
		/// expressed in degrees
		/// \param degrees The angle in degrees
		/// \return Angle<float> instance representing the angle
		//////////////////////////////////////////////////////////////////////////
		inline Angle<float> deg(float degrees){
			return lie::math::Angle<float>::degrees(degrees);
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper function for creating an instance of Angle<float> for an angle
		/// expressed in revolutions
		/// \param revolutions The angle in revolutions
		/// \return Angle<float> instance representing the angle
		//////////////////////////////////////////////////////////////////////////
		inline Angle<float> rev(float revolutions){
			return lie::math::Angle<float>::revolutions(revolutions);
		}
	}
}

#if !defined LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS && !defined LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
	//set a default as user did not specify either
	#define LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS
#endif

#if defined LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS && defined LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
	#error You must cannot define both LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS and LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
#endif

#ifdef LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS
	#include "Angle-ImplRad.hpp"
#elif defined LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
	#include "Angle-ImplDeg.hpp"
#else
	//shouldn't get here as default is set above if neither is defined
	#error You must define either LIE_MATH_DEFAULT_ANGLE_UNIT_RADIANS or LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
#endif

#endif