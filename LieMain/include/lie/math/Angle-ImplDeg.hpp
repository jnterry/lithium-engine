/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file 
/// \author Jamie Terry
/// \date 2015/06/17
/// \brief Contains the implementation for unit specified functions in the Angle
/// class, this file contains the implementations for when LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES
/// is defined
/// \warning This file should not be included manually, include Angle.hpp instead
/// 
/// \ingroup math
/////////////////////////////////////////////////

#ifdef LIE_MATH_DEFAULT_ANGLE_UNIT_DEGREES

#ifndef LIE_MATH_ANGLE_IMPL_HPP
#define LIE_MATH_ANGLE_IMPL_HPP

#include <cmath>

namespace lie{
	namespace math{
		template <typename T>
		Angle<T> Angle<T>::degrees(T value){
			return Angle<T>(value);
		}

		template <typename T>
		Angle<T> Angle<T>::radians(T value){
			return Angle<T>(radToDeg(value));
		}

		template <typename T>
		Angle<T> Angle<T>::revolutions(T value){
			return Angle<T>(value * 360.0);
		}

		template <typename T>
		Angle<T>& Angle<T>::setDegrees(T value){
			this->mValue = value;
			return *this;
		}

		template <typename T>
		Angle<T>& Angle<T>::setRadians(T value){
			this->mValue = radToDeg(value);
			return *this;
		}

		template <typename T>
		Angle<T>& Angle<T>::setRevolutions(T value){
			this->mValue = value * 360.0;
			return *this;
		}

		template <typename T>
		T Angle<T>::asDegrees() const{
			return this->mValue;
		}

		template <typename T>
		T Angle<T>::asRadians() const{
			return degToRad(this->mValue);
		}

		template <typename T>
		T Angle<T>::asRevolutions() const{
			return this->mValue / 360.0;
		}

		template <typename T>
		Angle<T>& Angle<T>::clamp(){
			if (this->mValue < 0){
				//negative, need to get between 0 and 360 degrees
				this->mValue = -this->mValue;
				this->mValue = fmod(this->mValue, 360);
				this->mValue = 360.0 - this->mValue;
			} else {
				this->mValue = fmod(this->mValue, 360);
			}
		}

	}
}

#endif
#endif