#ifndef LIE_MATH_VECTOR3_H
#define LIE_MATH_VECTOR3_H

#include "Vector.hpp"
#include <math.h>

namespace lie{
    namespace math{

        //forward deceleration
        template<typename T_Type>
        class Quaternion3;

        template<typename T_Type>
		class Vector3{
        public:
            T_Type x, y, z;

            /// @{
			/// @name Constructors and Assignment Functions
			/// \brief

            /////////////////////////////////////////////////
            /// \brief Default constructor, creates vector (0,0,0)
            /////////////////////////////////////////////////
            Vector3() : x(0), y(0), z(0){}

            /////////////////////////////////////////////////
            /// \brief Constructor that makes vector from passed in values
            /////////////////////////////////////////////////
            Vector3(T_Type xVal, T_Type yVal, T_Type zVal) : x(xVal), y(yVal), z(zVal){}

            /////////////////////////////////////////////////
            /// \brief Copy constructor, copies values from other vector into new one
            /////////////////////////////////////////////////
            Vector3(const Vector3& otherVector) : x(otherVector.x), y(otherVector.y), z(otherVector.z){}

            /////////////////////////////////////////////////
            /// \brief Sets values of this vector equal to values in some other vector
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator=(const Vector3<T_Type>& rhs){
				//check for self assignment
				if(this != &rhs){
					this->x = rhs.x;
					this->y = rhs.y;
					this->z = rhs.z;
				}
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets all components of this vector to the same value
			/// \return Reference to this vector for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Vector3<T_Type>& setAll(const T_Type newValue){
				this->x = newValue;
				this->y = newValue;
				this->z = newValue;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Sets the values of this vector to the specified values
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
			Vector3<T_Type>& set(const T_Type newX, const T_Type newY, const T_Type newZ){
                this->x = newX;
                this->y = newY;
                this->z = newZ;
				return *this;
			}

			/// @} //end of constructors and assignment

			/// @{
			/// @name Comparison Operators
			/// \brief

            /////////////////////////////////////////////////
            /// \brief Compares this vector to another, returns true if all components are equal
            /////////////////////////////////////////////////
            bool operator==(const Vector3& rhs){
                return (this->x == rhs.x &&
                        this->y == rhs.y &&
                        this->z == rhs.z);
            }

            /////////////////////////////////////////////////
            /// \brief Compares this vector to another, returns false if all components are equal
            /////////////////////////////////////////////////
            bool operator!=(const Vector3& rhs){
                return !this->operator==(rhs);
            }

            /// @} //end of comparison operators

            /// @{
			/// @name Basic Vector operations
			/// \brief Basic mathmatical operators for addition, subtraction etc of vectors

            /////////////////////////////////////////////////
            /// \brief Adds x,y and z of another vector to the x,y, and z components of this vector.
            /// Modifies this vector
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator+=(const Vector3& rhs){
				this->x += rhs.x;
				this->y += rhs.y;
				this->z += rhs.z;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Subtracts x,y and z components from corrosponding components of this vector.
            /// Modifies this vector
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator-=(const Vector3& rhs){
				this->x -= rhs.x;
				this->y -= rhs.y;
				this->z -= rhs.z;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Multiplies each component of this vector by the specfied scalar,
            /// thus scaling the vector
            ///
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator*=(const T_Type rhs){
				this->x *= rhs;
				this->y *= rhs;
				this->z *= rhs;
				return *this;
			}

			/////////////////////////////////////////////////
            /// \brief Multiplies each component of this vector by the corrosponding
            /// component in the rhs vector, the geometric equivilent is a non-uniform scale
            /// where the rhs vector's components represent the scale factor in the x,y and z direction
            ///
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator*=(const Vector3<T_Type>& rhs){
				this->x *= rhs.x;
				this->y *= rhs.y;
				this->z *= rhs.z;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Divides each component of this vector by the specfied scalar,
            /// thus scaling the vector
            ///
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator/=(const T_Type rhs){
				this->x /= rhs;
				this->y /= rhs;
				this->z /= rhs;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Divides each component of this vector by the corrosponding
            /// component in the rhs vector, the geometric equivilent is a non-uniform scale
            /// where the rhs vector's components represent 1/(scale factor) in the x,y and z direction
            ///
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3<T_Type>& operator/=(const Vector3<T_Type>& rhs){
				this->x /= rhs.x;
				this->y /= rhs.y;
				this->z /= rhs.z;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding component of
            /// the lhs vector + the corrosponding component of the rhs vector
            /// Does not modify either operand vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator+(const Vector3& rhs) const{
				Vector3<T_Type> result = *this;
				result += rhs;
				return result;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding component of
            /// the lhs vector - the corrosponding component of the rhs vector
            /// Does not modify either operand vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator-(const Vector3& rhs) const{
				Vector3<T_Type> result = *this;
				result -= rhs;
				return result;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding component of
            /// this vector multiplied by the specified scalar, thus scaling the vector
            /// Does not modify this vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator*(const T_Type rhs) const{
				Vector3<T_Type> result = *this;
				result *= rhs;
				return result;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding components of
            /// the lhs and rhs vectors mutlipled together.
            ///
            /// The geometric equivilent is scaling the lhs vector by a non-uniform scale where the rhs
            /// components each specify the scale factor in the corrosponding direction
            ///
            /// Does not modify either operand vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator*(const Vector3<T_Type> rhs) const{
				Vector3<T_Type> result = *this;
				result *= rhs;
				return result;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding component of
            /// this vector divided by the specified scalar, thus scaling the vector
            /// Does not modify this vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator/(const T_Type rhs) const{
				Vector3<T_Type> result = *this;
				result /= rhs;
				return result;
			}

            /////////////////////////////////////////////////
            /// \brief Constructs a new vector where each component is the corrosponding components of
            /// the lhs and rhs vectors mutlipled together.
            ///
            /// The geometric equivilent is scaling the lhs vector by a non-uniform scale where the rhs
            /// components each specify the scale factor in the corrosponding direction
            ///
            /// Does not modify either operand vector
            ///
            /// \return Vector3<T_Type> The newly constructed vector
            /////////////////////////////////////////////////
            Vector3<T_Type> operator/(const Vector3<T_Type> rhs) const{
				Vector3<T_Type> result = *this;
				result /= rhs;
				return result;
			}

            /// @} //end of basic vector operations

			/// @{
			/// @name Advanced Vector operations
			/// \brief Speciallised functions for dealing with one or more Vector3


            /////////////////////////////////////////////////
            /// \brief Returns the dot product between this and another Vector3
            /// Does not modify either operand
            /////////////////////////////////////////////////
			T_Type dot(const Vector3& rhs){
                return this->x * rhs.x + this->y * rhs.y + this->z * rhs.z;
			}

            /////////////////////////////////////////////////
            /// \brief Returns the cross product of two vectors, does not modify
            /// either operand Vector
            /////////////////////////////////////////////////
			Vector3<T_Type> cross(const Vector3& rhs){
                return Vector3<T_Type>(this->y * rhs.z - this->z * rhs.y,
                                       this->z * rhs.x - this->x * rhs.z,
                                       this->x * rhs.y - this->y * rhs.x);
			}

            /////////////////////////////////////////////////
            /// \brief Returns the distance squared between this vector and another
            /// getting distance squared is much quicker than getting actual distance,
            /// use this function when possible (eg: when comparing distances, compare their squares)
            /// \param const Vector3& otherVector -> distance between this and that vector
            /// \return T_Type -> the distance squared
            /// \see getDistanceTo(const Vector3& otherVector)
            /////////////////////////////////////////////////
            T_Type getDistanceToSquared(const Vector3& otherVector){
                return ((this->x - otherVector.x)*(this->x - otherVector.x) +
                        (this->y - otherVector.y)*(this->y - otherVector.y) +
                        (this->z - otherVector.z)*(this->z - otherVector.z));
            }

            /////////////////////////////////////////////////
            /// \brief Returns the distance between this vector and another
            /// getting distance much slower than getting distance squared,
            /// use getDistanceToSquared function when possible (eg: when comparing distances, compare their squares)
            /// \param const Vector3& otherVector -> distance between this and that vector
            /// \return T_Type -> the distance squared
            /// \see getDistanceToSquared(const Vector3& otherVector)
            /////////////////////////////////////////////////
            T_Type getDistanceTo(const Vector3& otherVector){return sqrt(this->getDistanceToSquared(otherVector));}

            /////////////////////////////////////////////////
            /// \brief Gets length squared of this vector (ie: the distance between this and the origin squared)
            /// \see getLength();
            /////////////////////////////////////////////////
            T_Type getLengthSquared(){return this->getDistanceToSquared(Origin);}

            /////////////////////////////////////////////////
            /// \brief Gets length of this vector (ie: the distance between this and the origin)
            /// use getLengthSquared() where possible, it is much faster!
            /// \see getLengthSquared();
            /////////////////////////////////////////////////
            T_Type getLength(){return this->getDistanceTo(Origin);}

            /////////////////////////////////////////////////
            /// \brief Returns a new vector3 containing the cross product between this vector
            /// and the passed in one
            /////////////////////////////////////////////////
            Vector3 getCrossProduct(const Vector3& rhs) const{
                return Vector3( this->y*rhs.z - this->z*rhs.y,
                                this->z*rhs.x - this->x*rhs.z,
                                this->x*rhs.y - this->y*rhs.x);
            }

            /////////////////////////////////////////////////
            /// \brief Normalizes this vector so that its length is 1
            /// \return Reference to this vector for operator chaining
            /////////////////////////////////////////////////
            Vector3& normalize(){
                T_Type length = this->getLength();
                *this /= length;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the normalized version of this vector, does not modify this vector
            /////////////////////////////////////////////////
            Vector3<T_Type> getNormalized() const{
                Vector3<T_Type> result = *this;
                result.normalize();
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Rotates this vector by the specified Quaternion3
            /// Modifies this Vector3, does not modify the quaternion
            ///
            /// \return Reference to this Vector3 for operator chaining
            ///
            /// \note This function will actually rotate the Vector3 by twice the amount stored in
            /// quat, this is because the rotation is done by multipling by the quat and by its conjugate,
            /// this removes the imaginary part but applies the rotation twice. However, as is convention,
            /// the quaternion fromAxisAngleRotation method returns a quaternion represening half the requested
            /// rotation, thus applying it twice applys the full rotation. Any manually constructed quaternions
            /// should also store half the angle you wish to rotate by
            ///
            /// \note This function makes a circular dependency between Vector3 and Quaternion3,
            /// hence it must be defined in a seperate file so it has the full definition
            /// of both the Vector3 class and the Quaterion3 class.
            /// Normally this would be done by pulling in the full Quaternion3.hpp in the
            /// cpp file, but this is a template class so cannot be in a cpp,
            /// hence it is defined in the file Vector3-impl.hpp.
            /// You must ensure you include that file to use this function
            /////////////////////////////////////////////////
            Vector3<T_Type>& rotate(const Quaternion3<T_Type>& quat);


            /////////////////////////////////////////////////
            /// \brief Constructs a new vector that is a copy of this and then rotates it by
            /// the specified Quaternion3, does not modify this vector or the quaternion
            ///
            /// \return Vector3 The new Vector3
            ///
            /// \note This function will actually rotate the Vector3 by twice the amount stored in
            /// quat, this is because the rotation is done by multipling by the quat and by its conjugate,
            /// this removes the imaginary part but applies the rotation twice. However, as is convention,
            /// the quaternion fromAxisAngleRotation method returns a quaternion represening half the requested
            /// rotation, thus applying it twice applys the full rotation. Any manually constructed quaternions
            /// should also store half the angle you wish to rotate by
            ///
            /// \note This function makes a circular dependency between Vector3 and Quaternion3,
            /// hence it must be defined in a seperate file so it has the full definition
            /// of both the Vector3 class and the Quaterion3 class.
            /// Normally this would be done by pulling in the full Quaternion3.hpp in the
            /// cpp file, but this is a template class so cannot be in a cpp,
            /// hence it is defined in the file Vector3-impl.hpp.
            /// You must ensure you include that file to use this function
            /////////////////////////////////////////////////
            Vector3<T_Type> getRotated(const Quaternion3<T_Type>& quat) const;

            /// @} //end of advanced vector operations


            static const Vector3 Origin;
            static const Vector3 UnitX;
            static const Vector3 UnitY;
            static const Vector3 UnitZ;

        };

        template <typename T>
        const Vector3<T> Vector3<T>::UnitX = Vector3<T>(1,0,0);

        template <typename T>
        const Vector3<T> Vector3<T>::UnitY = Vector3<T>(0,1,0);

        template <typename T>
        const Vector3<T> Vector3<T>::UnitZ = Vector3<T>(0,0,1);

        template <typename T>
        const Vector3<T> Vector3<T>::Origin = Vector3<T>(0,0,0);


        typedef Vector3<float> Vector3f;
        typedef Vector3<int> Vector3i;
        typedef Vector3<double> Vector3d;



    }//end of math::
}//end of lie::


#endif // LIE_MATH_VEC3_H
