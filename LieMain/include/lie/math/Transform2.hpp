/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Transform2.hpp
/// \author Jamie Terry
/// \date 2015/07/02
/// \brief Contains the template class lie::math::Transform2
/// 
/// \ingroup math
/////////////////////////////////////////////////

#ifndef LIE_MATH_TRANSFORM2_HPP
#define LIE_MATH_TRANSFORM2_HPP

#include "Vector2.hpp"
#include "Angle.hpp"

namespace lie{
	namespace math{
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Class representing a 2d transformation, has helper functions for
		/// manipulating the transformation as well as a function for obtaining the
		/// 3x3 matrix for the transformation.
		/// This is more convenient than managing a 3x3 matrix itself as once the matrix
		/// has been constructed from a series of rotations, translations, scales, etc,
		/// it is difficult to go back and change any one component.
		/// This class stores a translation, rotation and scale factors. The scale factors
		/// are relative to the local axes of the object, ie, changing the rotation will
		/// also 'rotate' the scale.
		/// When retrieving the final Matrix3x3 representing the transform the translation is
		/// done first, the object is then rotated about it's local origin by the stored
		/// rotation and finally the scale is applied.
		//////////////////////////////////////////////////////////////////////////
		template<typename T>
		class Transform2{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Constructs a new Tranform2 with default values as follows:
			///  - Origin: (0,0)
			///  - Translation: (0,0)
			///  - Rotation: 0 deg
			///  - Scale: (1,1)
			//////////////////////////////////////////////////////////////////////////
			Transform2() : mTranslation(0, 0), mOrigin(0, 0), mRotation(Angle<T>::degrees(0)),
				mScaleFactors(1, 1)
			{ /*empty body*/}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Copy constructor that creates a new instance of Transform2 that
			/// is a deep copy of the specified instance
			//////////////////////////////////////////////////////////////////////////
			Transform2(const Transform2& other) : mTranslation(other.mTranslation),
				mOrigin(other.mOrigin), mRotation(other.mRotation), mScaleFactors(other.mScaleFactors)
			{ /*empty body*/ }

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns const reference the translation component of this Transform2
			//////////////////////////////////////////////////////////////////////////
			const Vector2<T>& getTranslation() const{
				return this->mTranslation;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns const reference to the scale of this Transform2
			//////////////////////////////////////////////////////////////////////////
			const Vector2<T>& getScale() const{
				return this->mScaleFactors;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the rotation of this Transform2
			//////////////////////////////////////////////////////////////////////////
			const Angle<T>& getRotation() const{
				return this->mRotation;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the local origin of this Transform2 about which
			/// rotations and scale operations are applied. The world position of the
			/// origin post transformation is given by this.getTranslation()
			/// \return Vector2 holding the coordinates of the origin
			//////////////////////////////////////////////////////////////////////////
			const Vector2<T>& getOrigin() const{
				return this->mOrigin;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x and y scale factors of this Transform to the corresponding
			/// components of the specified Vector2
			/// \param newFactors Vector2 holding the new x and y scale factors
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setScale(const Vector2<T>& newFactors){
				this->mScaleFactors = newFactors;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x and y scale factors of this transform to the specified
			/// values discarding the previously stored values
			/// \param xFactor The factor to set the x scale factor to
			/// \param yFactor The factor to set the y scale factor to
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setScale(T xFactor, T yFactor){
				this->mScaleFactors.set(xFactor, yFactor);
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x scale factor of this Transform2 without changing the y
			/// scale factor
			/// \param xFactor the new factor to use for the x scale factor
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setScaleX(T xfactor){
				this->mScaleFactors.x = xFactor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the y scale factor of this Transform2 without changing the x
			/// scale factor
			/// \param yFactor the new factor to use for the y scale factor
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setScaleY(T yfactor){
				this->mScaleFactors.y = yFactor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets both the x and y scale factors of this Tranform2 to the same value
			/// \param factor the new factor to use for the scale factors
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setScale(T factor){
				this->mScaleFactors.x = factor;
				this->mScaleFactors.y = factor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x and y components of the translation of this 
			/// Transform to the corresponding components of the specified Vector2
			/// \param newFactors Vector2 holding the new x and y translation components
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setTranslation(const Vector2<T>& translation){
				this->mTranslation = translation;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x and y components of the translation of this transform 
			/// to the specified values discarding the previously stored values
			/// \param x The value to set the x component to
			/// \param y The value to set the y component to
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setTranslation(T x, T y){
				this->mTranslation.set(x, y);
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the x component  of the translation of this Transform2 without 
			/// changing the y component
			/// \param xFactor the new factor to use for the x scale factor
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setTranslationX(T newX){
				this->mTranslation.x = newX;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the y component of the translation of this Transform2 without 
			/// changing the x component
			/// \param newY the new value to use for the y component of the translation
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setTranslationY(T newY){
				this->mTranslation.y = newY;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the rotation of this Transform2 to the specified value
			/// \param newRotation The new value to use as the rotation of this transform
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& setRotation(Angle<T> newRotation){
				this->mRotation = newRotation;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds the specified angle to the rotation of this Transform2
			/// \param delta The angle to add to the current rotation of this transform
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& rotate(Angle<T> delta){
				mRotation += delta;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds the specified vector2 to the translation of this Transform2
			/// \param offset Vector2 containing the x and y values to add to the current
			/// x and y value's of the translation of this transform
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& move(const Vector2<T>& offset){
				mTranslation += offset;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds the specified x and y offsets to the translation of this
			/// Transform2
			/// \param xOffset The value to add to the current x value of the translation
			/// \param yOffset The value to add to the current y value of the translation
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& move(T xOffset, T yOffset){
				mTranslation.x += xOffset;
				mTranslation.y += yOffset;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Multiples the x and y scale factors of this Transform2 by the
			/// corresponding components of the specified vector
			/// \param factors Vector2 holding the x and y factors to multiply the current
			/// scale factors by
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& scale(const Vector2<T>& factors){
				mScaleFactors *= factors;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Multiplies both the x and y scale factors of this Transform2
			/// by the same factor
			/// \param uniformFactor the factor that will be multiplied by the current
			/// scale factors to obtain the new scale factors
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& scale(T uniformFactor){
				mScaleFactors *= uniformFactor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Multiplies the current scaling factors of this transform2 by the
			/// specified value
			/// \param xFactor The factor to multiply the current x scale factor by
			/// \param yFactor The factor to multiple the current y scale factor by
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& scale(T xfactor, T yFactor){
				mScaleFactors.x *= xfactor;
				mScaleFactors.y *= yFactor;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Inverts this Transform2 so that it represents the transformation 
			/// required to 'undo' what it currently represents, if you dont want to modify
			/// this instance use getInverse
			/// \return Reference to this object after it has been inverted for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& invert(){
				mTranslation *= -1;
				mOrigin *= -1;
				mScaleFactors.x = 1 / mScaleFactors.x;
				mScaleFactors.y = 1 / mScaleFactors.y;
				mRotation = -mRotation;
				return *this;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a new Transform2 that is the inverse of this one
			//////////////////////////////////////////////////////////////////////////
			Transform2<T> getInverse() const{
				Transform2 result(*this);
				result.invert();
				return result;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a 3x3 matrix representing the Transformation
			//////////////////////////////////////////////////////////////////////////
			Matrix3x3<T> asMatrix() const{
				Matrix3x3<T> result = Matrix3x3<T>::createTranslation(this->mOrigin * -1);
				result *= Matrix3x3<T>::createRotation(this->mRotation);
				result *= Matrix3x3<T>::createScale(this->mScaleFactors);
				result *= Matrix3x3<T>::createTranslation(this->mTranslation);
				return result;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two Transform2 objects represent the same
			/// transformation
			//////////////////////////////////////////////////////////////////////////
			bool operator==(const Transform2<T>& other) const{
				return	this->mTranslation == other.mTranslation &&
					this->mRotation == other.mRotation &&
					this->mScaleFactors == other.mScaleFactors &&
					this->mOrigin == other.mOrigin;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if two Transform2's do not represent the same 
			/// transformation
			//////////////////////////////////////////////////////////////////////////
			bool operator!=(const Transform2<T>& other) const{
				return !(other == (*this));
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets all attributes of this transform2 to the attributes of another
			/// \return Reference to this object for operator chaining
			//////////////////////////////////////////////////////////////////////////
			Transform2<T>& operator=(const Transform2<T>& other){
				this->mOrigin = other.mOrigin;
				this->mTranslation = other.mTranslation;
				this->mScaleFactors = other.mScaleFactors;
				this->mRotation = other.mRotation;
				return *this;
			}
		private:
			///< The translation relative to the world origin for this Transform2
			Vector2<T> mTranslation;

			///< The local origin of this object about which scales and rotations occur,
			///< the world position of the origin is given by mTranstlation
			Vector2<T> mOrigin;

			///< The x and y scale factors to use for this Transform2
			Vector2<T> mScaleFactors;

			///< The rotation about the local origin for this Transform2
			Angle<T> mRotation;
		};

		typedef Transform2<float> Transform2f;
		typedef Transform2<double> Transform2d;

	}
}

#endif LIE_MATH_TRANSFORM2_HPP