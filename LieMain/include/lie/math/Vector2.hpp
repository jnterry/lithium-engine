#ifndef LIE_MATH_VEC2_HPP
#define LIE_MATH_VEC2_HPP

#include <math.h>
#include "MaxPrecisionOfTypes.hpp"
#include "Vector.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Represents a 2D x and y coordinate which can either be interpreted
        /// as a point in space or  as a Vector describing a line from the origin
        /////////////////////////////////////////////////
        template <typename T>
        class Vector2 : public Vector<2, T>{
        public:
            /////////////////////////////////////////////////
            /// \brief Default constructor, creates the vector (0,0)
            /////////////////////////////////////////////////
			Vector2() : Vector({ 0, 0 }) {}

            /////////////////////////////////////////////////
            /// \brief Constructor that makes vector from passed in values
            /////////////////////////////////////////////////
			Vector2(T xVal, T yVal) : Vector({xVal, yVal}) {}

            /////////////////////////////////////////////////
            /// \brief Copy constructor, copies values from other vector into new one
            /////////////////////////////////////////////////
            template <typename T_Other>
			Vector2(const Vector2<T_Other>& otherVector) : Vector({ otherVector.x, otherVector.y }){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Returns the distance squared between this vector and another
            /// getting distance squared is much quicker than getting actual distance,
            /// use this function when possible (eg: when comparing distances, compare their squares)
            /// \param const Vector2& otherVector -> distance between this and that vector
            /// \return T -> the distance squared
            /// \see getDistanceTo(const Vector3& otherVector)
            /////////////////////////////////////////////////
            template <typename T_Other>
            float getDistanceToSquared(const Vector2<T_Other>& otherVector){
                return  ((this->x-otherVector.x)*(this->x-otherVector.x)) +
                        ((this->y-otherVector.y)*(this->y-otherVector.y));
            }

            /////////////////////////////////////////////////
            /// \brief Returns the distance between this vector and another
            /// getting distance much slower than getting distance squared,
            /// use getDistanceToSquared function when possible (eg: when comparing distances, compare their squares)
            /// \param const Vector2& otherVector -> distance between this and that vector
            /// \return float -> the distance squared
            /// \see getDistanceToSquared(const Vector3& otherVector)
            /////////////////////////////////////////////////
            template <typename T_Other>
            float getDistanceTo(const Vector2<T_Other>& otherVector){return sqrt(this->getDistanceToSquared<T_Other>(otherVector));}

            /////////////////////////////////////////////////
            /// \brief Gets length squared of this vector (ie: the distance between this and the origin squared)
            /// \see getLength();
            /////////////////////////////////////////////////
            float getLengthSquared(){return this->getDistanceToSquared<T>(Vector2<T>());}

            /////////////////////////////////////////////////
            /// \brief Gets length of this vector (ie: the distance between this and the origin)
            /// use getLengthSquared() where possible, it is much faster!
            /// \see getLengthSquared();
            /////////////////////////////////////////////////
            float getLength(){return this->getDistanceTo<T>(Vector2<T>());}

            /////////////////////////////////////////////////
            /// \brief Compares this vector to another, returns true if all components are equal
            /////////////////////////////////////////////////
            template<typename T_Other>
            bool operator==(const Vector2<T_Other>& rhs) const{
                return (this->x == rhs.x && this->y == rhs.y);
            }

            /////////////////////////////////////////////////
            /// \brief Compares this vector to another, returns false if all components are equal
            /////////////////////////////////////////////////
            template<typename T_Other>
            bool operator!=(const Vector2<T_Other>& rhs){return !this->operator==(rhs);}

            /////////////////////////////////////////////////
            /// \brief Adds x and y of another vector to the x and y components of this vector.
            /// Modifies this vector
            /////////////////////////////////////////////////
            template<typename T_Other>
            Vector2<T>& operator+=(const Vector2<T_Other>& rhs){
                this->x += rhs.x;
                this->y += rhs.y;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Subtracts rhs's x and y components from corrosponding components of this vector.
            /// Modifies this vector and returns it
            /////////////////////////////////////////////////
            template<typename T_Other>
            Vector2<T>& operator-=(const Vector2<T_Other>& rhs){
                this->x -= rhs.x;
                this->y -= rhs.y;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies this vectors x and y components by the passed in scalar
            /// Modifies this vector
            /////////////////////////////////////////////////
            Vector2<T>& operator*=(const T rhs){
                this->x *= rhs;
                this->y *= rhs;
                return *this;
            }

			//////////////////////////////////////////////////////////////////////////
			/// \brief Performs component wise multiplication of this vector by another,
			/// use .dot or  .cross to perform dot and cross products
			//////////////////////////////////////////////////////////////////////////
			Vector2<T>& operator*=(const Vector2<T> rhs){
				this->x *= rhs.x;
				this->y *= rhs.y;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Divides this vectors x and y and z components by the passed in scalar
            /// Modifies this vector
            /////////////////////////////////////////////////
            Vector2<T>& operator/=(const T rhs){
                this->x /= rhs;
                this->y /= rhs;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Adds two vectors leaving both unchanged, returning result as new vector
            /////////////////////////////////////////////////
            template<typename T_Other>
            Vector2 operator+(const Vector2<T_Other>& rhs) const{
                Vector2<T_Other> result(*this);
                result += rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Subtracts rhs vector from lhs vector leaving both unchanged, returning result as new vector
            /////////////////////////////////////////////////
            template<typename T_Other>
            Vector2<T_Other> operator-(const Vector2<T_Other>& rhs) const{
                Vector2<T_Other> result(*this);
                result -= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Creates a new vector that is a copy of this one and multiplies it by the passed in scaler, then
            /// returns the copy
            ///
            /// \note The returned vector will be of the higher precision type of the scalar and the vector
            /// Eg: Vector2<int> * float = Vector2<float>
            /////////////////////////////////////////////////
            template<typename T_Scalar>
            Vector2<typename lie::math::MaxPrecisionOfTypes<T, T_Scalar>::type > operator*(T_Scalar rhs) const{
                Vector2<typename lie::math::MaxPrecisionOfTypes<T, T_Scalar>::type> result(*this);
                result *= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Divides each component of this vector by some scalar (thus scaling the vector)
            /////////////////////////////////////////////////
            Vector2<T> operator/(const T rhs) const{
                Vector2<T> result(this);
                result /= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Sets values of this vector equal to values in some other vector
            /////////////////////////////////////////////////
            Vector2<T>& operator=(const Vector2<T>& rhs){
                this->x = rhs.x;
                this->y = rhs.y;
                return *this;
            }

			/////////////////////////////////////////////////
			/// \brief Sets both components to the specified value
			/// \return reference to this vector for operator chaining
			/////////////////////////////////////////////////
			Vector2<T>& setAll(T newValue){
				this->x = newValue;
				this->y = newValue;
				return *this;
			}

            /////////////////////////////////////////////////
            /// \brief Sets both the x and y values to the passed in values
            /// Convenience function only, eg:
            /// \code
            /// vec.x = 1;
            /// vec.y = 2;
            /// \endcode
            /// becomes:
            /// \code
            /// vec.set(1,2);
            /// \endcode
            /// The function also returns a reference to this vector, allowing operator chaining
            /////////////////////////////////////////////////
            Vector2<T>& set(T newX, T newY){
                this->x = newX;
                this->y = newY;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Sets this vector equal to an array of values. Allows the folling syntax
            /// \code
            /// Vector2<float> myVector;
            /// myVector = {0.5f, 1.8f};
            /// \endcode
            /////////////////////////////////////////////////
            Vector2<T>& operator=(const T values[2]){
                this->x = values[0];
                this->y = values[1];
                return *this;
            }

            static const Vector2<T> UnitX;
            static const Vector2<T> UnitY;
            static const Vector2<T> Origin;
        };

        template <typename T>
        const Vector2<T> Vector2<T>::UnitX = Vector2<T>(1,0);

        template <typename T>
        const Vector2<T> Vector2<T>::UnitY = Vector2<T>(0,1);

        template <typename T>
        const Vector2<T> Vector2<T>::Origin = Vector2<T>(0,0);

        typedef Vector2<int> Vector2i;
        typedef Vector2<unsigned int> Vector2ui;
        typedef Vector2<float> Vector2f;
        typedef Vector2<double> Vector2d;

    }
}

#endif // LIE_MATH_VEC2_HPP
