/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Matrix3x3.hpp
/// \author Jamie Terry
/// \brief Contains the lie::math::Matrix3x3 class
///
/// \ingroup math
/////////////////////////////////////////////////
#ifndef LIE_MATH_MATRIX3X3_HPP
#define LIE_MATH_MATRIX3X3_HPP

#include "Angle.hpp"
#include "SquareMatrix.hpp"
#include "Vector2.hpp"
#include "MaxPrecisionOfTypes.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Derived from the SquareMatrix class
        /// and provides speciallised functions which apply only to
        /// a 3x3 matrix, eg generating 2d transformations
        /////////////////////////////////////////////////
        template<typename T_Type>
        class Matrix3x3 : public SquareMatrix<T_Type, 3>{
        public:
            /////////////////////////////////////////////////
            /// \brief static const member Matrix3x3, represents identity matrix
            /// (1 0 0)
            /// (0 1 0)
            /// (0 0 1)
            /////////////////////////////////////////////////
            static const Matrix3x3<T_Type> Identity;

            /////////////////////////////////////////////////
            /// \brief Default constructor which returns an identity matrix
            /////////////////////////////////////////////////
            Matrix3x3() :
            SquareMatrix<T_Type, 3>({1,0,0, 0,1,0, 0,0,1})
            {/*Empty body*/}

            /////////////////////////////////////////////////
            /// \brief Constructor taking array of T_Types to set matrix values to
            /////////////////////////////////////////////////
            Matrix3x3(std::initializer_list<T_Type> data) :
            SquareMatrix<T_Type, 3>(data)
            {/*Empty body*/}

            /////////////////////////////////////////////////
            /// \brief Multiplies this matrix by a vector2 returning the transformed vector.
            /// The returned vector will contain the higher precision type out of the matrix and the vector,
            /// eg, a float vector and an int matrix will produce a float vector. An int vector and a float matrix
            /// will produce a float vector
            ///
            /// \note This functions assumes the "z" component is 1 and renormalizes the point to output it
            /// as a Vector2, this is so we can represent translations with matricies, 2x2 matricies can
            /// not represent translations of 2d points
            /////////////////////////////////////////////////
            template<typename T_Vec>
            Vector2<typename lie::math::MaxPrecisionOfTypes<T_Type, T_Vec>::type > operator* (const Vector2<T_Vec>& rhs) const{

                typename lie::math::MaxPrecisionOfTypes<T_Type, T_Vec>::type w =    this->mValues[6]*rhs.x +
                                                                                    this->mValues[7]*rhs.y +
                                                                                    this->mValues[8];

                return Vector2<typename lie::math::MaxPrecisionOfTypes<T_Type, T_Vec>::type >(
                            (this->mValues[0]*rhs.x + this->mValues[1]*rhs.y + this->mValues[2])/w,
                            (this->mValues[3]*rhs.x + this->mValues[4]*rhs.y + this->mValues[5])/w);
            }

            /////////////////////////////////////////////////
            /// \brief Muliplies two matricies together and returns the result as
            /// a new matrix, does not modify either operand matrix
            /////////////////////////////////////////////////
            Matrix3x3 operator*(const Matrix3x3<T_Type>& rhs){
                return Matrix3x3({
                    this->mValues[0]*rhs.mValues[0] + this->mValues[1]*rhs.mValues[3] + this->mValues[2]*rhs.mValues[6],
                    this->mValues[0]*rhs.mValues[1] + this->mValues[1]*rhs.mValues[4] + this->mValues[2]*rhs.mValues[7],
                    this->mValues[0]*rhs.mValues[2] + this->mValues[1]*rhs.mValues[5] + this->mValues[2]*rhs.mValues[8],

                    this->mValues[3]*rhs.mValues[0] + this->mValues[4]*rhs.mValues[3] + this->mValues[5]*rhs.mValues[6],
                    this->mValues[3]*rhs.mValues[1] + this->mValues[4]*rhs.mValues[4] + this->mValues[5]*rhs.mValues[7],
                    this->mValues[3]*rhs.mValues[2] + this->mValues[4]*rhs.mValues[5] + this->mValues[5]*rhs.mValues[8],

                    this->mValues[6]*rhs.mValues[0] + this->mValues[7]*rhs.mValues[3] + this->mValues[8]*rhs.mValues[6],
                    this->mValues[6]*rhs.mValues[1] + this->mValues[7]*rhs.mValues[4] + this->mValues[8]*rhs.mValues[7],
                    this->mValues[6]*rhs.mValues[2] + this->mValues[7]*rhs.mValues[5] + this->mValues[8]*rhs.mValues[8]});

            }

            /////////////////////////////////////////////////
            /// \brief Multiplies this matrix by another and modifies this (ths lhs)
            /// matrix, returns reference to modified lhs matrix for operator chaining
            /////////////////////////////////////////////////
            Matrix3x3<T_Type>& operator*=(const Matrix3x3<T_Type>& rhs){
                (*this) = (*this)*rhs;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies two matricies together leaving both unchanged
            /// and returning the result
            ///
            /// \note the result's type will be the higher precision type between
            /// the types stored by the two matricies, eg Matrix3x3<float> * Matrix3x3<int> would return
            /// a Matrix3x3<float>
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix3x3<typename lie::math::MaxPrecisionOfTypes<T_Type, T2>::type> operator* (const Matrix3x3<T2>& rhs) const{
                return Matrix3x3<typename lie::math::MaxPrecisionOfTypes<T_Type, T2>::type>({
                        (this->mValues[0]*rhs.mValues[0] + this->mValues[1]*rhs.mValues[3] + this->mValues[2]*rhs.mValues[6]),
                        (this->mValues[0]*rhs.mValues[1] + this->mValues[1]*rhs.mValues[4] + this->mValues[2]*rhs.mValues[7]),
                        (this->mValues[0]*rhs.mValues[2] + this->mValues[1]*rhs.mValues[5] + this->mValues[2]*rhs.mValues[8]),

                        (this->mValues[3]*rhs.mValues[0] + this->mValues[4]*rhs.mValues[3] + this->mValues[5]*rhs.mValues[6]),
                        (this->mValues[3]*rhs.mValues[1] + this->mValues[4]*rhs.mValues[4] + this->mValues[5]*rhs.mValues[7]),
                        (this->mValues[3]*rhs.mValues[2] + this->mValues[4]*rhs.mValues[5] + this->mValues[5]*rhs.mValues[8]),

                        (this->mValues[6]*rhs.mValues[0] + this->mValues[7]*rhs.mValues[3] + this->mValues[8]*rhs.mValues[6]),
                        (this->mValues[6]*rhs.mValues[1] + this->mValues[7]*rhs.mValues[4] + this->mValues[8]*rhs.mValues[7]),
                        (this->mValues[6]*rhs.mValues[2] + this->mValues[7]*rhs.mValues[5] + this->mValues[8]*rhs.mValues[8])});
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix3x3 instance that will translate a vertex by
            /// specified amounts in x and y directions
            /////////////////////////////////////////////////
            static Matrix3x3 createTranslation(T_Type xOffset, T_Type yOffset){
                return Matrix3x3({1,0,0, 0,1,0, xOffset,yOffset,1});
            }

            /////////////////////////////////////////////////
            /// \brief Creates a translation which will translate a point
            /// by the specified vector2 representing an offset from
            /// the original point
            /////////////////////////////////////////////////
            static Matrix3x3 createTranslation(const Vector2<T_Type>& translation){
                return createTranslation(translation.x, translation.y);
            }

            /////////////////////////////////////////////////
            /// \brief Creates a scale matrix where the object will be scaled
            /// equally in both the x and y direction by the specified factor
            /////////////////////////////////////////////////
            static Matrix3x3 createScale(T_Type uniformFactor){
                return Matrix3x3({uniformFactor,0,0, 0,uniformFactor,0, 0,0,1});
            }

            /////////////////////////////////////////////////
            /// \brief Creates a scale matrix where the object will be scaled
            /// by 2 different factors in the x and y direction
            /////////////////////////////////////////////////
            static Matrix3x3 createScale(T_Type xFactor, T_Type yFactor){
                return Matrix3x3({xFactor,0,0, 0,yFactor,0, 0,0,1});
            }

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creats a scale matrix where the object will be scaled by factors in
			/// the x and y direction
			/// \param factors Vector2 where the x and y components store the x and y scale
			/// factors respectively
			//////////////////////////////////////////////////////////////////////////
			static Matrix3x3 createScale(const Vector2<T_Type>& factors){
				return Matrix3x3<T_Type>::createScale(factors.x, factors.y);
			}

            /////////////////////////////////////////////////
            /// \brief Creates a matrix which will rotate a point around
            /// the origin by the specified angle
            /////////////////////////////////////////////////
            static Matrix3x3 createRotation(Angle<T_Type> angle){
				T_Type a = angle.asRadians();
                T_Type c = cos(a);
                T_Type s = sin(a);
                return Matrix3x3({c,-s,0, s,c,0, 0,0,1});
            }

        };

        template <typename T>
        const Matrix3x3<T> Matrix3x3<T>::Identity = Matrix3x3<T>({1,0,0, 0,1,0, 0,0,1});

        typedef Matrix3x3<float> Matrix3x3f;
        typedef Matrix3x3<double> Matrix3x3d;
        typedef Matrix3x3<int> Matrix3x3i;

    }
}



#endif // LIE_MATH_MATRIX3X3_HPP
