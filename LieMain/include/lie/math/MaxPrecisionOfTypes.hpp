#ifndef LIE_MATH_MAXPRECISIONOFTYPES_HPP
#define LIE_MATH_MAXPRECISIONOFTYPES_HPP

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Template class which can be used for determining which type from a list of types
        /// has the best precision.
        /// This is defined by what would be the result of adding the two types, eg:
        /// \code
        /// int + int = int
        /// int + float = float
        /// int + double = double
        /// int + unsigned int = unsigned int
        /// int + char = int
        /// char + float = float
        /// etc...
        /// \endcode
        /////////////////////////////////////////////////
        template<typename T1, typename T2>
        class MaxPrecisionOfTypes{
        public:
            ///this will be typedefed to the more precise of the two types
            typedef decltype(((T1)(1))+((T2)(1))) type;
        };

    }
}

#endif // LIE_MATH_MAXPRECISIONOFTYPES_HPP
