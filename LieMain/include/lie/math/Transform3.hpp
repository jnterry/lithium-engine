#ifndef LIE_MATH_TRANSFORMATION_HPP
#define LIE_MATH_TRANSFORMATION_HPP

#include "Vector3.hpp"
#include "Quaternion3.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief A Transform3 can be used to store and manipulate any 3d transformation in a more abstract
        /// way than directly dealing with a Matrix4x4, this is usually more convenient eg retrieving the
        /// translation component of a Transform3 can be done with a simple .getTranslation, this is much
        /// harder to do if dealing directly with Matrix4x4s
        ///
        /// A Transform3 is made up of the following parts:
        /// - Position relative the to worlds origin, this is unaffected by
        ///   local rotation, ie, a position of (1,2,3) is the same for an object
        ///   regardless of whether it has been rotated around some axis
        /// - Local rotation around the objects center
        /// - Scale (in each LOCAL axis direction, ie, the scale (1,2,1) will
        ///   always double the height of an object regardless of its rotation
        ///
        /// There is no component representing a rotation post-translation,
        /// eg, translate by +5 units on the x axis and then rotated 90 degrees anticlockwise
        /// around the z axis (hence resulting in the translation of +5 units in the y direction)
        /// If a post-translation rotation is required you should set the position component
        /// to a Vector3 which is rotated appropriately
        /////////////////////////////////////////////////
        template<typename T_Type>
        class Transform3{
        public:
            /// Position relative to world origin
            Vector3<T_Type> position;

            /// Scale where each component represents the scale factor in that
            /// direction
            Vector3<T_Type> scale;

            /// Rotation around objects position, this is the orientation of the object
            /// rather than a rotation around some point in space
            Quaternion3<T_Type> rotation;

            /// @{
            /// @name Constructors and Assignment Operator
            /// \brief


            /////////////////////////////////////////////////
            /// \brief Constructs a default Transform3 with the following attributes:
            ///  - Translation of (0,0,0)
            ///  - Scale of (1,1,1)
            ///  - Rotation of 0 degrees around the z axis
            /////////////////////////////////////////////////
            Transform3() : position(Vector3<T_Type>::Origin), scale(Vector3<T_Type>(1,1,1)),
            rotation(Quaternion3<T_Type>(Vector3<T_Type>(0,0,1),Angle<T_Type>::degrees(0))){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Constructs a Transform3 with the specified position
            /// Can optionally pass in a scale and rotation,
            /// if these are not passed in the default values are used
            /////////////////////////////////////////////////
            Transform3( Vector3<T_Type> pos, Vector3<T_Type> scale = Vector3<T_Type>(1,1,1),
                        Quaternion3<T_Type> rot = Quaternion3<T_Type>(Vector3<T_Type>(0,0,1),0))
            : position(pos), scale(scale), rotation(rot){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Constructs a Transform with the specified position
            /// and local rotation, uses a default scale of (1,1,1)
            /////////////////////////////////////////////////
            Transform3(Vector3<T_Type> pos, Quaternion3<T_Type> rot)
            : position(pos), scale(Vector3<T_Type>(1,1,1)), rotation(rot){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Creates deep copy of the specified Transform3
            /////////////////////////////////////////////////
            Transform3(const Transform3<T_Type>& other)
            : position(other.position), scale(other.scale), rotation(other.rotation){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Sets all values of this Transform3 equal to the
            /// values in the other Transform3
            /////////////////////////////////////////////////
            Transform3<T_Type>& operator=(const Transform3<T_Type>& other){
                this->position = other.position;
                this->scale = other.scale;
                this->rotation = other.rotation;
				return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Calculates and returns the Matrix4x4 which represents this
            /// Transform3's transformation
            /////////////////////////////////////////////////
            Matrix4x4<T_Type> asMatrix(){
                Matrix4x4<T_Type> result = Matrix4x4<T_Type>::Identity;

                result *= Matrix4x4<T_Type>::createScale(this->scale);
                result *= this->rotation.asMatrix();
                result *= Matrix4x4<T_Type>::createTranslation(this->position);
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Constructs and returns new Transform3 that is the
            /// inverse of this one, ie: it applies the exact oposite transformation
            /// to this one
            /////////////////////////////////////////////////
            Transform3<T_Type> getInverse(){
                Transform3<T_Type> result(*this);
                result.invert();
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Inverts all aspects of this Transform3 so that
            /// it applies the oposite transformation to its old one
            ///
            /// \return Reference to this Transform3 for operator chaining
            /////////////////////////////////////////////////
            Transform3<T_Type>& invert(){
                this->position *= -1;

                this->scale.x = 1/this->scale.x;
                this->scale.y = 1/this->scale.y;
                this->scale.z = 1/this->scale.z;

                this->rotation.invert();

                return *this;
            }
        };

        typedef Transform3<float> Transform3f;
        typedef Transform3<double> Transform3d;

    }
}

#endif // LIE_MATH_TRANSFORMATION_HPP
