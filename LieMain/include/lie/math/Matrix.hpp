/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Matrix.hpp
/// \author Jamie Terry
/// \brief Contains the lie::math::Matrix class
///
/// \ingroup math
/////////////////////////////////////////////////
#ifndef LIE_MATH_MATRIX_HPP
#define LIE_MATH_MATRIX_HPP

#include <lie/core/ArrayIterator.hpp>
#include <initializer_list>
#include <lie/core\ExcepOutOfRange.hpp>
#include <lie/core\Convert.hpp>

#include "MaxPrecisionOfTypes.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Generic Matrix class which can, using template parameters,
        /// represent a matrix of any type with any number of rows and columns.
        /// Derived classes should be used where appropriate, any square matrices should
        /// use SquareMatrix, 3x3 and 4x4 matrices also have (indirectly) derived classes
        ///
        /// \note To write general code for loops are used, if the compiler does not do loop unrolling
        /// these will be slower than custom written functions for specific sized matrices, profiling
        /// needs to be done
        /////////////////////////////////////////////////
        template<typename T, unsigned int TRows, unsigned int TCols>
        class Matrix{
            //we want the matrix class with any tparams to be friends of all other matrix tparams
            template<typename fT, unsigned int fTRows, unsigned int fTCols>
            friend class Matrix;

            ///Typedefed to the type of value this matrix stores
            typedef T type;

            ///the type of iterator returned by begin() and end()
            ///this iterator is non-const and can manipulate the matrix elements
            typedef lie::core::ArrayIterator<T> iterator;

            ///the type of iterator returned by cbegin() and cend()
            ///this iterator is const and represents elements as read only
            typedef lie::core::ArrayIterator<const T> const_iterator;
        public:
            /////////////////////////////////////////////////
            /// \brief Helper class which allows client code to get elements by doing matrix[][]
            /// Client code does not need to construct or pass around these, its purpose is to
            /// allow the matrix[][] syntax by having Matrix->operator[] return a class 
            /// (ie: this class) with a [] operator
            /////////////////////////////////////////////////
            static class _AccessHelper{
            public:
                _AccessHelper(unsigned int colIndex, T* valueArray)
                : mColIndex(colIndex), mValuesPtr(valueArray){
                    //empty body
                }

                T& operator[](unsigned int rowIndex){
                    return mValuesPtr[(mColIndex*TRows)+rowIndex];
                }

                const T& operator[](unsigned int rowIndex) const{
                    return mValuesPtr[(mColIndex*TRows)+rowIndex];
                }

				friend std::ostream& operator<<(std::ostream& os, const _AccessHelper& ah){
					os << "(";
					for (int r = 0; r < TRows - 1; ++r){
						os << ah[r] << ",";
					}
					os << ah[TRows - 1] << ")";
					return os;
				}
            private:
                ///Reference to the array containing the matrix values
                T* mValuesPtr;

                ///which column of the matrix this access helper refers to
                unsigned int mColIndex;
            }; //end of _AccessHelper class

            /////////////////////////////////////////////////
            /// @{
            /// @name Constructors and Assignment Operators
            /// \brief
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Default constructor, initializes all values to 0
            /////////////////////////////////////////////////
            Matrix(){
                for(unsigned int i = 0; i < TRows*TCols; ++i){
                    mValues[i] = 0;
                }
            }

            /////////////////////////////////////////////////
            /// \brief Constructor which takes a brace initializer list,
            /// elements are set to the corresponding element of the list.
            /// Element indices (for a 3x4 matrix) are as follows:
            /// \code
            ///  0  1  2  3
            ///  4  5  6  7
            ///  8  9  10  11
            /// \endcode
            /////////////////////////////////////////////////
            Matrix(std::initializer_list<T> dataArray){
                for(unsigned int i = 0; i < TRows*TCols; ++i){
                    mValues[i] = dataArray.begin()[i];
                }
            }

            /////////////////////////////////////////////////
            /// \brief Copy constructor that takes another matrix
            /// of any type with the same order.
            /// Can therefore be used to cast matrices, eg from Matrix<float, 3, 2>
            /// to Matrix<int, 3, 2>
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix(const Matrix<T2, TRows, TCols>& other){
                for(int i = 0; i < TRows*TCols; ++i){
                    mValues[i] = other.mValues[i];
                }
            }

            /////////////////////////////////////////////////
            /// \brief Sets all elements of this matrix equal to corresponding elements of other matrix
            /// \return Matrix<T, TRows, TCols>& for operator chaining
            /// \see Matrix<T, TRows, TCols>& operator= (float data[16])
            /////////////////////////////////////////////////
            Matrix<T, TRows, TCols>& operator= (const Matrix<T, TRows, TCols>& rhs){
                if(this != &rhs){
                    for(int i = 0; i < TRows*TCols; ++i){
                        this->mValues[i] = rhs.mValues[i];
                    }
                }
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Sets all elements of this matrix equal to corresponding element of array
            /// The array must have TRows*TCols elements
            /// Example:
            /// \code
            /// Matrix<int, 4, 4> = {1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16};
            /// \endcode
            /// \return Matrix<T, TRows, TCols>& for operator chaining
            /// \see  Matrix<T, TRows, TCols>& operator= (const Matrix<T, TRows, TCols>& rhs);
            /////////////////////////////////////////////////
            Matrix<T, TRows, TCols>& operator= (float data[TRows*TCols]){
                for(int i = 0; i < TRows*TCols; ++i){
                    this->mValues[i] += data[i];
                }
                return *this;
            }

            /// @}
            //end of constructors and assignment operators

            /////////////////////////////////////////////////
            /// @{
            /// @name Attribute Functions
            /// \brief
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Returns the number of rows this matrix has
            /// This is a constexpr function and is determined at compile time.
            /// It is most useful when calling functions which return matrices
            /// of different orders, eg: getTransposed()
            /////////////////////////////////////////////////
            constexpr unsigned int getRowCount() const{
                return TRows;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the number of columns this matrix has.
            /// This is a constexpr function and is determined at compile time.
            /// It is most useful when calling functions which return matrices
            /// of different orders, eg: getTransposed()
            /////////////////////////////////////////////////
            constexpr unsigned int getColumnCount() const{
                return TCols;
            }

            /// @}
            //end of attribute functions

            /////////////////////////////////////////////////
            /// @{
            /// @name Element Access Methods
            /// \brief Functions used to access single elements of the matrix,
            ///
            /// \note For all functions accessing element by a 2 index coordinate elements are as follows:
            /// \code
            /// 0,0  1,0  2,0  3,0
            /// 0,1  1,1  2,1  3,1
            /// 0,2  1,2  2,2  3,2
            /// 0,3  1,3  2,3  3,3
            /// \endcode
            /// However, elements are stored in memory in the following order:
            /// \code
            /// 0  1  2  3
            /// 4  5  6  7
            /// 8  9  10 11
            /// 12 13 14 15
            /// \endcode
            /// This ordering can be used with the begin() function,
            /// eg, for a 4x4 matrix matrix.begin()[8] == matrix.at(0,2) == matrix[0][2]
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Returns a const _AccessHelper which allows you to access a const reference to
            /// any element in the matrix with the syntax [column index][row index]
            ///
            /// \note The [][] syntax and at function both present the matrix
            /// in column major format, eg, the 4x4 matrix have the following element indexes:
            /// \code
            /// {[0][0] [1][0] [2][0] [3][0]}
            /// {[0][1] [1][1] [2][1] [3][1]}
            /// {[0][2] [1][2] [2][2] [3][2]}
            /// {[0][3] [1][3] [2][3] [3][3]}
            /// \endcode
            /// This is largely to match the glsl specification
            /////////////////////////////////////////////////
            const _AccessHelper operator[](unsigned int columnIndex) const{
                return _AccessHelper(columnIndex, this->mValues);
            }

            /////////////////////////////////////////////////
            /// \brief Returns an _AccessHelper which allows you to access a reference to
            /// any element in the matrix with the syntax [column index][row index]
            ///
            /// \note The [][] syntax and at function both present the matrix
            /// in column major format, eg, the 4x4 matrix have the following element indexes:
            /// \code
            /// {[0][0] [1][0] [2][0] [3][0]}
            /// {[0][1] [1][1] [2][1] [3][1]}
            /// {[0][2] [1][2] [2][2] [3][2]}
            /// {[0][3] [1][3] [2][3] [3][3]}
            /// \endcode
            /// This is largely to match the glsl specification
            /////////////////////////////////////////////////
            _AccessHelper operator[](unsigned int columnIndex){
                return _AccessHelper(columnIndex, this->mValues);
            }

            /////////////////////////////////////////////////
            /// \brief Returns the element at the specified row and column
            /// Unlike the [][] operator this function does bounds checking and
            /// will throw an instance of ExcepOutOfRange if either col or row
            /// is too high
            /// This is the const version of the function
            ///
            /// \note The [][] syntax and at function both present the matrix
            /// in column major format, eg, the 4x4 matrix have the following element indexes:
            /// \code
            /// {[0][0] [1][0] [2][0] [3][0]}
            /// {[0][1] [1][1] [2][1] [3][1]}
            /// {[0][2] [1][2] [2][2] [3][2]}
            /// {[0][3] [1][3] [2][3] [3][3]}
            /// \endcode
            /// This is largely to match the glsl specification
            /////////////////////////////////////////////////
			const T& at(unsigned int col, unsigned int row) const  throw(lie::core::ExcepOutOfRange<unsigned int>){
				if (col > TCols - 1){
					throw lie::core::ExcepOutOfRange<unsigned int>(__FUNCTION__, "col", col, 0, TCols - 1,
						"Attempted to matrix element");
				}
				if (row > TRows - 1){
					throw lie::core::ExcepOutOfRange<unsigned int>(__FUNCTION__, "row", row, 0, TRows - 1,
						"Attempted to matrix element");
				}
                return this->mValues[(col*TRows)+row];
            }

            /////////////////////////////////////////////////
            /// \brief Returns the element at the specified row and column
            /// Unlike the [][] operator this function does bounds checking and
            /// will throw an instance of ExcepOutOfRange if either col or row
            /// is too high
            ///
            /// \note The [][] syntax and at function both present the matrix
            /// in column major format, eg, the 4x4 matrix have the following element indexes:
            /// \code
            /// {[0][0] [1][0] [2][0] [3][0]}
            /// {[0][1] [1][1] [2][1] [3][1]}
            /// {[0][2] [1][2] [2][2] [3][2]}
            /// {[0][3] [1][3] [2][3] [3][3]}
            /// \endcode
            /// This is largely to match the glsl specification
            /////////////////////////////////////////////////
			T& at(unsigned int col, unsigned int row) throw(lie::core::ExcepOutOfRange<unsigned int>){
				if (col > TCols - 1){
					throw lie::core::ExcepOutOfRange<unsigned int>(__FUNCTION__, "col", col, 0, TCols - 1,
						"Attempted to matrix element");
				}
				if (row > TRows - 1){
					throw lie::core::ExcepOutOfRange<unsigned int>(__FUNCTION__, "row", row, 0, TRows - 1,
						"Attempted to matrix element");
				}
                return this->mValues[(col*TRows)+row];
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the first element of this matrix
            /////////////////////////////////////////////////
            iterator begin(){
                return iterator(&this->mValues[0], 0);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const_iterator to the first element of this matrix
            /////////////////////////////////////////////////
            const_iterator cbegin(){
                return const_iterator(&this->mValues[0], 0);
            }

            /////////////////////////////////////////////////
            /// \brief Returns an iterator to the element after the last element of this matrix
            /// \note An iterator returned by this function will produce garbage data if dereferenced.
            /// Dereferencing an iterator returned by this function caused undefined behavior,
            /// it should be used in comparisons only. Eg:
            /// \code
            /// for(auto it = mat.begin(); it != mat.end(); ++it){
            ///     //do something with the iterator here
            ///     it->x += 5;
            /// }
            /// \endcode
            /////////////////////////////////////////////////
            iterator end(){
                return iterator(&this->mValues[0], TRows*TCols);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a const_iterator to the element after the last element of this matrix
            /// \note An iterator returned by this function will produce garbage data if dereferenced.
            /// Dereferencing an iterator returned by this function caused undefined behavior,
            /// it should be used in comparisons only. Eg:
            /// \code
            /// for(auto it = mat.begin(); it != mat.end(); ++it){
            ///     //do something with the iterator here
            /// }
            /// \endcode
            /////////////////////////////////////////////////
            const_iterator cend(){
                return const_iterator(&this->mValues[0], TRows*TCols);
            }
            /// @}
            //end of element access functions

            /////////////////////////////////////////////////
            /// @{
            /// @name Arithmetic and Comparison Operators
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Returns true if two matrices of the same order have
            /// all corresponding elements identical to each other, else
            /// returns false
            /////////////////////////////////////////////////
            bool operator==(const Matrix<T, TRows, TCols>& other){
                //not sure the compiler can do loop unrolling here, may need to implement
                //custom version in fixed size matrix classes, :TODO: profile
                for(int i = 0; i < TRows*TCols; ++i){
                    if(this->mValues[i] != other.mValues[i]){
                        return false;
                    }
                }
                return true;
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if two matrices of the same order have
            /// any corresponding elements which are different, else returns false
            /////////////////////////////////////////////////
            bool operator!=(const Matrix<T, TRows, TCols>& other){
                return !this->operator==(other);
            }

            /////////////////////////////////////////////////
            /// \brief Modified lhs matrix by adding corresponding element of rhs matrix, leaves rhs unchanged
            /// \return Matrix<T, TRows, TCols>& for operator chaining (eg: mat1 += (mat2 - mat3))
            /// \see operator+(const Matrix<T, TRows, TCols>& rhs)
            /// \note This can be used to add matrices of different types but they must be of the same order
            /// (ie: same number of rows and columns), if the matrices contain different types then returns
            /// a matrix of the lhs operand type
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix<T, TRows, TCols>& operator+=(const Matrix<T2, TRows, TCols>& rhs){
                for(int i = 0; i < TRows*TCols; ++i){
                    this->mValues[i] += rhs.mValues[i];
                }
                return *this;
            }
            /////////////////////////////////////////////////
            /// \brief Modified lhs matrix by subtracting corresponding element of rhs matrix, leaves rhs unchanged
            /// \return reference to this for operating chainingMatrix
            /// \see operator-(const Matrix<T, TRows, TCols>& rhs)
            /// \note This can be used to add matrices of different types but they must be of the same order
            /// (ie: same number of rows and columns), if the matrices contain different types then what occurs
            /// is dependent on c++ rules, eg: int i = (2 - 1.9f); will set i to 1
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix<T, TRows, TCols>& operator-=(const Matrix<T2, TRows, TCols>& rhs){
                for(int i = 0; i < TRows*TCols; ++i){
                    this->mValues[i] -= rhs.mValues[i];
                }
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies ever element in this matrix by the passed in scalar
            /// Modifies THIS matrix
            /// \return Matrix<T, TRows, TCols>& for operator chaining (eg: mat1 += (mat2 - mat3))
            /// \note It is possible for the scalar to be of a different type to the matrix, eg: the matrix
            /// is a float matrix and the scalar a int, this is subject to the normal rules for c++, eg:
            /// \code
            /// int i = 1;
            /// i *= 2.8f; // i now equals 2
            /// \endcode
            /// The type of the lhs matrix will NEVER change
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix<T, TRows, TCols>& operator*=(T2 scalar){
                for(int i = 0; i < TRows*TCols; ++i){
                    this->mValues[i] *= scalar;
                }
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Adds two matrices together and returns result, leaves both operands unchanged
            /// \return Matrix<T, TRows, TCols>& for operator chaining (eg: mat1 -= (mat2 - mat3))
            /// \see operator+=(const Matrix<T, TRows, TCols>& rhs)
            /// \note The matrices added together must be of the same order but can store different
            /// types, in this case the resultant matrix will be the one with the higher precision type
            /// eg: in normal c++ float+int or int+float will return a float
            /// The more precise type is determined by the class MaxPrecisionOfTypes
            /////////////////////////////////////////////////
            template <typename T2>
            Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> operator+(const Matrix<T2, TRows, TCols>& rhs) const{
                Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> result = *this;
                result += rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Subtracts rhs matrix from lhs and returns result, leaves both operands unchanged
            /// \return Matrix<T, TRows, TCols>& for operator chaining (eg: mat1 -= (mat2 - mat3))
            /// \see operator-(const Matrix<T, TRows, TCols>& rhs)
            /// \note The matrices added together must be of the same order but can store different
            /// types, in this case the resultant matrix will be the one with the higher precision type
            /// eg: in normal c++ float+int or int+float will return a float
            /// The more precise type is determined by the class MaxPrecisionOfTypes
            /////////////////////////////////////////////////
            template <typename T2>
            Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> operator-(const Matrix<T2, TRows, TCols>& rhs) const{
                Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> result = *this;
                result -= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies ever element in this matrix by the passed in scalar.
            /// Does not modifies this matrix
            /// \return Matrix<T, TRows, TCols>& for operator chaining
            /// \note The scalar and the matrix do NOT  need to be of the same type,
            /// eg a Matrix<int> can be multiplied by a float.
            /// In this case the resultant matrix will be of the higher precision type
            /// as in normal c++ both int*float and float*int results in a float result.
            /// The more precise type is determined by the class MaxPrecisionOfTypes
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> operator*(T2 rhs) const{
                Matrix<typename lie::math::MaxPrecisionOfTypes<T,T2>::type, TRows, TCols> result = *this;
                result *= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Returns the transposed version of this matrix
            /// Does NOT modify this matrix
            ///
            /// \note All non square matrices have a different order when transposed
            /// As the order of a matrix cannot be changed after being constructed
            /// a new matrix is returned.
            /// For instances of SquareMatrix a .transpose() function is avaliable
            /// .transpose() is to .getTransposed() as += is to +
            /////////////////////////////////////////////////
            Matrix<T, TCols, TRows>& getTranspose() const{
                Matrix<T, TCols, TRows> result;
                for(int r = 0; r < TRows; ++r){
                    for(int c = 0; c < TCols; ++c){
                        result[r][c] = (*this)[c][r];
                    }
                }
                return result;
            }

            /// @}
            //end of arithmetic operators

        protected:
            ///The array storing the values of this matrix
            ///mutable so access helper can get it, note however the public interface does NOT
            ///allow changes to be made to this for a const Matrix
            mutable T mValues[TRows*TCols];
        };

    }
}



#endif // LIE_MATH_MATRIX_HPP
