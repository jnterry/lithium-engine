/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file 
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_MATH_VECTOR4_HPP
#define LIE_MATH_VECTOR4_HPP

#include "Vector.hpp"

namespace lie{
	namespace math{

		//////////////////////////////////////////////////////////////////////////
		/// \brief Represents a 4d vector
		//////////////////////////////////////////////////////////////////////////
		template<typename T_TYPE>
		class Vector4 : public Vector < 4, T_TYPE > {
		public:
			Vector4(T_TYPE newX, T_TYPE newY, T_TYPE newZ, T_TYPE newW){
				x = newX;
				y = newY;
				z = newZ;
				w = newW;
			}
		};

		typedef Vector4<float> Vector4f;
		typedef Vector4<int> Vector4i;
		typedef Vector4<double> Vector4d;


	}
}

#endif


