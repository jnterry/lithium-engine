#ifndef LIE_MATH_MAT4_H
#define LIE_MATH_MAT4_H

#include "SquareMatrix.hpp"
#include "Vector3.hpp"
#include "Angle.hpp"
#include <initializer_list>

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Derived from the SquareMatrix class
        /// and provides speciallised functions which apply only to
        /// a 4x4 matrix, eg generating 3d transformations
        /////////////////////////////////////////////////
        template<typename T_Type>
        class Matrix4x4 : public SquareMatrix<T_Type, 4>{
        public:
            /////////////////////////////////////////////////
            /// \brief static const member Matrix4x4, represents identity matrix
            /// (1 0 0 0)
            /// (0 1 0 0)
            /// (0 0 1 0)
            /// (0 0 0 1)
            /////////////////////////////////////////////////
            static const Matrix4x4 Identity;

            /////////////////////////////////////////////////
            /// \brief Default constructor which returns an identity matrix
            /////////////////////////////////////////////////
            Matrix4x4() : SquareMatrix<T_Type, 4>({1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}){
                /*Empty body*/
            }

            /////////////////////////////////////////////////
            /// \brief Constructor taking array of T_Types to set matrix values to
            /////////////////////////////////////////////////
            Matrix4x4(std::initializer_list<T_Type> data) :
            SquareMatrix<T_Type, 4>(data)
            {/*Empty body*/}

            /////////////////////////////////////////////////
            /// \brief Makes deep copy of another matrix
            /////////////////////////////////////////////////
            Matrix4x4(const Matrix4x4& otherMat) :
            SquareMatrix<T_Type, 4>({otherMat.mValues[0], otherMat.mValues[1], otherMat.mValues[2], otherMat.mValues[3],
                    otherMat.mValues[4], otherMat.mValues[5], otherMat.mValues[6], otherMat.mValues[7],
                    otherMat.mValues[8], otherMat.mValues[9], otherMat.mValues[10], otherMat.mValues[11],
                    otherMat.mValues[12], otherMat.mValues[13], otherMat.mValues[14], otherMat.mValues[15]})
            {/*Empty body*/}

			/*	The below functions implement functions in the base matrix class but with the loop
			   unrolled, depending on whether the compiler does loop unrolling itself these could be
			   faster, profiling needed
			   It is less likely that the operator== and != will be compiler loop unrolled as the
			   generic implementation uses a for with an if in each where as  the unrolled version
			   just does return [0][0]==[0][0] && [1][1]==[1][1] etc

            /////////////////////////////////////////////////
            /// \brief Returns true if all elements of the two matricies are equal
            /////////////////////////////////////////////////
            bool operator==(const Matrix4x4& rhs) const{
                return (mValues[0] == rhs.mValues[0] &&
                       this->mValues[1] == rhs.mValues[1] &&
                       this->mValues[2] == rhs.mValues[2] &&
                       this->mValues[3] == rhs.mValues[3] &&
                       this->mValues[4] == rhs.mValues[4] &&
                       this->mValues[5] == rhs.mValues[5] &&
                       this->mValues[6] == rhs.mValues[6] &&
                       this->mValues[7] == rhs.mValues[7] &&
                       this->mValues[8] == rhs.mValues[8] &&
                       this->mValues[9] == rhs.mValues[9] &&
                       this->mValues[10] == rhs.mValues[10] &&
                       this->mValues[11] == rhs.mValues[11] &&
                       this->mValues[12] == rhs.mValues[12] &&
                       this->mValues[13] == rhs.mValues[13] &&
                       this->mValues[14] == rhs.mValues[14] &&
                       this->mValues[15] == rhs.mValues[15]);
            }

            /////////////////////////////////////////////////
            /// \brief Returns true if atleast 1 element of the two matricies are not equal
            /////////////////////////////////////////////////
            bool operator!=(const Matrix4x4& rhs){return !(this->operator==(rhs));}


			/////////////////////////////////////////////////
            /// \brief Modified lhs matrix by adding corrosponding element of rhs matrix, leaves rhs unchanged
            /// \return Matrix4x4& for operator chaining (eg: mat1 += (mat2 - mat3))
            /// \see operator+(const Matrix4x4& rhs)
            /////////////////////////////////////////////////
            Matrix4x4& operator+=(const Matrix4x4& rhs){
                this->mValues[0] += rhs.mValues[0];
                this->mValues[1] += rhs.mValues[1];
                this->mValues[2] += rhs.mValues[2];
                this->mValues[3] += rhs.mValues[3];
                this->mValues[4] += rhs.mValues[4];
                this->mValues[5] += rhs.mValues[5];
                this->mValues[6] += rhs.mValues[6];
                this->mValues[7] += rhs.mValues[7];
                this->mValues[8] += rhs.mValues[8];
                this->mValues[9] += rhs.mValues[9];
                this->mValues[10] += rhs.mValues[10];
                this->mValues[11] += rhs.mValues[11];
                this->mValues[12] += rhs.mValues[12];
                this->mValues[13] += rhs.mValues[13];
                this->mValues[14] += rhs.mValues[14];
                this->mValues[15] += rhs.mValues[15];

                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Modified lhs matrix by subtracting corrosponding element of rhs matrix, leaves rhs unchanged
            /// \return Matrix4x4& for operator chaining (eg: mat1 -= (mat2 - mat3))
            /// \see operator-(const Matrix4x4& rhs)
            /////////////////////////////////////////////////
            Matrix4x4& operator-=(const Matrix4x4& rhs){
                this->mValues[0] -= rhs.mValues[0];
                this->mValues[1] -= rhs.mValues[1];
                this->mValues[2] -= rhs.mValues[2];
                this->mValues[3] -= rhs.mValues[3];
                this->mValues[4] -= rhs.mValues[4];
                this->mValues[5] -= rhs.mValues[5];
                this->mValues[6] -= rhs.mValues[6];
                this->mValues[7] -= rhs.mValues[7];
                this->mValues[8] -= rhs.mValues[8];
                this->mValues[9] -= rhs.mValues[9];
                this->mValues[10] -= rhs.mValues[10];
                this->mValues[11] -= rhs.mValues[11];
                this->mValues[12] -= rhs.mValues[12];
                this->mValues[13] -= rhs.mValues[13];
                this->mValues[14] -= rhs.mValues[14];
                this->mValues[15] -= rhs.mValues[15];

                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Adds two matricies together and returns result, leaves both operands unchanged
            /// \return Matrix4x4& for operator chaining (eg: mat1 -= (mat2 - mat3))
            /// \see operator+=(const Matrix4x4& rhs)
            /////////////////////////////////////////////////
            Matrix4x4 operator+(const Matrix4x4& rhs) const{
                Matrix4x4 result = *this;
                result += rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Subtracts rhs matrix from lhs and returns result, leaves both operands unchanged
            /// \return Matrix4x4& for operator chaining (eg: mat1 -= (mat2 - mat3))
            /// \see operator-(const Matrix4x4& rhs)
            /////////////////////////////////////////////////
            Matrix4x4 operator-(const Matrix4x4& rhs) const{
                Matrix4x4 result = *this;
                result -= rhs;
                return result;
            }

            /////////////////////////////////////////////////
            /// \brief Sets all elements of this matrix equal to corrosponding elements of other matrix
            /// \return Matrix4x4& for operator chaining
            /// \see Matrix4x4& operator= (T_Type data[16])
            /////////////////////////////////////////////////
            Matrix4x4& operator= (const Matrix4x4& rhs){
                if(this != &rhs){
                    this->mValues[0] = rhs.mValues[0];
                    this->mValues[1] = rhs.mValues[1];
                    this->mValues[2] = rhs.mValues[2];
                    this->mValues[3] = rhs.mValues[3];
                    this->mValues[4] = rhs.mValues[4];
                    this->mValues[5] = rhs.mValues[5];
                    this->mValues[6] = rhs.mValues[6];
                    this->mValues[7] = rhs.mValues[7];
                    this->mValues[8] = rhs.mValues[8];
                    this->mValues[9] = rhs.mValues[9];
                    this->mValues[10] = rhs.mValues[10];
                    this->mValues[11] = rhs.mValues[11];
                    this->mValues[12] = rhs.mValues[12];
                    this->mValues[13] = rhs.mValues[13];
                    this->mValues[14] = rhs.mValues[14];
                    this->mValues[15] = rhs.mValues[15];
                }

                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Sets all elements of this matrix equal to corrosponding element of array
            /// Example:
            /// \code
            /// matrix4 = {1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16};
            /// \endcode
            /// \return Matrix4x4& for operator chaining
            /// \see  Matrix4x4& operator= (const Matrix4x4& rhs);
            /////////////////////////////////////////////////
            Matrix4x4& operator= (T_Type data[16]){
                this->mValues[0] = data[0];
                this->mValues[1] = data[1];
                this->mValues[2] = data[2];
                this->mValues[3] = data[3];
                this->mValues[4] = data[4];
                this->mValues[5] = data[5];
                this->mValues[6] = data[6];
                this->mValues[7] = data[7];
                this->mValues[8] = data[8];
                this->mValues[9] = data[9];
                this->mValues[10] = data[10];
                this->mValues[11] = data[11];
                this->mValues[12] = data[12];
                this->mValues[13] = data[13];
                this->mValues[14] = data[14];
                this->mValues[15] = data[15];

                return *this;
            }

			*/ //end  of loop unrolled versions of inherited functions

            /////////////////////////////////////////////////
            /// \brief Multiplies this matrix by another, leaves rhs unchanged, modifies lhs
            ///
            /// \note This function multiplies the matricies in the oposite order
            /// Multiplying the matricies is equivilent to combining the transformations
            /////////////////////////////////////////////////
            Matrix4x4<T_Type>& operator*= (const Matrix4x4<T_Type>& rhs){
                this->operator=(this->operator*(rhs));
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies all elements of this matrix by the specified scalar
            /////////////////////////////////////////////////
            template<typename T_Scalar>
            Matrix4x4<T_Type>& operator*= (T_Scalar scalar){
                /// :TODO: this method should be inherited from Matrix but isnt... odd
                /// possible because of return type, the matrix version returns a Matrix<> rather
                /// than Matrix4x4
                this->mValues[0] *= scalar;
                this->mValues[1] *= scalar;
                this->mValues[2] *= scalar;
                this->mValues[3] *= scalar;
                this->mValues[4] *= scalar;
                this->mValues[5] *= scalar;
                this->mValues[6] *= scalar;
                this->mValues[7] *= scalar;
                this->mValues[8] *= scalar;
                this->mValues[9] *= scalar;
                this->mValues[10] *= scalar;
                this->mValues[11] *= scalar;
                this->mValues[12] *= scalar;
                this->mValues[13] *= scalar;
                this->mValues[14] *= scalar;
                this->mValues[15] *= scalar;
                return *this;
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies this matrix by a vector3 returning the transformed vector
            /// Note, a 4x4 matrix must be multiplied by a vector4, so this function assumes
            /// that the w component is 1, it also outputs the vector with w as 1 (and thus outputs a Vector3)
            /////////////////////////////////////////////////
            Vector3<T_Type> operator* (const Vector3<T_Type>& rhs) const{

                T_Type w =this->mValues[3]*rhs.x +this->mValues[7]*rhs.y +this->mValues[11]*rhs.z +this->mValues[15]/*+rhs.w -> Vector3 so assume w = 1*/;

                return Vector3<T_Type>((this->mValues[0]*rhs.x +this->mValues[4]*rhs.y +this->mValues[8]*rhs.z +this->mValues[12])/w,
                            (this->mValues[1]*rhs.x +this->mValues[5]*rhs.y +this->mValues[9]*rhs.z +this->mValues[13])/w,
                            (this->mValues[2]*rhs.x +this->mValues[6]*rhs.y +this->mValues[10]*rhs.z +this->mValues[14])/w);
            }

            /////////////////////////////////////////////////
            /// \brief Multiplies lhs and rhs matricies together leaving both unchanged and returning result
            /////////////////////////////////////////////////
            template<typename T2>
            Matrix4x4<typename lie::math::MaxPrecisionOfTypes<T_Type, T2>::type > operator* (const Matrix4x4<T2>& rhs) const{
                return Matrix4x4<typename lie::math::MaxPrecisionOfTypes<T_Type, T2>::type > ({
                   this->mValues[0] * rhs.mValues[0] +this->mValues[1] * rhs.mValues[4] +
                   this->mValues[2] * rhs.mValues[8] +this->mValues[3] * rhs.mValues[12],

                   this->mValues[0] * rhs.mValues[1] +this->mValues[1] * rhs.mValues[5] +
                   this->mValues[2] * rhs.mValues[9] +this->mValues[3] * rhs.mValues[13],

                   this->mValues[0] * rhs.mValues[2] +this->mValues[1] * rhs.mValues[6] +
                   this->mValues[2] * rhs.mValues[10] +this->mValues[3] * rhs.mValues[14],

                   this->mValues[0] * rhs.mValues[3] +this->mValues[1] * rhs.mValues[7] +
                   this->mValues[2] * rhs.mValues[11] +this->mValues[3] * rhs.mValues[15],



                   this->mValues[4] * rhs.mValues[0] +this->mValues[5] * rhs.mValues[4] +
                   this->mValues[6] * rhs.mValues[8] +this->mValues[7] * rhs.mValues[12],

                   this->mValues[4] * rhs.mValues[1] +this->mValues[5] * rhs.mValues[5] +
                   this->mValues[6] * rhs.mValues[9] +this->mValues[7] * rhs.mValues[13],

                   this->mValues[4] * rhs.mValues[2] +this->mValues[5] * rhs.mValues[6] +
                   this->mValues[6] * rhs.mValues[10] +this->mValues[7] * rhs.mValues[14],

                   this->mValues[4] * rhs.mValues[3] +this->mValues[5] * rhs.mValues[7] +
                   this->mValues[6] * rhs.mValues[11] +this->mValues[7] * rhs.mValues[15],



                   this->mValues[8] * rhs.mValues[0] +this->mValues[9] * rhs.mValues[4] +
                   this->mValues[10] * rhs.mValues[8] +this->mValues[11] * rhs.mValues[12],

                   this->mValues[8] * rhs.mValues[1] +this->mValues[9] * rhs.mValues[5] +
                   this->mValues[10] * rhs.mValues[9] +this->mValues[11] * rhs.mValues[13],

                   this->mValues[8] * rhs.mValues[2] +this->mValues[9] * rhs.mValues[6] +
                   this->mValues[10] * rhs.mValues[10] +this->mValues[11] * rhs.mValues[14],

                   this->mValues[8] * rhs.mValues[3] +this->mValues[9] * rhs.mValues[7] +
                   this->mValues[10] * rhs.mValues[11] +this->mValues[11] * rhs.mValues[15],



                   this->mValues[12] * rhs.mValues[0] +this->mValues[13] * rhs.mValues[4] +
                   this->mValues[14] * rhs.mValues[8] +this->mValues[15] * rhs.mValues[12],

                   this->mValues[12] * rhs.mValues[1] +this->mValues[13] * rhs.mValues[5] +
                   this->mValues[14] * rhs.mValues[9] +this->mValues[15] * rhs.mValues[13],

                   this->mValues[12] * rhs.mValues[2] +this->mValues[13] * rhs.mValues[6] +
                   this->mValues[14] * rhs.mValues[10] +this->mValues[15] * rhs.mValues[14],

                   this->mValues[12] * rhs.mValues[3] +this->mValues[13] * rhs.mValues[7] +
                   this->mValues[14] * rhs.mValues[11] +this->mValues[15] * rhs.mValues[15]
                });
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4instace that will scale a vertex by a uniform amount in all directions
            /// with scale center at the origin
            /////////////////////////////////////////////////
            static Matrix4x4 createScale(T_Type uniformFactor){return createScale(uniformFactor, uniformFactor, uniformFactor);}

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will scale a vertex by specified amounts in x,y,z
            /// directions with scale center at the origin
            /////////////////////////////////////////////////
            static Matrix4x4 createScale(T_Type xFactor, T_Type yFactor, T_Type zFactor){
                return Matrix4x4({xFactor,0,0,0, 0,yFactor,0,0, 0,0,zFactor,0, 0,0,0,1});
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will scale a vertex by corrosponding
            /// x,y,z factors in the Vector3 with scale center at the origin
            /////////////////////////////////////////////////
            static Matrix4x4 createScale(lie::math::Vector3<T_Type> factors){
                return Matrix4x4({factors.x,0,0,0, 0,factors.y,0,0, 0,0,factors.z,0, 0,0,0,1});
            }


            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will translate a vertex by specified amounts in x,y,z
            /// directions
            /////////////////////////////////////////////////
            static Matrix4x4 createTranslation(T_Type xOffset, T_Type yOffset, T_Type zOffset){
                //return Matrix4x4(1,0,0,xOffset, 0,1,0,yOffset, 0,0,1,zOffset, 0,0,0,1);
                return Matrix4x4({1,0,0,0, 0,1,0,0, 0,0,1,0, xOffset,yOffset,zOffset,1});
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will translate a vertex by specified vector3
            /////////////////////////////////////////////////
            static Matrix4x4 createTranslation(Vector3<T_Type> offsetVector){
                return createTranslation(offsetVector.x, offsetVector.y, offsetVector.z);
            }


            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will project a vertex into clip space with given
            /// parameters using perspective projection
            /////////////////////////////////////////////////
            static Matrix4x4<T_Type> createPerspectiveProjection(Angle<T_Type> fovy, T_Type width, T_Type height, T_Type zNear, T_Type zFar){
                //implementation derived from http://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/
                T_Type aspectRatio = width/height;
                T_Type f = 1/tanf(fovy.asRadians()/2.0);

                return Matrix4x4<T_Type>({  f/aspectRatio,  0,  0,                          0,
                                            0,              f,  0,                          0,
                                            0,              0,  (zFar+zNear)/(zNear-zFar),  -1,
                                            0,              0,  (2*zFar*zNear)/(zNear-zFar),0});
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that will project a vertex into clip space with given
            /// parameters using orthographic projection
            /////////////////////////////////////////////////
            static Matrix4x4 createOrthograhpicProjection(T_Type left, T_Type right, T_Type bottom, T_Type top, T_Type zNear, T_Type zFar){
                return Matrix4x4<T_Type>(   2/(right-left),0,0,0,
                                            0,2/(top-bottom),0,0,
                                            0,0,-2/(zFar-zNear),0,
                                            -((right+left)/(right-left)),-((top+bottom)/(top-bottom)),-((zFar+zNear)/(zFar-zNear)),1);
            }

            /////////////////////////////////////////////////
            /// \brief Creates and returns a Matrix4x4 instance that rotate a point
            /// around the x axis by the given angle
            /// \param angle The angle by which to rotate
            /////////////////////////////////////////////////
            static Matrix4x4<T_Type> createRotationX(Angle<T_Type> angle){
				T_Type a = angle.asRadians();
                T_Type c = cos(-a);
                T_Type s = sin(-a);

                return Matrix4x4<T_Type>({1,0,0,0,  0,c,-s,0,  0,s,c,0,  0,0,0,1});
            }

			/////////////////////////////////////////////////
			/// \brief Creates and returns a Matrix4x4 instance that rotate a point
			/// around the y axis by the given angle
			/// \param angle The angle by which to rotate
			/////////////////////////////////////////////////
			static Matrix4x4<T_Type> createRotationY(Angle<T_Type> angle){
				T_Type a = angle.asRadians();
                T_Type c = cos(-a);
                T_Type s = sin(-a);

                return Matrix4x4<T_Type>({c,0,s,0,  0,1,0,0,  -s,0,c,0,  0,0,0,1});
            }

			/////////////////////////////////////////////////
			/// \brief Creates and returns a Matrix4x4 instance that rotate a point
			/// around the z axis by the given angle
			/// \param angle The angle by which to rotate
			/////////////////////////////////////////////////
			static Matrix4x4<T_Type> createRotationZ(Angle<T_Type> angle){
				T_Type a = angle.asRadians();
                T_Type c = cos(-a);
                T_Type s = sin(-a);

                return Matrix4x4({c,-s,0,0,  s,c,0,0,  0,0,1,0,  0,0,0,1});
            }
        }; //end of matrix class

        template <typename T>
        const Matrix4x4<T> Matrix4x4<T>::Identity = Matrix4x4({1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1});

        typedef Matrix4x4<float> Matrix4x4f;
        typedef Matrix4x4<double> Matrix4x4d;
        typedef Matrix4x4<int> Matrix4x4i;
    }
}

#endif // LIE_MATH_MAT4_H
