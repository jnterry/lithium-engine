#ifndef LIE_MATH_SQUAREMATRIX_HPP
#define LIE_MATH_SQUAREMATRIX_HPP

#include "Matrix.hpp"

namespace lie{
    namespace math{

        /////////////////////////////////////////////////
        /// \brief Derived from the Matrix class and provides functionality specific
        /// to square matricies
        ///
        /// \note Matrix4x4 and Matrix3x3 should be used where appropriate
        ///
        /// \tparam T_Type The type of all the elements in this matrix
        /// \tparam T_Dim the dimension of the matrix, both rows and columns are set to this
        /////////////////////////////////////////////////
        template<typename T_Type, unsigned int T_Dim>
        class SquareMatrix : public Matrix<T_Type, T_Dim, T_Dim>{
        public:
            /////////////////////////////////////////////////
            /// \brief Default constructor, creates a new matrix with all
            /// elements set to 0
            /////////////////////////////////////////////////
            SquareMatrix() : Matrix<T_Type, T_Dim, T_Dim>(){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Constructs matrix by setting elememts to the passed in array of values
            /////////////////////////////////////////////////
            SquareMatrix(T_Type data[T_Dim*T_Dim]) : Matrix<T_Type, T_Dim, T_Dim>(data){
                //empty body
            }

            SquareMatrix(std::initializer_list<T_Type> data) : Matrix<T_Type, T_Dim, T_Dim>(data){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Transposes this matrix.
            /// The equivilent of:
            /// \code
            /// matrix = matrix.getTranspose();
            /// \endcode
            /// However, this method is more efficent in terms of memory, no new matrix is allocated
            ///
            /// \note This is only defined for square matricies as transposing a non-square
            /// matrix results in a matrix of a different order and as the matrix order is defined by
            /// compile time templates a new matrix must be returned if the order changes
            /////////////////////////////////////////////////
            SquareMatrix<T_Type, T_Dim>& transpose(){
                /*  How does the following code work?
                    We want to swap certain pairs of values when transposing, lets list them
                    Below the element is the index of the array that it is in

                    2x2:
                    0 1  ->  0 2
                    2 3  ->  1 3
                          row0
                     swap: 1
                     with: 2
                    delta: 1

                    3x3:
                    0 1 2  -> 0 3 6
                    3 4 5  -> 1 4 7
                    6 7 8  -> 2 5 8
                           row0   row1
                     swap: 1, 2,   5
                     with: 3, 6,   7
                    delta: 2, 4,   2

                    4x4:
                    00 01 02 03  ->  00 04 08 12
                    04 05 06 07  ->  01 05 09 13
                    08 09 10 11  ->  02 06 10 14
                    12 13 14 15  ->  03 07 11 15
                           row 0        row 1    row 2
                     swap: 1, 2,  3,    6,  7,   11
                     with: 4, 8, 12,    9, 13,   14
                    detla: 3, 6, 9,     3, 6,    3,

                    we need to generate the "index1" sequences:
                    2x2: 1
                    3x3: 1,2, 5,
                    4x4, 1,2,3, 6,7, 11

                    the deltas are the (dimension-1) times table
                    2x2: 1
                    3x3: 2,4, 2
                    4x4, 3,6,9, 3,6, 3

                    index2 is index1 + the delta
                */

                T_Type swapSpace;
                unsigned int index1;
                unsigned int index2;

                for(int i = 1; i < T_Dim; ++i){
                    for(int j = i; j < T_Dim; ++j){
                        //j starts at i, this allows us to skip the first few elements of each row

                        //index1=  rowIndex *  some factor
                        index1 =    (i-1)   *  T_Dim + j;

                        //index2=index1 +     the delta, which is...
                        //                 dim-1 times table
                        //                           which multiple of the times table?
                        //                           happens to be j-i+1
                        index2 = index1 + ((T_Dim-1)*(j-i+1));

                        //now we've generated the indexes do a standard swap
                        swapSpace = this->mValues[index2];
                        this->mValues[index2] = this->mValues[index1];
                        this->mValues[index1] = swapSpace;
                    }
                }

                return *this;
            }


        };

    }
}

#endif // LIE_MATH_SQUAREMATRIX_HPP
