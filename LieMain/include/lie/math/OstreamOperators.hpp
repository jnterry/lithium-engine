/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file OstreamOperators.hpp
/// \author Jamie Terry
/// \date 2015/06/19
/// \brief Contains functions that allow you to stream Math types to
/// a std::ostream, for example: 
/// \code
/// Vector2f pos(10,6);
/// std::cout << "Position: " << pos << std::endl;
/// \endcode
/// 
/// \ingroup math
/////////////////////////////////////////////////

#ifndef LIE_MATH_OSTREAMOPERATORS_HPP
#define LIE_MATH_OSTREAMOPERATORS_HPP

#include <ostream>
#include "Vector.hpp"
#include "Vector2.hpp"
#include "Matrix.hpp"

template<unsigned int COUNT_T, typename TYPE_T>
std::ostream& operator<<(std::ostream& os, lie::math::Vector<COUNT_T, TYPE_T>& vec){
	os << "(";
	for (int i = 0; i < TYPE_T; ++i){
		os << vec.data[i] << ", ";
	}
	os << ")";
	return os;
}

std::ostream& operator<<(std::ostream& os, const lie::math::Vector2f& vec){
	os << "(" << vec.x << ", " << vec.y << ")";
	return os;
}

template<typename T, unsigned int TRows, unsigned int TCols>
std::ostream& operator<<(std::ostream& os, const lie::math::Matrix<T, TRows, TCols>& mat){
	os << "[";
	for (int c = 0; c < TCols-1; ++c){
		os << mat[c] << ", \n";
	}
	os << mat[TCols - 1];
	os << "]";
	return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const lie::math::Angle<T>& angle){
	os << angle.asDegrees() << "�";
	return os;
}



#endif LIE_MATH_OSTREAMOPERATORS_HPP