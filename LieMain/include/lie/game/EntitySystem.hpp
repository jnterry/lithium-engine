#ifndef LIE_GAME_ENTITYSYSTEM_HPP
#define LIE_GAME_ENTITYSYSTEM_HPP

#include "EntityFilter.hpp"

namespace lie{
    namespace game{


        class EntitySystem{
        friend class EntityScene;
        public:
            EntitySystem(EntityScene scene, EntityFilter filter);

            virtual void update() {}
        protected:
            ///< The scene that this system is in.
            EntityScene& mScene;
        private:
            ///< This is the filter that entities must satisfy in order to be processed by this system
            EntityFilter mFilter;


        };


    }
}

#endif // LIE_GAME_ENTITYSYSTEM_HPP
