#ifndef LIE_GAME_SCREEN_HPP
#define LIE_GAME_SCREEN_HPP

#include <lie/render\Event.hpp>

namespace lie{
    namespace game{
        //forward decleration
        class ScreenStack;

        /////////////////////////////////////////////////
        /// \brief A Screen represents some state that the game can be in.
        /// Eg: The main menu, the settings menu, a loading screen, the main game,
        /// a minigame, an inventory screen, etc
        /// It groups together code and data for each state allowing different
        /// states to be completely seperated.
        ///
        /// There are two main ways to manage multiple states, the simplest is to use
        /// the ScreenStack class, see its documentation for details.
        ///
        /// The second method is for a Screen to hold an instance of another Screen directly
        /// This can be useful when more advanced interactions between screens are needed.
        /// For example an OptionsMenu screen may wish to hold an instance of a GraphicsOptions
        /// screen, a GameOptions screen etc. Doing this will allow it to selectivly pass
        /// events and rending calls to these screens. For example, it may have tabs at the top for
        /// selecting the option type you wish to view, the main OptionsMenu screen can then translate
        /// mouse move/click events by the rendered offset and pass the events to the contained screens
        /// directly. The Screen stack would then contain only the OptionsMenu screen.
        ///
        /// \see ScreenStack
        /////////////////////////////////////////////////
        class Screen{
        public:
            //the screen stack automatically updates the protectdd mScreenStack*
            //when pushing/poping a screen
            //also calls private onXXX methods
            friend class ScreenStack;

            /////////////////////////////////////////////////
            /// \brief This function should return true if this screen allows
            /// subsequent screens in the stack to be rendered. If false is returned
            /// screens below this screen will not be rendered.
            ///
            /// \note update and handleInput both return bools directly indicating
            /// whether the next screen in the stack should be updated/recieve input.
            /// This is not the case for rendering as lower screens need to be rendered
            /// before higer screens, however the higher screen decides wether lower screens
            /// are rendererd. For example:
            ///  - OptionsMenu - blocks render
            ///  - MainMenu
            ///  - Pretty3dBackground
            /// In the above layout the MainMenu should not be rendered when the OptionsMenu
            /// is above it, however when the OptionsMenu is removed then the Pretty3dBackground
            /// needs to be rendered BEFORE the MainMenu, however the MainMenu decides whether lower
            /// screens are rendered. Due to the decision making being in the oposite order to the
            /// rendering a seperate function is needed.
            /// For updating and input the higher screens is updated/recieves input and can in that
            /// function decide whether the next screen should also be updated/recieve input
            /////////////////////////////////////////////////
            virtual bool allowsRender() = 0;

            /////////////////////////////////////////////////
            /// \brief In this function a screen should update itself.
            /// Dependent on the screen this can mean different things,
            /// eg, the main game screen would simulate physics, ai, etc in this
            /// function.
            /// If true is returned the next screen in the stack will also
            /// be updated, else it will not have its update function called
            /////////////////////////////////////////////////
            virtual bool update() = 0;

            /////////////////////////////////////////////////
            /// \brief In this function a screen should render itself.
            /////////////////////////////////////////////////
            virtual void render() = 0;

            /////////////////////////////////////////////////
            /// \brief This function will be called for each event that occurs
            /// that has not yet been blocked by a higher screen in the screen stack.
            ///
            /// This function should return true if it wants the next screen in the stack
            /// to also recieve the event. If false is returned the next screen will not
            /// have handleInput called for THIS event
            ///
            /// You may wish to pass on some events under all circumstances, eg a window resize.
            /////////////////////////////////////////////////
            virtual bool handleInput(lie::rend::Event& e) = 0;

        protected: //child classes should be able to access this
            ScreenStack* mScreenStack;
        private:
            //why private?
            //the ScreenStack is a friend so can call these, we do NOT want
            // derived classes calling them

            /////////////////////////////////////////////////
            /// \brief This function will be called by the ScreenStack
            /// when this screen is poped from the stack
            ///
            /// \note The the mScreenStack member of screen will be automatically
            /// set to nullptr when the ScreenStack pops it
            ///
            /// \note This function does not need to be overidden by derived classes,
            /// the default implementation is to simply return
            /////////////////////////////////////////////////
            virtual void onPop(){return;}

            /////////////////////////////////////////////////
            /// \brief This function will be called by the ScreenStack
            /// when this screen is initially pushed onto the stack
            ///
            /// \param screenStack The instance of ScreenStack this screen
            /// is being added to
            ///
            /// \note The mScreenStack member of screen will be automatically set
            /// to point at the ScreenStack that is having this screen pushed
            /// onto it.
            ///
            /// \note This function does not need to be overidden by derived classes,
            /// the default implementation is to simply return
            /////////////////////////////////////////////////
            virtual void onPush(){return;}

            /////////////////////////////////////////////////
            /// \brief This function will be called by the ScreenStack when
            /// this screen was the top screen of the stack and a new screen
            /// was pushed onto the stack
            /// \note This function does not need to be overidden by derived classes,
            /// the default implementation is to simply return
            /////////////////////////////////////////////////
            virtual void onCover(){return;}

            /////////////////////////////////////////////////
            /// \brief This function will be called by the ScreenStack when
            /// this screen was the 2nd item in the screen stack and the old
            /// top screen was poped, hence making this screen the top screen
            /// \note This function does not need to be overidden by derived classes,
            /// the default implementation is to simply return
            /////////////////////////////////////////////////
            virtual void onUncover(){return;}

        };
    }
}

#endif // LIE_GAME_SCREEN_HPP
