#ifndef LIE_GAME_SCREENSTACK_HPP
#define LIE_GAME_SCREENSTACK_HPP

#include <lie/render\Event.hpp>

#include "Screen.hpp"

#include <vector>

namespace lie{
    namespace game{
        class ScreenStack{
        public:
            /////////////////////////////////////////////////
            /// \brief Pushes a new screen onto the screen stack.
            /// The pushed screen will be made the new top screen.
            ///
            /// onPush will be called on the new screen and then onCovered
            /// will be called on the old top screen. This order is garentied
            /// and could be important in some situations, eg if the onPush method
            /// requests the loading of some lie::core::Resource and onCovered requests
            /// some be unloaded. If the order was reverse resources may be un-nessacerally
            /// unloaded and then reloaded
            /////////////////////////////////////////////////
            void pushScreen(Screen* screen);

            /////////////////////////////////////////////////
            /// \brief Pops the top screen off the screen stack
            /// and returns a pointer to it.
            /// \note It is YOUR responsibility to manage the pointer
            /// once it is returned, you may wish to delete the screen
            /// instantly or cache it for later use.
            ///
            /// onUncovered will be called on the new top screen and then onPoped
            /// will be called on screen being removed. This order is garentied
            /// and could be important in some situations, eg if the onUncovered method
            /// requests the loading of some lie::core::Resource and onPoped requests
            /// some be unloaded. If the order was reverse resources may be un-nessacerally
            /// unloaded and then reloaded
            ///
            /// \return Pointer to the popped screen, nullptr if no screen was on the stack
            /// so no screen could be popped
            /////////////////////////////////////////////////
            Screen* popScreen();

            /////////////////////////////////////////////////
            /// \brief Returns reference to the current top screen on the stack
            /////////////////////////////////////////////////
            Screen* getTopScreen();

            /////////////////////////////////////////////////
            /// \brief Updates the top screen in the stack, if
            /// that screens update function returns true updates the next
            /// screen and so on until all screens have been updated or
            /// a screen's update function returns false
            /////////////////////////////////////////////////
            void update();

            /////////////////////////////////////////////////
            /// \brief Goes through the stack and finds the first screen that
            /// does not allow render
            /// That screen and all screens above that screen are then rendered
            /// in reverse order to the order they are in the stack.
            /// Eg, a stack as follows:
            /// - PauseScreen - allows render
            /// - HUD - allows render
            /// - MainGame - does NOT allow render
            /// - MainMenu
            /// All screens above and including MainGame will be rendered in reverse
            /// order, ie: MainGame will be rendered followed by the HUD followed by
            /// the PauseScreen
            /////////////////////////////////////////////////
            void render();

            /////////////////////////////////////////////////
            /// \brief Passes the specified event to the top screen in the stack,
            /// if it's handleInput function returns true the event is then passed to
            /// the next screen in the stack and so on unit all screens are passed the event
            /// or a screen's handleInput function returns false
            /////////////////////////////////////////////////
            void handleInput(lie::rend::Event& e);
        private:
            ///vector containg the screens of this ScreenStack
            ///not actually a stack as the ScreenStack needs to be able to
            ///iterate the screens in any order, however the public interface
            ///presents this as a stack
            std::vector<Screen*> mScreens;

        };
    }
}

#endif // LIE_GAME_SCREENSTACK_HPP
