#ifndef LIE_GAME_ENTITYFILTER_HPP
#define LIE_GAME_ENTITYFILTER_HPP

#define LIE_GAME_MAX_COMPONENT_TYPES 32

#include "Entity.hpp"
#include <bitset>


namespace lie{
    namespace game{

        /////////////////////////////////////////////////
        /// \brief An EntityFilter stores data that can be used to filter
        /// a set of entities to just those that match the filter.
        /// It is used in EntitySystem to determine which entities should be
        /// processed
        /////////////////////////////////////////////////
        class EntityFilter{
        public:
            /////////////////////////////////////////////////
            /// \brief Adds the specified type to the list of required components
            /// that an entity must have in order to match this filter
            /////////////////////////////////////////////////
            template<typename T>
            EntityFilter& require(){

            }

            /////////////////////////////////////////////////
            /// \brief Adds the specified type to the list of components that
            /// an entity cannot have to pass this filter
            /////////////////////////////////////////////////
            template<typename T>
            EntityFilter& forbid(){

            }

            /////////////////////////////////////////////////
            /// \brief Returns true if the specified entity matches this filter,
            /// else returns false
            /////////////////////////////////////////////////
            bool passes(EntityId ent);
        private:
            ///< Bitset specifing which components are required, if bit 0 is set, then component
            ///< id 0 is required, etc
            std::bitset<LIE_GAME_MAX_COMPONENT_TYPES> mRequiredList;

            ///< Bitset specifing which components are forbiden from entities, if bit 0 is set, then component
            ///< id 0 is forbidden, etc
            std::bitset<LIE_GAME_MAX_COMPONENT_TYPES> mForbidenList;
        };

    }
}

#endif // LIE_GAME_ENTITYFILTER_HPP
