#ifndef LIE_GAME_ENTITY_HPP
#define LIE_GAME_ENTITY_HPP

namespace lie{
    namespace game{
        typedef unsigned int EntityId;
    }
}

#endif // LIE_GAME_ENTITY_HPP
