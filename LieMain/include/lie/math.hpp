/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file math.hpp
/// \author Jamie Terry
/// \brief Convenience header that includes all other header files in the
/// math module
///
/// \defgroup math Math Module
/// \brief Contains various math related classes for both 2d and 3d maths.
/// \note The math module is header only and much of it involves templates
///
/// \ingroup math
/////////////////////////////////////////////////
#ifndef LIE_MATH_H
#define LIE_MATH_H

///basic math related util files
#include "math\Constants.hpp"
#include "math\MaxPrecisionOfTypes.hpp"
#include "math\Angle.hpp"

///basic 2d and 3d math types
#include "math\Vector.hpp"
#include "math\Vector2.hpp"
#include "math\Vector3.hpp"
#include "math\Matrix.hpp"
#include "math\SquareMatrix.hpp"
#include "math\Matrix4x4.hpp"
#include "math\Matrix3x3.hpp"
#include "math\Quaternion3.hpp"
#include "math\Vector3-impl.hpp"

///more advanced 2d and 3d math types
#include "math\Shape2.hpp"
#include "math\Transform2.hpp"
#include "math\Transform3.hpp"

//misc
#include "math\OstreamOperators.hpp"

namespace lie{
    /////////////////////////////////////////////////
    /// \brief Contains all the math classes in the Lithium Engine
    /////////////////////////////////////////////////
    namespace math{}
}


#endif // LIE_MATH_H
