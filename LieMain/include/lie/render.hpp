/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file render.hpp
/// Convinence header that includes all other header files in the
/// utilities module
///
/// \defgroup render Rendering Module
/// \brief Contains all classes for opening/managing rendering contexts and then drawing to them
///
/// \note Currently there is a single implementation for this module, the opengl version
/// However no interface exposes opengl types, hence it should be possible to write a new
/// set of cpps for the same hpps to define a new implementation, eg for directx
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_H
#define LIE_RENDER_H

#include "render/VideoMode.hpp"
#include "render/Device.hpp"
#include "render/RenderContext.hpp"
#include "render/Monitor.hpp"
#include "render/Keyboard.hpp"
#include "render/Mouse.hpp"
#include "render/Event.hpp"
#include "render/Window.hpp"

#include "render/Shader.hpp"
#include "render/ShaderProgram.hpp"
#include "render/ShaderProgramBuilder.hpp"
#include "render/ShaderParameterType.hpp"
#include "render/Color.hpp"
#include "render/BlendMode.hpp"
#include "render/FaceMode.hpp"
#include "render/Pass.hpp"
#include "render/Technique.hpp"
#include "render/Methodology.hpp"
#include "render/Material.hpp"
#include "render/MaterialType.hpp"
#include "render/MaterialTypeBuilder.hpp"
#include "render/GpuBuffer.hpp"
#include "render/MeshVertexLayout.hpp"
#include "render/MeshDataBuffer.hpp"
#include "render/Mesh.hpp"
#include "render/MeshLoaders.hpp"
#include "render/MeshUtils.hpp"
#include "render/MeshGenerator.hpp"
#include "render/PixelFormat.hpp"
#include "render/Texture.hpp"
#include "render/RenderTexture.hpp"
#include "render/PointLight.hpp"
#include "render/DirectionalLight.hpp"

#include "render/Camera2d.hpp"
#include "render/Renderer2d.hpp"
#include "render/Renderer2dImmediate.hpp"

#include "render/Camera3d.hpp"
#include "render/Renderer3d.hpp"
#include "render/Renderer3dImmediate.hpp"

#include "render/ResourceLoaderShader.hpp"
#include "render/ResourceLoaderTexture.hpp"


namespace lie{
    /////////////////////////////////////////////////
    /// \brief Contains classes for 3d and 2d rendering,
    /// and window management
    /////////////////////////////////////////////////
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief Contains implementation details for the rendering module.
        /// These are mostly platform specific or rendering api (opengl, directx)
        /// specific classes that are NOT part of the public interface.
        /// They should not be used by client code!
        /// By only including files in the include folder you will not be able to
        /// use them as only forward declerations are visible, actual definitions are
        /// in the src folder.
        /////////////////////////////////////////////////
        namespace detail{

        }

    }
}


#endif // LIE_RENDER_H
