#ifndef LIE_CORE_METATYPE_HPP
#define LIE_CORE_METATYPE_HPP

#include <string>
#include <initializer_list>
#include <vector>

#include "QualifiedMetaType.hpp"
#include "MetaTypeField.hpp"

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Immutable class which defines the fields held by a type
        /// Use a TypeBuilder to create an instance at runtime and a TypeDefiner
        /// to create one at compile time
        /////////////////////////////////////////////////
        class MetaType{
        public:
            typedef unsigned char Flag_t;
            typedef unsigned char FieldIndex_t;

            /////////////////////////////////////////////////
            /// \brief Various flags which store extra data about the type
            /////////////////////////////////////////////////
            enum Flags : Flag_t{
                None = 0,

                ///< A concrete type has a corrosponding compile time type, this is true true for all primatives
                ///< and TypeMetas that are based on a compile time class or struct
                Concrete = 1,

                ///< Set if the type only contains variables by value (ie: no pointers or references)
                ///< Hence the type will never refer to extra memory above its static size
                StaticMem = 2,
            };

            /////////////////////////////////////////////////
            /// \brief Constructor that creates a new MetaType instance
            /////////////////////////////////////////////////
            MetaType(std::string name, size_t staticSize, std::initializer_list<MetaTypeField> fields, Flag_t flags = Flags::None);

            ~MetaType();

            /////////////////////////////////////////////////
            /// \brief Returns the name of the type this MetaType describes
            /////////////////////////////////////////////////
            std::string getName() const;

            /////////////////////////////////////////////////
            /// \brief Returns the static size of the type
            /// Does not include any dynamically allocated memory the type may use!
            ///
            /// Some types will hold an array that is not a constant size for all instances
            /// of a type. The c++ compiler needs to know how much memory to allocate for a
            /// type at compile time, this means that the class must be a fixed size no matter how big
            /// the array it used is. Therefore a pointer must be held in the class which points at some block
            /// in memory, this block can be any size. This is the ONlY way instances of the same class can use
            /// different amounts of memory (although it may sometimes be wrapped up in a more farmilar way, eg std::vector)
            /// The c++ sizeof keyword will only include the size of the pointer to the dynamic memory block and NOT the size
            /// of the actual block.
            /// In the same way, this getStaticSize() function only returns the compile time known size (ie: the size of the pointer,
            /// and not the size of the block it points to!)
            ///
            /// \return size_t The size in bytes of this type
            /////////////////////////////////////////////////
            size_t getStaticSize() const;

            /////////////////////////////////////////////////
            /// \brief returns true if the type has the listed flags, else false
            /////////////////////////////////////////////////
            bool is(Flag_t flags) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if the type is atomic,
            /// an atomic type can not be broken down any furthur, thus it
            /// - has 0 fields
            /// - a non 0 static size
            /// - is MetaType::StaticMem
            /// All primative types (int, float, etc) are atomic
            /////////////////////////////////////////////////
            bool isAtomic() const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of fields that the MetaType has
            /////////////////////////////////////////////////
            unsigned char getFieldCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns the field at the specified index in the MetaType
            /////////////////////////////////////////////////
            const MetaTypeField& getField(FieldIndex_t fieldIndex) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two MetaTypes are the EXACT same instance.
            /// Why?
            /// The MetaType system depends on there only being a single MetaType instance
            /// per actual type, int only = int, not uint, etc, therefore for MetaTypes
            /// to be refering to the same type, they must be the same instance
            /////////////////////////////////////////////////
            bool operator==(const MetaType& other) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two MetaTypes are NOT the exact same instance
            /////////////////////////////////////////////////
            bool operator!=(const MetaType& other) const {return !this->operator==(other);}
        private:
            std::vector<MetaTypeField> mFields;

            //the name of this type
            const std::string mName;

            //the compile time known size of the type, equivilent to the sizeof() keyword
            const size_t mStaticSize;

            //stores extra flags about the type, eg wheter it is a primative type,
            //if it is pod, can be copied, etc
            const Flag_t mFlags;
        };

    }
}

#endif // LIE_CORE_METATYPE_HPP
