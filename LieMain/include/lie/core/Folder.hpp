#ifndef LIE_CORE_FOLDER_HPP
#define LIE_CORE_FOLDER_HPP
#include <lie/core\PathStyle.hpp>

#include <vector>
#include <string>
#include "ExcepFileIO.hpp"

namespace lie{
    namespace core{

        ///forward decleration
        namespace detail{
            class FolderImpl;
        }
        class File;

        /////////////////////////////////////////////////
        /// \brief The Folder class represents a path to a folder that may or may not exist
        /// physically. It provides functionality to querry the folders existance, create and
        /// delete the folder, list the files in the folder, etc
        /// \see lie::core::File
        /////////////////////////////////////////////////
        class Folder{
        public:
            friend class lie::core::detail::FolderImpl;

            /////////////////////////////////////////////////
            /// \brief Creates a new path from a string, this version tries to guess the
            /// style of the input string
            /// Call the overloaded version by specifying the path style
            /////////////////////////////////////////////////
            Folder(std::string path);

            //Folder(std::string path, lie::core::PathStyle style);

            /////////////////////////////////////////////////
            /// \brief Creates and returns a deep copy of the passed in Folder object
            /////////////////////////////////////////////////
            Folder(const Folder& otherdir);

            /////////////////////////////////////////////////
            /// \brief Returns the root file path of the system
            /// If multiple roots exist (like on windows where each volume has a root - C:\, D:\, etc)
            /// returns the root of the default volume
            /////////////////////////////////////////////////
            static Folder getRoot();

            /////////////////////////////////////////////////
            /// \brief Returns a directroy object which represents the folder containing the execuatble
            /// file that is currently running for this program
            /////////////////////////////////////////////////
            static Folder getExecutableFolder();

            /////////////////////////////////////////////////
            /// \brief Returns a string containing the name of the volume this Folder is on
            /// Eg: On windows "C:", "D:", etc
            /////////////////////////////////////////////////
            std::string getVolume() const;

            /////////////////////////////////////////////////
            /// \brief Sets the volume name for this Folder
            /////////////////////////////////////////////////
            void setVolume(std::string volume);

            /////////////////////////////////////////////////
            /// \brief Pushes a new folder onto the end of this Folder object
            /// Eg: pushing "images" onto "\game\data\" produces "\game\data\images"
            /// \param dir The name of the new Folder
            /// \return Folder& Reference to this Folder object for operator overloading, etc
            /////////////////////////////////////////////////
            Folder& push(std::string dir);

            /////////////////////////////////////////////////
            /// \brief Pops the last Folder element off of this Folder object
            /// Eg: poping the Folder "\game\data\images\" produces "\game\data\"
            /// \return Folder& Reference to this Folder object for operator overloading, etc
            /////////////////////////////////////////////////
            Folder& pop();

            /////////////////////////////////////////////////
            /// \brief Returns true if the Folder is root of its volume
            /////////////////////////////////////////////////
            bool isRoot() const;

            /////////////////////////////////////////////////
            /// \brief Returns a new instance of Folder which is the parent of this Folder
            /// If this is a root Folder, returns a deep copy of this instance
            /////////////////////////////////////////////////
            Folder getParent() const;

            /////////////////////////////////////////////////
            /// \brief Returns true if the Folder exists physically
            /// \return true if the Folder exists, else false
            /// \note This only checks if the Folder exists as a Folder,
            /// ie, if the path exists but as a file, returns false
            /////////////////////////////////////////////////
            bool exists() const;

            /////////////////////////////////////////////////
            /// \brief Attempts to create the Folder if it does not yet exist
            /// \param createParents if true will recurivly create any parent directories that do not exist, defaults to true
            /// \return false Either the Folder already existed or we failed to create it, most likely
            /// due to insufficient permisions
            /// true The Folder was created succsessfully
            ///
            /// \note if createParents is true and we succsessfully create some of the directories parents
            /// but then fail to create the final Folder the false is returned and the created parents will
            /// NOT be deleted
            /////////////////////////////////////////////////
            bool create(bool createParents = true) const;

            /////////////////////////////////////////////////
            /// \brief Returns a string representing the Folder's path and name
            /// By default returns it in the style of the native os, however can optionally
            /// return it in the style of another os
            /// \param style The style dictating how the path should be formatted
            /// \see lie::core::PathStyle
            /////////////////////////////////////////////////
            std::string getFullPath(lie::core::PathStyle style = lie::core::PathStyle::Native) const;

            /////////////////////////////////////////////////
            /// \brief Returns the last element of the path to the Folder, ie its name
            /// Eg: C:\folder1\folder2 returns "folder2"
            /////////////////////////////////////////////////
            std::string getName() const;

            /////////////////////////////////////////////////
            /// \brief Returns a vector containing Folder objects representing each of
            /// the subolders of this one
            /// \return std::vector<Folder>
            /// \note If the folder does not exist physically returns an empty vector
            /// \throw lie::core::ExcepFileIO, this can occur for multiple reasons, eg a folder name that is
            /// longer than the os supports, an os error, etc
            /////////////////////////////////////////////////
            std::vector<Folder> getSubfolders() const throw(lie::core::ExcepFileIO);

            /////////////////////////////////////////////////
            /// \brief Returns a vector containing File objects representing the files
            /// contained in this folder
            /// \return std::vector<File>
            /// \note If the folder does not exist physically returns an empty vector
            /// \throw lie::core::ExcepFileIO, this can occur for multiple reasons, eg a folder name that is
            /// longer than the os supports, an os error, etc
            /////////////////////////////////////////////////
            std::vector<File> getFiles() const throw(lie::core::ExcepFileIO);


            /////////////////////////////////////////////////
            /// \brief Returns true if all aspects of two directories are equal
            /////////////////////////////////////////////////
            bool operator==(const Folder& other) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two directories are not equal
            /////////////////////////////////////////////////
            bool operator!=(const Folder& other) const{return !this->operator==(other);}

        private:
            ///a list of directories, the final element is the name of  the target Folder
            std::vector<std::string> mDirList;

            ///the name of the volume this path refers to
            ///Eg: Windows -> C:, D:, E:, etc
            ///unix does not have volumes, instead everything is a subfolder
            ///of the root Folder "/"
            ///consoles use various prefixes to specify the volumne
            ///eg: /dev_bdvd/ for the blueray drive (pg 264 og Game Engine Architecture -> Jason Gregory)
            std::string mVolumeName;

            /////////////////////////////////////////////////
            /// \brief Default constructor for creating Folder object
            /// Use static get methods, copy constructor or construct from string
            /////////////////////////////////////////////////
            Folder();
        };


    }
}

#endif // LIE_CORE_Folder_HPP
