#ifndef LIE_CORE_BYTEBUFFER_HPP
#define LIE_CORE_BYTEBUFFER_HPP

#include "Convert.hpp"
#include "ExcepOutOfRange.hpp"
#include "ByteIterator.hpp"
#include <vector>
#include <string>
#include <cstring>
#include <type_traits>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A ByteBuffer is a container class which stores
        /// a dynamically growing byte array, much like an std::vector<uint8_t>
        /// However, unlike a vector it also allows other primative types to be
        /// inserted, automatically converting them to an appropriate number of bytes.
        /// Additionally any primative type can be retrieved with its bytes starting at
        /// a specified index
        ///
        /// \note The interface many simerallities with an std::vector however
        /// due to the different nature of these two classes there are places where the
        /// interface differs
        /////////////////////////////////////////////////
        class ByteBuffer{
        public:

            /// This is the primitive type which holds the bytes of the buffer
            typedef byte_t value_type;

            /////////////////////////////////////////////////
            /// \brief Returns the current number of bytes stores in the buffer
            /////////////////////////////////////////////////
            size_t size() const {return this->mBytes.size();}

            /////////////////////////////////////////////////
            /// \brief Returns the maximum theoretical limit for the number of bytes
            /// the buffer can contain due to hardware or software limitations.
            /// However the container is not guaranteed to be able to reach that size,
            /// it may fail to allocate storage at any point before that size is reached
            /////////////////////////////////////////////////
            size_t max_size() const {return this->mBytes.max_size();}

            /////////////////////////////////////////////////
            /// \brief Resizes the buffer so that it contains newSize bytes.
            ///
            /// If newSize is smaller than the ByteBuffer's current size then the contents
            /// is reduced to the first "newSize" bytes, all subsequent bytes are
            /// remove and destroyed
            ///
            /// If newSize is larger than the ByteBuffer's current size  then the existing
            /// bytes are preserved and additional bytes are inserted so that the total number is
            /// equal to newSize, by defualt these newBytes will be initialized to 0, however
            /// a differnt value can be optionally passed in
            ///
            /// \param newSize The new number of bytes for the ByteBuffer to contain
            ///
            /// \param val The value all new bytes will be assigned, defaults to 0
            /////////////////////////////////////////////////
            void resize(size_t newSize, value_type val = 0){this->mBytes.resize(newSize, val);}

            /////////////////////////////////////////////////
            /// \brief Returns the size of the storage space currently allocated for
            /// the ByteBuffer, in the number of bytes
            ///
            /// This will be greater than or equal to size, the exta size is so extra memory
            /// does not need to be allocated on every insertion
            ///
            /// \see reserve
            /////////////////////////////////////////////////
            size_t capacity() const {return this->mBytes.capacity();}

            /////////////////////////////////////////////////
            /// \brief Alters the capacity of this ByteBuffer, useful if you
            /// know the size of the data you will be adding and do not want
            /// reallocation occuring while inserting
            ///
            /// \param n The minumum number of bytes to allocate
            ///
            /// \note The capcaity may be greater than n, but will always be atleast n
            /////////////////////////////////////////////////
            void reserve(size_t n) {this->mBytes.reserve(n);}

            /////////////////////////////////////////////////
            /// \brief Decreases the ByteBuffer's capacity so that its
            /// memory footprint is decreased.
            ///
            /// \note The capcaity may remain larger than the size, this is
            /// implementation dependent
            /////////////////////////////////////////////////
            void shrink_to_fit(){this->mBytes.shrink_to_fit();}

            /////////////////////////////////////////////////
            /// \brief Returns by value the byte at the specified index in
            /// the ByteBuffer
            ///
            /// \note No check is performed to ensure index is within range,
            /// if it is out of range undefined behaviour will occur
            /////////////////////////////////////////////////
            value_type operator[](size_t index) const{return this->mBytes[index];}

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the byte at the specified index in
            /// the ByteBuffer
            ///
            /// \note No check is performed to ensure index is within range,
            /// if it is out of range undefined behavior will occur
            /////////////////////////////////////////////////
            value_type& operator[](size_t index) {return this->mBytes[index];}

            /////////////////////////////////////////////////
            /// \brief Reads the specified type from the buffer, the first byte of the type
            /// is at the specified index. Eg, reading an int32 at index 10 will make the int
            /// from index 10,11,12 and 13
            ///
            /// \param index The index of the first byte that should be read as part of the type
            ///
            /// \note When reading an instance of type T the buffers size must be >= than index+sizeof(T),
            /// eg, a 4 byte type from index 5 will read index 5,6,7 and 8. Hence the size must be 9 at a
            /// minium (>= 5+4)
            /// If LIE_DEBUG is defined this is checked and an instance of lie::core::ExcepOutOfRange will be
            /// thrown if this is not the case. If LIE_DEBUG is not defined then no checking will be done
            /// and undefined behavior will occur if this condition is not met
            /////////////////////////////////////////////////
            template<typename T>
            T& at(size_t index) throw(lie::core::ExcepOutOfRange<size_t>){
                #ifdef LIE_DEBUG
                    if(!(index + sizeof(T) <= this->size())){
                        throw lie::core::ExcepOutOfRange<size_t>(
							__FUNCTION__, "index", index, 0, this->size() - sizeof(T),
							std::string("Tried to read an instance of type" + std::string(typeid(T).name()) + 
							" from index " + lie::core::toString(index) +
                            " of a ByteBuffer of size " + lie::core::toString(this->size()) +
                             ", need to read " + lie::core::toString(sizeof(T)) + " bytes (the size of the type)"));
                    }
                #endif
                return bytesToType<T>(&this->mBytes[index]);
            }

			/////////////////////////////////////////////////
			/// \brief Reads the specified type from the buffer, the first byte of the type
			/// is at the specified index. Eg, reading an int32 at index 10 will make the int
			/// from index 10,11,12 and 13
			///
			/// \param index The index of the first byte that should be read as part of the type
			///
			/// \note When reading an instance of type T the buffers size must be >= than index+sizeof(T),
			/// eg, a 4 byte type from index 5 will read index 5,6,7 and 8. Hence the size must be 9 at a
			/// minimum (>= 5+4)
			/// If LIE_DEBUG is defined this is checked and an instance of lie::core::ExcepOutOfRange will be
			/// thrown if this is not the case. If LIE_DEBUG is not defined then no checking will be done
			/// and undefined behavior will occur if this condition is not met
			/////////////////////////////////////////////////
			template<typename T>
			const T& at(size_t index) const{
				#ifdef LIE_DEBUG
					if (!(index + sizeof(T) <= this->size())){
						throw lie::core::ExcepOutOfRange(std::string("Tried to read an instance of type T where T = ") +
							std::string(typeid(T).name()) + " at index " + lie::core::toString(index) +
							" from a ByteBuffer of size " + lie::core::toString(this->size()) +
							", index is too high to read " + lie::core::toString(sizeof(T)) + " bytes (the size of T)");
					}
				#endif
				return bytesToType<T>(&this->mBytes[index]);
			}

            /////////////////////////////////////////////////
            /// \brief const version which returns a const pointer to the memory
            /// array used internally to store its bytes
            /////////////////////////////////////////////////
            const value_type* data() const{return this->mBytes.data();}

            /////////////////////////////////////////////////
            /// \brief non-const version which returns a pointer to the memory
            /// array used internally to store its bytes
            /////////////////////////////////////////////////
            value_type* data(){return this->mBytes.data();}

            /////////////////////////////////////////////////
            /// \brief Removes all bytes from the ByteBuffer leaving it with a size of 0
            /// A reallocation is not guarrentied to occur and so capacity may or may not
            /// be changed
            /////////////////////////////////////////////////
            void clear(){this->mBytes.clear();}
			
			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds a copy of all the bytes stored in the other ByteBuffer to the end
			/// of this byte buffer. The passed in byte buffer is not modified
			/// \param other The ByteBuffer whose bytes you wish to append to the end of this one
			//////////////////////////////////////////////////////////////////////////
			void push_back(const ByteBuffer& other);

            /////////////////////////////////////////////////
            /// \brief Adds the specified object to the end of the buffer
            /// The buffers size will be increased by sizeof(T)
            /// \note This overload should only be used for objects passed by value,
            /// additionally type T should be a primitive type or a type
            /// which contains no pointers or references
            /////////////////////////////////////////////////
            template<typename T>
            void push_back(T obj){
                static_assert(!std::is_pointer<T>::value, "You must specify the size of the data array pointed to when pushing back a pointer Use push_back(ptr, size)");

                //:TODO: There really should be a static_assert here, but asserting what?
                //is_pod is too restrictive, eg, pushing vectors works fine but doesn't count
                //as a pod, although maybe it isn't guarrentied to work correctly... test!
                //also add to pointer push back

                //static_assert(std::is_pod<T>::value, "Can only push back pod types (and types with an explicit support, eg: std::string)");

                this->push_back(&obj, sizeof(obj));
            }

            /////////////////////////////////////////////////
            /// \brief Adds the specified string to the end of the buffer.
            /// Includes the null terminator character.
            /// Each char in the string takes up one byte, hence the size is increased
            /// by str.length() + 1, +1 for the null terminator
            /////////////////////////////////////////////////
            void push_back(std::string str){
                //:TODO: memcpy would be better
                for(int i = 0; i < str.size(); ++i){
                    this->mBytes.push_back(str.at(i));
                }
                this->mBytes.push_back('\0');
            }

            /////////////////////////////////////////////////
            /// \brief Pushes back an array of bytes, must specify the arrays
            /// size IN BYTES, NOT ELEMENT COUNT!
            /// Use sizeof where possible
            /////////////////////////////////////////////////
            template<typename T>
            void push_back(T* ptr, size_t byteSize){
                size_t vecSize = this->mBytes.size();

                if(this->mBytes.capacity() < this->mBytes.size() + byteSize){
                    //then need to reserve some more space in the vector
                    this->mBytes.reserve(this->mBytes.capacity() + byteSize);
                }

                //now vector definitely contains enough bytes to insert the data
                //resize so the vector thinks the added bytes are inserted
                this->mBytes.resize(vecSize + byteSize);

                //copy the bytes into it
                memcpy(&(this->mBytes[vecSize]), ptr, byteSize);

            }

            /////////////////////////////////////////////////
            /// \brief Synatical sugar for the push_back method, eg:
            /// \code
            /// bb.push_back(5);
            /// bb.push_back(1.23f);
            /// \endcode
            /// can instead be expressed as:
            /// \code
            /// bb << 5 << 1.23f;
            /// \endcode
            /// Note however that the push_back method can be clearer and even
            /// nessacerry as the template paramater can be explicitely specified rather
            /// than relying on template argument deduction, this is not
            /// possible with an operator overload.
            /// push_back() must also be used when adding arrays by pointer as the size
            /// of the array needs to be specified
            /////////////////////////////////////////////////
            template<typename T>
            ByteBuffer& operator<<(T obj){
                static_assert(!std::is_pointer<T>::value, "You cannot add pointers with operator<<, use push_back(pointer, size). This is to allow for adding arrays.");
                this->push_back(obj);
                return *this;
            }

        private:
            std::vector<value_type> mBytes;
        };

    }
}

#endif // LIE_CORE_BYTEBUFFER_HPP
