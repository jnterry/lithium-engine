#ifndef LIE_CORE_HANDLE_HPP
#define LIE_CORE_HANDLE_HPP

#include <memory>

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A SharedHandle is a way of sharing a single object between
        /// multiple owners, the managed object will be deleted when:
        /// - The last SharedHandle refering to it is delted
        /// - The last SharedHandle refering to it is assigned to a different pointer
        ///
        /// \note Currently typedefed to std::shared_ptr, if/when custom allocators
        /// are added it will also identify which allocator allocated it so the object
        /// can be delted correctly
        /////////////////////////////////////////////////
        template<typename T>
        using SharedHandle = std::shared_ptr<T>;

        /////////////////////////////////////////////////
        /// \brief A WeakHandle refers to an object which is managed by one or
        /// more SharedHandle, it does not count as an owner and so the object
        /// may be deleted at anytime if the last remaining SharedHandle is deleted
        /// or reassigned. In order to gain access to the object a SharedHandle must
        /// first be made from the WeakHandle by calling "lock".
        ///
        /// It is also helpful in breaking a circular dependency of SharedHandels
        ///
        /// \note Currently typedefed to std::shared_ptr, if/when custom allocators
        /// are added it will also identify which allocator allocated it so the object
        /// can be delted correctly
        /////////////////////////////////////////////////
        template<typename T>
        using WeakHandle = std::weak_ptr<T>;

        /////////////////////////////////////////////////
        /// \brief A UniqueHandle will manages a Handle to a dynamically allocated object.
        /// Only a single instance of UniqueHandle can own a given dynamically allocated object.
        /// The object will be deleted when:
        ///  - The UniqueHandle is deleted
        ///  - The UniqueHandle is reasigned to some other Handle
        ///
        /// \note Currently typedefed to std::shared_ptr, if/when custom allocators
        /// are added it will also identify which allocator allocated it so the object
        /// can be delted correctly
        /////////////////////////////////////////////////
        template<typename T>
        using UniqueHandle = std::unique_ptr<T>;

    }
}

#endif // LIE_CORE_HANDLE_HPP
