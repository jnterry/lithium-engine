/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Hash.hpp
/// Function for obtaining the hash of a string, constexpr so that it
/// can be done by compiler when possible
///
/// \ingroup core
/////////////////////////////////////////////////
#ifndef LIE_CORE_HASH_HPP
#define LIE_CORE_HASH_HPP

#include <cstring>
#include <string>

namespace lie{
    namespace core{
        typedef unsigned int HashValue;


        namespace detail{
            inline constexpr HashValue _hashStringRecursive(HashValue hash, const char* str){
                return (!*str ? hash : _hashStringRecursive((hash<<5) + hash + *str, str+1));
            }
        }

        inline constexpr HashValue hashString(const char* str){
            return (!str ? 0 : detail::_hashStringRecursive(5381, str));
        }


        /////////////////////////////////////////////////
        /// \brief Class that wraps a constant string and automatically calculates its hash value
        /// If you only need to hash value and do not need to retrieve the original string then use
        /// the "hashString()" function instead
        /////////////////////////////////////////////////
        class HashedString{
        private:
            //char* mString;
            HashValue mHash;
        public:
            explicit constexpr HashedString(const char* str) :
                mHash(hashString(str))//,
                //mString(new char[std::char_traits<char>::length(str)])
                {
                    //strcpy(mString, str);
                }

            HashValue getHash(){return mHash;}
            //char* getString(){return mString;}
        };

    }
}


#endif // LIE_CORE_HASH_HPP
