/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Log.hpp
/// The logging system for Lithium Engine
///
/// \ingroup core
/////////////////////////////////////////////////
#ifndef LIE_CORE_LOGGER_HPP
#define LIE_CORE_LOGGER_HPP

#include <lie/core\RTimeMsg.hpp>
#include <lie/core\LogMsgWritterBase.hpp>

#include <vector>


#if defined(LIE_DISABLE_DLOG_TRACE) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_TRACE(module, message)

#else
    #define LIE_DLOG_TRACE(module, message) \
    lie::core::DLog.write(LIE_RTMSG_TRACE(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_INFO) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_INFO(module, message)
#else
    #define LIE_DLOG_INFO(module, message) \
    lie::core::DLog.write(LIE_RTMSG_INFO(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_REPORT) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_REPORT(module, message)
#else
    #define LIE_DLOG_REPORT(module, message) \
    lie::core::DLog.write(LIE_RTMSG_REPORT(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_WARN) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_WARN(module, message)
#else
    #define LIE_DLOG_WARN(module, message) \
    lie::core::DLog.write(LIE_RTMSG_WARN(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_ERROR) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_ERROR(module, message)
#else
    #define LIE_DLOG_ERROR(module, message) \
    lie::core::DLog.write(LIE_RTMSG_ERROR(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_FATAL) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG_FATAL(module, message)
#else
    #define LIE_DLOG_FATAL(module, message) \
    lie::core::DLog.write(LIE_RTMSG_FATAL(module) << message);
#endif

#if defined(LIE_DISABLE_DLOG_CUSTOM) || defined(LIE_DISABLE_DLOG_ALL)
    #define LIE_DLOG(level, module, message)
#else
    #define LIE_DLOG(level, module, message) \
    lie::core::DLog.write(LIE_RTMSG(level, module) << message);
#endif

namespace lie{
    namespace core{

        class Logger{
        public:
            Logger();
            ~Logger();

            /////////////////////////////////////////////////
            /// \brief Adds a new LogWritter (output) to this logger
            /// A shared pointer is used allowing the writters to be shared between loggers
            /// or to be used by a logger and manually, its lifetime will be automaticly managed
            /// and it will be deleted when no longer needed by any log or manual user
            /////////////////////////////////////////////////
            void addWritter(LogMsgWritterPtr writter);

            /////////////////////////////////////////////////
            /// \brief Writes a RTimeMsg to the log
            /// A macro can (should) be used to generate the log message
            /// These macros are defined are in RTimeMsg.h
            /// Eg: LIE_RTMSG_TRACE
            ///     LIE_RTMSG_ERROR
            /// Usage example:
            /// \code
            /// logger.write(LIE_RTMSG_TRACE("module name - eg "lie::core::Logger") << "message item 1" << " item 2");
            /// \endcode
            /////////////////////////////////////////////////
            void write(const RTimeMsg& msg);


        private:
            std::vector<LogMsgWritterPtr> mWritters;
        };

        extern Logger DLog;
    }
}


#endif // LIE_CORE_LOG_HPP
