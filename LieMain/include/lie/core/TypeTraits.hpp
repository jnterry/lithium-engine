#ifndef LIE_CORE_TYPETRAITS_HPP
#define LIE_CORE_TYPETRAITS_HPP

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Template class which will count the
        /// level of pointer indirection at compile time, eg
        /// \code
        /// std::cout << lie::core::CountPointerLevel<int> << std::endl; //prints 0
        /// std::cout << lie::core::CountPointerLevel<int*> << std::endl; //prints 1
        /// std::cout << lie::core::CountPointerLevel<int**> << std::endl; //prints 2
        /// std::cout << lie::core::CountPointerLevel<int***> << std::endl; //prints 3
        /// std::cout << lie::core::CountPointerLevel<const int***> << std::endl; //prints 4
        /// \endcode
        /////////////////////////////////////////////////
        template<typename T>
        class CountPointerLevel{
        public:
            enum val{ value = 0 };
        };

        template<typename T>
        class CountPointerLevel<T*>{
        public:
            enum val{ value = 1 + CountPointerLevel<T>::value };
        };

    }
}

#endif // LIE_CORE_TYPETRAITS_HPP
