#ifndef LIE_CORE_LOGMSGWRITTERBASE_HPP
#define LIE_CORE_LOGMSGWRITTERBASE_HPP

#include <lie/core\RTimeMsg.hpp>

#include <memory>
namespace lie{
    namespace core{
        /////////////////////////////////////////////////
        /// \brief Class that writes a runtime message to some device (file, cout, stream, network, etc)
        /// if the message satisfies the specified filter
        /////////////////////////////////////////////////
        class LogMsgWritterBase{
        public:
            /////////////////////////////////////////////////
            /// \brief Writes the specified message to this writter's output device
            /// if the filter is satified
            /// (specifically calls the virtual "onWrite()" method which child classes MUST overide
            /////////////////////////////////////////////////
            void write(const RTimeMsg& msg);

            /////////////////////////////////////////////////
            /// \brief Set the filter that messages must satisfy to be written by this writter
            /// if null is passed in then writter will write all messages passed to it via write()
            /////////////////////////////////////////////////
            void setFilter(RTimeMsgFilter filter);

            virtual void onWrite(const RTimeMsg& msg) = 0;

            LogMsgWritterBase() : mFilter(nullptr){}

            //virtual destructor so child classes can clean up (eg, closing file, network connection, etc)
            virtual ~LogMsgWritterBase(){}
        private:
            RTimeMsgFilter mFilter;

        };

        /////////////////////////////////////////////////
        /// \brief A shared smart pointer to an instance of a RTimeMsgWritter,
        /// used to share the writter between logs or between a log and manual use
        /////////////////////////////////////////////////
        typedef std::shared_ptr<LogMsgWritterBase> LogMsgWritterPtr;

    }
}


#endif // LIE_CORE_LogMsgWritter_HPP
