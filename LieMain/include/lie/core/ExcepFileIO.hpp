#ifndef LIE_CORE_EXCEPFILEIO_HPP
#define LIE_CORE_EXCEPFILEIO_HPP

#include "Exception.hpp"


namespace lie{
    namespace core{
        /////////////////////////////////////////////////
        /// \brief This excpetion is thrown when an error related to file io occurs
        /// This can be a wide range of things from not having permision to open/edit a file
        /// to a file not existing to file names being to long for the os
        /////////////////////////////////////////////////
        class ExcepFileIO : public Exception{
        public:
			ExcepFileIO(std::string function, std::string path, std::string details, Exception* cause = nullptr, lie::core::Logger& logger = lie::core::DLog);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the path of the folder or file that was being operated on
			/// when the error occured
			//////////////////////////////////////////////////////////////////////////
			std::string getPath();
		private:
			std::string mPath;
        };

    }
}


#endif // LIE_CORE_EXCEPFILEIO_HPP
