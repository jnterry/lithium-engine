#ifndef LIE_CORE_RESOURCE_HPP
#define LIE_CORE_RESOURCE_HPP

#include "File.hpp"
#include "Hash.hpp"
#include "NonCopyable.hpp"

namespace lie{
    namespace core{

        //forward decleration
        template <typename T>
        class ResourceHandle;

        /////////////////////////////////////////////////
        /// \brief A Resource stores information about data which is too be loaded
        /// in from a file
        ///
        /// This class is should generally not used directly by clients.
        /// A ResourceHandle should be used anywhere that you are using a resource (eg, rendering a mesh)
        /// as it acts as a speciallised smart pointer
        ///
        /// The ResourceDatabase manages a set of instances of Resource
        ///
        /// The two main uses of this class (to client code) are:
        /// <ol>
        /// <li>When adding Resource objects to a ResourceDatabase - call: <br>
        /// resDatabase.addResource<TYPE>(new lie::core::Resource<TYPE>(file, "test.txt"), byteOffset, Name)); <br>
        /// however standard solutions exist such as scanning a directory and automaticaly generating the resources, hence this does
        /// not need to be done unless you want to manually add resources from a custom file format, locations, etc </li>
        /// <li>Finding a resource in a ResourceDatabase is slightly slow as it involves map lookups, using a ResourceHandle
        /// will request the resource be loaded so if at any point you wish to reference a Resource without loading it you should
        /// store a pointer to the instance of Resource, you can then later make a ResourceHandle from it</li>
        /// </ol>
        /////////////////////////////////////////////////
        template <typename T>
        class Resource : NonCopyable{
            friend class ResourceDatabase;
            friend class ResourceHandle<T>;
            friend class ResourceLoadPolicy;
        public:

            /////////////////////////////////////////////////
            /// \brief Constucts a new Resource from the specified file,
            /// byte offset and name
            /////////////////////////////////////////////////
            Resource(File file, unsigned int fileByteOffset, std::string resName, std::string loaderName) :
                mFile(file), mFileByteOffset(fileByteOffset), mName(resName),
                mInstPtr(nullptr), mNameHash(lie::core::hashString(resName.c_str())),
                mLoaderNameHash(lie::core::hashString(loaderName.c_str())){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Returns the hash value of the resources name
            /////////////////////////////////////////////////
            lie::core::HashValue getHash(){
                return this->mNameHash;
            }

            /////////////////////////////////////////////////
            /// \brief Returns a string containing the resources name
            /////////////////////////////////////////////////
            std::string getName(){
                return this->mName;
            }
        private:
            /////////////////////////////////////////////////
            /// \brief Returns true if the resource is currently loaded, else returns false
            /////////////////////////////////////////////////
            bool isLoaded(){
                return mInstPtr != nullptr;
            }

            /////////////////////////////////////////////////
            /// \brief Increments the request count and notifies the ResourceDatabase
            /////////////////////////////////////////////////
            void request(){
                ++this->mRequestCount;
            }

            void release(){
                --this->mRequestCount;
            }

            ///< Pointer to the instance of T that is this resource, nullptr if resource is not loaded
            T* mInstPtr;

            ///< The file containing this resource
            File mFile;

            ///< Hash value of the name of the type of loader to be used, eg a .mesh file will be loaded by a loader with the hash of "mesh"
            lie::core::HashValue mLoaderNameHash;

            ///< The number of bytes into the file that the resource data starts at, for individual files
            /// resource will start at byte 0, for pack files (eg: zip, custom format, etc) it will be non-0
            unsigned int mFileByteOffset;

            ///< The name of the resource
            std::string mName;

            ///< The hash of the name of the resource
            lie::core::HashValue mNameHash;

            ///< Counts the number of times that the resource has been requested
            unsigned short mRequestCount;
        };

    }
}

#endif // LIE_CORE_RESOURCE_HPP
