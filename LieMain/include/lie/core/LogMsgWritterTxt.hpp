#ifndef LIE_CORE_LOGMSGWRITTERTXT_HPP
#define LIE_CORE_LOGMSGWRITTERTXT_HPP

#include <lie/core\LogMsgWritterBase.hpp>
#include <fstream>

namespace lie{
    namespace core{

        class LogMsgWritterTxt : public LogMsgWritterBase{
        public:
            void onWrite(const RTimeMsg& msg);

            /////////////////////////////////////////////////
            /// \brief Opens specified file and prepares it for logging
            /////////////////////////////////////////////////
            LogMsgWritterTxt(std::string filename);

            /////////////////////////////////////////////////
            /// \brief Closes the file
            /////////////////////////////////////////////////
            ~LogMsgWritterTxt();
        private:
            std::ofstream mFile;


        };

    }
}

#endif // LIE_CORE_LogMsgWritterTXT_HPP
