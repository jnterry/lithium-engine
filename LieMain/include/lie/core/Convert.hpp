/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Convert.hpp
/// Utility functions for converting variable types (numeric to string
/// and vice versa) and a function for comparing floats to see if they are
/// approximately equal (ie, equal allowing for floating point errors, rounding,
/// truncation etc)
///
/// \ingroup core
/////////////////////////////////////////////////
#ifndef LIE_SYSTEM_CONVERT_HPP
#define LIE_SYSTEM_CONVERT_HPP
#include <string>
#include <ostream>

namespace lie{
    namespace core{
		class ExcepParseError;

        /////////////////////////////////////////////////
        /// \brief Parses an int from a string and returns the int, if the string contains
        /// a non integer value throws an exception. The integer may be preceded by any amount
        /// of white space and a + or minus sign, eg, valid formats:
        ///  - "1"
        ///  - "+1"
        ///  - "-1"
        ///  - "   1"
        ///  - "   +1"
        ///  - "   -1"
        /// Invalid Formats (will throw exception):
        ///  - ""
        ///  - "a"
        ///  - "1a"
        ///  - "a1"
        ///  - "++1"
        ///  - "--1"
        ///  - "1.1"
        /// \throw lie::core::ExcepParseError if the string does not contain an int
        /// or contains an int followed by extra characters, eg, 1abc, 1.2
        /// \note If the string contains an integer which is too large or too small to be
        /// stored by an int then the maximum or minimum int value is returned respectively,
        /// no exception is thrown
        /////////////////////////////////////////////////
		int parseInt(std::string str) throw(lie::core::ExcepParseError);

        /////////////////////////////////////////////////
        /// \brief Parses a double from a string and returns the double
        /// \throw lie::core::ExcepInvalidParameter if the string does not contain a double
        /////////////////////////////////////////////////
		double parseDouble(std::string str) throw(lie::core::ExcepParseError);

        /////////////////////////////////////////////////
        /// \brief Parses a float from a string and returns the float
        /// \throw lie::core::ExcepInvalidParameter if the string does not contain a float
        /////////////////////////////////////////////////
		float parseFloat(std::string str) throw(lie::core::ExcepParseError);

        /////////////////////////////////////////////////
        /// \brief Compares two floats and returns true if the floats are roughly equal
        /// Works by the fact there are a finite number of floats
        /// Imagine an array of all floats, this function returns true if
        /// f2 is between FloatsArray[index of f1 - maxOffset] and FloatsArray[index of f1 + maxOffset]
        ///
        /// \param f1 -> the first float to compare
        /// \param f2 -> the second float to compare
        /// \param maxOffset -> the maximum number of floats allowed between f1 and f2 where the function
        /// will still return true, defaults to 20
        /// \return true if the floats are almost equal (within maxOffset), else false
        /////////////////////////////////////////////////
        bool compareFloats(float f1, float f2, int maxOffset = 20);

        /////////////////////////////////////////////////
        /// \brief Compares two doubles and returns true if the doubles are roughly equal
        /// Works by the fact there are a finite number of doubles
        /// Imagine an array of all doubles, this function returns true if
        /// f2 is between FloatsArray[index of f1 - maxOffset] and FloatsArray[index of f1 + maxOffset]
        ///
        /// \param f1 -> the first float to compare
        /// \param f2 -> the second float to compare
        /// \param maxOffset -> the maximum number of doubles allowed between f1 and f2 where the function
        /// will still return true, defaults to 20
        /// \return true if the doubles are almost equal (within maxOffset), else false
        /////////////////////////////////////////////////
        bool compareDoubles(double d1, double d2, int maxOffset = 20);

		/////////////////////////////////////////////////
		/// \brief Converts a number into a string
		/// \param number The number to convert into a string
		/// \return std::string containing representation of the number
		///
		/// \note c++11 introduces a new to_string function which converts numbers to strings,
		/// however mingw's string header has a bug where it is not defined due to #ifdef blocks.
		/// Newer versions fix this, but for compatibility this function is used
		/// Also overloads exist for bools, etc
		/////////////////////////////////////////////////
		template<typename T>
		std::string toString(T number){
			std::stringstream ss;
			ss << number;
			return ss.str();
		}
    }
}

#endif
