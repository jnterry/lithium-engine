#ifndef LIE_CORE_ENDIAN_HPP
#define LIE_CORE_ENDIAN_HPP

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Performs a runtime check to determine if the system
        /// is big or little endian, returns true if the system is little
        /// endian
        /////////////////////////////////////////////////
        bool isSystemLittleEndian();

        std::uint8_t swapEndian(std::uint8_t val);
        std::int8_t swapEndian(std::int8_t val);
        std::uint16_t swapEndian(std::uint16_t val);
        std::int16_t swapEndian(std::int16_t val);
        std::uint32_t swapEndian(std::uint32_t val);
        std::int32_t swapEndian(std::int32_t val);
        std::uint64_t swapEndian(std::uint64_t val);
        std::int64_t swapEndian(std::int64_t val);

        std::uint8_t toLittleEndian(std::uint8_t val);
        std::int8_t toLittleEndian(std::int8_t val);
        std::uint16_t toLittleEndian(std::uint16_t val);
        std::int16_t toLittleEndian(std::int16_t val);
        std::uint32_t toLittleEndian(std::uint32_t val);
        std::int32_t toLittleEndian(std::int32_t val);
        std::uint64_t toLittleEndian(std::uint64_t val);
        std::int64_t toLittleEndian(std::int64_t val);

        std::uint8_t toBigEndian(std::uint8_t val);
        std::int8_t toBigEndian(std::int8_t val);
        std::uint16_t toBigEndian(std::uint16_t val);
        std::int16_t toBigEndian(std::int16_t val);
        std::uint32_t toBigEndian(std::uint32_t val);
        std::int32_t toBigEndian(std::int32_t val);
        std::uint64_t toBigEndian(std::uint64_t val);
        std::int64_t toBigEndian(std::int64_t val);


    }
}


#endif // LIE_CORE_ENDIAN_HPP
