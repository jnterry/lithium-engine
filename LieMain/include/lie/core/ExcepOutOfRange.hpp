#ifndef LIE_CORE_EXCEPOUTOFRANGE_HPP
#define LIE_CORE_EXCEPOUTOFRANGE_HPP

#include "Exception.hpp"
#include "StringUtils.hpp"

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief A class to be thrown when some value is outside of an acceptable
        /// range, for example, an array index is too high.
        /// \tparam T The type of value that is out of range
        /////////////////////////////////////////////////
        template<typename T>
        class ExcepOutOfRange : public Exception{
        public:

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new ExcepOutOfRange exception
			/// \param function The name of the function throwing the exception, use
			/// the compiler macro __FUNCTION__
			/// \param valueName the name of the value that is out of range
			/// \param value The offending value that is out of range
			/// \param low The lowest acceptable value
			/// \param high The highest acceptable value
			/// \param details human readable string with extra details
			/// \param cause Pointer to the exception that caused this one to be thrown
			//////////////////////////////////////////////////////////////////////////
			ExcepOutOfRange(std::string function, std::string valueName, T value, T low, T high,
				std::string details, Exception* cause = nullptr,
				lie::core::Logger& logger = lie::core::DLog)
				: mLow(low), mHigh(high), mValue(value),
				Exception("lie::core::ExcepOutOfRange", function, 
					generateMessage(valueName, value, low, high, details, logger), cause, logger){
				//empty body
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the lowest acceptable value
			//////////////////////////////////////////////////////////////////////////
			T getLow() const{
				return this->mLow;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the highest acceptable value
			//////////////////////////////////////////////////////////////////////////
			T getHigh() const{
				return this->mHigh;
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the offending value that was out of range
			//////////////////////////////////////////////////////////////////////////
			T getValue() const{
				return this->mValue;
			}
		private:
			///< The lowest acceptable value
			T mLow;

			///< The highest acceptable value
			T mHigh;

			///< The offending value (ie, value that is not in range)
			T mValue;

			static std::string generateMessage(std::string valueName, T value, T low, T high, std::string details, lie::core::Logger& logger){
				StringBuilder sb;
				sb << details << "\n";
				sb << "The value " << valueName;
				if (value < low){
					sb << " is too low";
				} else if (value > high){
					sb << " is too high";
				} else {
					//somethings gone wrong, either exception shouldn't have been thrown or low and high are incorrect
					sb << " doesn't appear to be out of bounds (??? error)";
					logger.write(LIE_RTMSG_ERROR("ExcepOutOfRange") << "ExcepOutOfRange is being constructed but the value does not appear to be out of range, value: "
						<< value << ", range: " << low << " - " << high);
				}
				sb << ", value: " << value << ", acceptable range: " << low << " - " << high;
				return sb;
			}
        };

    }
}


#endif // LIE_CORE_EXCEPOUTOFRANGE_HPP

