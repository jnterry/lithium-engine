/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file StringUtil.hpp
/// \author Jamie Terry
/// \date 2015/08/04
/// \brief Contains various utilities for dealing with std::string
/// 
/// \ingroup core
/////////////////////////////////////////////////

#ifndef LIE_CORE_STRINGUTIL_HPP
#define LIE_CORE_STRINGUTIL_HPP

#include <string>
#include <sstream>
#include <algorithm>
#include <vector>

namespace lie{
	namespace core{
		/////////////////////////////////////////////////
		/// \brief Utility class that can convert any number of streamable objects into a string
		/// \code
		/// std::string str = lie::core::StringBuilder() << "num: " << 1.23f << ", succseed: " << false;
		/// \endcode
		/////////////////////////////////////////////////
		class StringBuilder{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new StringBuilder
			//////////////////////////////////////////////////////////////////////////
			StringBuilder();

			/////////////////////////////////////////////////
			/// \brief Adds a new item to the stream, compile time error if the
			/// type cannot be streamed
			/////////////////////////////////////////////////
			template<typename T>
			StringBuilder& operator<<(const T& val){
				mStream << val;
				return *this;
			}

			template<>
			StringBuilder& operator<<(const std::string& val){
				return (*this) << val.c_str();
			}

			StringBuilder& operator<<(const char* val);

			/////////////////////////////////////////////////
			/// \brief Allows an instnace of the class to be implicitelly cast to a std::string
			/// Allows the following syntax:
			/// \code
			/// std::string s = lie::core::StringBuilder() << 1 << true << " test";
			/// \endcode
			/////////////////////////////////////////////////
			operator std::string() const;

			/////////////////////////////////////////////////
			/// \brief Returns the string that has been created, not usually
			/// needed as will be implicitly cast to a std::string in most cases.
			/////////////////////////////////////////////////
			std::string str() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds one to the indent level, the indent string will be repeated
			/// getIndentLevel() times after each new line character.
			/// \note If the level is already at its maximum (255) this function will return false
			/// and do nothing
			//////////////////////////////////////////////////////////////////////////
			bool incrementIndentLevel();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Subtracts one from the indent level, the indent string will be repeated
			/// getIndentLevel() times after each new line character.
			/// \note If the level is already at its minimum (0) this function will return false
			/// and do nothing
			//////////////////////////////////////////////////////////////////////////
			bool decrementIndentLevel();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the current indent level of this StringBuilder, the
			/// indent string will be repeated getIndentLevel() times after each new
			/// line character
			/// \param level The new level to use
			//////////////////////////////////////////////////////////////////////////
			void setIndentLevel(unsigned char level);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the current indent level of this StringBuilder, the
			/// indent string will be repeated getIndentLevel() times after each new
			/// line character
			//////////////////////////////////////////////////////////////////////////
			unsigned char getIndentLevel();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the string to be used to indent new lines when indent level
			/// is > 0, this string will be inserted after each new line character 
			/// getIndentLevel() times. If not changed 4 spaces will be used.
			//////////////////////////////////////////////////////////////////////////
			void setIndentString(std::string str);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the string currently being used to indent new lines, this
			/// string will be repeated getIndentLevel() times after each new line character
			//////////////////////////////////////////////////////////////////////////
			std::string getIndentString();
		private:
			///< The string stream being used to build the stream
			std::ostringstream mStream;
			
			///< The number of times to repeat mIndentString after a new line character
			unsigned short mIndentLevel;

			///< The string used to indent each new line if indent level > 0
			std::string mIndentString;
		};
	
		//////////////////////////////////////////////////////////////////////////
		/// \brief Compares two strings and returns true if they are equal, ignoring
		/// the case of the characters A-Z/a-z
		//////////////////////////////////////////////////////////////////////////
		bool equalsIgnoreCase(std::string left, std::string right);

		//////////////////////////////////////////////////////////////////////////
		/// \brief Compares two characters and returns true if they are equal or
		/// are equal ignoring the case of the letter. Non-letters are only
		/// equal if they are the same character.
		//////////////////////////////////////////////////////////////////////////
		bool equalsIgnoreCase(char l, char r);
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Splits the string 'str' into tokens, with each token beginging and
		/// ending on a seperator character. 
		/// Adds each token of the string to the result vector. The result vector is
		/// passed by reference to this function rather than returning a std::vector
		/// in order to avoid copying the vector or newing it and returning a pointer.
		/// \param result std::vector of strings that will have the tokens push_back()ed
		/// into, this vector will not be cleared, the new elements will be appended to the end
		/// \param str The string to split
		/// \param seperator The character that will be used to split the string,
		/// for example, using '.' will split the string "hello.world" into
		/// two tokens each appended to the vector with the values "hello" and "world"
		/// \return int containing the number of new elements appended to the result std::vector
		/// \note The start and end of the string ARE considered separators unless the first and last character
		/// are separators, therefore, the following strings when split by "/" will all be split into 
		/// 3 components: "a/b/c", "/a/b/c", "a/b/c/.", "/a/b/c/".
		/// A string that does not contain any of the seperator character will be considered as a single token, as
		/// the start and end of the string will be considered as separators in this case, therefore the string will
		/// be appended to the vector unchanged.
		/// A string of length 1 that contains a just the seperator character will not be split, 0 elements will be
		/// appended to the vector as the first and last character is a seperator, hence neither the start
		/// or end of the string counts as a seperator, hence there is no pair of separators between
		/// which a token is found.
		/// If two instances of the seperator are adjacent an empty string will be pushed back to the
		/// result vector, ie, the string "//" when separated by "/" would have a single. The exception
		/// to this rule is that an empty string which would normally be considered as having a seperator
		/// at the start and the end does not have an empty token pushed back to the vector
		//////////////////////////////////////////////////////////////////////////
		int stringSplit(std::vector<std::string>& result, std::string str, char seperator);
	}
}

#endif //LIE_CORE_STRINGUTIL_HPP