/////////////////////////////////////////////////
/// \file Chars.hpp
/// \brief Useful utilitiy functions for checking if a character is a number, letter,
/// capital letter, etc
///
/// \ingroup core
/////////////////////////////////////////////////
#ifndef LIE_CORE_CHARS_HPP
#define LIE_CORE_CHARS_HPP

namespace lie{
    namespace core{
        namespace chars{
            /////////////////////////////////////////////////
            /// \brief returns true if the character is A,B,C...Z
            /// else returns false
            /////////////////////////////////////////////////
            bool isCapitalLetter(char c);

            /////////////////////////////////////////////////
            /// Returns true if the character is a,b,c,...z
            /// else returns false
            /////////////////////////////////////////////////
            bool isLowercaseLetter(char c);

            /////////////////////////////////////////////////
            /// \brief returns true if the character is a...z or A...Z
            /// else returns false
            /////////////////////////////////////////////////
            bool isLetter(char c);

            /////////////////////////////////////////////////
            /// \brief Returns true if the character is 0,1...9
            /// else returns false
            /////////////////////////////////////////////////
            bool isNumeric(char c);
        }
    }
}

#endif // LIE_CORE_CHARS_HPP
