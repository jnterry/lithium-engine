#ifndef LIE_CORE_TYPEINFO_HPP
#define LIE_CORE_TYPEINFO_HPP

#include <type_traits>
#include "MetaType.hpp"
#include "TypeTraits.hpp"

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief The TypeInfo class can be used to retrieve the MetaType
        /// instance for a concrete (ie: compile time defined) type
        ///
        /// This is done at compile time through templates and hence has minimum
        /// overhead, eg:
        /// \code
        /// lie::core::MetaType intMeta = lie::core::TypeInfo<int>::type;
        /// \endcode
        ///
        /// However, it isn't just magic, if you want it to work for your custom type
        /// you must provide the MetaType, otherwise you will get a compile time error
        /// eg:
        /// \code
        /// lie::core::TypeInfo<MyAwesomeClass>::type.getName();
        /// \endcode
        /// will produce the error:
        /// \code
        /// undefined reference to 'lie::core::TypeInfo<MyAwesomeClass>::type
        /// \endcode
        /// To define the MetaType for your type you must do the following, either
        /// in the header file for your type or the cpp. The header is likely
        /// better as that way it is only generated if you include your type, rather
        /// than being compiled anyway as it would be in a cpp
        /// \code
        /// template<> const lie::core::MetaType lie::core::TypeInfo<MyAwesomeType>::type
        /// = MetaType("MyAwesomeType", {field array here}, sizeof(MyAwesomeType), lie::core::MetaType::Concrete | OTHERFLAGS);
        /// \endcode
        /// Thats quite a lot of code, much of which is unessacery, feel free to use the helper macro, as follows:
        /// \code
        /// LIE_DEF_TYPE(MyAwesomeType, {Field1, Field2, ...}, Flags);
        /// \endcode
        /// The Flags is optionally, whether you specify any or not, lie::core::MetaType::Concrete will be added
        /// since the TypeInfo only works for concrete types.
        /////////////////////////////////////////////////
        template<typename T>
        class TypeInfo{
        public:
            static const MetaType type;
        };


        /////////////////////////////////////////////////
        /// \brief Returns a QualifiedMetaType for the specified template parameter
        /// \note This only works if TypeInfo<Unqualified-Type>::type has been defined somewhere,
        /// where  Unqualfied-Type is the base type of T, eg
        ///   - int* -> int
        ///   - int& -> int
        ///   - const int** -> int
        ///   - etc
        /////////////////////////////////////////////////
        template <typename T>
        QualifiedMetaType typeInfoQualified(){
            return lie::core::QualifiedMetaType(TypeInfo<T>::type, CountPointerLevel<T>::value, std::is_reference<T>::value);
        }
    }
}

#endif // LIE_CORE_TYPEINFO_HPP
