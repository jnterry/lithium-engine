#ifndef LIE_CORE_TYPES_HPP
#define LIE_CORE_TYPES_HPP

#include <string>
#include <cstddef>

namespace lie{
    namespace core{

    enum class PrimativeType{
        /// \brief An 8 bit unsigned type
        uInt8,

        /// \brief An 8 bit signed type
        Int8,

        /// \brief A 16 bit unsigned type
        uInt16,

        /// \brief A 16 bit signed type
        Int16,

        /// \brief A 32 bit unsigned type
        uInt32,

        /// \brief A 32 bit signed type
        Int32,

        /// \brief A 32 bit floating point type
        Float,

        /// \brief A 64 bit floating point type
        Double,
    };

    /////////////////////////////////////////////////
    /// \brief Returns the size in bytes of a PrimativeType
    /////////////////////////////////////////////////
    size_t primativeSize(PrimativeType type);

    /////////////////////////////////////////////////
    /// \brief Returns a std::string with the name of a primative, eg, "Int32",
    /// "Float", etc
    /////////////////////////////////////////////////
    std::string primativeString(PrimativeType type);

    /////////////////////////////////////////////////
    /// \brief Returns the PrimativeType for a given template parameter
    /// \note If the specified type is not a primative a compile time error will occur
    /////////////////////////////////////////////////
    template<typename T>
    PrimativeType getPrimativeType();

    template<> PrimativeType getPrimativeType<unsigned char>();
    template<> PrimativeType getPrimativeType<char>();
    template<> PrimativeType getPrimativeType<unsigned short>();
    template<> PrimativeType getPrimativeType<short>();
    template<> PrimativeType getPrimativeType<unsigned int>();
    template<> PrimativeType getPrimativeType<int>();
    template<> PrimativeType getPrimativeType<float>();
    template<> PrimativeType getPrimativeType<double>();

    }
}

#endif // LIE_CORE_TYPES_HPP
