/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ExcepParseError.hpp 
/// \author Jamie Terry
/// \date 2015/08/04
/// \brief Contains the lie::core::ExcepParseError class
/// 
/// \ingroup core
/////////////////////////////////////////////////

#ifndef LIE_CORE_EXCEPPARSEERROR_HPP
#define LIE_CORE_EXCEPPARSEERROR_HPP

#include "Exception.hpp"
#include <string>

namespace lie{
	namespace core{

		/////////////////////////////////////////////////
		/// \brief This exception is thrown when a function that is attempting to parse a
		/// string encounters something that is not the valid format
		/////////////////////////////////////////////////
		class ExcepParseError : public Exception{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new ExcepParseError
			/// \param function Name of the function throwing this exception
			/// \param badString The string to be parsed
			/// \param formatDescription Human readable description of the format the string should
			/// have adhered to (can be set to empty string, exception message will still be pretty)
			/// \param formatRegex regex describing the format the string should
			/// have adhered to (can be set to empty string, exception message will still be pretty)
			/// \param details Any additional details describing the problem, (can be set to empty 
			/// string, exception message will still be pretty) 
			/// \param cause Pointer to the exception that caused this exception to be thrown, defaults
			/// to nullptr which indicates that this exception was not thrown as a result of another
			/// exception being thrown
			//////////////////////////////////////////////////////////////////////////
			ExcepParseError(std::string function, std::string badString, std::string formatDesciption, 
				std::string formatRegex, std::string details, 
				Exception* cause = nullptr, lie::core::Logger& logger = lie::core::DLog);
			
			virtual std::string type() const;
		private:
			///< The string that was being parsed but that is invalid
			std::string mBadString;

			///< Human readable string describing the required format
			std::string mFormatDesciption;

			///< regex of the required format, empty string if not applicable
			std::string mFormatRegex;
		};

	}
}

#endif // LIE_CORE_EXCEPPARSEERROR_HPP