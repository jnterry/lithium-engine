#ifndef LIE_CORE_NONCOPYABLE_HPP
#define LIE_CORE_NONCOPYABLE_HPP

namespace lie{
    namespace core{

        /////////////////////////////////////////////////
        /// \brief Serves no use on its own, makes derived classes non-copyable
        /////////////////////////////////////////////////
        class NonCopyable{
        protected:
            //protected, derive classes need to be able to call when they are constructed
            NonCopyable(){}
            ~NonCopyable(){}
        private:
            //private, nothing can call copy constructors
            NonCopyable(const NonCopyable&){}
            const NonCopyable& operator=(const NonCopyable&){return *this;}
        };

    }
}

#endif // LIE_CORE_NONCOPYABLE_HPP
