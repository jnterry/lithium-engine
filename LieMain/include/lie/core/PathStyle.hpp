#ifndef LIE_CORE_PATHSTYLE_HPP
#define LIE_CORE_PATHSTYLE_HPP

#include <lie/core\Config.hpp>

namespace lie{
    namespace core{

        enum class PathStyle{
            /////////////////////////////////////////////////
            /// \brief The path style used by windows
            /// eg: C:\folder\file.ext
            /////////////////////////////////////////////////
            Windows,

            /////////////////////////////////////////////////
            /// \brief The path style used on unix systems (linix, mac)
            /////////////////////////////////////////////////
            Unix,

            /////////////////////////////////////////////////
            /// \brief Native is set to be the same as another enum option based off the
            /// system that lie is compile for
            /////////////////////////////////////////////////

            #if defined (LIE_OS_WINDOWS)
            Native = Windows,
            #elif defined (LIE_OS_LINIX) || defined (LIE_OS_MACOS)
            Native = Unix,
            #endif
        };

    }
}

#endif // LIE_CORE_PATHSTYLE_HPP
