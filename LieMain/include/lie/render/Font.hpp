/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Font.hpp
/// Contains the lie::rend::Font class
/// 
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_FONT_HPP
#define LIE_RENDER_FONT_HPP

#include "Texture.hpp"
#include "../math/Aabb2.hpp"
#include <cstdint>
#include <stdint.h>
#include <map>

namespace lie{
	namespace core{
		class File;
	}
	namespace rend{

		//////////////////////////////////////////////////////////////////////////
		/// \brief The Font class represents some Font that can be used to render text,
		/// it does not contain information about font styling (size, weight, italics, etc)
		//////////////////////////////////////////////////////////////////////////
		class Font{
		public:
			///< The type used to represent a unicode code point
			typedef std::uint32_t CodePoint;
			struct Glyph{
				///< The bounding rectangle of this glyph in the texture
				lie::math::Aabb2i textureCoords;
			};

			//////////////////////////////////////////////////////////////////////////
			/// \brief Loads data from a ttf file
			/// \return True if the load succeeded, else false. This font object will not
			/// be changed if this function returns false
			//////////////////////////////////////////////////////////////////////////
			bool loadFromFile(const lie::core::File& f);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the texture holding the distance field for this font
			/// \note This function is designed for internal use by the engine and
			/// is not guaranteed to not change in future versions.
			//////////////////////////////////////////////////////////////////////////
			Texture& _getDistField();

			//////////////////////////////////////////////////////////////////////////
			/// \brief
			//////////////////////////////////////////////////////////////////////////
			Glyph& getGlyph(CodePoint codepoint);
		private:
			std::map<CodePoint, Glyph> mGlyphs;
			
			Texture mDistField;
		};
	}
}

#endif