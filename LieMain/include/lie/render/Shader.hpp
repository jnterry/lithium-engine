#ifndef LIE_RENDER_SHADER_HPP
#define LIE_RENDER_SHADER_HPP

#include <string>
#include <lie/core\File.hpp>
#include <lie/core/NonCopyable.hpp>

namespace lie{
    namespace rend{

        //forward decleration
        class ShaderProgram;

        /////////////////////////////////////////////////
        /// \brief Class used to manipulate opengl shader with oop interface
        /// \ingroup glbackend
        /////////////////////////////////////////////////
        class Shader : public lie::core::NonCopyable{
            friend class ShaderProgram;
			friend class ShaderProgramBuilder;
            public:
                enum class Type{
                    ///< A Vertex shader is run once for each vertex to be drawn.
                    ///< Used to transform verticies from model space to world
                    ///< and then projected space
                    Vertex,

                    ///< A Pixel shader is run to determine the color for a rendered
                    ///< pixel, it can be called multiple times for a single pixel if
                    ///< an object is drawn and then a closer object is drawn on top.
                    Pixel,

                    ///< A Geometry shader takes some geometry, performs calculations
                    ///< and outputs 0 or more primatives to be rendered
                    Geometry,

                    ///< NOT a valid shader type, this simply counts how many different shader types
                    ///< there are
                    Count,
                };
                /////////////////////////////////////////////////
                /// \brief Constructor that makes a new shader of the type specified
                /////////////////////////////////////////////////
                Shader(Type shaderType);

                /////////////////////////////////////////////////
                /// \brief Deletes the shader with the rendering api
                /////////////////////////////////////////////////
                ~Shader();

                /////////////////////////////////////////////////
                /// \brief Function to load the source code into the shader from a lie::core::File, then compiles the shader
                /// \param file the file to load source code from
                /// \return whether the load and compile was successful or not
                /// \retval true The file was opened, loaded and compiled, no syntax errors found
                /// \retval false Either the file does not exist or contained syntax errors
                /// \see getErrors() to get a string containing the errors of this shader
                /////////////////////////////////////////////////
                bool loadAndCompile(const lie::core::File& file);

                /////////////////////////////////////////////////
                /// \brief Function to load the source code into the shader from any input stream, then compiles the shader
                /// \param inputSteam The stream from which to load
                /// \return whether the load and compile was successful or not
                /// \retval true Code loaded and compile without errors
                /// \retval false Code contains errors
                /// \see getErrors() to get a string containing the errors of this shader
                /////////////////////////////////////////////////
                bool loadAndCompile(std::istream& inputStream);

				/////////////////////////////////////////////////
				/// \brief Function to load the source code into the shader from any input stream, then compiles the shader
				/// \param source String containing the source code for the shader
				/// \return whether the load and compile was successful or not
				/// \retval true Code loaded and compile without errors
				/// \retval false Code contains errors
				/// \see getErrors() to get a string containing the errors of this shader
				/////////////////////////////////////////////////
				bool loadAndCompile(std::string& source);

				bool loadAndCompile(const char*);

                /////////////////////////////////////////////////
                /// \brief Checks the shader has been compiled successfully and is ready to use
                /// \return bool indicating the state of the shader
                /// \retval true The shader has been loaded and compiled without errors
                /// \retval false Either the shader has not had any source code loaded, or the loaded code contains errors
                /// \see getErrors() to get a string of the errors with the code
                /////////////////////////////////////////////////
                bool isOkay() const;

                /////////////////////////////////////////////////
                /// \brief returns a string containing the error for this shader
                /// \return string containing the error log of this shader
                /// \see isOkay() to quickly check if this shader has any errors
                /////////////////////////////////////////////////
                std::string getErrors() const;

                /////////////////////////////////////////////////
                /// \brief returns the type of this shader
                /////////////////////////////////////////////////
                Type getType() const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the type of this shader as a human readable string
				//////////////////////////////////////////////////////////////////////////
				std::string getTypeString() const;
            private:
                unsigned int mRef;
                Type mType;
        };//end of class shader
    }//end of gl::
}//end of lie::


#endif // LIE_RENDER_SHADER_H
