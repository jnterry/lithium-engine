/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file MaterailTypeBuilder.hpp
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MATERIALTYPEBUILDER_HPP
#define LIE_RENDER_MATERIALTYPEBUILDER_HPP

#include <vector>
#include <string>
#include "../core/NonCopyable.hpp"
#include "ShaderParameterType.hpp"
#include "MaterialType.hpp"
#include "../core/ByteBuffer.hpp"
#include "../math/Matrix4x4.hpp"
#include "../math/Matrix3x3.hpp"
#include "../math/Vector2.hpp"
#include "../math/Vector3.hpp"

namespace lie{

	namespace rend{

		//////////////////////////////////////////////////////////////////////////
		/// \brief A MaterialTypeBuilder is a utility class for building a single instance
		/// of MaterialType. This class exists as MaterialType is immutable once constructed.
		/// Note that you MUST use this to create a MaterialType as its constructor is private.
		/// In addition you can only get an instance of this class by using a Device
		/// Hence to create a MaterialType you should do the following: <br>
		/// \code
		/// Device* d = Device::create();
		/// MaterialType* t = d->createMaterailType(1)
		///		.addAttribute("name", 2)
		///		.addAttribute("name2", lie::math::Vector3f::Unit)
		///		....... etc ......
		///		.build();
		/// \endcode
		//////////////////////////////////////////////////////////////////////////
		class MaterialTypeBuilder{
			friend class Device;
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Mat4x4f with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, const lie::math::Matrix4x4f& defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of the type ShaderParameterType::Mat3x3f with
			/// the specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, const lie::math::Matrix3x3f& defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Vec4f with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, const lie::math::Vector4f& defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Vec3f with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, const lie::math::Vector3f& defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Vec2f with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, const lie::math::Vector2f& defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Float with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, float defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Adds an attribute of type ShaderParameterType::Int32 with the
			/// specified name and default value
			/// \param The name of the parameter to add
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder& addAttribute(std::string name, int defaultValue);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates the MaterailType that has been defined using this MaterailTypeBuilder
			/// Once built this MaterailTypeBuilder can be safely deleted and CANNOT be used again,
			/// as such calling .build() a second time will return nullptr
			//////////////////////////////////////////////////////////////////////////
			const MaterialType* build();
		private:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use a Device to create instances of this class
			/// Creates a new MaterialTypeBuilder that will create a MaterialType with 
			/// the specified number of textures
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder(unsigned char textureCount, Device& dev);

			///< The attributes the created MaterialType will have
			std::vector<MaterialType::AttributeMeta> mAttribMetas;

			///< The number of textures the created MaterialType will have
			unsigned short mTextureCount;

			///< True if this MaterialTypeBuilder has built a MaterialType
			bool mHasBuilt;

			///< The Device that created this MaterialTypeBuilder
			Device& mDevice;

			lie::core::ByteBuffer mDefaults;
		};
	}
}

#endif //LIE_RENDER_MATERIALTYPEBUILDER_HPP