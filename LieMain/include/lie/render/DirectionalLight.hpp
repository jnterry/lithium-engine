#ifndef LIE_RENDER_DIRECTIONALLIGHT_HPP
#define LIE_RENDER_DIRECTIONALLIGHT_HPP

#include "Color.hpp"
#include "../math/Vector3.hpp"

namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief A DirectionalLight can be thought of as a light
        /// that is infinitely far away and shines light towards some direction.
        /// This is useful for representing a sun.
        /////////////////////////////////////////////////
        struct DirectionalLight{
            DirectionalLight(Color newColor, lie::math::Vector3f newDirection)
            : color(newColor), direction(newDirection){
                //empty body
            }

            ///< The color of light emitted by this light source
            ///< The alpha component represents the light's intensity
            Color color;

            ///< The direction that the light is shining in,
            ///< should be a normalized vector
            lie::math::Vector3f direction;
        };

    }
}

#endif // LIE_RENDER_DIRECTIONALLIGHT_HPP
