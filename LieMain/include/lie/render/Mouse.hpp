/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Mouse.hpp
/// Contains the Mouse class, provides accsess to the physical Mouse input
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MOUSE_HPP
#define LIE_RENDER_MOUSE_HPP

#include <lie/math\Vector2.hpp>

namespace lie{
    namespace rend{

        class Mouse{
        public:
            /////////////////////////////////////////////////
            /// \brief Enum containing possible mouse buttons
            ///
            /// Note, they are not mutually exclusive, eg, you can hold left and right
            /// mouse buttons simultaniously
            ///
            /// Note2, left and right buttons are the logical rather than physical left and right,
            /// it is possible to switch the left and right keys under most OSs, the left and right
            /// buttons DO reflect this switch
            /////////////////////////////////////////////////
            enum Button{
                None = 0, ///no mouse button pressed
                Left = 1, ///left click
                Right = 2, ///right click
                Middle = 4, ///middle button pressed, usually the scroll wheel pressed down
                XButton1 = 8, ///extra button 1, eg a back button on the mouse
                XButton2 = 16, ///extra button 2, eg a forwards button on the mouse
            };

            /////////////////////////////////////////////////
            /// \brief Returns the current position of the mouse pointer
            /////////////////////////////////////////////////
            static lie::math::Vector2i getPosition(){return lie::math::Vector2i(0,0);}

            /////////////////////////////////////////////////
            /// \brief Returns the current button(s) that are being pressed
            /////////////////////////////////////////////////
            static Button getButton(){return None;}
        };

    }
}

#endif // LIE_RENDER_MOUSE_HPP
