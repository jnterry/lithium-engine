/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Mouse.hpp
/// Contains the Pass class
///
/// \ingroup render
/////////////////////////////////////////////////
#ifndef LIE_RENDER_PASS_HPP
#define LIE_RENDER_PASS_HPP

#include <string>
#include "FaceMode.hpp"
#include "BlendMode.hpp"
#include "DepthTestMode.hpp"

namespace lie{
    namespace rend{

		//forward deceleration
		class Renderer3d;
		class Material;
		class ShaderProgram;

        /////////////////////////////////////////////////
        /// \brief A pass is a collection of settings that define how both 2d and 3d geometry should be rendered
        /// It also defines which ShaderProgram should be used
        /// More complex objects may use multiple passes to achieve the desired effect. Eg: Rendering glass may require
        /// a pass for a semi clear object, a pass for specular light, etc
        /// This can be done manually by using multiple passes and then rendering the geometry multiple times, once with each
        /// Alternatively, using the "Technique" class will do this for you automatically
        /////////////////////////////////////////////////
        class Pass{
        public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Enum with the possible sources for textures to be passed to the ShaderProgram
			//////////////////////////////////////////////////////////////////////////
			enum class TextureSource : unsigned char{
				//////////////////////////////////////////////////////////////////////////
				/// \brief The texture will be supplied by the material in use
				/// The argument is the index of the texture in the material to use
				//////////////////////////////////////////////////////////////////////////
				MATERIAL,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The texture will be supplied by the renderer3d 
				//////////////////////////////////////////////////////////////////////////
				//RENDERER,
				 
				//////////////////////////////////////////////////////////////////////////
				/// \brief The texture will be supplied by the output of a previous pass
				//////////////////////////////////////////////////////////////////////////
				//OTHER_PASS,
				// 
				// 

				UNKNOWN,
			};

			//////////////////////////////////////////////////////////////////////////
			/// \brief Enum containing a list of the possible sources for parameters to the
			/// ShaderProgram in use. The majority of these sources can be used for both
			/// Renderer2ds and Renderer3ds, however some will be ignored by one or the other,
			/// this behavior is documented, sources that only apply to 3D renderer have the suffix
			/// _3D, sources that only apply to 2d renderer have the suffix _2D
			//////////////////////////////////////////////////////////////////////////	
			enum class ParameterSource{
				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be provided by the one of the attributes of the
				/// material in use. <br>
				/// The argument is the index of the material parameter to use
				//////////////////////////////////////////////////////////////////////////
				MATERIAL_ATTRIBUTE,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a float matrix representing the transformation of
				/// the geometry from local space to world space. This will be a 3x3 matrix if using a Renderer2d
				/// or a 4x4 matrix if using a Renderer3d. This argument is passed into the .draw calls, no
				/// parameter source argument is needed
				/// No argument is needed.
				//////////////////////////////////////////////////////////////////////////
				MODEL_MATRIX,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a float matrix representing the transformation that
				/// is required to translate geometry from world space to "relative to camera" space
				/// This depends on the Camera in use by the Renderer. It will be a 3x3 matrix using a 
				/// Renderer2d or a 4x4 matrix using a Renderer3d
				/// No argument is needed.
				//////////////////////////////////////////////////////////////////////////
				VIEW_MATRIX,

				//////////////////////////////////////////////////////////////////////////
				/// \brief If using a 3d renderer the parameter will be a 4x4 float matrix representing 
				/// the transformation that is required to translate geometry from "relative to camera" space to screen space, 
				/// ie, the cube between (-1,-1,-1) and (1,1,1). <br>
				/// If a 2d renderer is used the parameter will be a 3x3 float matrix representing the transformation
				/// required to translate the current layer in order to produce the correct parallax effect.
				/// No argument is needed in either case.
				//////////////////////////////////////////////////////////////////////////
				PROJECTION_MATRIX,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a float matrix representing the model, view and
				/// projection matricides multiplied together. Thus the matrix represents the transformation
				/// to move the geometry from local space to screen space. A renderer3d will supply a 4x4 matrix
				/// and a renderer2d will supply a 3x3 matrix, note that renderer2ds have no projection matrix
				/// so this is simply model * view.
				/// The View and Projection matrices are supplied by the Renderer and the model 
				/// is the matrix supplied to the renderer's .draw function <br>
				/// No argument is needed.
				//////////////////////////////////////////////////////////////////////////
				MVP_MATRIX,

				//////////////////////////////////////////////////////////////////////////
				/// \brief Used under a 3d Renderer The parameter will be a 3d float vector representing 
				/// the camera's world position, used under a 2d Renderer the parameter will be a 2d float vector
				/// representing the center of the camera's view area in world coordinates.
				/// This is dependent on the camera of the Renderer in use, no argument is needed.
				//////////////////////////////////////////////////////////////////////////
				CAMERA_POSITION,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a 3d float vector of unit length representing the direction
				/// that the camera is looking in
				/// \warning This parameter is ignored by a 2d renderer
				//////////////////////////////////////////////////////////////////////////
				CAMERA_LOOK_DIRECTION_3D,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a 2d or 3d float vector (depending on whether Renderer2d
				/// or Renderer3d is used) of unit length representing the up direction of the camera in use,
				/// no argument is needed
				//////////////////////////////////////////////////////////////////////////
				CAMERA_UP_DIRECTION,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a single float representing the the near cut of of the 
				/// camera, anything closer to the camera than this will not be rendered
				/// \warning This parameter source is ignored by Renderer2d 
				//////////////////////////////////////////////////////////////////////////
				CAMERA_NEAR_CUT_OFF_3D,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a single float representing the the far cut of of the 
				/// camera, anything further from the camera than this will not be rendered
				/// \warning This parameter source is ignored by Renderer2d 
				//////////////////////////////////////////////////////////////////////////
				CAMERA_FAR_CUT_OFF_3D,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The parameter will be a 3d float vector representing the color of the ambient light
				/// as provided by the renderer in use, the x, y and z components represent r,g and b respectively
				//////////////////////////////////////////////////////////////////////////
				AMBIENT_LIGHT_COLOR,

				DIR_LIGHT_COLOR,
				DIR_LIGHT_DIRECTION,

				POINT_LIGHT_POSITION,
				POINT_LIGHT_COLOR,
				POINT_LIGHT_ATTENUATION,

				//////////////////////////////////////////////////////////////////////////
				/// \brief This parameter will hold the number of milliseconds since the Renderer was
				/// created
				//////////////////////////////////////////////////////////////////////////
				TIME,

				UNKNOWN,
			};

            /////////////////////////////////////////////////
            /// \brief constructor which sets all values to their default
            /// and takes a reference to the ShaderProgram this pass will use
            /////////////////////////////////////////////////
            Pass(lie::rend::ShaderProgram& program);

            /////////////////////////////////////////////////
            /// \brief Begins using this Pass for rendering, this Pass will be used until another Pass is used
            ///
            /// \note Calling set methods on a Pass currently in use will not change the Pass in use
            /// until "use()" is called again
            /////////////////////////////////////////////////
            void use();

            /////////////////////////////////////////////////
            /// \brief Returns this Pass's Back face mode
            /////////////////////////////////////////////////
            FaceMode getBackFaceMode();

            /////////////////////////////////////////////////
            /// \brief Returns this Pass's front face mode
            /////////////////////////////////////////////////
            FaceMode getFrontFaceMode();

            /////////////////////////////////////////////////
            /// \brief Sets the FaceMode of back faces, default for back faces is cull
            /////////////////////////////////////////////////
            void setBackFaceMode(FaceMode mode);

            /////////////////////////////////////////////////
            /// \brief Sets the FaceMode for front faces, default for front faces is filled
            /////////////////////////////////////////////////
            void setFrontFaceMode(FaceMode mode);

            /////////////////////////////////////////////////
            /// \brief Sets the FaceMode for both front and back face modes to the same FaceMode
            /////////////////////////////////////////////////
            void setFaceMode(FaceMode mode);

            /////////////////////////////////////////////////
            /// \brief Convenience function which sets both front and back face modes to the passed in values
            /////////////////////////////////////////////////
            void setFaceMode(FaceMode front, FaceMode back);

            /////////////////////////////////////////////////
            /// \brief Sets the width that any line segments drawn with this Pass should be
            /// Line segments may be drawn either by drawing polygons in wire frame mode or by drawing
            /// line primatives
            /////////////////////////////////////////////////
            void setLineWidth(unsigned char val);

            /////////////////////////////////////////////////
            /// \brief Sets the size that any points drawn with this Pass should be
            /// Points may be drawn either by drawing polygons in point cloud mode or by drawing
            /// point primitives
            /////////////////////////////////////////////////
            void setPointSize(unsigned char val);

            /////////////////////////////////////////////////
            /// \brief Returns the line width of this Pass
            /////////////////////////////////////////////////
            unsigned char getLineWidth();

            /////////////////////////////////////////////////
            /// \brief Returns the point size of this Pass
            /////////////////////////////////////////////////
            unsigned char getPointSize();

            /////////////////////////////////////////////////
            /// \brief Sets the depth test mode for this pass
            /////////////////////////////////////////////////
            void setDepthTestMode(lie::rend::DepthTestMode dtmode);

            /////////////////////////////////////////////////
            /// \brief Returns the depth test mode of this pass
            /////////////////////////////////////////////////
            lie::rend::DepthTestMode getDepthTestMode();

            /////////////////////////////////////////////////
            /// \brief Returns the ShaderProgram used by this pass
            /////////////////////////////////////////////////
            lie::rend::ShaderProgram& getShaderProgram();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the source of the texture for the specified texture channel in
			/// the shader program 
			/// \param channel The index of the channel you wish to modify, must range between
			/// 0 and .getShaderProgram().getTextureChannelCount()-1
			/// \param source The source to use for the channel
			/// \param arg The argument to use, its meaning depends on the source specified
			//////////////////////////////////////////////////////////////////////////
			void setTextureSource(int channel, TextureSource source, int arg = 0);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the source of the texture for the specified texture channel in the
			/// shader program
			/// \param channelName The name of the channel to modify
			/// \param source The source to use for the channel
			/// \param arg The argument to use, its meaning depends on the source specified
			/// \return bool representing whether the specified channel was found, use
			/// ShaderProgram::getTextureChannelIndex(std::string name) to see if a channel exists
			/// \retval true The ShaderProgram contains a texture channel with the specified name
			/// \retval false The ShaderProgram does not contain a texture channel with the specified name
			//////////////////////////////////////////////////////////////////////////
			bool setTextureSource(std::string channelName, TextureSource source, int arg = 0);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the TextureSource for the specified channel
			/// \param channel The index of the channel you wish to query, must range between
			/// 0 and .getShaderProgram().getTextureChannelCount()-1
			//////////////////////////////////////////////////////////////////////////
			TextureSource getTextureSource(int channel) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the argument to the TextureSouce for the specified channel
			/// \param channel The index of the channel you wish to query, must range between
			/// 0 and .getShaderProgram().getTextureChannelCount()-1
			//////////////////////////////////////////////////////////////////////////
			int getTextureSourceArg(int channel) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the source for a certain parameter in the ShaderProgram used by this
			/// pass
			/// \param paramIndex The index of the parameter in the ShaderProgram, use
			/// ShaderProgram::getParamterIndex(std::string parameterName)
			/// \param source The source to use for the specified parameter
			/// \param arg The argument for the source enum, is not needed for all sources, check
			/// the documentation for the source you are using. You do not need to specify this if
			/// the source does not require an argument
			//////////////////////////////////////////////////////////////////////////
			void setParameterSource(int paramIndex, ParameterSource source, int arg = 0);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the source for a certain parameter in the ShaderProgram used by this pass
			/// \param paramName The name of the parameter in the ShaderProgram
			/// \param source The source to use for the specified parameter
			/// \param arg The argument for the source enum, is not needed for all sources, check
			/// the documentation for the source you are using. You do not need to specify this if
			/// the source does not require an argument
			/// \return True if a parameter with that name was found and the source has been set,
			/// else false
			//////////////////////////////////////////////////////////////////////////
			bool setParameterSource(std::string paramName, ParameterSource source, int arg = 0);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the parameter source for the specified parameter
			//////////////////////////////////////////////////////////////////////////
			ParameterSource getParameterSource(int paramIndex) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the parameter source argument for the specified parameter
			//////////////////////////////////////////////////////////////////////////
			int getParameterSourceArg(int paramIndex) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if all aspects two Passes are equal, else returns false
            /////////////////////////////////////////////////
            bool operator==(const Pass& other) const;
        private:
			struct ParameterMeta{
				ParameterMeta();
				ParameterSource source;
				int arg;
			};

			struct TextureMeta{
				TextureMeta();
				TextureSource source;
				int arg;
			};

            /////////////////////////////////////////////////
            /// \brief Calls api specific functions to setup rendering
            /// to use this pass
            /// \note This function is called by the "use" function
            /// which sets up non api specific things
            /////////////////////////////////////////////////
            void _useApiSpecific();
			
            ///< The face mode used for polygons facing towards the camera
            FaceMode mFrontFaceMode;

            ///< The face mode used for polygons facing away from the camera
            FaceMode mBackFaceMode;

            ///< The depth test mode, should almost always be none for 2d
            DepthTestMode mDepthTestMode;

            ///< The width in pixels that lines drawn with this pass will be
            unsigned char mLineWidth;

            ///< The size in pixels that points drawn with this pass will be
            unsigned char mPointSize;

            ///< The shader program that will be used to render this pass
            ShaderProgram& mShaderProgram;

			///< The sources and config for the textures that need to be bound for the ShaderProgram
			TextureMeta* mTextureSources;

			///< The number of parameters the ShaderProgram has
			int mParameterCount;

			///< The sources of the parameters needing to be passed to the ShaderProgram
			ParameterMeta* mParameterSources;
        };

    }
}

#endif // LIE_RENDER_PASS_HPP
