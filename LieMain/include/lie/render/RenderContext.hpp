#ifndef LIE_RENDER_RENDERCONTEXT_HPP
#define LIE_RENDER_RENDERCONTEXT_HPP

#include "../core/NonCopyable.hpp"

namespace lie{
    namespace rend{

        //forward declerations
        namespace detail{
            class DeviceImpl;
            class RenderContextImpl;
        }

        class RenderContext : lie::core::NonCopyable{
            friend class detail::RenderContextImpl;
            friend class detail::DeviceImpl;
        public:
            /////////////////////////////////////////////////
            /// \brief Makes this context the current context for the thread which calls this function
            /////////////////////////////////////////////////
            void makeCurrent();

            /////////////////////////////////////////////////
            /// \brief Deletes the context and its platform specific implementation
            /////////////////////////////////////////////////
            ~RenderContext();
        private:
            /////////////////////////////////////////////////
            /// \brief Private constructor - you must use a render system to create contexts
            /////////////////////////////////////////////////
            RenderContext();

            detail::RenderContextImpl* mImpl; //platform specific code and data held in this

        };

    }
}

#endif // LIE_RENDER_RENDERCONTEXT_HPP
