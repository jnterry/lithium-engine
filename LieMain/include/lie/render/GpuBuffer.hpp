/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file GpuBuffer.hpp
/// Contains the GpuBuffer enum class
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_GPUDATABUFFER_HPP
#define LIE_RENDER_GPUDATABUFFER_HPP

namespace lie{
    namespace rend{

        //forward decleration
        namespace detail{
            struct GpuBufferImpl;
        }

        /////////////////////////////////////////////////
        /// \brief A GpuBuffer represents an array of data stored on the gpu
        /// this data could be verticies of a mesh, vertex normals, colors, uv coordinates,
        /// pixel data, something completely different, etc
        ///
        /// \note When the GpuBuffer is deleted so is the corrosponding buffer on the Gpu!
        /////////////////////////////////////////////////
        class GpuBuffer{
        public:

            /////////////////////////////////////////////////
            /// \brief enum containing a list of useage types for a buffer
            /// \note Any buffer type can be used for any purpose, however the underlying api may be
            /// able to make optimisations based off the type you specify
            /////////////////////////////////////////////////
            enum class UsageType{
                /////////////////////////////////////////////////
                /// \brief Buffer will be storing data about geometry, eg
                /// vertex positions, colors, normals, uv coordiantes, etc
                /////////////////////////////////////////////////
                Geometry,

                /////////////////////////////////////////////////
                /// \brief Buffer will be storing index data
                /// A Geometry buffer stores the vertex data, the index buffer then
                /// stores a list of items to join into faces, eg 1,2,3 will form a triangle
                /// linking vertex 1, 2 and 3
                /////////////////////////////////////////////////
                Index,
            };

            /*/////////////////////////////////////////////////
            /// \brief Enum containing options for how frequently the user expects the contents of the buffer to change
            /// \note It is possible for the contents to change constantly even if "once" is specified, it is also possible
            /// for it to never change even if "frequent" is used. These flags only provide hints to the implementation as
            /// it  may  be able to optimise the storage based on the usage, careful profiling is needed to determine the best option
            /////////////////////////////////////////////////
            enum class UpdateFrequency{
                Once, ///< The data in the buffer is not going to be changed after initially being set
                Occasional, ///< The data will occassionally change
                Frequent, ///< The data will be changed after every use (or almost every use)
            };

            /////////////////////////////////////////////////
            /// \brief enum containing options for how the user expects to accsess the data
            /// \note Data can always be both read from and written to the buffer regardless of the hint supplied
            /// The implementation may however be able to optimise  how the data is stored based of the hint specified
            /////////////////////////////////////////////////
            enum class AccessType{
                /////////////////////////////////////////////////
                /// \brief User will be uploading data to the GPU to store in this buffer
                /////////////////////////////////////////////////
                Read,

                /////////////////////////////////////////////////
                /// \brief User will be downloading data generated on the GPU which is then stored in this buffer
                /////////////////////////////////////////////////
                Write,

                /////////////////////////////////////////////////
                /// \brief GPU will generate data and store it in this buffer, the GPU will then use the data elsewhere
                /// Data will not be uploaded from user to GPU or downloaded from the GPU to the user
                /////////////////////////////////////////////////
                Neither,
            };*/
            /////////////////////////////////////////////////
            /// \brief Constructor which registers the new buffer with the underlying rendering api
            /// and performs any other needed initilisation
            /////////////////////////////////////////////////
            GpuBuffer(UsageType usage);

            /////////////////////////////////////////////////
            /// \brief Destructor which cleans up any internal data and notifies the underlying
            /// rendering api that the buffer should be deleted
            /////////////////////////////////////////////////
            ~GpuBuffer();

            /////////////////////////////////////////////////
            /// \brief Buffers a new data set discarding any data previously stored in the buffer
            ///
            /// \param data A void* to the data that should be buffered
            /// \param dataSize The size in bytes of the data, should ideally be calculated with sizeof() to help prevent errors
            /////////////////////////////////////////////////
            void bufferData(const void* data, unsigned int dataSize);

            /////////////////////////////////////////////////
            /// \brief Pointer to the api specfic implementation of the GPU buffer
            /// Public as other classes in the render module need accsess and can include the corrosponding private header,
            /// however client code should not have accsess to this as they should not include the private header containing it
            /////////////////////////////////////////////////
            detail::GpuBufferImpl* mImpl;

        private:
            UsageType mUsageType;
        };

    }
}

#endif // LIE_RENDER_GPUDATABUFFER_HPP
