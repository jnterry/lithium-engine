/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file MeshGenerator.hpp
/// Contains helper functions for generating 3d meshes
/// 
/// \ingroup render
/////////////////////////////////////////////////

namespace lie{
	namespace rend{

		//forward decelerations
		class Device;
		class MeshVertexLayout;
		class Mesh;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Helper class that can be used to generate basic meshes, can
		/// generate positions, normals and texture coordinates
		//////////////////////////////////////////////////////////////////////////
		class MeshGenerator{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Constant used to represent an attribute that should not be generated
			//////////////////////////////////////////////////////////////////////////
			static const unsigned char ATTRIBUTE_NO_GEN = 255;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new default MeshGenerator that will generate meshes
			/// with the specified Device. The VertexLayout used will be the vertex layout
			/// returned by getDefaultVertexLayout
			/// \param device The Device instance that will be used to create the meshes
			//////////////////////////////////////////////////////////////////////////
			MeshGenerator(lie::rend::Device& device);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new MeshGenerator with the specified settings
			/// \param device The Device instance that will be used to create the meshes
			/// \param vertexLayout The layout of the vertices of the generated mesh
			/// \param positionAttribute The index of the attribute in vertexLayout that will be
			/// used to store 3d position data, must be a 3 component float vector
			/// \param normalAttribute The index of the attribute in vertexLayout that will be
			/// used to normal data, must be a 3 component float vector
			/// \param uvAttribute The index of the attribute in vertexLayout that will be
			/// used to uv (texture) coordinates, must be a 2 component float vector
			//////////////////////////////////////////////////////////////////////////
			MeshGenerator(lie::rend::Device& device, const lie::rend::MeshVertexLayout& vertexLayout,
				unsigned char positionAttribute, unsigned char normalAttribute, unsigned char uvAttribute);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Enum of the different types of position that generated meshes can have 
			/// relative to to the origin
			//////////////////////////////////////////////////////////////////////////
			enum class Position{
				//////////////////////////////////////////////////////////////////////////
				/// \brief The center of the generated mesh's AABB will be at (0,0,0)
				//////////////////////////////////////////////////////////////////////////
				CENTERED,

				//////////////////////////////////////////////////////////////////////////
				/// \brief Positive X, Positive Y, Positive Z, ie: all position's
				/// X,Y and Z values will be greater than or equal to 0 
				//////////////////////////////////////////////////////////////////////////
				PXPYPZ,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The default position type, equal to CENTERED
				//////////////////////////////////////////////////////////////////////////
				DEFAULT = CENTERED,
			};

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a reference to the default MeshVertexLayout <br>
			/// Structured as follows:
			/// - Vertex3f (position)
			/// - Vertex3f (normal)
			/// - Vertex2f (texture coordinate)
			//////////////////////////////////////////////////////////////////////////
			static const lie::rend::MeshVertexLayout& getDefaultVertexLayout();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Generates and returns a cube mesh with all sides having the specified length
			/// \param sideLength The length of all the sides of the cube
			/// \param position The position of the cube relative to the local origin of the mesh
			/// \return Pointer to newly generated mesh, the memory pointed to belongs to the calling code,
			/// it will not be deleted and no reference will be kept to it by Lithium Engine
			//////////////////////////////////////////////////////////////////////////
			Mesh* genCube(float sideLength, Position position = Position::DEFAULT);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Generates a regular polygon with the specified number of sides
			/// \param Radius The radius of the circle that all of the polygon's vertices's
			/// lie on
			/// \param numSides The number of sides and thus vertices's the polygon has
			/// \param position The position of the generated polygon relative to the origin of the mesh
			/// \param twoSided If true then two sets of faces are made with opposite winding order,
			/// hence there is no side where back face culling will not draw the polygon, defaults to true
			/// \return Pointer to newly generated mesh, the memory pointed to belongs to the calling code,
			/// it will not be deleted and no reference will be kept to it by Lithium Engine
			//////////////////////////////////////////////////////////////////////////
			Mesh* genRegularPolygon(float radius, unsigned int numSides, bool twoSided = true,
				Position position = Position::DEFAULT);
		private:
			lie::rend::Device& mDevice; ///< The device that will be used to generate meshes
			const lie::rend::MeshVertexLayout& mVertexLayout;
			unsigned char mPosAttrib; ///< index of the position attribute in mVertexLayout
			unsigned char mNormalAttrib; ///< index of the normal attribute in mVertexLayout
			unsigned char mUvAttrib; ///< index of the texture coordinate attribute in mVertexLayout
		};

	}
}