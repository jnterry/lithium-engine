/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file FaceMode.hpp
/// Contains the FaceMode enum class, options on how faces should be drawn (filled, wire frame, etc)
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_FACEMODE_HPP
#define LIE_RENDER_FACEMODE_HPP

namespace lie{
	namespace rend{

        /////////////////////////////////////////////////
        /// \brief Enum containing options on how a face should be drawn
        /// These cannot be used together!
        /// If you wish to use multiple (eg, wireframe with a larger point at each vertex)
        /// then you must create a Technique which combines two passes, one with wire frame mode
        /// and the other with point mode
        /////////////////////////////////////////////////
        enum class FaceMode : unsigned char{
            Cull = 0, ///does not draw the face
            PointCloud = 1, ///draws points at each vertex
            Wireframe = 2, ///draws lines between each pair of verticies
            Filled = 3, ///draws filled faces
        };

	}//end of rend::
}//end of lie::

#endif
