#ifndef LIE_RENDER_RENDERER3D_HPP
#define LIE_RENDER_RENDERER3D_HPP

#include "../math/Aabb2.hpp"
#include "Renderer.hpp"
#include "Methodology.hpp"
#include "Camera3d.hpp"
#include "DirectionalLight.hpp"
#include "PointLight.hpp"
#include "../math/Matrix4x4.hpp"
#include "../core/Timer.hpp"

namespace lie{
    namespace rend{

        //forward decelerations
        class Mesh;
		class Pass;

        /////////////////////////////////////////////////
        /// \brief Abstract class defining the interface for Renderer
        ///
        /// A renderer is responsible for drawing graphics to the currently active render target.
        /// This can be done in a number of ways. For example: <br>
        /// RendererImmediate: <br>
        ///     Immediately upon the "drawXXX()" call renders the object to the render target <br>
        /// RendererSorted: <br>
        ///     In drawXXXXX() functions the objects are added to a list of items. Upon the call to
        ///     display() the list is sorted in some way to speed up rendering, for example grouping all objects
        ///     using the same shader together. Transparent geometry is also sorted to be drawn last. The drawing is
        ///     then done by traversing this sorted list when "display()" is called<br>
        /// etc...
        ///
        ///
        /// \note
        /// When possible you should directly store derived classes by value
        /// and not with pointers, this will eliminate virtual overhead.
        /// Ie:
        /// \code
        /// lie::rend::RendererImmediate mRenderer;
        /// \endcode
        /// should be prefered to
        /// \code
        /// lie::rend::Renderer* mRenderer = new lie::rend::RendererImmediate();
        /// \endcode
        /// The renderer will almost always be choosen at compile time, so this should
        /// not be an issue. Functions taking a renderer as a parameter (object.draw(renderer))
        /// should be templated so they can take any renderer.
        /////////////////////////////////////////////////
		class Renderer3d : public Renderer{
        public:
            /////////////////////////////////////////////////
            /// \brief Allows you to change whether fog is enabled for this renderer
            /// \param enabled Whether fog should be enabled for this renderer or not
            /// \see isFogEnabled
            /// \see setFogColor
            /////////////////////////////////////////////////
            virtual void enableFog(bool enabled) = 0;

            /////////////////////////////////////////////////
            /// \brief Returns whether fog is currently enabled for this renderer
            /// \see enableFog
            /// \see setFogColor
            /////////////////////////////////////////////////
            virtual bool isFogEnabled() = 0;

            /////////////////////////////////////////////////
            /// \brief Sets the fog color for this renderer,
            /// alpha channel of color used for fog intensity.
            /// \see enableFog
            /// \see getFogColor
            /////////////////////////////////////////////////
            virtual void setFogColor(lie::rend::Color color) = 0;

            /////////////////////////////////////////////////
            /// \brief Returns the current color of the fog this renderer uses.
            /// \note There can be a color set even if fog is disabled, however it makes no difference
            /// to the scene if fog is disabled.
            /// \see setFogColor
            /// \see isFogEnabled
            /////////////////////////////////////////////////
            virtual lie::rend::Color getFogColor() = 0;

            /////////////////////////////////////////////////
            /// \brief Draws the specified mesh
            /////////////////////////////////////////////////
			virtual void draw(const lie::rend::Methodology& meth, const lie::rend::Material& mat, const lie::math::Matrix4x4f& modelMatrix, const lie::rend::Mesh& mesh) = 0;


            /////////////////////////////////////////////////
            /// \brief Returns a reference to the Camera3d currently in use
            /// by this Renderer3d
            /// \note Changes will not be applied until begin is called
            /////////////////////////////////////////////////
            Camera3d& getCamera();

            /////////////////////////////////////////////////
            /// \brief Sets the camera that will be used by this renderer
            /// \note Changes will not be applied until begin is called
            /////////////////////////////////////////////////
            void setCamera(Camera3d cam);
        protected: //derived classes need access

			//////////////////////////////////////////////////////////////////////////
			/// \brief Begins using the specified pass by binding appropriate shaders,
			/// textures and setting shader uniforms.
			/// \param pass The pass to being using
			/// \param mat The material instance containing parameters for the pass's shader program
			//////////////////////////////////////////////////////////////////////////
			void usePass(Pass& pass, const Material& mat, const math::Matrix4x4f& modelMatrix);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Begins using the specified pass by binding appropriate shaders,
			/// textures and setting shader uniforms.
			/// \param pass The pass to being using, if nullptr is specified then calls
			/// useFixedFunctionPipeline, otherwise calls the overload of usePass that takes
			/// a reference to a pass
			/// \param mat The material instance containing parameters for the pass's shader program
			/// \note This overload is able to take nullptr, this indicates that the fixed function pipeline
			/// should be used
			//////////////////////////////////////////////////////////////////////////
			void usePass(Pass* pass, const Material& mat, const math::Matrix4x4f& modelMatrix){
				if (pass == nullptr){
					useFixedFunctionPipeine();
				} else {
					usePass(*pass, mat, modelMatrix);
				}
			}

            ///< The camera this renderer will use to view the 3d scene
            Camera3d mCamera;
        };

    }
}

#endif // LIE_RENDER_RENDERER3D_HPP
