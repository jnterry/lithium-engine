#ifndef LIE_REND_TECHNIQUE_HPP
#define LIE_REND_TECHNIQUE_HPP
#include <vector>

#include "Pass.hpp"

namespace lie{
    namespace rend{

        //forward deceleration
        class Pass;

        /////////////////////////////////////////////////
        /// \brief A Technique can be thought of as a method used to render geometry.
        /// A single technique is made up of one or more passes which, when combined, produce the
        /// final image
        /////////////////////////////////////////////////
        class Technique{
        public:
            ///TEMPORARY ONLY
            Technique(lie::rend::Pass* thePass) : pass(thePass){}

			///< may be set to nullptr indicating the fixed function pipeline should be used
            lie::rend::Pass* pass;

            /*unsigned int getPassCount();

            /////////////////////////////////////////////////
            /// \brief Adds a new pass to the Technique
            /////////////////////////////////////////////////
            void addPass(Pass* pass);

            /////////////////////////////////////////////////
            /// \brief Removes the pass at the specified index
            /////////////////////////////////////////////////
            void removePass(unsigned int index);

            /////////////////////////////////////////////////
            /// \brief Returns the Pass at the specified index
            /////////////////////////////////////////////////
            Pass* getPass(unsigned int index);*/
        private:
            //std::vector<lie::rend::Pass&> mPasses;
        };

    }
}

#endif // LIE_REND_TECHNIQUE_HPP
