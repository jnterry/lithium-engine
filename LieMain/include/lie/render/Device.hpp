#ifndef LIE_RENDER_DEVICE_HPP
#define LIE_RENDER_DEVICE_HPP

#include <string>
#include <lie/render\VideoMode.hpp>
#include <lie/render\RenderContext.hpp>
#include <lie/render\Window.hpp>
#include <lie/render\Mesh.hpp>
#include <lie/core\ExcepOutOfRange.hpp>


namespace lie{
    namespace rend{

        //forward decelerations
        class RenderTarget;
		class MaterialTypeBuilder;
		class RenderTexture;
        namespace detail{
            struct DeviceImpl;
        }
		

        /////////////////////////////////////////////////
        /// \brief To access the GPU for storing textures/models etc in video ram,
        /// doing rendering operation, etc you must first make a "RenderContext".
        /// Contexts can only be active in a single thread at once, hence to do resource
        /// loading in a background thread you must create multiple context's which share
        /// resources between them.
        /// A Device is responsible for creating RenderContexts, this includes render
        /// targets and windows. All RenderContexts created by a single Device share
        /// resources between them. Also, upon creation the Device will initiate various
        /// items in the background to ensure the GPU is ready to be accessed.
        /// You must create a Device before making any other calls to functions in the
        /// lie::rend:: namespace.
        /// Multiple Devices can be created if you wish to have contexts which do not share
        /// resources
        /////////////////////////////////////////////////
        class Device{
            friend class DeviceImpl;
        public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates and returns a new Device, performs any necessary setup with the
			/// underlying rendering api (eg, opengl).
			/// You MUST create a Device before using any other part of the lie::rend namespace
			/// \return A new Device instance or nullptr if the Device could not be initialized
			//////////////////////////////////////////////////////////////////////////
			static Device* createDevice();

            /////////////////////////////////////////////////
            /// \brief Destructor to clean up the render system
            /////////////////////////////////////////////////
            ~Device();

            /////////////////////////////////////////////////
            /// \brief Returns the name of the api being used
            /// eg: OpenGL, DirectX, SoftwareRenderer, etc
            /////////////////////////////////////////////////
            std::string getApiName() const;

            /////////////////////////////////////////////////
            /// \brief Returns full name and version information about the api in use
            /// Eg: OpenGL 3.2, DirectX 9, etc
            /////////////////////////////////////////////////
            std::string getApiNameFull() const;

            /////////////////////////////////////////////////
            /// \brief Returns the major version of the underlying api being used
            /////////////////////////////////////////////////
            int getApiMajorVersion() const;

            /////////////////////////////////////////////////
            /// \brief Returns the minor version of the underlying api being used
            /////////////////////////////////////////////////
            int getApiMinorVersion() const;

            /////////////////////////////////////////////////
            /// \brief Enum of features that may or may not be supported, use
            /// querryFeature() to querry whether the system supports the chosen feature
            /////////////////////////////////////////////////
            enum class Feature{
                /////////////////////////////////////////////////
                /// \brief Returns the hardware limit for the maximum number of attributes that a Mesh
                /// can have
                /// Attempting to create a mesh with more than this many attributes will result in an error
                /////////////////////////////////////////////////
                MaxMeshAttributes,

                //:TODO: DOC?
                MaxTextureUnits,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The maximum number of color buffers that can be attached to a single 
				/// RenderTexture instance
				//////////////////////////////////////////////////////////////////////////
				MaxRenderTextureColorBuffers,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The maximum width in pixels for a RenderTexture
				//////////////////////////////////////////////////////////////////////////
				MaxRenderTextureWidth,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The maximum height in pixels for a RenderTexture
				//////////////////////////////////////////////////////////////////////////
				MaxRenderTextureHeight,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The maximum number of color buffers a pixel shader can simultaneously
				/// write to, this only really applies when using a RenderTexture as windows
				/// always only have a single color buffer
				//////////////////////////////////////////////////////////////////////////
				MaxPixelShaderColorOutputs,

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the maximum width and height of a Texture, note that this
				/// value is reported by the underlying rendering api but is not always accurate
				/// if non standard formats are used for storing the color data.
				//////////////////////////////////////////////////////////////////////////
				MaxTextureDimension,

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the maximum number of pixels a texture can contain
				//////////////////////////////////////////////////////////////////////////
				MaxTexturePixelCount,

				//////////////////////////////////////////////////////////////////////////
				/// \brief The maximum number of generic 4-component vertex attributes that can
				/// be passed to a vertex shader
				//////////////////////////////////////////////////////////////////////////
				MaxVertexAttributes
            };
            /////////////////////////////////////////////////
            /// \brief Allows client to query whether a certain feature is supported by the render system
            /// Meaning of return value varies between features, look at the documentation on each feature to
            /// see what the return value means
            /////////////////////////////////////////////////
            int querryFeature(Feature feat) const;

            /////////////////////////////////////////////////
            /// \brief Returns the default context that the render system uses
            /////////////////////////////////////////////////
            RenderContext* getDefaultContext();

            /////////////////////////////////////////////////
            /// \brief Creates a new RenderContext that is set up to share resources
            /// with the default context of the render system
            /////////////////////////////////////////////////
            RenderContext* createContext();

            /////////////////////////////////////////////////
            /// \brief Creates and returns a new window whose context is shared with this Device
            /// Remember to call "makeCurrent()" on the new window!
            ///
            /// \param videomode The desired size and depth bit of the new window
            /// \param title The title for the new window
            /// \param style Style flags for the window to be made, defualts to lie::rend::RenderWindow::Default
            /// \return Window The new window that was created
            /////////////////////////////////////////////////
            Window* createWindow(VideoMode videomode, std::string title, int style = lie::rend::Window::Default);

            /////////////////////////////////////////////////
            /// \brief Creates and returns a new mesh object with the specified number of attributes
            ///
            /// \param int attributeCount The number of attributes the created mesh object should have room for
            /// If this exceeds querryFeature(Feature::MaxMeshAttributes) a lie::core::ExcepOutOfRange exception will be thrown
            ///
            /// \return new instance of Mesh
            /////////////////////////////////////////////////
			Mesh* createMesh(unsigned char attributeCount) throw(lie::core::ExcepOutOfRange<unsigned char>);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new MaterailTypeBuilder that can be used to create a MaterialType
			//////////////////////////////////////////////////////////////////////////
			MaterialTypeBuilder createMaterialType(unsigned char textureCount);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new RenderTexture instance with the specified parameters
			/// \param width The width in pixels of the RenderTexture's buffers
			/// \param height The height in pixels of the RenderTexture's buffers
			/// \param colorBuffers The number of color buffers to attach the to RenderTexture
			/// \param depthBuffer If true a depth buffer will be attached to the RenderTexture
			/// \param stencilBuffer If true a stencil buffer will be attached to the RenderTexture
			/// \return Pointer to new RenderTexture instance or nullptr if the Device failed to create
			/// the RenderTexture for some reason
			//////////////////////////////////////////////////////////////////////////
			RenderTexture* createRenderTexture(int width, int height, int colorBuffers, bool depthBuffer, bool stencilBuffer);
        private:
            /////////////////////////////////////////////////
            /// \brief Vector containing a list of contexts which are managed by this render system.
            /// Context and index 0 is the default context for the system
            /////////////////////////////////////////////////
            std::vector<RenderContext*> mContexts;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use the static createDevice() to create
			/// an instance of this class
			//////////////////////////////////////////////////////////////////////////
			Device();

            detail::DeviceImpl* mImpl;//class which holds platform specific data and functions
        };
    }
}

#endif // LIE_RENDER_DEVICE_HPP
