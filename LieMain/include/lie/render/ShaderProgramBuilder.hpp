/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ShaderProgramBuilder.hpp
/// Contains the lie::rend::ShaderProgramBuilder class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_SHADERPROGRAMBUILDER_HPP
#define LIE_RENDER_SHADERPROGRAMBUILDER_HPP

#include <vector>
#include "../core/NonCopyable.hpp"

namespace lie{
	namespace rend{

		class Shader;
		class ShaderProgram;
		namespace detail{
			class ShaderProgramImpl;
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief The ShaderProgramBuilder is to be used to create new ShaderProgram
		/// instances, it compiles together various Shader instances and finds parameters
		/// sources for use in the final ShaderProgram instance
		////////////////////////////////////////////////////////////////////////// 
		class ShaderProgramBuilder : public core::NonCopyable{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new ShaderProgramBuilder
			//////////////////////////////////////////////////////////////////////////
			ShaderProgramBuilder();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Attaches the specified Shader object to this ShaderProgramBuilder,
			/// when .build() is called all attached shaders will be compiled and linked
			/// to form the finished ShaderProgram
			//////////////////////////////////////////////////////////////////////////
			void addShader(const Shader& shader);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Attempts to build the shader program, if successful returns a new instance
			/// of ShaderProgram, else returns nullptr
			/// \note If the build was not successful use getErrors() to get a desciption of the
			/// errors that have occurs
			/// \return New instance of ShaderProgram or nullptr if the program could not be built
			/// (eg, there is a compile error with one of the attached shaders)
			//////////////////////////////////////////////////////////////////////////
			ShaderProgram* build();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns an std::string containing a desciption of the errors that lead
			/// to the program no being able to be built. Will return empty string if the build
			/// was successful. If .build() has not been called will return a string indicating
			/// that fact
			//////////////////////////////////////////////////////////////////////////
			std::string ShaderProgramBuilder::getErrors() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates and returns a new Shader instance that is the default implementation
			/// for a 2d vertex shader
			////////////////////////////////////////////////////////////////////////// 
			static Shader& createDefault2dVertexShader();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates and returns a new Shader instance that is the default implementation
			/// for a 2d pixel shader
			//////////////////////////////////////////////////////////////////////////
			static Shader& createDefault2dPixelShader();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates and returns a new ShaderProgram that has the default 2d vertex
			/// and pixel shaders attached
			//////////////////////////////////////////////////////////////////////////
			static ShaderProgram& createDefault2dShaderProgram();
		private:
			enum class State{
				NOT_BUILT,
				BUILD_SUCCESSFUL,
				BUILD_FAILED,
			};

			//////////////////////////////////////////////////////////////////////////
			/// \brief API specific function to get errors with a built shader program, this will
			/// only be called if mSProgImpl is not nullptr because a .build() has been attempted
			//////////////////////////////////////////////////////////////////////////
			std::string _getErrorsAPI() const;

			///< The built program, nullptr if build failed or not yet built
			ShaderProgram* mProgram;

			///< Once build() has been called set to the ShaderProgramImpl
			detail::ShaderProgramImpl* mSProgImpl;

			std::vector<const Shader*> mShaders;

			State mState;
		};

	}
}

#endif //LIE_RENDER_SHADERPROGRAMBUILDER_HPP