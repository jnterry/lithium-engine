/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file BlendMode.hpp
/// Contains the BlendMode enum class
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_BLENDMODE_HPP
#define LIE_RENDER_BLENDMODE_HPP
namespace lie{
	namespace rend{

        /////////////////////////////////////////////////
        /// \brief Enum containing options on how layers should be blended together
        /// Below comments assume source is the new layer and dest is the old layer
        /////////////////////////////////////////////////
        enum class BlendMode : unsigned char{
            Alpha = 0, //Pixel = Source * Source.a + Dest
            Add, //Pixel = Source + Dest
            Multiply, //Pixel = Source * Dest
            None, //Pixel = Source
            Minus, //Pixel = Source - Destination
            RMinus, //Pixel = Destination - Source
            Divide, //Pixel = Source  / Destination
            RDivide, //Pixel = Destination / Source
            Difference,//Pixel = abs(Source - Dest)
            Min, //Pixel.r = min(Source.r, Dest.r); Pixel.g = min(Source.g, Dest.g); .....
            Max, //Pixel.r = max(Source.r, Dest.r); Pixel.g = max(Source.g, Dest.g); .....


            Count, //Not a mode, simply counts how many blend modes there are, using it will instead use "Alpha" (the default mode)
        };

        /////////////////////////////////////////////////
        /// \brief Takes a blend mode and returns true if the order makes a difference
        /// to the result
        /// ie, returns true if:
        /// useBlendMode(X); draw(objA); draw(objB); != useBlendMode(X); draw(objB); draw(objA);
        /////////////////////////////////////////////////
        bool isOrderDependent(BlendMode mode);

	}//end of rend::
}//end of lie::

#endif
