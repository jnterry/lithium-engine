#ifndef LIE_RENDER_MESH_UTILS_HPP
#define LIE_RENDER_MESH_UTILS_HPP

#include "MeshDataBuffer.hpp"

namespace lie{
    namespace rend{
        namespace mesh{

            /////////////////////////////////////////////////
            /// \brief Calculates smooth normals for the specified MeshDataBuffer
            /// \param mdb The MeshDataBuffer you wish to modify
            /// \param positionAttrib The attribute of the MeshDataBuffer that stores position data
            /// \param normalAttrib The attribute of  the MeshDataBuffer in which to store the calculated normals
            /// \tparam POS_T The type storing positions
            /// \tparam NORM_T The type storing normals
            /// \note POS_T and NORM_T must be lie::math::Vector3<something> or  another vector represnetation
            /// which supports addition, .normalize() and .cross()
            /////////////////////////////////////////////////
            template<typename POS_T, typename NORM_T>
            void calculateSmoothNormals(MeshDataBuffer& mdb, unsigned char positionAttrib, unsigned char normalAttrib){
                //set all current normals to 0
                mdb.clearAttribData(normalAttrib);

                size_t posOffset = mdb.getVertexLayout().getAttribOffset(positionAttrib);
                size_t normOffset = mdb.getVertexLayout().getAttribOffset(normalAttrib);

                //calculate face normals, add to the normal for each vertex in the face
                //using iter for unsigned char (ie, byte array), then cast to correct type acording to tparam
                for(auto it = mdb.beginFace<unsigned char>(); it != mdb.endFace<unsigned char>(); ++it){
                    POS_T& p0 = *((POS_T*)((it->v0)+posOffset)); //get references to all the positions
                    POS_T& p1 = *((POS_T*)((it->v1)+posOffset));
                    POS_T& p2 = *((POS_T*)((it->v2)+posOffset));
                    POS_T edge1 = p1 - p0; //calc two edges of the triangle
                    POS_T edge2 = p2 - p0;

                    NORM_T normal = edge1.cross(edge2).normalize();

                    //now we have the normal, add it to the normal of the verticies in the face
                    *((NORM_T*)((it->v0)+normOffset)) += normal;
                    *((NORM_T*)((it->v1)+normOffset)) += normal;
                    *((NORM_T*)((it->v2)+normOffset)) += normal;
                }

                //if a vertex was in multiple faces then the length of its normal will be the number of faces it appeared
                //in (as we add a length 1 normal to it for each face it is in), now renormalize everything
                for(int i = 0; i < mdb.getVertexCount(); ++i){
                    ((NORM_T*)((mdb.getVertex<unsigned char>(i)+normOffset)))->normalize();
                }
            }

            /////////////////////////////////////////////////
            /// \brief Generates a mesh which has a line representing each normal for a mesh.
            /// \param inMesh The MeshDataBuffer for the Mesh that you want to generate the normal mesh for
            /// \param inPos The attribute of the positions in the inMesh
            /// \param outPos The attribute of the normals in the inMesh
            /// \param outMesh The MeshDataBuffer you want to store the generated normal mesh in
            /// \param outPos The attribute of the outMesh MeshDataBuffer that you want to store position
            /// data in
            /////////////////////////////////////////////////
            void generateNormalMesh(MeshDataBuffer& inMesh, unsigned char inPos, unsigned char inNorm,
                                     MeshDataBuffer& outMesh, unsigned char outPos);

        }
    }
}

#endif // LIE_RENDER_MESH_UTILS_HPP
