/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file RenderTexture.hpp
/// Contains the lie::rend::RenderTexture class
/// 
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_RENDERTEXTURE_HPP
#define LIE_RENDER_RENDERTEXTURE_HPP

#include "../core/NonCopyable.hpp"
#include "RenderTarget.hpp"
#include "VideoMode.hpp"

namespace lie{
	namespace rend{

		class Texture;
		class Device;
		namespace detail{
			class RenderTextureImpl;
		}

		//////////////////////////////////////////////////////////////////////////
		/// \brief Represents one or more Texture objects that can be rendered to
		/// without disturbing any displayed window
		/// A RenderTexture may have any number of color buffers, 0 or 1 depth buffer
		/// and 0 or 1 stencil buffers. Note however that a pixel shader can only write
		/// to a certain number of color buffers simultaneously, this can be queried
		/// using Device::querryFeature(MaxPixelShaderColorOutputs)
		//////////////////////////////////////////////////////////////////////////
		class RenderTexture : lie::core::NonCopyable, public RenderTarget{
			friend class Device;
		public:
			~RenderTexture();

			/////////////////////////////////////////////////
			/// \brief Makes this target active on this thread
			/// All render calls will be applied to this target
			/////////////////////////////////////////////////
			void makeCurrent();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Query whether this RenderTexture has a depth buffer
			/// \retval true This RenderTexture has an associated depth buffer
			/// \retval false This RenderTexture does not have an associated depth buffer
			//////////////////////////////////////////////////////////////////////////
			bool hasDepthBuffer();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a pointer to the depth buffer for this RenderTexture,
			/// this RenderTexture will always own the returned Texture object and will delete it
			/// when its destructor is called. Do not delete it yourself or cache it beyond
			/// the destruction of this RenderTexture!
			/// \return Texture* to the depth buffer associated with this RenderTexture, or nullptr
			/// if this RenderTexture has no depth buffer
			//////////////////////////////////////////////////////////////////////////
			Texture* getDepthBuffer();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Query whether this RenderTexture has a stencil buffer
			/// \retval true This RenderTexture has an associated stencil buffer
			/// \retval false This RenderTexture does not have an associated stencil buffer
			//////////////////////////////////////////////////////////////////////////
			bool hasStencilBuffer();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a pointer to the stencil buffer for this RenderTexture,
			/// this RenderTexture will always own the returned Texture object and will delete it
			/// when its destructor is called. Do not delete it yourself or cache it beyond
			/// the destruction of this RenderTexture!
			/// \return Texture* to the stencil buffer associated with this RenderTexture, or nullptr
			/// if this RenderTexture has no stencil buffer
			//////////////////////////////////////////////////////////////////////////
			Texture* getStencilBuffer();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the number of color buffers this RenderTexture owns
			//////////////////////////////////////////////////////////////////////////
			int getColorBufferCount();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the specified color buffer for this RenderTexture, 
			/// this RenderTexture will always own the returned Texture object and will delete it
			/// when its destructor is called. Do not delete it yourself or cache it beyond
			/// the destruction of this RenderTexture!
			/// \param index The index of the color buffer to retrieve, defaults to 0. Must be between
			/// 0 and getColorBufferCount()-1
			/// \retrun Texture* to the specified color buffer
			/// \note Bounds checking is NOT performed, if index is out of range a garbage pointer
			/// will be returned or the program will crash. Use getColorBufferCount()
			//////////////////////////////////////////////////////////////////////////
			Texture* getColorBuffer(int index = 0);

			const VideoMode& getVideoMode() const override;
		private:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use Device::createRenderTexture(...)
			/// \param size Vector2i representing the resolution in pixels of the buffers
			/// \param colorBufferCount The number of color buffers in the colorBuffers array
			/// \param colorBuffers Pointer to array of the color buffers
			/// \param depthBuffer Pointer to the depth buffer for this RenderTexture, or nullptr if
			/// the RenderTexture should not have a depth buffer
			/// \param stencilBuffer Pointer to the stencil buffer for this RenderTexture, or nullptr
			/// if the RenderTexture should not have a stencil buffer
			/// \param impl Pointer to detail::RenderTextureImpl that contains render api specific details 
			/// for this RenderTexture
			//////////////////////////////////////////////////////////////////////////
			RenderTexture(lie::math::Vector2i size, int colorBufferCount, Texture* colorBuffers, Texture* depthBuffer, Texture* stencilBuffer, detail::RenderTextureImpl* impl);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Render api specific function that should delete the render target with the
			/// rendering api, should not clean up the member variables of this class as that is
			/// handeled by this classes destructor
			//////////////////////////////////////////////////////////////////////////
			void _apiDestroy();

			///< Represents the resolution of this RenderTexture
			VideoMode mVideoMode;

			///< Pointer to render API specific implementation data
			detail::RenderTextureImpl* mImpl;

			///< Pointer to the texture that is the depth buffer for this RenderTexture
			///< Will be nullptr if this RenderTexture does not have a depth buffer
			Texture* mDepthBuffer;

			///< Pointer to the texture that is the stencil buffer for this RenderTexture
			///< Will be nullptr if this RenderTexture does not have a depth buffer
			Texture* mStencilBuffer;

			///< Pointer to array of Texture*s which each point to the color buffers of this 
			///< RenderTexture. Length of the array is given by the member "mColorBufferCount"
			Texture* mColorBuffers;

			///< The number of color buffers attached to this render texture
			int mColorBufferCount;
		};
	}
}

#endif //LIE_RENDER_RENDERTEXTURE_HPP