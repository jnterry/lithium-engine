/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file DepthTestMode.hpp
/// Contains the DepthTestMode enum class
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_DEPTHTESTMODE_HPP
#define LIE_RENDER_DEPTHTESSTMODE_HPP

namespace lie{
    namespace rend{
        /////////////////////////////////////////////////
        /// \brief enum class containing the different types of depth test
        /// that can be used by a Renderer
        /// \ingroup Render
        /////////////////////////////////////////////////
        enum class DepthTestMode{
            None, ///< Depth testing is not performed
            LessThan, ///< Objects must have a depth of less than the current object to be drawn
            GreaterThan, ///< Objects must have a depth of greater than the current object to be drawn
        };
    }
}

#endif // LIE_RENDER_DEPTHTESTMODE_HPP
