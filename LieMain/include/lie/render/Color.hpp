/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Color.hpp
/// Contains the Color class, allows you to create and manipulate colors
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_COLOR_H
#define LIE_RENDER_COLOR_H

namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief Class containing RGBA color data and includes functions to manipulate
        /// the color
        /////////////////////////////////////////////////
        struct Color{
            public:
                ///< The red component of  this color, 0 for no red, 255 for full red
                unsigned char r;

                ///< The green component of this color, 0 for no green, 255 for full green
                unsigned char g;

                ///< The blue component of this color, 0 for no blue, 255 for full blue
                unsigned char b;

                ///< The alpha component of this color, 0 for fully transparent, 255 for fully opaque
                unsigned char a;

                /////////////////////////////////////////////////
                /// \brief Default constructor, creates opaque black color
                /////////////////////////////////////////////////
                Color(){}

                /////////////////////////////////////////////////
                /// \brief Constructor creating color from passed in RGB and optionally alpha components
                /////////////////////////////////////////////////
                Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 255) :
                    r(red), g(green), b(blue), a(alpha){}


                /////////////////////////////////////////////////
                /// \brief Sets red, green and blue components of this color (using RGB color model)
                /////////////////////////////////////////////////
                void setRGBA(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 255){
                    r = red;
                    g = green;
                    b = blue;
                    a = alpha;
                }

                /////////////////////////////////////////////////
                /// \brief Sets color equal to HSL color passed in
                /////////////////////////////////////////////////
                void setHSL(float hue, float saturation, float lightness);

                static const Color Black;       ///< Black predefined color
                static const Color White;       ///< White predefined color
                static const Color Red;         ///< Red predefined color
                static const Color Green;       ///< Green predefined color
                static const Color Blue;        ///< Blue predefined color
                static const Color Yellow;      ///< Yellow predefined color
                static const Color Magenta;     ///< Magenta predefined color
                static const Color Cyan;        ///< Cyan predefined color
                static const Color Transparent; ///< Transparent (black) predefined color
        };

    }//end of rend::
}//end of lie::

#endif // LIE_RENDER_COLOR_H
