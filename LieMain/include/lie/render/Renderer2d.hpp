#ifndef LIE_RENDERER2D_HPP
#define LIE_RENDERER2D_HPP

#include "../math/Aabb2.hpp"
#include "../math/Matrix3x3.hpp"
#include "../math/Shape2.hpp"
#include "Camera2d.hpp"
#include "Renderer.hpp"
#include "../core/NonCopyable.hpp"

namespace lie{
    namespace rend{

		class Methodology;
		class RenderTarget;
		class Mesh;
		class Shape2d;
		class Material;
		class Pass;


        /////////////////////////////////////////////////
        /// \brief A 2D renderer is used to draw two objects to a RenderTarget with
        /// respect to some camera and target view port.
        /// 
        /// Various functions exist for rendering different 2D objects, each draw call
        /// requires a Methodology that describes how the geometry should be rendered.
        /// 
        /// The Renderer2D is capable of drawing to multiple "layers", these allow you
        /// to draw objects on top of each other, for example drawing buildings over the
        /// sky and terrain background. Objects in higher layers are always drawn over
        /// objects in lower layers. No guarantees are made about the order objects are
        /// drawn in within each layer allowing the Renderer2d to perform optimizations, such 
        /// as drawing all objects that use the same shader at the same time, hence if objects 
        /// overlap and you wish one to always be drawn on top of another you must use the layers.
        /// 
        /// Layers can also have an associated "parallax factor" that allows layers to move over each
        /// other giving a sense of depth. The number of pixels a layer is moved by when the camera
        /// is moved is given by: cameraMoveDistance * parralaxFactor <br>
        /// These factors default to 1 which means that for every 1 pixel the camera moves the 
        /// layer will also move 1 pixel. Setting the factor to a value between 0 and 1 means 
        /// the layer will move less than the camera and thus appear to be far away. The value
        /// can be set to 0 to cause a layer to be stationary. Negative values and values greater than
        /// 1 can be used, but have no real-world equivalent. <br>
        /// Note that parallax factors can be set independently for the x and y directions
        /////////////////////////////////////////////////
		class Renderer2d : lie::core::NonCopyable, public Renderer{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs new Renderer2d
            /// \param numLayers The number of layers this Renderer2d should have
            /// defaults to 1
            /////////////////////////////////////////////////
            Renderer2d(unsigned short numLayers = 1);

			virtual ~Renderer2d();

            ///@{
            ///@name Drawing Functions

            /////////////////////////////////////////////////
            /// \brief Draws an instance of lie::math::Shape2
            /// \param meth The methodology that should be used to render the shape
            /// \param modelMatrix The matrix that transforms the shape from its 
            /// local space into world space
            /// \param layer The layer that the shape should be drawn to, higher layers are
            /// drawn on top of lower layers
            /////////////////////////////////////////////////
			virtual void draw(const Methodology& meth, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer, lie::math::Shape2f& shape) = 0;

			

            ///@} //end drawing functions

            /// @{
            /// @name Renderer Setting functions

            /////////////////////////////////////////////////
            /// \brief Sets the camera used by this Renderer to the specified one
            /// \return Reference to the internal camera object, can be used to modify
            /// the camera of this renderer
            /////////////////////////////////////////////////
            void setCamera(Camera2d& cam);

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the camera currently in use by this
            /// Renderer2d, as it is a reference the camera can be modified and the
            /// changes will be applied to this Renderer after the next call to begin
            /////////////////////////////////////////////////
            Camera2d& getCamera();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the number of layers that this Renderer2d can render to
			//////////////////////////////////////////////////////////////////////////
			unsigned short getLayerCount() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Changes the number of layers that this Renderer2d can render to
			/// \param count The new number of layers
			/// \note If layer count is increased any new layers have their parallax
			/// factors set to 1, if the layer count is decreased the layers with the
			/// highest layer indexes are deleted, hence layers indexes are always in the
			/// range 0 to layerCount-1
			//////////////////////////////////////////////////////////////////////////
			void setLayerCount(unsigned short count);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the parallax factor for the specified layer
			/// \param layer The index of the layer whose parallax factor you wish to retrieve
			/// 
			/// The parallax factor determines how much a layer will move as the camera moves,
			/// The layer moves by cameraMovement * parrallaxFactor.
			/// The factor can be set independently for the x and y directions
			/// for example:
			///  - factor 1.0 (default) -> the layer will follow the camera exactly, if the camera moves
			///  one pixel the layer will also move 1 pixel
			///  - 0 < factor < 1 -> the layer will move less than the camera, eg, factor 0.5, if the camera
			///  moves 2 pixels the layer will move 1 pixel
			///  - factor = 0.0 -> the layer will not move at all, good for infinite distance objects, eg the sky
			///  - factor < 0 -> the layer will move in the wrong direction (unrealistic)
			///  - factor > 1 -> the layer will move more than the camera (unrealisitc)
			//////////////////////////////////////////////////////////////////////////
			lie::math::Vector2f getLayerParrallaxFactor(unsigned short layer) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the X parallax factor for the specified layer
			/// \param layer The index of the layer whose X parallax factor you wish to retrieve
			//////////////////////////////////////////////////////////////////////////
			float getLayerParrallaxFactorX(unsigned short layer) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the Y parallax factor for the specified layer
			/// \param layer The index of the layer whose Y parallax factor you wish to retrieve
			//////////////////////////////////////////////////////////////////////////
			float getLayerParrallaxFactorY(unsigned short layer) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the parallax factor for both the X and Y directions
			/// \param layer The index of the layer whose parallax factor you wish to modify
			/// \param factor The factor for both the X and Y directions
			//////////////////////////////////////////////////////////////////////////
			void setLayerParrallaxFactor(unsigned short layer, float factor){
				this->setLayerParrallaxFactor(layer, lie::math::Vector2f(factor, factor));
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the parallax factor for both the X and Y directions
			/// \param layer The index of the layer whose parallax factor you wish to modify
			/// \param factorX the factor to use for the parallax in the horizontal direction
			//////////////////////////////////////////////////////////////////////////
			void setLayerParrallaxFactor(unsigned short layer, float factorX, float factorY){
				this->setLayerParrallaxFactor(layer, lie::math::Vector2f(factorX, factorY));
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the parallax factor for both the X and Y directions
			/// \param layer The index of the layer whose parallax factor you wish to modify
			/// \param factors Vector2f containing the new X and Y parallax factors
			//////////////////////////////////////////////////////////////////////////
			void setLayerParrallaxFactor(unsigned short layer, lie::math::Vector2f factors);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the parallax factor for the X direction without changing the Y factor
			/// \param layer The index of the layer whose parallax factor you wish to modify
			/// \param factorX The factor for the parallax X factor
			//////////////////////////////////////////////////////////////////////////
			void setLayerParrallaxFactorX(unsigned short layer, float factorX);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the parallax factor for the Y direction without changing the X factor
			/// \param layer The index of the layer whose parallax factor you wish to modify
			/// \param factorY The factor for the parallax Y factor
			//////////////////////////////////////////////////////////////////////////
			void setLayerParrallaxFactorY(unsigned short layer, float factorY);



            /// @}

        protected: //derived classes need access to these

			//////////////////////////////////////////////////////////////////////////
			/// \brief Uses the specified pass by binding the appropriate shader program,
			/// program parameters, and textures
			//////////////////////////////////////////////////////////////////////////
			void usePass(Pass& pass, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer);
            

            ///The camera currently used to view the scene
            lie::rend::Camera2d mCamera;

			///< The number of layers this Renderer2d can renderer to
			unsigned short mNumLayers;

			///< Pointer to array of Vector2f instances containing the parallax factors
			///< for the layers, length of array is given by mNumLayers
			lie::math::Vector2f* mParrallaxFactors;
        };

    }
}

#endif // LIE_RENDERER2D_HPP
