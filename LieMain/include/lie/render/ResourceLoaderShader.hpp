/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ResourceLoaderShader.hpp
/// \author Jamie Terry
/// \date 2015/06/19
/// \brief Contains the lie::rend::ResourceLoaderShader class
/// 
/// \ingroup renderer
/////////////////////////////////////////////////

#ifndef LIE_RENDER_RESOURCELOADERSHADER_HPP
#define LIE_RENDER_RESOURCELOADERSHADER_HPP

namespace lie{
	namespace core{
		class File;
	}
	namespace rend{

		class ShaderProgram;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Function that may be registered with a ResourceDatabase for loading shaders
		/// The file to be loaded must consist of any number of lines of the following format:
		/// [TYPE] [FILENAME]
		/// Where type specified the shader stage and filename specified the name of a file
		/// in the same folder as the file that contains the shaders source code
		/// Eg, folder contains the following:
		/// PhongVertex.glsl
		/// PhongPixel.glsl
		/// Phong.shader
		/// Phong.shader is passed to this function, its contents is as follows:
		/// Vertex PhongVertex.glsl
		/// Pixel PhongPixel.glsl
		//////////////////////////////////////////////////////////////////////////
		lie::rend::ShaderProgram* resourceLoaderShaderFileList(lie::core::File& file, unsigned int byteOffset);

	}
}

#endif