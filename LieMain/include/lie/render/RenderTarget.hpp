#ifndef LIE_RENDER_RENDERTARGET_HPP
#define LIE_RENDER_RENDERTARGET_HPP

#include "BufferTypes.hpp"

namespace lie{
    namespace rend{

		class VideoMode;

        /////////////////////////////////////////////////
        /// \brief A RenderTarget represents any set of buffers that can be drawn to
        /// Eg: Texture, Window, etc
        /// RenderTargets CANNOT be shared between contexts and so belong to a specific
        /// RenderContext
        /// A RenderContext always has a default RenderTarget, and always has one of its
        /// render targets active, any rendering done by that render context will be done
        /// on its active render target.
        /////////////////////////////////////////////////
        class RenderTarget{
        public:
            /////////////////////////////////////////////////
            /// \brief Returns the render target which is active on this thread
            /////////////////////////////////////////////////
            //RenderTarget& getCurrentTarget();

            /////////////////////////////////////////////////
            /// \brief Makes this target active on this thread
            /// All render calls will be applied to this target
            /////////////////////////////////////////////////
            virtual void makeCurrent() = 0;

            /////////////////////////////////////////////////
            /// \brief Returns the a VideoMode instance representing the size and color depth
            /// of this RenderTarget
            /////////////////////////////////////////////////
			virtual const VideoMode& getVideoMode() const = 0;

            /////////////////////////////////////////////////
            /// \brief Clears the specified buffer(s) attached to this render target,
            /// \note This functions makes this RenderTarget the current target of this thread,
            /// ie, it calls makeCurrent()
            /////////////////////////////////////////////////
            void clear(char buf = lie::rend::BufferTypes::All);
        private:
        };

    }
}

#endif // LIE_RENDER_RENDERTARGET_HPP
