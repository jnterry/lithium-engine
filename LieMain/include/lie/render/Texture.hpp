#ifndef LIE_RENDER_TEXTURE_HPP
#define LIE_RENDER_TEXTURE_HPP

#include <lie/math/Vector2.hpp>
#include <lie/core/File.hpp>
#include <lie/core/NonCopyable.hpp>
#include "PixelFormat.hpp"

namespace lie{
    namespace rend{

        //forward deceleration
        namespace detail{
            struct TextureImpl;
        }

        /////////////////////////////////////////////////
        /// \brief A texture is a 2d array of pixels and their color value
        /// stored on the GPU
        /////////////////////////////////////////////////
        class Texture : lie::core::NonCopyable{
			friend class Device;
        public:
            ///< The type used to represent which unit to bind a texture to
            typedef unsigned char UnitId;

            /////////////////////////////////////////////////
            /// \brief Creates a new empty texture with the specified width
            /// and height, defaults to a 1 by 1 texture
            /// \param width The width in pixels of the texture to create, defaults to 1
            /// \param height The height in pixels of the texture to create, defaults to 1
            /// \param pixelFormat The pixel format that the texture data will be stored in
            /////////////////////////////////////////////////
			Texture(int width = 1, int height = 1, PixelFormat pixelFormat = PixelFormat::RGBA_8U);

            /////////////////////////////////////////////////
            /// \brief Loads the specified file and uploads the data to
            /// the gpu
            /// \return true if load was successful, else false
            /////////////////////////////////////////////////
            bool loadFromFile(lie::core::File& file);

            /////////////////////////////////////////////////
            /// \brief Deletes the textures data on the gpu
            /////////////////////////////////////////////////
            ~Texture();

            /////////////////////////////////////////////////
            /// \brief Returns the width in pixels of the texture
            /////////////////////////////////////////////////
            int getWidth();

            /////////////////////////////////////////////////
            /// \brief Returns the height in pixels of the texture
            /////////////////////////////////////////////////
            int getHeight();

            /////////////////////////////////////////////////
            /// \brief returns a lie::math::Vector2ui where the x component
            /// is the textures width and the y component is the textures height
            /////////////////////////////////////////////////
            const lie::math::Vector2i& getSize();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the size of this texture, any data loaded into it is discarded
			//////////////////////////////////////////////////////////////////////////
			void setSize(int width, int height);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Changes the pixel format of this texture, any existing data stored on
			/// the gpu will be deleted
			//////////////////////////////////////////////////////////////////////////
			void setFormat(PixelFormat pixelFormat);

            /////////////////////////////////////////////////
            /// \brief Binds this texture to the specified texture unit
            /// Multiple units can be used to use multiple textures at one, eg
            /// texture, normal map, etc
            /// \note There is a hardware limmit on this, use Device::querryFeature
            /////////////////////////////////////////////////
            void bind(UnitId unit);

            /////////////////////////////////////////////////
            /// \brief Unbinds the currently bound texture from the specified unit,
            /// returning it to the default texture
            /////////////////////////////////////////////////
            static void clearBound(UnitId uint);
        private:
            /////////////////////////////////////////////////
            /// \brief Rendering API dependent function which buffers the given char array
            /// onto the gpu
            /// \param data The data to upload to the GPU, if nullptr is specified no data is
            /// uploaded but the texture is created on the GPU with the size and format stored in
            /// this instance
            /////////////////////////////////////////////////
            void _storeOnGpu(unsigned char* data);

            lie::math::Vector2i mSize;

            detail::TextureImpl* mImpl;

			PixelFormat mPixelFormat;
        };

    }
}

#endif // LIE_RENDER_TEXTURE_HPP
