/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Event.hpp
/// Contains the Event class, events are created from windows when user
/// input occurs
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_EVENT_HPP
#define LIE_RENDER_EVENT_HPP

#include <lie/render\Keyboard.hpp>
#include <lie/render\Mouse.hpp>

namespace lie{
    namespace rend{
        /////////////////////////////////////////////////
        /// \ingroup render
        /// \brief An event is produced when user input occurs and there is at least one
        /// window open
        /// They are querried from a window with the getEvent() and waitForEvent() functions
        /////////////////////////////////////////////////
        class Event{
        public:
            struct ResizeEvent{
                unsigned int width;///new width of client area in pixels
                unsigned int height;///new height of client area in pixels
            };

            struct KeyEvent{
                Keyboard::Key keycode; ///the code of the key that has been pressed
                bool alt;
                bool ctrl;
                bool shift;
                bool system;
            };

            struct TextEvent{
                ///UTF-32 unicode value of the entered character.
                ///Text event automatically handles modifiers, eg shift a gives an 'A', not 'a'
                unsigned int unicode;
            };

            /////////////////////////////////////////////////
            /// \brief Data for when mouse is moved or a mouse button is pressed/released
            ///
            /// For a mouse button event:
            /// Button -> the  button which changed state (was pressed or was released)
            /// x and y -> the mouse's  position when the button changed state
            ///
            /// For a mouse move event:
            /// Button -> the button which was held as the mouse was moved
            /// (will be none if the mouse  was moved while no buttons were held)
            /// x and y -> the new mouse's position
            /// \param
            /////////////////////////////////////////////////
            struct MouseEvent{
                Mouse::Button button;///the button that was pressed while the mouse was being moved
                int x;///the new x position of the mouse pointer
                int y;///the new y position of the mouse pointer
            };

            struct MouseWheelEvent{
                int delta; ///number of ticks the mouse wheel has moved, +ve is up, -ve is down
                int x; ///the x position of the mouse when the wheel moved
                int y; ///the y position of the mouse when the wheel moved
            };

            enum Type{
                ///user requested the window  be closed, eg pressing red x in coner or pressed alt-f4 etc
                ///union data -> none
                Closed,

                ///the window was resized
                ///union data -> ResizeEvent size;
                Resized,

                ///the window lost focus, ie user clicked on another window/desktop
                ///union data -> none
                LostFocus,

                ///window gained focus, ie user clicked on the window while it did not have focus
                ///union data -> none
                GainedFocus,

                ///generated when the keyboard is used, gives the input as utf-32 codes,
                ///This is far easier than doing:
                ///\code
                /// if(key == Keyboard::A){
                ///     if(shiftPressed){
                ///         return 'A';
                ///     }else{
                ///         return 'a';
                ///     {
                /// } //etc...
                ///\endcode
                ///union data -> TextEvent text
                TextEntered,

                ///a previously unpressed keyboard key was pressed
                ///union data -> none
                KeyPressed,

                ///a previously pressed keyboard key was released
                ///union data -> none
                KeyReleased,

                ///the mouse wheel was scrolled
                ///union data -> MouseWheelEvent
                MouseWheelMoved,

                ///a mouse button was pressed
                ///union data -> MouseEvent
                MouseButtonPressed,

                ///a mouse button was released
                ///union data -> MouseEvent
                MouseButtonReleased,

                ///the mouse was moved or dragged
                ///union data -> MouseEvent
                MouseMoved,

                ///the mouse cursor entered the window's client area, reported regardless of whether the window had focus when event occured
                ///union data -> none
                MouseEntered,

                ///the mouse cursor left the window's client area, reported regardless of whether the window had focus when event occured
                ///union data -> none
                MouseLeft,

                ///the window was minimised
                Minimised,

                ///A previously minimised window was maximised
                Restored,
            };
            Type type;

            union{
                ResizeEvent size;
                KeyEvent key;
                TextEvent text;
                MouseEvent mouse;
                MouseWheelEvent mouseWheel;
            };
        };

    }
}

#endif // LIE_RENDER_EVENT_HPP
