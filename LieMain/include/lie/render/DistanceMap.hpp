/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file DistanceMap.hpp
/// Contains function to create a distance map from a texture 
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_DISTANCEMAP_HPP
#define LIE_RENDER_DISTANCEMAP_HPP

namespace lie{
	namespace rend{
		class Texture;
		class Device;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Creates and returns a distance map for a specified texture.
		/// The distance map is created by converting the original texture into greyscale
		/// if it isn't already (note: parameter original is NOT modified). A new texture
		/// is created of the same size as the original, its pixels are set to the value
		/// equal to how far the corresponding pixel in the original image is to a pixel with
		/// a greater than or equal greyscale value to threshold
		/// \note The caller takes ownership of the returned texture object and is responsible
		/// for deleting it
		//////////////////////////////////////////////////////////////////////////
		Texture* createDistanceMap(Texture& original, Device& device, float threshold = 1);
	}
}

#endif //LIE_RENDER_DISTANCEMAP_HPP