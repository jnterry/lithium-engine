/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Keyboard.hpp
/// Contains the Methodology class
///
/// \ingroup render
/////////////////////////////////////////////////
#ifndef LIE_RENDER_METHODOLOGY_HPP
#define LIE_RENDER_METHODOLOGY_HPP

#include "ShaderParameterType.hpp"
#include <string>
#include <vector>

namespace lie{
    namespace rend{

		//forward deceleration
		class Material;
		class Technique;
		class MaterialType;
		class Device;

        /////////////////////////////////////////////////
        /// \brief A Methodology is a collection of one or more "Technique"s that can be used
        /// to render geometry.
        /// When a Methodology is active the Technique that is used will be automatically chosen
        /// Techniques which cannot be used on the current hardware as it uses unsupported features
        /// will never be used.
        /// From the Techniques that can be performed, the appropriate one will be selected based
        /// off the objects LOD
        /////////////////////////////////////////////////
		class Methodology{
        public:

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a new Methodology that can be used to render the specified MaterialType
			//////////////////////////////////////////////////////////////////////////
			Methodology(const lie::rend::MaterialType& matType, lie::rend::Technique& theTechnique);
			static lie::rend::Methodology* createDefault2D(Device& device);
			const lie::rend::MaterialType& getMaterialType();
			lie::rend::Technique& technique;
		private:
			const lie::rend::MaterialType& mMatType;


        };

    }
}

#endif // LIE_RENDER_METHODOLOGY_HPP
