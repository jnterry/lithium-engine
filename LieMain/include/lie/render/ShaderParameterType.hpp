/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Contains the lie::rend::ShaderParameterType enum
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_SHADERPARAMETERTYPE_HPP
#define LIE_RENDER_SHADERPARAMETERTYPE_HPP

namespace lie{
	namespace rend{

		/////////////////////////////////////////////////
		/// \brief The types of parameters that can be passed to a shader
		/////////////////////////////////////////////////
		enum class ShaderParameterType{
			Mat4x4f, ///< A 4 by 4 matrix of 32 bit floating point numbers
			Mat3x3f, ///< A 3 by 3 matrix of 32 bit floating point numbers
			Vec4f, ///< A vector with 4 32 bit floating point components, eg x,y,z and w
			Vec3f, ///< A vector with 3 32 bit floating point components, eg x,y,z
			Vec2f, ///< A vector with 2 32 bit floating point components, eg x,y
			Float, ///< A single 32 bit float point number
			Int32, ///< A single signed 32 bit integer


			Unknown, ///< Represents an unknown or invalid parameter type
			
		};
	}
}

#endif