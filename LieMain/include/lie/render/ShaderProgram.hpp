#ifndef LIE_RENDER_SHADERPROGRAM_HPP
#define LIE_RENDER_SHADERPROGRAM_HPP

#include "Shader.hpp"
#include "Color.hpp"
#include "ShaderParameterType.hpp"
#include <string>
#include <vector>
#include <lie/math/Matrix4x4.hpp>
#include <lie/math/Matrix3x3.hpp>
#include <lie/math/Vector2.hpp>
#include <lie/math/Vector3.hpp>
#include <lie/math/Vector4.hpp>
#include <lie/core/NonCopyable.hpp>


namespace lie{
    namespace rend{

        //forward deceleration
        namespace detail{
            class ShaderProgramImpl;
        }

        /////////////////////////////////////////////////
        /// \class ShaderProgram
        /// \brief A ShaderProgram instance represents a compiled and linked ShaderProgram
        /// registered with the underlying graphics API. Use the ShaderProgramBuilder class
        /// to create an instance.
        /// \ingroup render
        /////////////////////////////////////////////////
        class ShaderProgram : public lie::core::NonCopyable{
			friend class ShaderProgramBuilder;
            public:
                /////////////////////////////////////////////////
                /// \brief deletes this ShaderProgram and the corresponding shader
                /// program registered with the graphics api.
                /////////////////////////////////////////////////
                ~ShaderProgram();

                /////////////////////////////////////////////////
                /// \brief Makes this the currently used shader program
                /////////////////////////////////////////////////
                void use() const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Unbinds the currently active shader program, hence reverting to
				/// the render api's fixed function pipeline
				//////////////////////////////////////////////////////////////////////////
				static void useNone(); 

                /////////////////////////////////////////////////
                /// \brief Queries weather the program has a parameter with the given name
                ///
                /// \param std::string -> the name of the parameter to check
                /// \return true if the parameter exists, else false
                ///
                /// Note, just because a parameter is declared in a shader, it does not nessaserally exist!
                /// If a parameter is declared but not used then the graphics card driver may optimize
                /// it out, in this case this function will return false!
                /////////////////////////////////////////////////
                bool hasParameter(std::string parameterName) const;

                /////////////////////////////////////////////////
                /// \brief Gets the total number of parameters in this program
                /// \return int, the number of parameters in this program
                ///
                /// The count only includes "active" parameters, the graphics card drivers
                /// will often optimize out declared but unused parameters, these are not "active"
                /// and are not included in this count
                /////////////////////////////////////////////////
                int getParameterCount() const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the name of the Parameter with the specified index in the ShaderProgram
				/// \param index The index of the parameter whose name you wish to retrieve, must be between 0
				/// and getParameterCount()-1
				/// \return std::string holding the name of the parameter
				//////////////////////////////////////////////////////////////////////////
				std::string getParameterName(int index) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the type of the parameter at the specified index in the ShaderProgram
				/// /// \param index The index of the parameter whose name you wish to retrieve, must be between 0
				/// and getParameterCount()-1
				/// \return The type of the parameter
				//////////////////////////////////////////////////////////////////////////
				ShaderParameterType getParameterType(int index) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the index of the parameter with the specified name or -1
				/// if the parameter does not exist
				//////////////////////////////////////////////////////////////////////////
				int getParameterIndex(std::string name) const;

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
                /// \param index The index of the parameter to change
                /// \param value the data to set the parameter to
                /// \return bool indicating weather parameter value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the parameter does not exist or it is not of the type passed in,
                /// thus the parameter's value remains unchanged
                /////////////////////////////////////////////////
                bool setParameter(int index, lie::math::Matrix4x4f value);

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
                /// \param value the data to set the parameter to
                /// \return bool indicating whether parameter value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the parameter does not exist or it is not of the type passed in,
                /// thus parameter
				bool setParameter(int index, lie::math::Matrix3x3f value);

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
                /// \param value the data to set the parameter to
                /// \return bool indicating whether parameter value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the parameter does not exist or it is not of the type passed in,
                /// thus the parameter's value remains unchanged
                /////////////////////////////////////////////////
				bool setParameter(int index, float value);

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
                /// \param value  the data to set the uniform to
                /// \return bool indicating whether uniform value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the uniform variable does not exist or it is not of the type passed in,
                /// thus the parameter's value remains unchanged
                /////////////////////////////////////////////////
				bool setParameter(int index, int value);

				/////////////////////////////////////////////////
				/// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
				/// \param value the data to set the parameter to
				/// \return bool indicating whether parameter value was set to passed in value
				/// \retval true The parameter's value was set to the passed in value
				/// \retval false Either the parameter does not exist or it is not of the type passed in,
				/// thus the parameter's value remains unchanged
				/////////////////////////////////////////////////
				bool setParameter(int index, lie::math::Vector2f value);

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
                /// \param lie::math::Vector3f the data to set the parameter to
                /// \return bool indicating whether parameter value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the parameter does not exist or it is not of the type passed in,
                /// thus the parameter's value remains unchanged
                /////////////////////////////////////////////////
				bool setParameter(int index, lie::math::Vector3f value);

				/////////////////////////////////////////////////
				/// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
				/// \param value the data to set the parameter to
				/// \return bool indicating whether parameter value was set to passed in value
				/// \retval true The parameter's value was set to the passed in value
				/// \retval false Either the parameter does not exist or it is not of the type passed in,
				/// thus the parameter's value remains unchanged
				/////////////////////////////////////////////////
				bool setParameter(int index, lie::math::Vector4f value);

                /////////////////////////////////////////////////
                /// \brief Sets a parameter for this shader program, takes a string name and variable to make it equal
				/// \param index The index of the parameter to change
                /// \param value the data to set the parameter to
                /// \return bool indicating whether parameter value was set to passed in value
                /// \retval true The parameter's value was set to the passed in value
                /// \retval false Either the parameter does not exist or it is not of the type passed in,
                /// thus the parameter's value remains unchanged
                /////////////////////////////////////////////////
                bool setParameter(int index, Color value);

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the number of texture channels this ShaderProgram takes as input
				//////////////////////////////////////////////////////////////////////////
				int getTextureChannelCount() const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the name of the texture channel at the specified index
				/// \param channel The index of the texture channel to query, must be between 0
				/// and getTextureChannelCount()-1
				//////////////////////////////////////////////////////////////////////////
				std::string getTextureChannelName(int channel) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the index of the texture channel with the specified name
				/// \return int holding the index of the specified channel or -1 if no channel
				/// with that name exists
				//////////////////////////////////////////////////////////////////////////
				int getTextureChannelIndex(std::string name) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Sets which texture unit a certain texture channel should read from
				/// \param channel The index of the texture channel to modify, must be between 0
				/// and getTextureChannelCount()-1
				/// \param boundUnit the texture unit that should be used for this channel,
				/// the texture used will be the last texture that has Texture.bind(boundUnit)
				/// called
				//////////////////////////////////////////////////////////////////////////
				void setTextureChannelBinding(int channel, int boundUnit);

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns true if two ShaderProgram objects refer to the same 
				/// shader program as registered with the underlying rendering API
				/// \note Two different ShaderProgam instances should NEVER refer to the same 
				/// shader program resisted on the with the rendering api.
				//////////////////////////////////////////////////////////////////////////
				bool operator==(const ShaderProgram& other) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns true if two ShaderProgam objects do not refer to the same
				/// ShaderPgorm as registered with the underlying rendering API. It may be
				/// that the shaders have the exact same code and parameter values, however
				/// if they are actually two different ShaderPrograms as far as the rendering API
				/// is concerned this function will return false
				//////////////////////////////////////////////////////////////////////////
				bool operator!=(const ShaderProgram& other) const{ return !this->operator==(other); }
            private:
				///< Render API specific implementation
                detail::ShaderProgramImpl* mImpl;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Returns the location of the specified parameter in the ShaderProgram
				/// \param name The name of the parameter to get the location for, as it is
				/// named in the shader
				/// \return int The location of the parameter or -1 if the parameter does not exist
				//////////////////////////////////////////////////////////////////////////
				int getParameterLocation(int index) const;

				//////////////////////////////////////////////////////////////////////////
				/// \brief Creates a ShaderProgram with the specified details, private, designed to be 
				/// called by ShaderProgramBuilder
				//////////////////////////////////////////////////////////////////////////
				ShaderProgram(detail::ShaderProgramImpl* impl);
        };
    }//end of gl::
}


#endif // LIE_RENDER_SHADERPROGRAM_H
