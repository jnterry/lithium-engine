#ifndef LIE_RENDER_RENDER2DIMMEDIATE_HPP
#define LIE_RENDER_RENDER2DIMMEDIATE_HPP

#include <lie/math\Shape2.hpp>
#include "Renderer2d.hpp"
#include "RenderTarget.hpp"

namespace lie{
    namespace rend{

		class Material;
		class Methodology;

        class Renderer2dImmediate : public lie::rend::Renderer2d {
        public:
			Renderer2dImmediate(unsigned short layerCount);
			void onBegin(RenderTarget& target) override;
			void draw(const Methodology& meth, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer, math::Shape2f& shape) override;
        private:

        };

    }
}

#endif // LIE_RENDER_RENDER2DIMMEDIATE_HPP
