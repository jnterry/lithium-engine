/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file MaterialType.hpp
/// Contains the lie::rend::MaterialType class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MATERIALTYPE_HPP
#define LIE_RENDER_MATERIALTYPE_HPP

#include <string>
#include "ShaderParameterType.hpp"
#include "Material.hpp"

namespace lie{
	namespace rend{
		
		//forward deceleration
		class Material;
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief A MaterialType defines the set of attributes and textures that a 
		/// Material must have. The class is immutable as once Material instances
		/// have been made modifying the data in this class would cause crashes, 
		/// therefore you must use a MaterialTypeBuilder to construct instances of
		/// this class.
		//////////////////////////////////////////////////////////////////////////
		class MaterialType{
			friend class MaterialTypeBuilder;
			friend class Material;
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Deletes this MaterialType
			//////////////////////////////////////////////////////////////////////////
			~MaterialType();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Creates a Material with all values set to the defaults
			//////////////////////////////////////////////////////////////////////////
			Material* createMaterial() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns true if an attribute with the specified name exists
			//////////////////////////////////////////////////////////////////////////
			bool hasAttribute(std::string name) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the index of the attribute with the specified name
			/// \return the index of the specified attribute or -1 if the attribute does
			/// not exist
			/// \sa hasAttribute(std::string name)
			//////////////////////////////////////////////////////////////////////////
			int getAttributeIndex(std::string name) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the number of textures that materials of this type have
			//////////////////////////////////////////////////////////////////////////
			unsigned char getTextureCount() const;
		private:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use a MaterialTypeBuilder to create instances
			/// of this class.
			/// Creates a new instance of MaterialType with the specified number of attributes
			/// \param attribuCount The number of attributes this MaterailType has
			/// \param defaults The default values for the parameters of this MaterialType
			//////////////////////////////////////////////////////////////////////////
			MaterialType(unsigned char attribCount, const lie::core::ByteBuffer& defaults);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the byte offset of the specified attribute in a Material object
			/// \param index The index of the attribute whose byte offset you wish to retrieve
			//////////////////////////////////////////////////////////////////////////
			int getAttributeByteOffset(int index) const;
			
			struct AttributeMeta{
				AttributeMeta(std::string name = "", ShaderParameterType type = ShaderParameterType::Unknown, unsigned short byteOffset = 0);
				void setTo(const AttributeMeta& other);

				std::string name; ///< The name of the attribute
				lie::rend::ShaderParameterType type; ///< The type of the attribute
				unsigned short byteOffset; ///< The byte offset of the data for this attribute in a Material
			};

			///< The attributes that Material instances of this type have
			AttributeMeta* mAttributes;

			///< The number of attributes this MaterialType has
			unsigned char mAttributeCount;

			///< The number of textures that Material instances of this type have
			unsigned char mTextureCount;

			///< A material instance with the default values for all attributes
			Material mDefaultMat;
		};
	}	
}

#endif