/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file MeshLoaders.hpp
/// Contains various functions for loading mesh data
///
/// \defgroup render Rendering Module
/// \brief Contains all classes for opening/managing rendering contexts and then drawing to them
///
/// \note Currently there is a single implementation for this module, the opengl version
/// However no interface exposes opengl types, hence it should be possible to write a new
/// set of cpps for the same hpps to define a new implementation, eg for directx
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MESHLOADERS_HPP
#define LIE_RENDER_MESHLOADERS_HPP

#include "MeshDataBuffer.hpp"
#include "../core/File.hpp"

namespace lie{
    namespace rend{
        namespace mesh{
            /////////////////////////////////////////////////
            /// \brief Loads an obj mesh from the specified file and stores it in
            /// the specified MeshDataBuffer
            ///
            /// \param file The file to load the obj from
            /// \param mdb Reference to a MeshDataBuffer that you want to store the data in, this is an output!
            /// \param positionAttrib The id of the attribute in the MeshDataBuffer in which to store position data,
            ///                       enter a -ve if you wish to discard position data
            /// \param normalAttrib The id of the attribute in the MeshDataBuffer in which to store normal data,
            ///                     enter a -ve if you wish to discard normal data
            /// \param texCordAttrib The id of the attribute in the MeshDataBuffer in which to store texture coordinate data,
            ///                     enter a -ve if you wish to discard texture coordinate data
            /// \return true Succseeded to load mesh
            ///         false Failed to load mesh
            ///
            /// \note The attribute used to store positions MUST have 3 components, the attribute used to store texture coordinates
            /// must have 2 components.
            /////////////////////////////////////////////////
            bool loadObj(lie::core::File file, lie::rend::MeshDataBuffer& mdb, int positionAttrib, int normalAttrib, int texCordAttrib);
        }
    }
}

#endif // LIE_RENDER_MESHLOADERS_HPP
