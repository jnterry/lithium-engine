/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ResourceLoaderTexture.hpp
/// \author Jamie Terry
/// \date 2015/06/19
/// \brief Contains various functions that can be registered as resource loaders
/// with a ResourceDatabase for loading various image formats
/// 
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_RESOURCELOADERJPG_HPP
#define LIE_RENDER_RESOURCELOADERJPG_HPP

namespace lie{
	namespace core{
		class File;
	}
	namespace rend{

		class Texture;
		
		//////////////////////////////////////////////////////////////////////////
		/// \brief Simple resource loader that loads a single texture from a jpeg file
		//////////////////////////////////////////////////////////////////////////
		lie::rend::Texture* resourceLoaderTextureJpg(lie::core::File& file, unsigned int byteOffset);

	}
}

#endif //LIE_RENDER_RESOURCELOADERJPG_HPP