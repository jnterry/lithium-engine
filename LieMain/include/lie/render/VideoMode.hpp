#ifndef LIE_RENDER_VIDEOMODE_HPP
#define LIE_RENDER_VIDEOMODE_HPP

#include <vector>
#include <lie/math\Vector2.hpp>

namespace lie{
    namespace rend{

        struct VideoMode{
            /////////////////////////////////////////////////
            /// \brief Constructs new defualt video mode with all members set to 0
            /////////////////////////////////////////////////
            VideoMode();

            /////////////////////////////////////////////////
            /// \brief Constructs new video mode with the specified parameters
            /// \param newWidth The width of the video mode in pixels
            /// \param newHeight The height of the video mode in pixels
            /// \param newBitsPerPixel number of bits to store color data with, defaults to 32
            /////////////////////////////////////////////////
			VideoMode(int newWidth, int newHeight, int newBitsPerPixel = 32)
			: VideoMode(math::Vector2i(newWidth, newHeight), bitsPerPixel){}

			/////////////////////////////////////////////////
			/// \brief Constructs new video mode with the specified parameters
			/// \param newSize Vector2i where x holds the width of the video mode and y
			/// the height of the video mode
			/// \param newBitsPerPixel number of bits to store color data with, defaults to 32
			/////////////////////////////////////////////////
			VideoMode(const math::Vector2i& newSize, int newBitsPerPixel = 32);

            /////////////////////////////////////////////////
            /// \brief Copy constructor -> creates new instance of video mode by copying data from another
            /////////////////////////////////////////////////
            VideoMode(const VideoMode& other);

            /////////////////////////////////////////////////
            /// \brief Returns true if this video mode can be used for a full screen window
            /// Has no relavence for anything else, eg, rendering to image can use any size and bpp
            /////////////////////////////////////////////////
            bool isValidFullscreen() const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two video modes are equal
            /////////////////////////////////////////////////
            bool operator==(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Returns true if two video modes are not equal
            /////////////////////////////////////////////////
            bool operator!=(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Checks if this video mode is "less" than the other passed in one
            /// Checks bits per pixel followed by width followed by height, returns true if any
            /// one is less than the others respective attribute
            /////////////////////////////////////////////////
            bool operator<(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Checks if this video mode is "greater" than the other passed in one
            /// Checks bits per pixel followed by width followed by height, returns true if any
            /// one is greater than the others respective attribute
            /////////////////////////////////////////////////
            bool operator>(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Checks if this video mode is "less" than the other passed in one
            /// Checks bits per pixel followed by width followed by height, returns true if any
            /// one is less than the others respective attribute
            /////////////////////////////////////////////////
            bool operator<=(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Checks if this video mode is "less" than the other passed in one
            /// Checks bits per pixel followed by width followed by height, returns true if any
            /// one is less than the others respective attribute
            /////////////////////////////////////////////////
            bool operator>=(const VideoMode& other) const;

            /////////////////////////////////////////////////
            /// \brief Returns the current desktop mode in use
            /////////////////////////////////////////////////
            static VideoMode getDesktopMode();

            /////////////////////////////////////////////////
            /// \brief Returns vector containing the currently supported video modes
            /// Based on your moniter, graphics card, os, etc
            /////////////////////////////////////////////////
            static std::vector<VideoMode> getFullscreenModes();

            ///< The size of the video mode in pixels, x is the width and y the height
            lie::math::Vector2i size;

            int bitsPerPixel; ///< Bits used per pixel to store color data
        };

    }
}



#endif // LIE_RENDER_VIDEOMODE_HPP
