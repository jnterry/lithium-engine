#ifndef LIE_RENDER_WINDOW_HPP
#define LIE_RENDER_WINDOW_HPP

#include <lie/render\RenderTarget.hpp>
#include <lie/render\RenderContext.hpp>
#include <lie/render\Monitor.hpp>
#include <lie/render\Event.hpp>
#include <lie/render\VideoMode.hpp>
#include <lie/core/NonCopyable.hpp>

#include <string>
#include <queue>

#include "RenderTarget.hpp"

namespace lie{
    namespace rend{

        //forward declerations
        class Device;
        namespace detail{
            class WindowImpl;
        }

        class Window : public RenderTarget, lie::core::NonCopyable{
            //allow window impl to call the pushEvent() function
            friend class lie::rend::detail::WindowImpl;
            friend class lie::rend::Device;
        public:
            /////////////////////////////////////////////////
            /// \brief Flags dictating the visual style of a window in a cross
            /// platform mannor
            /// Note: A window may have features you do not request due to OS limitations, eg on Windows
            /// a window that has a minimise button MUST also have a close button
            /// However, you will always atleast have features that you do request
            /////////////////////////////////////////////////
            enum Style{
                None = 0, //window that has not borders or buttons
                Borders = 1, //window will have thin boarders around it
                Titlebar = 2, //window will have a thicker title bar at the top, automatically sets Borders to true
                Resizeable = 4, //will be possble to resize the window if set, automatically sets Borders to true
                CloseButton = 8, //will have a close button if set, automatically sets Titlebar (and thus borders) to true
                MinimiseButton = 16, //window will have a minise button, automatically sets Titlebar (and thus borders) to true
                MaximiseButton = 32, //window will have a maximise button, automatically sets Titlebar (and thus borders) to true
                Fullscreen = 64, //window will be fullscreen, all other modifiers will be ignored if this is set

                //the default settings for a window, title bar and all options (close, minimise, maximise), resizeable, not fullscreen
                Default = Borders | Titlebar | CloseButton | MaximiseButton | MinimiseButton | Resizeable
            };


            //////////////////////////////////////////////////////////////////////////////////////////////
            //FUNCTIONS FROM RENDER TARGET
            //Windows expose the same interface as a render target (despite not being dervied from them)
            //These are the functions that are common to both a window and render target

            /////////////////////////////////////////////////
            /// \brief Makes this window the current RenderTarget for this thread
            /////////////////////////////////////////////////
            void makeCurrent();

            /////////////////////////////////////////////////
            /// \brief Displays what has been drawn since the last call to display()
            /// Internally swaps the buffers for the window.
            /////////////////////////////////////////////////
            void display();

            /////////////////////////////////////////////////
            /// \brief Returns the video mode of the windows client area
            /// ie: its width, height and bit depth
            ///
            /// \note This can be an expensive(ish) operation dependent on OS, render api etc
            /// If you use the video mode a lot in a frame you may wish to cache it
            /////////////////////////////////////////////////
            const VideoMode& getVideoMode() const;

            //////////////////////////////////////////////////////////////////////////////////////////////
            //FUNCTIONS FOR WINDOW
            //Window specific functions (ie: not from render target)

            /////////////////////////////////////////////////
            /// \brief Returns the caption of the window (ie: the name that appears on its title bar)
            /////////////////////////////////////////////////
            std::string getCaption();

            /////////////////////////////////////////////////
            /// \brief Sets the caption of the window to a new string
            /////////////////////////////////////////////////
            void setCaption(std::string cap);

            /////////////////////////////////////////////////
            /// \brief Returns the style flags of the window
            /////////////////////////////////////////////////
            int getStyle();

            /////////////////////////////////////////////////
            /// \brief Sets the style parameters of the window
            /////////////////////////////////////////////////
            void setStyle(int style);

            /////////////////////////////////////////////////
            /// \brief Creates the window with the set parameters, can be called while the window
            /// exists to recreate it with new style or caption
            /////////////////////////////////////////////////
            void create(VideoMode mode);

            /////////////////////////////////////////////////
            /// \brief Closes the window
            /////////////////////////////////////////////////
            void close();

            /////////////////////////////////////////////////
            /// \brief If the window is not minimised, minimises the window to the taskbar (or equivilent on other OSs)
            /// \see restore()
            /////////////////////////////////////////////////
            void minimise();

            /////////////////////////////////////////////////
            /// \brief If the window is currently minised, restores the window and makes it visible
            /// If the window is currently maximised, restores the window to normal size
            /// \see minimise()
            /// \see maximise()
            /////////////////////////////////////////////////
            void restore();

            /////////////////////////////////////////////////
            /// \brief Maximises the window
            /////////////////////////////////////////////////
            void maximise();

            /////////////////////////////////////////////////
            /// \brief Returns true if the window is currently minimised
            /////////////////////////////////////////////////
            bool isMinimised();

            /////////////////////////////////////////////////
            /// \brief Returns true if the window is currently maximised
            /////////////////////////////////////////////////
            bool isMaximised();

            /////////////////////////////////////////////////
            /// \brief Returns true if close() has not been called since the last call to create()
            /// and hence the window is still open.
            /////////////////////////////////////////////////
            bool isOpen();

            /////////////////////////////////////////////////
            /// \brief Sets the current position of the window relative to the selected monitor
            /// Defaults to being the primary monitor
            /////////////////////////////////////////////////
            void setPosition(int x, int y, Monitor monitor = lie::rend::Monitor::getPrimaryMonitor());

            /////////////////////////////////////////////////
            /// \brief Returns the x position of the window relative to the monitor it is on
            /////////////////////////////////////////////////
            int getXPosition();

            /////////////////////////////////////////////////
            /// \brief Returns the y position of  the window relative to the monitor it is on
            /////////////////////////////////////////////////
            int getYPosition();

            /////////////////////////////////////////////////
            /// \brief Returns the monitor that the window is on
            /////////////////////////////////////////////////
            Monitor getCurrentMonitor();

            /////////////////////////////////////////////////
            /// \brief Returns true if the window currently has focus, else returns false
            /////////////////////////////////////////////////
            bool hasFocus();

            /////////////////////////////////////////////////
            /// \brief Returns true if there is an unprocessed event in the event queue of this window
            /////////////////////////////////////////////////
            bool hasEvent();

            /////////////////////////////////////////////////
            /// \brief Fills in the passed in event structure with details of the first event in the windows queue
            /// Returns true if there is an event in the queue and hence the event object now has details about it
            /// Else returns false and leaves the event object unchanged.
            ///
            /// \param event Reference to the event structure which should have the details copyed into
            /// \return true if the window had an event and the details have been copied into the event structure, else fals
            ///
            /// Example Usage:
            /// \code
            /// lie::rend::Event e;
            /// while(window.pollEvent(e)){
            ///     //event processing code here
            ///     switch(e.type){
            ///     case lie::rend::Event::Close:
            ///         window.close();
            ///         break;
            ///     }
            /// }
            /// \endcode
            /// \see hasEvent()
            /// \see waitForEvent()
            /////////////////////////////////////////////////
            bool pollEvent(Event& event);

            /////////////////////////////////////////////////
            /// \brief If there is an event in the event queue copies event data into the "event" object and returns
            /// Else waits until there is an event, once there is fills in data and returns
            ///
            /// Usages of this function include a thread dedicated to event handling that wants to wait while there is
            /// no event to process or an editor program which only updates + renders when a input is recieved
            /////////////////////////////////////////////////
            void waitForEvent(Event& event);

            /////////////////////////////////////////////////
            /// \brief Destructor
            /// Automatically calls close
            /// Cleans up internal resources
            /////////////////////////////////////////////////
            ~Window();
        private:
            /////////////////////////////////////////////////
            /// \brief Creates a new window instance from a videomode
            /// Can optionally pass in a window caption which will appear on the window's title bar
            /// Can also pass in style parameters
            /// \note Windows must be created by a Device, hence this constructor is private
            /// \see lie::rend::Device::createWindow
            /////////////////////////////////////////////////
            Window(lie::rend::VideoMode videomode, std::string caption = "LIE Render Window", int style = Style::Default);


            /////////////////////////////////////////////////
            /// \brief Pops the top event off the event queue
            /////////////////////////////////////////////////
            void popEvent();

            /////////////////////////////////////////////////
            /// \brief Adds the new event to the end of the event queue
            /////////////////////////////////////////////////
            void pushEvent(Event e);

            /////////////////////////////////////////////////
            /// \brief Returns the next event in the window's event queue
            /// Does NOT pop the event from the queue, use "popEvent()"
            /////////////////////////////////////////////////
            Event getNextEvent();

        private:
            //the style flags for this window
            int mStyle;

            //the caption of the window, will be displayed on the top title bar (if it exists)
            std::string mCaption;

            //pointer to implementation, implementation varies between platforms and render api
            lie::rend::detail::WindowImpl* mImpl;

            //the event queue of this window
            //:TODO: Possible replace with a circle buffer?
            std::queue<lie::rend::Event> mEvents;
        };//end of class Window

    }//end of rend
}//end of lie
#endif // LIE_RENDER_WINDOW_HPP
