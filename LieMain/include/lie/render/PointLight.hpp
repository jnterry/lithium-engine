#ifndef LIE_RENDER_POINTLIGHT_HPP
#define LIE_RENDER_POINTLIGHT_HPP

#include "../math/Vector2.hpp"
#include "../math/Vector3.hpp"
#include "Color.hpp"

namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief Holds all data about a point light.
        /// A point light is a light which emits light equally
        /// in all directions from a single position
        /// \tparam T_POSITION the type of vector that will be used to store
        /// the position variable of the point light, this allows this class to
        /// be used by both 2d and 3d renderer. Must be either lie::math::Vector2<T>
        /// or lie::math::Vector3<T>
        /////////////////////////////////////////////////
        template <typename T_POSITION>
        struct PointLight{
			PointLight(T_POSITION position = T_POSITION::Origin,
                       Color newColor = Color::White,
                       lie::math::Vector3f newAttenuation = lie::math::Vector3f(1,0,0))
            : color(newColor), position(newPosition), attenuation(newAttenuation){
                //empty body
            }

            ///< The color of light emitted by this light source
            ///< The alpha component represents the light's intensity
            Color color;

            ///< The position in the world where the point light is located
			T_POSITION position;

            /////////////////////////////////////////////////
            /// \brief Vector3f storing the attenuation factors for the PointLight.
            ///
            /// Attenuation defines how the intensity of the light decreases
            /// over distance, the components of the vector3f represent the following:
            ///  - x: Quadratic Coefficient
            ///  - y: Linear Coefficient
            ///  - z: Constant factor
            /// The attenuation factor is given by:
            /// 1.0 / (x*dist*dist + y*dist + z)
            /// where dist is the distance between the object being illuminated and the light.
            /// The intensity of the light is multiplied by the attenuation factor to determine how
            /// brightly a point is lit by the light.
            ///
            /// The vector which represents realistic attenuation is (1,0,0)
            /////////////////////////////////////////////////
            lie::math::Vector3f attenuation;
        };

		///< A point light that has a 2d floating point vector storing its position
		typedef PointLight<math::Vector2f> PointLight2f;

		///< A point light that has a 3d floating point vector storing its position
		typedef PointLight<math::Vector3f> PointLight3f;

    }
}

#endif // LIE_RENDER_POINTLIGHT_HPP
