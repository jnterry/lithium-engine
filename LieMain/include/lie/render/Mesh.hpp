/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Mesh.hpp
/// Contains the Mesh class, defines how data in one or more GpuBuffer should be used to define geometry
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MESH_HPP
#define LIE_RENDER_MESH_HPP

#include "GpuBuffer.hpp"
#include "..\core\Types.hpp"
#include "..\core\Handle.hpp"

namespace lie{
    namespace rend{

        class Device;

        /////////////////////////////////////////////////
        /// \brief Holds infomation about how data in one or more GpuBuffer should be interpreted to form
        /// a static piece of geometry. This geometry can be either 2d or 3d.
        ///
        /// It stores an some number of arbitary attributes about the geometry, eg vertex position,
        /// vertex normals, vertex color, etc
        /// The maximum number of attributes is limited by hardware, the value can be querryed through the render
        /// system.
        /// In glsl shader code each attribute corrosponds to a "layout (location = XXX) in TYPE VAR_NAME"
        /// where XXX is the id of the attribute
        ///
        /// Meshes can be indexed or direct.
        /// In direct mode the data is stored in the Gpu buffers in order, if a vertex is in two faces it is stored multiple
        /// times in the gpu buffer.
        /// In indexed mode there is an additional index buffer which stores a list of indicies refering to the vertex buffer,
        /// if a vertex occurs in two faces it is stored once and only its index is repeated twice
        /// In general index mode should be prefered as it saves VRAM.
        /// Note however there is only one index buffer due to hardware contraints, hence all attributes of a vertex must be identical
        /// for it to be used twice, eg the same postion with different normals will need to have the position stored twice!
        /// see http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/ for more details.
        ///
        /// \note No runtime checking is done to ensure that you are using all the attributes in the mesh, if any attribute has not
        /// been set (eg: make mesh to hold 4 attributes, only set 3) crashes will occur!
        /////////////////////////////////////////////////
        class Mesh{
            friend class lie::rend::Device;
        public:
            ///< The type used to identify attributes by index/id
            typedef unsigned char AttributeId;

            /////////////////////////////////////////////////
            /// \brief Each vertex of a Mesh is made up of some number of attributes,
            /// for examples, a vertex position, vertex color, texture coordiante,
            /// vertex normal, etc
            /// This structure holds data that describes some arbitarry attribute,
            /// for example the type of data, number of components per vertex (eg, a Vector3<float>
            /// would have 3 components of type float)
            /////////////////////////////////////////////////
            struct AttributeData{
                /////////////////////////////////////////////////
                /// \brief Constructor taking values to set all the member varialbes to
                /////////////////////////////////////////////////
                AttributeData(lie::core::SharedHandle<GpuBuffer> newGpuBuffer, unsigned char newComponentCount, lie::core::PrimativeType newType,
                                    bool newNormalized, unsigned short newStride, unsigned int newBufferOffset);

                AttributeData();

                /////////////////////////////////////////////////
                /// \brief Sets all values of this AttributeData object to the passed in values.
                ///
                /// \param newGpuBuffer Pointer to the GpuBuffer holding the data
                ///
                /// \param newComponentCount The number of components making up each data point, eg a 3d vector will
                /// be made of 3 floats, a color with rgba has 4 components
                ///
                /// \param newType The type of data each component is
                ///
                /// \param newNormalized If true then fixed point data types will be normalized (converted into the range of -1 to 1 for
                /// signed types or 0 to 1 for unsigned types), if  false values should be used directly
                ///
                /// \param newStride The number of bytes between consecutive elements in the array
                /// Eg: if you have an array of just vertex position data and there is no gap between them the stride is 0,
                /// if the array contains a 12 byte position, 12 byte normal, 12 byte position etc then the stride is 12 bytes
                ///
                /// \param newBufferOffset The number of bytes into the GpuBuffer that the data starts
                /////////////////////////////////////////////////
                void set(lie::core::SharedHandle<GpuBuffer> newGpuBuffer, unsigned char newComponentCount, lie::core::PrimativeType newType,
                                    bool newNormalized, unsigned short newStride, unsigned int newBufferOffset);

                /////////////////////////////////////////////////
                /// \brief SharedHandle to the gpu buffer containing the described data
                /////////////////////////////////////////////////
                lie::core::SharedHandle<GpuBuffer> gpuBuffer;

                /////////////////////////////////////////////////
                /// \brief The number of components making up this vertex attribute
                /// Eg: Vec1 is made up of a single component where as Vec4 contains 4 components of the same type
                /////////////////////////////////////////////////
                unsigned char componentCount;

                /////////////////////////////////////////////////
                /// \brief The type of this attribute
                /////////////////////////////////////////////////
                lie::core::PrimativeType type;

                /////////////////////////////////////////////////
                /// \brief If true then signed integer types will be mapped to the range -1 to 1
                /// and unsigned integer types will be mapped to the range 0 to 1.
                /// If false integer types will not be mapped
                /////////////////////////////////////////////////
                bool normalized;

                /////////////////////////////////////////////////
                /// \brief The number of bytes between the start of one of these to the start of the next
                /// eg: <br>
                ///   0   1    2    3      4     5    6    7    8  <br>
                /// PosX PosY PosZ NormX NormY NormZ PosX PosY PosZ<br>
                /// Assuming each component (x,y,z) is stored in 1 byte then the stride is 6
                /// as PosX of vertex 1 is stored at index 0 and PosX of vertex 2 is at index 6, hence the difference
                /// in bytes is 6
                /////////////////////////////////////////////////
                unsigned short stride;

                /////////////////////////////////////////////////
                /// \brief The number of bytes into the buffer that the data starts at
                /////////////////////////////////////////////////
                unsigned int bufferOffset;
            };

            /////////////////////////////////////////////////
            /// \brief Sets the number of verticies in this mesh object
            /////////////////////////////////////////////////
            void setVertexCount(unsigned int vcount);

            /////////////////////////////////////////////////
            /// \brief Returns the number of verticies in this mesh object
            /////////////////////////////////////////////////
            unsigned int getVertexCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of attributes this mesh contains
            /////////////////////////////////////////////////
            unsigned char getAttributeCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the AttributeData struct for the specified
            /// attribute id
            /////////////////////////////////////////////////
            const AttributeData& getAttribute(AttributeId attributeId) const;

            /////////////////////////////////////////////////
            /// \brief Returns a reference to the AttributeData struct for the specified
            /// attribute id
            /////////////////////////////////////////////////
            AttributeData& getAttribute(AttributeId attributeId);

            /////////////////////////////////////////////////
            /// \brief Destructor, cleans up internal data
            /////////////////////////////////////////////////
            ~Mesh();

            /////////////////////////////////////////////////
            /// \brief Returns true if this mesh is using index mode (ie, another GpuBuffer is)
            /// used which stores data about
            /////////////////////////////////////////////////
            bool isIndexed() const;

            /////////////////////////////////////////////////
            /// \brief Sets the GpuBuffer containing the index data for this mesh and the byte offset for the buffer
            /// \param GpuBuffer* buf -> pointer to the GpuBuffer containg the data. If nullptr is passed
            /// then we assume the mesh is using direct mode
            /// \param byteOffset -> the number of bytes into the GpuBuffer the index array starts
            /// \return void
            /////////////////////////////////////////////////
            void setIndexBufferParams(lie::core::SharedHandle<GpuBuffer> buf, unsigned int byteOffset);

            /////////////////////////////////////////////////
            /// \brief Sets which gpu buffer index data is stored in
            /// \param GpuBuffer* buf -> pointer to the GpuBuffer containg the data. If nullptr is passed
            /// then we assume the mesh is using direct mode
            /// \return void
            /////////////////////////////////////////////////
            void setIndexBuffer(lie::core::SharedHandle<GpuBuffer> buf);

            /////////////////////////////////////////////////
            /// \brief Sets how many bytes into the index buffer the index array starts
            /////////////////////////////////////////////////
            void setIndexBufferByteOffset(unsigned int byteOffset);

            /////////////////////////////////////////////////
            /// \brief Returns a pointer to the gpu buffer holding the index data for this mesh
            /// Will return nullptr if the mesh is being used in direct mode
            /////////////////////////////////////////////////
            const lie::core::SharedHandle<GpuBuffer> getIndexBuffer() const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of bytes into the index gpu buffer that the
            /// index array starts
            /////////////////////////////////////////////////
            unsigned int getIndexBufferOffset() const;
        private:
            ///< private constructor, this class should be constructed by a Device
            Mesh(unsigned char attributeCount);

            ///< The number of attributes this mesh contains
            unsigned char mAttributeCount;

            ///< Pointer to first element of AttributeData array owned by this mesh, see mAttributeCount
            /// to find the arrays size
            AttributeData* mAttributes;

            ///< The number of verticies in the mesh
            unsigned int mVertexCount;

            ///< Pointer to the GPU buffer holding the indicies for which vertecies to link up into faces
            /// Every 3 items in this will be interpreted as another triangle
            /// If it is null we assume the mesh is in direct mode and every 3 items in the attribute buffers
            /// make up a face
            lie::core::SharedHandle<GpuBuffer> mIndexBuffer;

            ///< The number of bytes into the mIndexBuffer that the index array starts
            /// Ignored if mIndexBuffer is nullptr
            unsigned int mIndexBufferOffset;
        };


    }
}

#endif // LIE_RENDER_MESH_HPP
