#ifndef LIE_RENDER_MESHDATABUFFER_HPP
#define LIE_RENDER_MESHDATABUFFER_HPP

#include "../core/Convert.hpp"
#include "../core/ByteBuffer.hpp"

#include "Mesh.hpp"
#include "Device.hpp"
#include "MeshVertexLayout.hpp"


namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief A collection of buffers stored in system RAM that store all the data that is
        /// needed to create an instance of Mesh. The data is stored in system RAM, once
        /// a Mesh instance is created the data is uploaded to video RAM. While the data is
        /// in a MeshDataBuffer various calculations can be performed on it, eg calculating normals.
        ///
        /// These function are NOT members of the class as they can just as easily be written
        /// as functions using the public interface, and this means new functions can be added
        /// which have the same calling syntax, eg:
        /// \code
        /// myMeshData.calculateNormals(); //lie function
        /// doSomethingCool(myMeshData); //project function
        /// \endcode
        /// vs
        /// \code
        /// lie::rend::mesh::calculateNormals(myMeshData); //lie rend libary
        /// lie::ext::mesh::simplifyMesh(myMeshData); //lie extension libary
        /// proj::doSomethingCool(myMeshData); //project function
        /// \endcode
        ///
        /// The engine allows for a flexible vertex format using the MeshVertexLayout class.
        /////////////////////////////////////////////////
        class MeshDataBuffer{
        public:
            typedef unsigned int IndexType;

            /////////////////////////////////////////////////
            /// \brief Struct containing references to the 3 verticies of type VERT_T (tparam)
            /// that make up a single triangular face.
            /// Can be retrieved from the MeshDataBuffer allowing you to access the vertices for a single face.
            /// You can modify the data in the MeshDataBuffer by:
            /// \code
            /// face.v0() = myVertexInstace;
            /// face.v0().position = Vector3f(1,2,3);
            /// \endcode
            ///
            /// The design of this class is not as good as I would like...
            /// Originally it had 3 public references allowing the syntax
            /// \code
            /// face.v0 = ...
            /// \endcode
            /// However, an iterator must support -> and the -> operator must
            /// return a pointer. As the FaceIterator generates instances of this
            /// class as it is dereferenced, for it to return a pointer it must
            /// store an instance as a member variable (cant return ptr to local varialbe).
            /// If this class contained references then the references could not be changed
            /// to point at a new variable, hence the FaceIterator would need to contain a pointer
            /// to a Face and dynamically new and delete them in order to change the refered
            /// varialbe when it moves to a new Face.
            /// Instead this internally stores pointers as they can be changed to point at a new Vertex,
            /// but these are not public as client code should NOT be able to change what they point to, eg:
            /// /code
            /// face.v0 = new Vertex(1,2,3); //this modifies the face, not the MeshDataBuffer, this is not what  we want
            /// *face.v0 = Vertex(1,2,3) //this would modify the MeshDataBuffer, but isnt the nicest or most obvious syntax
            /// /endcode
            /// Instead you get references with the v0(), v1() and v2() functions
            ///
            /// \tparam The type of vertex for this MeshDataBuffer
            ///
            /// \note The tparam can be anything, if it is not a struct compatible with the
            /// layout specified by the MeshVertexLayout for this MeshDataBuffer then
            /// modifing the data through a face object could cause strange things...
            /// You wont get a crash as the memory is allocated.
            /////////////////////////////////////////////////
            template<typename VERT_T>
            struct Face{
            public:
                Face(VERT_T* vert0, VERT_T* vert1, VERT_T* vert2) : v0(vert0), v1(vert1), v2(vert2){
                    //empty body
                }

                Face(const Face<VERT_T>& other) : v0(other.v0), v1(other.v1), v2(other.v2){
                    //empty body
                }

                ///< Pointer to the data for the first vertex of the Face
                VERT_T* v0;

                ///< Pointer to the data for the second vertex of the Face
                VERT_T* v1;

                ///< Pointer to the data for the thrid vertex of the Face
                VERT_T* v2;
            };

            /////////////////////////////////////////////////
            /// \brief Iterator which can go through the list of all verticies
            /// stored in the MeshDataBuffer
            ///
            /// \tparam VERT_T The compile time type used to represent each vertex, type MUST
            /// match what is specified by the MeshVertexLayout for this MeshDataBuffer
            /////////////////////////////////////////////////
            template<typename VERT_T>
            class VertexIterator{

            };

            /////////////////////////////////////////////////
            /// \brief Iterates over the faces stored in the MeshDataBuffer
            ///
            /// If there is no index buffer takes each 3 consecutive verticies
            /// and assumes they form a triange (ie: vert 0,1,2 for triangle0, vert 3,4,5 for triange1, etc)
            ///
            /// If there is an index buffer then each 3 consecutive entries in the index
            /// buffer will form faces
            /////////////////////////////////////////////////
            template<typename VERT_T>
            class FaceIterator{
            public:
                friend class MeshDataBuffer;

                /////////////////////////////////////////////////
                /// \brief Advanced the iterator to the next face
                /////////////////////////////////////////////////
                FaceIterator<VERT_T>& operator++(){
                    ++mFaceIndex;
                    return *this;
                }

                /////////////////////////////////////////////////
                /// \brief Advanced the iterator to the next face
                /////////////////////////////////////////////////
                FaceIterator<VERT_T> operator++(int dummyParamForPostfix){
                    FaceIterator<VERT_T> iter = *this;
                    ++mFaceIndex;
                    return iter;
                }

                /////////////////////////////////////////////////
                /// \brief Moves the iterator back to the previous face
                /////////////////////////////////////////////////
                FaceIterator<VERT_T>& operator--(){
                    --mFaceIndex;
                    return *this;
                }

                /////////////////////////////////////////////////
                /// \brief Moves the iterator back to the previous face
                /////////////////////////////////////////////////
                FaceIterator<VERT_T> operator--(int dummyParamForPostfix){
                    FaceIterator<VERT_T> iter = *this;
                    --mFaceIndex;
                    return iter;
                }

                /////////////////////////////////////////////////
                /// \brief Returns true if two FaceIterator instances are equal.
                /// Ie: They refer to the same MeshDataBuffer and the same face in it
                /////////////////////////////////////////////////
                bool operator==(const FaceIterator<VERT_T>& other) const{
                    return ((&this->mMDB == &other.mMDB) &&
                            (this->mFaceIndex == other.mFaceIndex));
                }

                /////////////////////////////////////////////////
                /// \brief Returns true if two iterators are not equal
                /// Ie: they either refer to different arrays or to different
                /// indexes of the array
                /////////////////////////////////////////////////
                bool operator!=(const FaceIterator<VERT_T>& other) const{
                    return !(this->operator==(other));
                }

                /////////////////////////////////////////////////
                /// \brief Returns an instance of Face which has references to the vertecies
                /// for the current face
                /////////////////////////////////////////////////
                Face<VERT_T>* operator->(){
                    //this function HAS to return a pointer or we get a compile error, but we dont want to have to
                    //delete it if we create a new face object, use an internal one

                    //update internal face
                    this->mCurrentFace = this->mMDB.getFace<VERT_T>(this->mFaceIndex);
                    return &this->mCurrentFace; //return pointer to it
                }

                /////////////////////////////////////////////////
                /// \brief Returns an instance of Face which has references to the vertecies
                /// for the current face
                /////////////////////////////////////////////////
                Face<VERT_T> operator*(){
                    return mMDB.getFace<VERT_T>(mFaceIndex);
                }

                /////////////////////////////////////////////////
                /// \brief Allows random access to elements by offset, NOT by absolute indexes.
                /// Returns an instance of Face which has references to the vertices for the face
                /// at index (currentFaceIndex + offset)
                /////////////////////////////////////////////////
                Face<VERT_T> operator[](int offset){
                    return mMDB.getFace<VERT_T>(mFaceIndex + offset);
                }

                /////////////////////////////////////////////////
                /// \brief Returns the current face index of the iterator
                /////////////////////////////////////////////////
                size_t getFaceIndex(int offset = 0){
                    return mFaceIndex;
                }

            private:
                /////////////////////////////////////////////////
                /// \brief Constructs a new iterator from a reference to the
                /// buffer that should be read from and the face index at which to start
                /////////////////////////////////////////////////
                FaceIterator(MeshDataBuffer& mdb, size_t mFaceIndex) : mMDB(mdb), mFaceIndex(mFaceIndex), mCurrentFace(mdb.getFace<VERT_T>(0)){
                    //empty body
                }
                MeshDataBuffer& mMDB;
                unsigned int mFaceIndex;
                Face<VERT_T> mCurrentFace;
            };

            /////////////////////////////////////////////////
            /// \brief Creates a new MeshDataBuffer for the specified MeshVertexLayout
            ///
            /// \note This can not be edited once the MeshDataBuffer has been constructed
            /////////////////////////////////////////////////
            MeshDataBuffer(const MeshVertexLayout& vertexDesc);

            /////////////////////////////////////////////////
            /// \brief Cleans up data stored by the MeshDataBuffer, buildMesh() will have
            /// transfered the data to video ram
            /////////////////////////////////////////////////
            ~MeshDataBuffer();

            /////////////////////////////////////////////////
            /// \brief Returns the MeshVertexLayout that descibes the vertices held by
            /// this MeshDataBuffer
            /////////////////////////////////////////////////
            const MeshVertexLayout& getVertexLayout() const;

            /////////////////////////////////////////////////
            /// \brief Appends the specifie
            ///
            /// \param vertexData Pointer to an array holding the data to be appended
            /// \param vertexCount The number of verticies to read from the specified array,
            /// obviously if a number higher that the size of the array is specified
            /// the MeshDataBuffer will read garbage data or get a segfault!
            ///
            /// \note The type of the vertex must be that which is specified by the MeshVertexLayout
            /// However, this does not mean it need be an actual type.
            /// Eg, a MeshVertexLayout specifing a structure of:
            /// \code
            /// attrib0: 3 component float (position)
            /// attrib1: 3 component float (normal)
            /// attrib2: 2 component float (uvCoord)
            /// \endcode
            /// Will accept an array of the struct:
            /// \code
            /// struct MeshVertex{
            ///     Vector3f position;
            ///     Vector3f normal;
            ///     Vector2f uvCoord
            /// }
            /// \endcode
            /// And an array of raw floats, eg:
            /// \code
            ///     //positions             normals          uvCoords
            /// {   0.0f, 0.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 0.0f,  //vertex0
            ///     0.0f, 0.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 0.0f,  //vertex1
            ///     0.0f, 0.0f, 0.0f,   0.0f, 0.0f, 0.0f,   0.0f, 0.0f,} //vertex2
            /// \endcode
            /// All it wants is some continuous buffer of data, it doesnt care if the data is
            /// part of a type or not
            /////////////////////////////////////////////////
            void appendVerticies(const void* vertexData, size_t vertexCount);

            /////////////////////////////////////////////////
            /// \brief Sets the data for a single attribute in the MeshDataBuffer.
            /// Eg, for a vertex struct of:
            /// \code
            /// struct Vertex{
            ///     float position[3];
            ///     float normal[3];
            ///     float texCoordp[2];
            /// }
            /// \endcode
            /// You can set all the position data by setting attrib 0.
            /// If you do this then you need to pass in an array of floats, the first 3 are used to
            /// set the position attribute of vertex 0, the next 3 for vertex 1, etc.
            /// This is effectivly turning an array of positions into an array of positions with
            /// gaps for the other attributes. The other attributes will not be modified.
            ///
            /// \param attribId Which attribute to modify
            /// \param data Pointer to array holding the new data, the type of data must be that
            /// specified by the MeshVertexLayout for the specified attribute id
            /// \param vertexCount The number of verticies you wish to read from the data array
            /// \param startVertex The starting vertex of the MeshDataBuffer you wish to modify
            ///
            /// \note This function will automatically call setVertexCount if it needs more room to
            /// store the data
            /////////////////////////////////////////////////
            void setAttribData(unsigned char attribId, const void* data, size_t vertexCount, size_t startVertex = 0);


            /////////////////////////////////////////////////
            /// \brief Resets all data for the specifed attribute to 0
            /////////////////////////////////////////////////
            void clearAttribData(unsigned char attribId);

            /////////////////////////////////////////////////
            /// \brief Clears ALL vertex data from the MeshDataBuffer
            /// This has no effect on any Mesh instances that have already
            /// been built from the data as their data is stored in video ram
            /////////////////////////////////////////////////
            void clearVerticies();

            /////////////////////////////////////////////////
            /// \brief Sets the number of verticies stored by this MeshDataBuffer
            ///
            /// If the number is larger than before then adds the required number of
            /// new verticies on the end, initilising all values to 0
            ///
            /// If the number is smaller than the previous number then the last n vertices
            /// are deleted, their data is lost
            /////////////////////////////////////////////////
            void setVertexCount(size_t vertexCount);

            /////////////////////////////////////////////////
            /// \brief Returns the number of verticies stored by the MeshDataBuffer
            /// \note This is NOT nessacerally equal to the number of verticies the created
            /// Mesh will have, the value returned by this function is the number of verticies
            /// actually stored in this MeshDataBuffer, the created Mesh instance's vertex count
            /// is equal to the number of vertices needing to be drawn. If this MeshDataBuffer has an
            /// index buffer then the number of verticies to draw will not (always) equal the number
            /// verticies stored.
            /// To get the amount of vertices that will be drawn call getDrawnVertexCount()
            /////////////////////////////////////////////////
            size_t getVertexCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of faces stored by the MeshDataBuffer
            /// If there is no index buffer returns the number of verticies/3
            /// as each triple form a triangle, if there is an index buffer returns
            /// indexCount/3
            /////////////////////////////////////////////////
            size_t getFaceCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns true if the MeshDataBuffer has an index buffer which
            /// will be used to decide which verticies form faces
            /////////////////////////////////////////////////
            bool hasIndexBuffer() const;

            /////////////////////////////////////////////////
            /// \brief Returns true if the index buffer is valid, this
            /// is when:
            ///  - All values in the buffer staisfy 0 <= value < getVertexCount()
            /////////////////////////////////////////////////
            bool validateIndexBuffer() const;

            /////////////////////////////////////////////////
            /// \brief Removes all data from the internal index buffer
            /////////////////////////////////////////////////
            void clearIndexBuffer();

            /////////////////////////////////////////////////
            /// \brief Appends the specified array of IndexType
            /// to the index buffer
            /// \param data Pointer to array of IndexType holding the index data
            /// \param intCount The number of IndexType to append from the array
            /////////////////////////////////////////////////
            void appendIndicies(IndexType* data, size_t indexCount);

            /////////////////////////////////////////////////
            /// \brief Returns the number of indicies in the index buffer of this
            /// MeshDatBuffer object
            /////////////////////////////////////////////////
            size_t getIndexCount() const;

            ////////////////////////////////////////////////
            /// \brief Builds and returns a Mesh object, uses the specified Device
            /// to creaLIE_RENDER_MESHDATABUFFER_HPPte the mesh
            /// \return Mesh* Pointer to the created mesh, or nullptr if an error occured meaning
            /// the mesh could not be created
            /////////////////////////////////////////////////
            Mesh* buildMesh(Device& rsys) const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of vertices a built Mesh instance would have to
            /// draw.
            /// If this MeshDataBuffer has an index buffer returns the number of indicies in it,
            /// else returns the number of vertices stored by the MeshDataBuffer
            /////////////////////////////////////////////////
            size_t getDrawnVertexCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns a instance of Face that contains references to
            /// the 3 verticies in the face with the specified index
            /////////////////////////////////////////////////
            template<typename T>
            Face<T> getFace(unsigned int faceindex){
                if(this->hasIndexBuffer()){
                    //then need to obey it to find vertices
                    size_t vertSize = this->mVertexLayout.getSize();
                    size_t firstIndexIndex = faceindex*3;
                    return Face<T>( (T*)(&this->mData[vertSize * this->mIndexBuffer[firstIndexIndex]]),
                                    (T*)(&this->mData[vertSize * this->mIndexBuffer[firstIndexIndex+1]]),
                                    (T*)(&this->mData[vertSize * this->mIndexBuffer[firstIndexIndex+2]]));
                } else {
                    size_t vertSize = this->mVertexLayout.getSize();

                    //why this code?
                    //f = face index
                    //v = vertex index
                    //s = vertex size
                    //assume vertex size = 1 bytes
                    //f     v     byte
                    //0     0       0
                    //0     1       1
                    //0     2       2
                    //1     0       3
                    //1     1       4
                    //1     2       5
                    //2     0       6
                    //2     1       7
                    //2     2       8
                    //general formula is (faceIndex*3 + vertexIndex) * vertexSize
                    return Face<T>( (T*)(&this->mData[(faceindex*3)*(vertSize)]),
                                    (T*)(&this->mData[(faceindex*3 +1)*(vertSize)]),
                                    (T*)(&this->mData[(faceindex*3 +2)*(vertSize)]));
                }
            }

            /////////////////////////////////////////////////
            /// \brief Returns pointer to the requested vertex as the
            /// type requested
            /// \note The pointer returned is guarrentied to lie within the
            /// the data belonging to this MeshDataBuffer (assuming index < getVertexCount()),
            /// however, if the you specify is larger than the actual vertex format then part
            /// of the returned type could lie outside the data buffer or be a part of another
            /// vertex.
            /////////////////////////////////////////////////
            template <typename T>
            T* getVertex(unsigned int index){
                return (T*)(&this->mData[index*this->getVertexLayout().getSize()]);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a FaceIterator to the first Face in this
            /// MeshDataBuffer
            /////////////////////////////////////////////////
            template<typename T>
            FaceIterator<T> beginFace(){
                return FaceIterator<T>(*this, 0);
            }

            /////////////////////////////////////////////////
            /// \brief Returns a FaceIterator to the Face past the last
            /// face of this MeshDataBuffer, dereferncing it results in
            /// undefined behaviour
            /////////////////////////////////////////////////
            template<typename T>
            FaceIterator<T> endFace(){
                return FaceIterator<T>(*this, this->getFaceCount());
            }
        private:
            const MeshVertexLayout mVertexLayout;

            lie::core::ByteBuffer mData;

            std::vector<IndexType> mIndexBuffer;
        };

    }
}

#endif // LIE_RENDER_MESHDATABUFFER_HPP
