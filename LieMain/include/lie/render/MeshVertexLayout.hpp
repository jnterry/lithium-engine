#ifndef LIE_RENDER_MESHVERTEXLAYOUT_HPP
#define LIE_RENDER_MESHVERTEXLAYOUT_HPP

#include "..\core\Types.hpp"
#include <initializer_list>
#include <vector>

namespace lie{
    namespace rend{
        /////////////////////////////////////////////////
        /// \brief This class descibes the layout of each vertex in a mesh
        ///
        /// A single vertex will have some number of attributes, eg, position,
        /// normal, color, texture coordinate, etc
        ///
        /// Each attribute has a certain type, eg, Vector3f is likely the format for a postion,
        /// where as Vector2f will probably be used for a texture coordinate. The type is stored
        /// in the MeshVertexLayout::Attribute struct.
        ///
        /// \note Rather than using this class a MetaType could have been used, however this
        /// would be more difficult to check the validity of as a MetaType can descibe any format
        /// where as a mesh vertex has alot of restrictions.
        /// :TODO: Allow making MeshVertexLayout from a MetaType? -> this class would act as a "compiled"
        /// version that is quick and easy to deal with for the code.
        /////////////////////////////////////////////////
        class MeshVertexLayout{
        public:
            /////////////////////////////////////////////////
            /// \brief Struct that can be used to represent an n dimensional primative type vector.
            /// Eg, a lie::math::Vector3<float> has the type lie::core::PrimativeType::Float and
            /// a component count of 3
            /////////////////////////////////////////////////
            struct Attribute{
                ///The type of primative stored, eg, float, int, etc
                lie::core::PrimativeType type;

                ///The number of components for a single instance of this attribute
                ///eg, a Vector3<float> would have 3 components of type float
                unsigned char componentCount;

                /////////////////////////////////////////////////
                /// \brief Creates a new instance with specified type and component count
                /////////////////////////////////////////////////
                Attribute(lie::core::PrimativeType newType, unsigned char newComponentCount);

                /////////////////////////////////////////////////
                /// \brief Returns the number of bytes needed to store an instance of this attribute
                /// defined as sizeof(type)*componentCount
                /////////////////////////////////////////////////
                size_t getSize() const;
            };

            /////////////////////////////////////////////////
            /// \brief Creates a new empty MeshVertexLayout
            /////////////////////////////////////////////////
            MeshVertexLayout();

            /////////////////////////////////////////////////
            /// \brief Creates a new MeshVertexLayout with the specified
            /// attributes
            /////////////////////////////////////////////////
            MeshVertexLayout(std::initializer_list<Attribute> attributes);

            /////////////////////////////////////////////////
            /// \brief Adds a new attribute to the MeshVertexLayout
            /////////////////////////////////////////////////
            void addAttrib(const Attribute& attribute);

            /////////////////////////////////////////////////
            /// \brief Returns the number of bytes that would be needed to store
            /// all the data for a single vertex
            /////////////////////////////////////////////////
            size_t getSize() const;

            /////////////////////////////////////////////////
            /// \brief Returns the number of attributes that the descibed vertex
            /// has
            /////////////////////////////////////////////////
            size_t getAttribCount() const;

            /////////////////////////////////////////////////
            /// \brief Returns reference to the attribute at the specified index in the
            /// MeshVertexLayout
            /////////////////////////////////////////////////
            Attribute& getAttrib(size_t index);

            /////////////////////////////////////////////////
            /// \brief Returns reference to the attribute at the specified index in the
            /// MeshVertexLayout
            /////////////////////////////////////////////////
            const Attribute& getAttrib(size_t index) const;

            /////////////////////////////////////////////////
            /// \brief Retunrns the how many bytes into the vertex the given attribute starts, eg
            /// \code
            /// struct Vertex{
            ///     float position[3]; //offset 0, at the start of the vertex
            ///     float normal[3]; //offset sizeof(float)*3
            ///     float texCoord[2]; //offset sizeof(float)*6
            ///
            /// };
            /////////////////////////////////////////////////
            size_t getAttribOffset(size_t index) const;

        private:
            std::vector<Attribute> mAttribs;
        };


    }
}

#endif // LIE_RENDER_MESHVERTEXLAYOUT_HPP
