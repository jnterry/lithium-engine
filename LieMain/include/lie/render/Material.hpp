/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Material.hpp
/// Contains the lie::rend::Material class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MATERIAL_HPP
#define LIE_RENDER_MATERIAL_HPP

#include <string>
#include "../math/Matrix4x4.hpp"
#include "../math/Matrix3x3.hpp"
#include "../math/Vector2.hpp"
#include "../math/Vector3.hpp"
#include "../math/Vector4.hpp"
#include "../core/ByteBuffer.hpp"

namespace lie{
	namespace rend{

		//forward deceleration
		class MaterialType;
		class Texture;

		//////////////////////////////////////////////////////////////////////////
		/// \brief The Material class stores a list of textures and parameter values that
		/// will be used by a Methodology to render geometry. A Material is specific to
		/// a certain Methodology
		//////////////////////////////////////////////////////////////////////////
		class Material{
			friend class MaterialType;
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Deletes this Material and all associated data
			//////////////////////////////////////////////////////////////////////////
			~Material();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the MaterialType instance that created this Material
			//////////////////////////////////////////////////////////////////////////
			const MaterialType& getMaterialType() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the texture to use for the specified index in this Material
			//////////////////////////////////////////////////////////////////////////
			void setTexture(int index, Texture* tex);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the texture at the specified index in this Material
			//////////////////////////////////////////////////////////////////////////
			Texture* getTexture(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Mat4x4f at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param value The new value for the attribute
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, const lie::math::Matrix4x4f& value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Mat3x3f at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, const lie::math::Matrix3x3f& value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Vec4f at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, const lie::math::Vector4f& value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Vec3f at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, const lie::math::Vector3f& value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Vec2f at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, const lie::math::Vector2f& value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Float at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \return Reference to this for operator chaining
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, float value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the value of an attribute of type ShaderParameterType::Int32 at the
			/// specified index
			/// \param index The index of the attribute to modify, use .getMaterialType().getAttributeIndex()
			/// \param defaultValue The default value for any created Material instances
			/// \note Bounds checking is not performed, if index is too large undefined behavior will occur!
			//////////////////////////////////////////////////////////////////////////
			void setAttribute(int index, int value);

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets the attribute with the specified name to the specified value.
			/// \note This function is equivalent to:
			/// \code
			/// int i = mat.getMaterialType()->getAttributeIndex(name);
			/// mat.setAttribute(i, value);
			/// \endcode
			/// \return True if an attribute with the specified name exists, else false
			//////////////////////////////////////////////////////////////////////////
			template<typename T>
			bool setAttribute(std::string name, T value){
				int attribIndex = this->mMatType.getAttributeIndex(name);
				if (attribIndex != -1){
					this->setAttribute(attribIndex, value);
					return true;
				} else {
					return false;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Mat4x4f
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// Matrix4x4f or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			const lie::math::Matrix4x4f& getAttributeMat4x4f(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Mat4x4f
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// Matrix3x3f or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			const lie::math::Matrix3x3f& getAttributeMat3x3f(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Mat4x4f
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// Vector3f or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			const lie::math::Vector4f& getAttributeVec4f(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Mat4x4f
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// Vector3f or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			const lie::math::Vector3f& getAttributeVec3f(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Mat4x4f
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// Vector2f or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			const lie::math::Vector2f& getAttributeVec2f(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Float
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// float or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			float getAttributeFloat(int index) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the value of an attribute of the type ShaderParameterType::Int32
			/// at the specified index
			/// \param index The index of the attribute to retrieve
			/// \note Bounds and type checking is not performed, if the attribute is not a 
			/// int or the index is too high undefined behavior occurs!
			//////////////////////////////////////////////////////////////////////////
			int getAttributeInt32(int index) const;

		private:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Private constructor, use MaterialType.createMaterial()
			/// \param matType The MaterialType creating this Material
			/// \param attribByteSize the total size in bytes of all the attribute data
			/// required for this material
			//////////////////////////////////////////////////////////////////////////
			Material(const MaterialType& matType);
			
			///< The MaterialType that created this Material
			const MaterialType& mMatType;

			///< The data for the material parameters, size and contents depends on the
			///< Methodology that created this material
			lie::core::ByteBuffer mParamData;

			///< Pointer to array of Texture pointers to use while using this material
			///< length of array is given by the mMatType.getTextureCount()
			Texture** mTextures;
		};
	}
}

#endif //LIE_RENDER_MATERIAL_HPP