#ifndef LIE_RENDER_RENDERER3DIMMEDIATE_HPP
#define LIE_RENDER_RENDERER3DIMMEDIATE_HPP

#include "Renderer3d.hpp"
#include "../core/Timer.hpp"

namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief This is the most basic renderer, it simply draws
        /// as and when it received draw calls.
        /////////////////////////////////////////////////
        class Renderer3dImmediate : public Renderer3d{
        public:

            /////////////////////////////////////////////////
            /// \brief Calls render api specific functions to prepare the RenderTarget
            /// to be drawn to
            /////////////////////////////////////////////////
            void onBegin(RenderTarget& target) override;

            /////////////////////////////////////////////////
            /// \brief Allows you to change whether fog is enabled for this renderer
            /// \param enabled Whether fog should be enabled for this renderer or not
            /// \see isFogEnabled
            /// \see setFogColor
            /////////////////////////////////////////////////
            void enableFog(bool enabled);

            /////////////////////////////////////////////////
            /// \brief Returns whether fog is currently enabled for this renderer
            /// \see enableFog
            /// \see setFogColor
            /////////////////////////////////////////////////
            bool isFogEnabled();

            /////////////////////////////////////////////////
            /// \brief Sets the fog color for this renderer,
            /// alpha channel of color used for fog intensity.
            /// \see enableFog
            /// \see getFogColor
            /////////////////////////////////////////////////
            void setFogColor(lie::rend::Color color);

            /////////////////////////////////////////////////
            /// \brief Returns the current color of the fog this renderer uses.
            /// \note There can be a color set even if fog is disabled, however it makes no difference
            /// to the scene if fog is disabled.
            /// \see setFogColor
            /// \see isFogEnabled
            /////////////////////////////////////////////////
            lie::rend::Color getFogColor();

            /////////////////////////////////////////////////
            /// \brief Sets the ambient light for this renderer,
            /// colors alpha channel is used as ambient light intensity
            /// \see getAmbientLightColor
            /////////////////////////////////////////////////
            void setAmbientLightColor(lie::rend::Color color);

            /////////////////////////////////////////////////
            /// \brief Returns the ambient light color in use by this renderer
            /// see setAmbientLightColor
            /////////////////////////////////////////////////
            lie::rend::Color getAmbientLightColor();

            /////////////////////////////////////////////////
            /// \brief Draws the specified mesh
            /////////////////////////////////////////////////
			void draw(const lie::rend::Methodology& meth, const lie::rend::Material& mat, const lie::math::Matrix4x4f& modelMatrix, const lie::rend::Mesh& mesh);
        private:
            lie::rend::Color mAmbientLightColor;
            lie::rend::Color mFogColor;
            bool mIsFogEnabled;
        };

    }
}

#endif // LIE_RENDER_RENDERER3DIMMEDIATE_HPP
