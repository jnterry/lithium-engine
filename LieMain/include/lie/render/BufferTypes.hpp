/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file BufferTypes.hpp
/// Contains the BufferTypes enum class, eg: Color, Depth, etc
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_BUFFERTYPES_HPP
#define LIE_RENDER_BUFFERTYPES_HPP
namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief Enum class of the different types of buffer for RenderTargets, windows, etc
        /// Act as bit flags
        /////////////////////////////////////////////////
        struct BufferTypes{
            enum vals{
                Color = 1,
                Depth = 2,
                Stencil = 4,
                All = Color | Depth | Stencil,
            };
        private:
            BufferTypes(){}
        };


    }
}

#endif // LIE_RENDER_BUFFERTYPES_HPP
