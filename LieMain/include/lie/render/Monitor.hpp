/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Monitor.hpp
/// Contains the Monitor class, allows you to querry the number, size etc of monitors connected
///
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_MONITOR_HPP
#define LIE_RENDER_MONITOR_HPP

#include <string>
#include <vector>
#include <lie/render\VideoMode.hpp>

namespace lie{
    namespace rend{

        namespace detail{
            //forward decleration
            class MonitorImpl;
        }

        class Monitor{
        public:
            /////////////////////////////////////////////////
            /// \brief Returns a vector containing all the monitors currently connected
            /////////////////////////////////////////////////
            static std::vector<Monitor> getAllMonitors();

            /////////////////////////////////////////////////
            /// \brief Retunrs the primary monitor of the system, this is the users prefered monitor
            /// and usally the one with the global gui elements of the os, eg task/menu bar
            /////////////////////////////////////////////////
            static Monitor getPrimaryMonitor();

            /////////////////////////////////////////////////
            /// \brief Retunrs the number of currently connected monitors
            /////////////////////////////////////////////////
            static Monitor getMonitorCount();

            /////////////////////////////////////////////////
            /// \brief Returns a vector of video modes which this montior can support at full screen
            /////////////////////////////////////////////////
            std::vector<VideoMode> getFullscreenModes();

            /////////////////////////////////////////////////
            /// \brief Retunrs the native video mode of this montior (ie: its normal resolution and bpp)
            /////////////////////////////////////////////////
            VideoMode getDesktopMode();

            /////////////////////////////////////////////////
            /// \brief Returns the (rough) dpi of this monitor
            /////////////////////////////////////////////////
            double getDPI();

            /////////////////////////////////////////////////
            /// \brief Returns the (rough) width of the monitor in millimeters
            /////////////////////////////////////////////////
            double getPhysicalWidth();

            /////////////////////////////////////////////////
            /// \brief Returns the (rough) height of the monitor in millimeters
            /////////////////////////////////////////////////
            double getPhysicalHeight();

            /////////////////////////////////////////////////
            /// \brief Returns the name of the monitor, not guarrentied to be unique
            /////////////////////////////////////////////////
            std::string getName();
        private:
            lie::rend::detail::MonitorImpl* mImpl;
        };

    }
}

#endif // LIE_RENDER_MONITOR_HPP
