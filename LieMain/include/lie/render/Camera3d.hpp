#ifndef LIE_REND_CAMERA3D_HPP
#define LIE_REND_CAMERA3D_HPP


#include <lie/math/Angle.hpp>
#include <lie/math/Vector3.hpp>
#include <lie/math/Vector2.hpp>
#include <lie/math/Matrix4x4.hpp>
#include <lie/math/Transform3.hpp>


namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief Class used for storing data about and manipulating
        /// a camera in 3d space
        /////////////////////////////////////////////////
        class Camera3d{
        public:
            enum class ProjectionType{
                Perspective,
                Orthogonal
            };

            lie::math::Transform3f transform;
            lie::math::Anglef fov;
            float nearCutOff;
            float farCutOff;


            /////////////////////////////////////////////////
            /// \brief Creates a default 3d camera with attributes as follows:
            ///  - Position: (0,0,0)
            ///  - Look Direction: (0,0,-1)
            ///  - Up Direction: (0,1,0)
            ///  - Field of View: 45 degrees
            ///  - Near Cutoff: 0.1f
            ///  - Far Cutoff: 200.0f
            /////////////////////////////////////////////////
            Camera3d();

            /////////////////////////////////////////////////
            /// \brief Causes this camera to look at the specified point in the 3d scene.
            /// This is a conviencience function which will calculate the look direction
            /// based off the camera's current position and the point you want to look at.
            /// Ie, this code:
            /// camera.lookAt(pointToLookAt);
            /// It is the equivlient of the following code:
            /// \code
            /// camera.setLookDirection(pointToLookAt - camera.getPosition());
            /// \endcode
            /// \return Reference to this Camera3d for operator chaining
            /////////////////////////////////////////////////
            Camera3d& lookAt(const lie::math::Vector3f& point);

            /////////////////////////////////////////////////
            /// \brief Returns a Matrix4x4f representing the transformation needed to
            /// convert from world space to camera space
            /// \param viewportSize The size of the viewport being rendered to
            /////////////////////////////////////////////////
            lie::math::Matrix4x4f getTransformation(const lie::math::Vector2i& viewportSize) const;
			
			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the Projection matrix for this Camera3d
			//////////////////////////////////////////////////////////////////////////
			lie::math::Matrix4x4f getProjectionMatrix(const lie::math::Vector2i& viewportSize) const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the View matrix for this camera, ie, the transformation
			/// that transforms positions from world space (ie, offset from origin) to
			/// camera space (ie, offset from camera position)
			//////////////////////////////////////////////////////////////////////////
			lie::math::Matrix4x4f getViewMatrix() const;
		private:

        };

    }
}

#endif // LIE_REND_CAMERA3D_HPP
