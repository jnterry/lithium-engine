#ifndef LIE_RENDER_CAMERA2D_HPP
#define LIE_RENDER_CAMERA2D_HPP

#include <lie/math/Angle.hpp>
#include <lie/math/Matrix3x3.hpp>
#include <lie/math/Vector2.hpp>

namespace lie{
    namespace rend{

        /////////////////////////////////////////////////
        /// \brief 2d camera that defines the "view region", ie: the part of the 2d scene that is rendered
        ///
        /// The view region is simply a rectangle of any size with a given center point and clockwise rotation
        /////////////////////////////////////////////////
        class Camera2d{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs a new Camera2d with the specified size for its view region.
            /// The center position of the camera will be set to (0,0)
            /// Rotation will be initialiazed to 0
            /////////////////////////////////////////////////
            Camera2d(lie::math::Vector2i viewRegionSize);

            /////////////////////////////////////////////////
            /// @{
            /// @name Setter Methods
            /// \brief
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Sets the center point of the view region
            /// \return Reference to this Camera2d for operator chaining
            /// \see move
            /////////////////////////////////////////////////
            Camera2d& setCenter(float x, float y);

            /////////////////////////////////////////////////
            /// \brief Sets the center point of the view region
            /// \return Reference to this Camera2d for operator chaining
            /// \see move
            /////////////////////////////////////////////////
            Camera2d& setCenter(const lie::math::Vector2f& center);

            /////////////////////////////////////////////////
            /// \brief Sets the size of the view region
            /// \return Reference to this Camera2d for operator chaining
            /// \see zoom
            /////////////////////////////////////////////////
            Camera2d& setSize(float width, float height);

            /////////////////////////////////////////////////
            /// \brief Sets the width of the view region of this
            /// camera to the x component of the passed in Vector2
            /// and the height to its y component
            /// \return Reference to this Camera2d for operator chaining
            /// \note The passed in Vector can be of any type, internally
            /// a float size is stored, hence using a Vector2<double> will
            /// result in a lose of precision
            /// \see zoom
            /////////////////////////////////////////////////
            template<typename T>
            Camera2d& setSize(lie::math::Vector2<T> size){
                return this->setSize(size.x, size.y);
            }

            /////////////////////////////////////////////////
            /// \brief Sets the current clockwise rotation of the camera in degrees.
            /// Default value is 0
            /// \return Reference to this Camera2d for operator chaining
            /// \see rotate
            /////////////////////////////////////////////////
            Camera2d& setRotation(lie::math::Angle<float> angle);

            /// @} //end setter methods



            /////////////////////////////////////////////////
            /// @{
            /// @name Offset Methods
            /// \brief Modifies some property of the Camera2d by an offset rather
            /// than directly setting it
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Moves the view region by the specified amount
            /// \return Reference to this Camera2d for operator chaining
            /// \see setCenter
            /////////////////////////////////////////////////
            Camera2d& move(float xOffset, float yOffset);

            /////////////////////////////////////////////////
            /// \brief Moves the view region by the specified amount
            /// \return Reference to this Camera2d for operator chaining
            /// \see setCenter
            /////////////////////////////////////////////////
            Camera2d& move(const lie::math::Vector2f& offset);

            /////////////////////////////////////////////////
            /// \brief Modifies the current rotation of the camera by the set amount
            /// \return Reference to this Camera2d for operator chaining
            /////////////////////////////////////////////////
			Camera2d& rotate(lie::math::Angle<float> angle);

            /////////////////////////////////////////////////
            /// \brief Resizes the view region by the specified scale factor
            /// hence changing the size of rendered objects.
            /// - 0 < scaleFactor < 1: view region get smaller, hence objects appear bigger
            /// - scaleFactor = 1: size unchanged
            /// - 1 < scaleFactor: view region gets larger, hence objects appear smaller
            /// \return Reference to this Camera2d for operator chaining
            /////////////////////////////////////////////////
            Camera2d& zoom(float scaleFactor);

            /// @} //end offset methods

            /////////////////////////////////////////////////
            /// @{
            /// @name Get Methods
            /// \brief
            /////////////////////////////////////////////////

            /////////////////////////////////////////////////
            /// \brief Returns the current center point of the view region
            /////////////////////////////////////////////////
            lie::math::Vector2f getCenter() const;

            /////////////////////////////////////////////////
            /// \brief Returns a Vector2f representing the size of the view region
            /// where x is the width and y is the height
            /////////////////////////////////////////////////
            lie::math::Vector2f getSize() const;

            /////////////////////////////////////////////////
            /// \brief Returns the current height of the view region
            /////////////////////////////////////////////////
            float getHeight() const;

            /////////////////////////////////////////////////
            /// \brief Returns the current width of the view region
            /////////////////////////////////////////////////
            float getWidth() const;

            /////////////////////////////////////////////////
            /// \brief Returns a lie::math::Matrix3x3f representing the transformation
            /// this camera provides
            /// \param viewportSize The size of the view port that is being rendered to, 
            /// if this is not equal to the camera's view region then everything needs
            /// to be scaled up/down
            /////////////////////////////////////////////////
            lie::math::Matrix3x3f getTransformation(const math::Vector2i& viewportSize) const;

            /// @} //end get methods

        private:
            ///clockwise rotation of the camera
			lie::math::Angle<float> mRotation;

            ///center point of the view region
            lie::math::Vector2f mCenter;

            ///size of the view region, x is the width, y the height
            lie::math::Vector2f mSize;

            ///the transformation matrix representing this camera
            lie::math::Matrix3x3f mTransform;
        };

    }
}

#endif // LIE_RENDER_CAMERA2D_HPP
