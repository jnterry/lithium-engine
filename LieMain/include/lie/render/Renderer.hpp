/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Renderer.hpp
/// \author Jamie Terry
/// \date 2015/06/20
/// \brief Contains the lie::rend::Renderer class
/// 
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_RENDERER_HPP
#define LIE_RENDER_RENDERER_HPP

#include "../math/Aabb2.hpp"
#include "../core/Timer.hpp"
#include "../core/Time.hpp"
#include "Color.hpp"

namespace lie{
	namespace rend{

		class RenderTarget;

		//////////////////////////////////////////////////////////////////////////
		/// \brief Base class for Renderer2d and Renderer3d that defines common
		/// interface and functionality between the two classes
		//////////////////////////////////////////////////////////////////////////
		class Renderer{
		public:
			//////////////////////////////////////////////////////////////////////////
			/// \brief Constructs a new default Renderer
			//////////////////////////////////////////////////////////////////////////
			Renderer();

			virtual ~Renderer(){}

			//////////////////////////////////////////////////////////////////////////
			/// \brief This function must be called each frame before calling any .draw()
			/// functions, it allows the render to setup anything it needs to before begining
			/// to draw
			/// \param target The RenderTarget that the Renderer will draw to this frame
			//////////////////////////////////////////////////////////////////////////
			void begin(RenderTarget& target);

			//////////////////////////////////////////////////////////////////////////
			/// \brief This function must be called each frame after all .draw() calls have
			/// been made, depending on the implementation may do very little or may actually
			/// submit the draw calls to the GPU
			//////////////////////////////////////////////////////////////////////////
			void end();

			/////////////////////////////////////////////////
			/// \brief Sets the view port for this Renderer.
			///
			/// The view port is the region of the RenderTarget that this renderer
			/// will modify. The values are in pixels.
			///
			/// Changes will not be applied unit the next call to begin
			/////////////////////////////////////////////////
			void setViewport(const lie::math::Aabb2i& aabb);

			/////////////////////////////////////////////////
			/// \brief Returns a reference to the view port of this Renderer
			/// The view port can be modified with the reference, however changes will
			/// not be applied unit the next call to begin
			/////////////////////////////////////////////////
			lie::math::Aabb2i& getViewport();

			/////////////////////////////////////////////////
			/// \brief Returns a const reference to the view port of this Renderer
			/////////////////////////////////////////////////
			const lie::math::Aabb2i& getViewport() const;

			/////////////////////////////////////////////////
			/// \brief Sets the ambient light for this renderer,
			/// colors alpha channel is used as ambient light intensity
			/// \see getAmbientLightColor
			/////////////////////////////////////////////////
			void setAmbientLightColor(lie::rend::Color color);

			/////////////////////////////////////////////////
			/// \brief Returns a reference ambient light color in use by this renderer,
			/// you may modify the color using this reference but changes are not 
			/// guaranteed to take effect until the next call to begin
			/// \see setAmbientLightColor
			/////////////////////////////////////////////////
			lie::rend::Color& getAmbientLightColor();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns a const reference to the ambient light color in use by this renderer,
			/// \see setAmbientLightColor
			//////////////////////////////////////////////////////////////////////////
			const lie::rend::Color& getAmbientLightColor() const;
			
			//////////////////////////////////////////////////////////////////////////
			/// \brief Resets the internal timer of this Renderer, by default the timer
			/// counts up from when the Renderer was created and is only reset to 0
			/// by this function. The value of this timer is passed to the ShaderPrograms
			/// if the Pass::ShaderParameterSource::Time source is used
			//////////////////////////////////////////////////////////////////////////
			void resetTimer();

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the time between the last call to .begin() and .end(),
			/// this value only changes when .end() is called, hence you can retrieve the
			/// frame time for the last frame even after .begin() has been called on this frame.
			//////////////////////////////////////////////////////////////////////////
			lie::core::Time getLastFrameTime() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Returns the time since the last call to .begin(), hence this value
			/// represents how long this Renderer has been drawing for
			////////////////////////////////////////////////////////////////////////// 
			lie::core::Time getFrameTime() const;

			//////////////////////////////////////////////////////////////////////////
			/// \brief Function that can be overridden by derived classes to perform any setup
			/// before .draw calls are made
			/// \param target The RenderTarget that should be drawn to this frame
			//////////////////////////////////////////////////////////////////////////
			virtual void onBegin(RenderTarget& target){};

			//////////////////////////////////////////////////////////////////////////
			/// \brief Function that can be overridden by derived classes to perform cleanup or
			/// submit draw calls to the graphics api after all .draw calls have been made
			//////////////////////////////////////////////////////////////////////////
			virtual void onEnd(){};

			
		protected:

			//////////////////////////////////////////////////////////////////////////
			/// \brief Sets up the rendering api to used its in built fixed function pipeline,
			/// no shaders or pass data is used
			//////////////////////////////////////////////////////////////////////////
			void useFixedFunctionPipeine();

			///< the current view port, ie the region of the RenderTarget that will be modified
			///< by this renderer
			lie::math::Aabb2i mViewport;

			///< The ambient light color used to render the scene
			lie::rend::Color mAmbientLightColor;

			///< Timer that counts up from the creation of the Renderer or since .resetTimer()
			///< was called
			lie::core::Timer mTimer;

			///< Timer that is reset each time .begin() is called
			lie::core::Timer mFrameTimer;

			///< The difference in time between the last time .end() and .begin() was called
			lie::core::Time mLastFrameTime;
		};
	}
}

#endif //LIE_RENDER_RENDERER_HPP