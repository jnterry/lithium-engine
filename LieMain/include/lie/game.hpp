/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file game.hpp
/// \author Jamie Terry
/// \brief Convinence header that includes all other header files in the
/// game module
///
/// \defgroup game Game Module
/// \brief The Game Module contains high level utility classes and functions to aid
/// in the creation of games, for example game scene manament and game state managment.
///
/// Lithium Engine is designed in such a way that it is possible to make a game
/// without using this module. There are many ways to organise scene data, game state,
/// etc, the implementations and classes in this module provide a good starting point
/// however other methods could be made which talk to the render, core and math modules
/// directly.
///
/// \ingroup game
/////////////////////////////////////////////////

#include "game/Screen.hpp"
#include "game/ScreenStack.hpp"

#include "game/Entity.hpp"
#include "game/EntityFilter.hpp"
#include "game/EntityScene.hpp"
#include "game/EntitySystem.hpp"
#include "game/CachedEntityFilter.hpp"
