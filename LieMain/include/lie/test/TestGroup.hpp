/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file TestGroup.hpp
/// \author Jamie Terry
/// \brief Contains the class lie::test::TestGroup
///
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_TESTGROUP_HPP
#define LIE_TEST_TESTGROUP_HPP

#include "TestObject.hpp"
#include <vector>
#include <fstream>
namespace lie{
    namespace test{

        /////////////////////////////////////////////////
        /// \brief A TestGroup manages a group of TestObjects
        /// and can report summaries of its managed objects
        /// As a TestGroup is itself a TestObject TestGroups can
        /// be added to others to make a tree like grouping
        /////////////////////////////////////////////////
        class TestGroup : public TestObject{
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs a new TestGroup with the specified name
            /////////////////////////////////////////////////
            TestGroup(std::string name);

            /////////////////////////////////////////////////
            /// \brief Returns the total number of passed tests for all the
            /// TestObjects managed by this test group
            /////////////////////////////////////////////////
            unsigned int getPassCount();

            /////////////////////////////////////////////////
            /// \brief Returns the total number of failed tests for all the
            /// TestObjects managed by this test group
            /////////////////////////////////////////////////
            unsigned int getFailCount();

            /////////////////////////////////////////////////
            /// \brief Adds a TestObject to this TestGroup, once added
            /// this this TestGroup owns the TestObject and will delete it in its
            /// destructor.
            /// Intended usage is:
            /// \code
            /// tester.add(new ValueTest(...));
            /// \endcode
            /// The main exception to this will be TestGroups as you will want to add
            /// tests to them, hence for test groups you should do the following:
            /// lie::core::TestGroup subGroup = new lie::core::TestGroup("group name");
            /// parentGroup.add(subGroup);
            /// subGroup->add(...)
            /// \endcode
            /////////////////////////////////////////////////
            void add(TestObject* obj);


            /////////////////////////////////////////////////
            /// \brief Creates a new TestGroup with the specifed name and adds it to
            /// this TestGroup. This is a convienience functions for shortening your code.
            /// Eg: this:
            /// \code
            /// lie::test::TestGroup* convertGroup = new lie::test::TestGroup("convert");
			/// group->add(convertGroup);
			/// \endcode
			/// can become:
			/// \code
            /// lie::test::TestGroup* convertGroup = group->createSubgroup("convert");
			/// group->add(convertGroup);
			/// \endcode
			///
            /// \param name The name of the new group to make
            /////////////////////////////////////////////////
            TestGroup* createSubgroup(std::string name);


            /////////////////////////////////////////////////
            /// \brief Saves a html report file detailing the test results
            /////////////////////////////////////////////////
            bool saveReport(std::string fileName);
        private:
            ///gets a html summary of this group and its child TestObjects, used when saving a html report
            void getHtmlSummary(std::ostream& ss);

            std::vector<TestObject*> mTestObjects;
        };

    }
}

#endif // LIE_TEST_TESTGROUP_HPP
