/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file TestObject.hpp
/// \author Jamie Terry
/// \brief Contains the class lie::test::TestObject, the base class for all
/// testing related classes
///
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_TESTOBJECT_HPP
#define LIE_TEST_TESTOBJECT_HPP

#include <string>
#include <sstream>

namespace lie{
    namespace test{
        /////////////////////////////////////////////////
        /// \brief Base class for any class which can be added to
        /// and be managed by a TestGroup
        /////////////////////////////////////////////////
        class TestObject{
            friend class TestGroup;
        public:
            /////////////////////////////////////////////////
            /// \brief Constructs a new TestObject with the specified name
            /////////////////////////////////////////////////
            TestObject(std::string name);

            /////////////////////////////////////////////////
            /// \brief Pure virutal function that must be overidden by derived classes
            /// to return the number of tests which passed
            /////////////////////////////////////////////////
            virtual unsigned int getPassCount() = 0;

            /////////////////////////////////////////////////
            /// \brief Pure virutal function that must be overidden by derived classes
            /// to return the number of tests which failed
            /////////////////////////////////////////////////
            virtual unsigned int getFailCount() = 0;

            /////////////////////////////////////////////////
            /// \brief Returns the sum of the getFailCount and the getPassCount for
            /// this test object, ie: the total number of tests in the TestObject
            /////////////////////////////////////////////////
            unsigned int getTestCount(){
                return (this->getPassCount() + this->getFailCount());
            }

            /////////////////////////////////////////////////
            /// \brief Returns the name of this TestObject
            /////////////////////////////////////////////////
            std::string getName();

            /////////////////////////////////////////////////
            /// \brief Returns the id of this test object. IDs are guarrentied
            /// to be unique even for completely seperate TestObjects
            /////////////////////////////////////////////////
            unsigned int getId();

        protected:
            /////////////////////////////////////////////////
            /// \brief Returns a string which will be added to the html report file
            ///
            /// This really shouldn't be here... Ideally we would have some "HtmlReportGenerator"
            /// class which does this and use the visitor pattern so new report types
            /// can be added, however as many of the tests eg: (ValueTest) are templates
            /// and you cant have virtual templates this doesnt seem possible... or atleast easy
            ///
            /// Also means we could remove the ID generation and put it in the report generator since
            /// the ids only matter for that
            /////////////////////////////////////////////////
            virtual void getHtmlSummary(std::ostream& ss) = 0;
        private:
            std::string mName;
            unsigned int mId;
            static unsigned int mNextId;
        };
    }
}

#endif // LIE_TEST_TESTOBJECT_HPP
