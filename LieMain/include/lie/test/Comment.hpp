/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Comment.hpp
/// \author Jamie Terry
/// \brief Contains the class lie::test::Comment
///
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_COMMENT_HPP
#define LIE_TEST_COMMENT_HPP

#include "TestObject.hpp"

namespace lie{
    namespace test{

        /////////////////////////////////////////////////
        /// \brief A comment stores some message to include in a test report
        /// file but does not contain an actual test
        /// This is useful for manual debuging or letting a reader know what is going
        /// on
        /////////////////////////////////////////////////
        class Comment : public TestObject{
        public:
            friend class TestGroup;

            /////////////////////////////////////////////////
            /// \brief Constructs a new comment object with the passed in string as the comment
            /////////////////////////////////////////////////
            Comment(std::string comment) : TestObject(comment) {/*empty body*/}

            /////////////////////////////////////////////////
            /// \brief Comments are not tests, returns 0
            /////////////////////////////////////////////////
            unsigned int getPassCount();

            /////////////////////////////////////////////////
            /// \brief Comments are not tests, returns 0
            /////////////////////////////////////////////////
            unsigned int getFailCount();

        protected:
            /////////////////////////////////////////////////
            /// \brief Returns html summary string for the comment, used when generating
            /// a html report file
            /////////////////////////////////////////////////
            void getHtmlSummary(std::ostream& ss);
        };

    }
}

#endif // LIE_TEST_COMMENT_HPP
