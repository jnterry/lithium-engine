/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file ValueTest.hpp
/// \author Jamie Terry
/// \brief Contains the class lie::test::ValueTest, the basic
/// amd most commonly used test type
///
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_VALUETEST_HPP
#define LIE_TEST_VALUETEST_HPP

#include "../core/Convert.hpp"
#include "TestObject.hpp"

namespace lie{
    namespace test{

        /////////////////////////////////////////////////
        /// \brief The ValueTest compares two instances of the same type
        /// and determines if they are equal, if they are then the test passed
        /// if not the tested failed
        ///
        /// \tparam T The type of object that is being compared by this ValueTest
        ///
        /// \note Type T must support operator==, ie T1 == T2 must not be a compile error!
        /////////////////////////////////////////////////
        template<typename T>
        class ValueTest : public TestObject{
        public:
            ///The type of result this ValueTest stores
            typedef T ResultType;

            /////////////////////////////////////////////////
            /// \brief Constructs new ValueTest from a name
            /// and two instances of type T, if they are equal then the test passed,
            /// else the test failed
            /////////////////////////////////////////////////
            ValueTest(std::string name, T expected, T actual) :
                TestObject(name), mExpected(expected), mActual(actual){
                //empty body
            }

            /////////////////////////////////////////////////
            /// \brief Returns 1 if this test failed, else returns 0
            /////////////////////////////////////////////////
            unsigned int getFailCount(){
                if(areEqual(this->mExpected, this->mActual)){
                    return 0;
                } else {
                    return 1;
                }
            }

            /////////////////////////////////////////////////
            /// \brief Returns 1 if this test passed, else returns 0
            /////////////////////////////////////////////////
            unsigned int getPassCount(){
                if(areEqual(this->mExpected, this->mActual)){
                    return 1;
                } else {
                    return 0;
                }
            }

        private:
            bool areEqual(T t1, T t2){
                return t1 == t2;
            }

            void getHtmlSummary(std::ostream& ss){
                if(areEqual(this->mExpected, this->mActual)){
                    ss << "<li class=\"test-pass\">";
                } else {
                    ss << "<li class=\"test-fail\">";
                }
                ss << this->getName() << " - Expected: " << this->mExpected << ", actual: " << this->mActual << "</li>" << std::endl;
            }


            //The expected result for the test
            T mExpected;

            //The actual result for the test
            T mActual;
        };

        template<> bool ValueTest<float>::areEqual(float t1, float t2){
            return lie::core::compareFloats(t1, t2, 20);
        }

        template<> bool ValueTest<double>::areEqual(double t1, double t2){
            return lie::core::compareDoubles(t1, t2, 20);
        }

    }
}

#endif // LIE_TEST_VALUETEST_HPP
