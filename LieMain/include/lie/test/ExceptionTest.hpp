/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file ExceptionTest.hpp
/// \author Jamie Terry
/// \brief Contains the class lie::test::ExceptionTest
///
/// \ingroup test
/////////////////////////////////////////////////
#ifndef LIE_TEST_EXCEPTIONTEST_HPP
#define LIE_TEST_EXCEPTIONTEST_HPP

#include "TestObject.hpp"
#include <typeinfo>
#include <functional>
#include <lie/core/Convert.hpp>

namespace lie{
    namespace test{

		//////////////////////////////////////////////////////////////////////////
		/// \brief Dummy class which can be used as the ExpectedExceptionType tparam
		/// for ExceptionTest to indicate that is should pass if an exception is caught,
		/// rather than if an exception of some specific type is caught
		//////////////////////////////////////////////////////////////////////////
		class ExceptionTestAcceptAny{};

		//////////////////////////////////////////////////////////////////////////
		/// \brief Calls a function in a try catch block and passes is an exception
		/// of the expected type is caught, else fails and reports if any exception
		/// was caught and if not what the function returned.
		//////////////////////////////////////////////////////////////////////////
		template<typename ExpectedExceptionType, typename FuncReturn, typename... FuncArgs>
		class ExceptionTest : public TestObject{
		public:
			ExceptionTest(std::string name, std::function<FuncReturn(FuncArgs...)> func, FuncArgs... args)
				: TestObject(name), mCaught(false), mCaughtType(nullptr), mExceptionMsg(""){
				try{
					mReturnValue = func(args...);
				} catch (ExpectedExceptionType e){
					mCaught = true;
					mCaughtType = &typeid(ExpectedExceptionType);
					if (std::is_base_of<std::exception, ExpectedExceptionType>::value){
						mExceptionMsg = e.what();
					}
				} catch (int e) {
					mCaught = true;
					mCaughtType = &typeid(int);
					mExceptionMsg = lie::core::toString(e);
				} catch (std::exception e){
					mCaught = true;
					mCaughtType = &typeid(std::exception);
					mExceptionMsg = e.what();
				} catch (...) {
					mCaught = true;
					//no way for us to know what type
				}
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test failed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getFailCount(){
				if (this->getPassCount() == 0){
					return 1;
				} else {
					return 0;
				}
			}

			/////////////////////////////////////////////////
			/// \brief Returns 1 if this test passed, else returns 0
			/////////////////////////////////////////////////
			unsigned int getPassCount(){
				if (std::is_same<ExpectedExceptionType, ExceptionTestAcceptAny>::value){
					if (this->mCaught){
						return 1;
					} else {
						return 0;
					}
				} else {
					//dont accept any
					if (this->mCaught && this->mCaughtType->hash_code() == typeid(ExpectedExceptionType).hash_code()){
						return 1;
					} else {
						return 0;
					}
				}
			}

		private:
			void getHtmlSummary(std::ostream& ss){
				if (this->getPassCount() == 1){
					ss << "<li class=\"test-pass\">";
				} else {
					ss << "<li class=\"test-fail\">";
				}
				ss << this->getName();
				if (std::is_same<ExpectedExceptionType, ExceptionTestAcceptAny>::value){
					ss << " - Expected to catch an exception of any type ";
					if (this->mCaught){
						if (this->mCaughtType == nullptr){
							ss << ", caught exception of unknown type";
						} else {
							ss << ", caught exception of type: " << mCaughtType->name();
						}
					} else {
						ss << ", but no exception was thrown by the tested code, function returned " << mReturnValue;
					}
				} else {
					ss << " - Expected to catch exception of type \"" << typeid(ExpectedExceptionType).name() << "\" ";
					if (this->mCaught){
						if (this->mCaughtType == nullptr){
							ss << "but caught exception of unknown type";
						}
						else if (this->getPassCount() == 1){
							ss << "and caught exception of correct type";
						}
						else {
							ss << "but caught exception type: " << mCaughtType->name();
						}
					} else {
						ss << ", but no exception was thrown by the tested code, function returned " << mReturnValue;
					}
				}
				if (this->mExceptionMsg != ""){
					ss << "<br><b>Exception Message:</b><br>";
					ss << "<pre>";
					ss << this->mExceptionMsg;
					ss << "</pre>";
				}

				ss << std::endl << "</li>" << std::endl;
			}

			///< if true an exception was caught when the function was called
			bool mCaught;

			///< the type of exception that was caught, nullptr if no exception was caught
			///< or the type is not known (ie, caught with catch(...) syntax)
			const std::type_info* mCaughtType;

			///< The value returned by the function, only set if the function does not throw
			FuncReturn mReturnValue;

			///< The message of the exception, will be empty string if the exception type
			///< is not known or no exception is caught
			std::string mExceptionMsg;
		};

    }
}


#endif // LIE_TEST_EXCEPTIONTEST_HPP


