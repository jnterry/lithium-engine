#include <lie/game\ScreenStack.hpp>

namespace lie{
    namespace game{

            void ScreenStack::pushScreen(Screen* screen){
                this->mScreens.push_back(screen);
                screen->mScreenStack = this;
                screen->onPush();
                if(this->mScreens.size() > 1){
                    //then there was already a screen on the stack
                    this->mScreens[this->mScreens.size()-2]->onCover();
                }
            }

            Screen* ScreenStack::popScreen(){
                if(this->mScreens.size() != 0){
                    Screen* poppedScreen = this->mScreens[this->mScreens.size()-1];
                    this->mScreens.pop_back();
                    if(this->mScreens.size() != 0){
                        this->mScreens[this->mScreens.size()-1]->onUncover();
                    }
                    poppedScreen->onPop();
                    return poppedScreen;
                } else {
                    return nullptr;
                }

            }

            Screen* ScreenStack::getTopScreen(){
                return this->mScreens[this->mScreens.size()];
            }

            void ScreenStack::update(){
                for(auto it = this->mScreens.rbegin(); it != this->mScreens.rend(); ++it){
                    if(!(*it)->update()){
                        return;
                    }
                }
            }

            void ScreenStack::render(){
                unsigned int blockerIndex = 0;
                for(int i = this->mScreens.size()-1; i >=0; --i){
                    if(!this->mScreens[i]->allowsRender()){
                        blockerIndex = i;
                        break;
                    }
                }

                for(int i = blockerIndex; i < this->mScreens.size(); ++i){
                    this->mScreens[i]->render();
                }
            }

            void ScreenStack::handleInput(lie::rend::Event& e){
                for(auto it = this->mScreens.rbegin(); it != this->mScreens.rend(); ++it){
                    if(!(*it)->handleInput(e)){
                        return;
                    }
                }
            }



    }
}
