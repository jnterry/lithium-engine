#include <lie/test\Comment.hpp>
#include <ostream>

namespace lie{
    namespace test{

        unsigned int Comment::getPassCount(){return 0;}
        unsigned int Comment::getFailCount(){return 0;}

        void Comment::getHtmlSummary(std::ostream& ss){
            ss << "<li>" << this->getName() << "</li>" << std::endl;
        }

    }
}
