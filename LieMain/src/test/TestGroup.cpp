#include <lie/test\TestGroup.hpp>

namespace lie{
    namespace test{
        TestGroup::TestGroup(std::string name) : TestObject(name) {/*empty body*/}

        unsigned int TestGroup::getPassCount(){
            unsigned int result = 0;
            for(unsigned int i = 0; i < this->mTestObjects.size(); ++i){
                result += this->mTestObjects[i]->getPassCount();
            }
            return result;
        }

        unsigned int TestGroup::getFailCount(){
            unsigned int result = 0;
            for(unsigned int i = 0; i < this->mTestObjects.size(); ++i){
                result += this->mTestObjects[i]->getFailCount();
            }
            return result;
        }

        void TestGroup::add(TestObject* obj){
            this->mTestObjects.push_back(obj);
        }


        TestGroup* TestGroup::createSubgroup(std::string name){
            TestGroup* result = new TestGroup(name);
            this->add(result);
            return result;
        }


        void TestGroup::getHtmlSummary(std::ostream& ss){
            std::string cssClass;

            if(this->getFailCount()==0 && this->getPassCount() > 0){
                cssClass = "pass";
            } else if(this->getFailCount()> 0 && this->getPassCount() > 0){
                cssClass = "mixed";
            } else if(this->getFailCount()> 0 && this->getPassCount() == 0){
                cssClass = "fail";
            }

            ss  << "<div class =\"group-" << cssClass << "\" id=\"" << this->getId() << "\">" << std::endl;
            ss  << "<li><label for=\"" << this->getId() << "-check\"><h3><a href=\"#" << this->getId() << "\">Pin</a> "
                << this->getName() << " (<font color=\"#008000\">Pass: " << this->getPassCount()
                << "</font>, <font color = \"#800000\">Fail: " << this->getFailCount() << "</font>, Total: "
                << this->getTestCount() << ", " << (this->getPassCount()/(float)this->getTestCount())*100
                << "% passed)</h3></label></li>" << std::endl;

            ss << "<input type=\"checkbox\" " << ((this->getFailCount()>0)?"checked " : "") << "id = \"" << this->getId() << "-check\">" << std::endl;

            ////////////////////////////////////////////////////////////////////////////////
            //print out child test objects in their own ul
            ss << "<ul>" << std::endl;
            for(unsigned int i = 0; i < this->mTestObjects.size(); ++i){
                this->mTestObjects[i]->getHtmlSummary(ss);
            }
            ss << "</ul>" << std::endl << "</div>" << std::endl; //end this TestGroup's div
        }

        bool TestGroup::saveReport(std::string fileName){
            std::ofstream fout(fileName.c_str());
            if(!fout.is_open()){
                return false;
            }

            fout << "<html>" << std::endl;
            fout << "<head>" << std::endl;
            fout << "<link rel=\"stylesheet\" type=\"text\\css\" href=\"unit-test-style.css\">" << std::endl;
            fout << "<title>LIE Unit Test Report </title>" << std::endl;
            fout << "</head>" << std::endl;


            fout << "<body>" << std::endl;
            fout << "<font face=\"arial\">" << std::endl;
            fout << "<h1>Lithium Engine Unit Test Report File</h1>" << std::endl;

            fout << "<h2>Fail List" << std::endl;


            fout << "<h2>Full List - Click Group Name to Expand/Collapse Tree</h2>" << std::endl;
            fout << "Click group name to expand/collapse tree </br>" << std::endl;
            fout << "Click pin to get a link to that test, reloading the page will jump to that test immediately</br>" << std::endl;
            this->getHtmlSummary(fout);

            fout << "</font>" << std::endl << "</body>" << std::endl;
            fout << "</html>" << std::endl;

            return true;
        }
    }
}
