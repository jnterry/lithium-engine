#include <lie/test\TestObject.hpp>

namespace lie{
    namespace test{

        unsigned int TestObject::mNextId = 0;

        TestObject::TestObject(std::string name) : mName(name), mId(mNextId){
            ++mNextId;
        }

        std::string TestObject::getName(){
            return this->mName;
        }

        unsigned int TestObject::getId(){
            return this->mId;
        }
    }
}
