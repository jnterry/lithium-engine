#include <lie/render/ResourceLoaderShader.hpp>
#include <lie/render/Shader.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/render/ShaderProgramBuilder.hpp>
#include <lie/core/File.hpp>
#include <fstream>

namespace lie{
	namespace rend{
		lie::rend::ShaderProgram* resourceLoaderShaderFileList(lie::core::File& file, unsigned int byteOffset){
			std::ifstream fin(file.getFullPath());
			if (!fin.is_open()){
				return nullptr;
			}
			std::vector<std::string> tokens;
			while (!fin.eof()){
				std::string temp;
				fin >> temp;
				tokens.push_back(temp);
			}
			if (tokens.size() % 2 != 0){
				LIE_DLOG_ERROR("ResourceLoaderShaderFileList", "Malformed .shader file:" << file.getFullPath() << " - odd number of tokens found.");
				return nullptr;
			}
			lie::rend::ShaderProgramBuilder progBuilder;
			std::vector<lie::rend::Shader*> shaders;
			for (int i = 0; i < tokens.size(); i += 2){
				lie::rend::Shader::Type type;
				if (tokens[i] == "Vertex"){
					type = lie::rend::Shader::Type::Vertex;
				}
				else if (tokens[i] == "Pixel"){
					type = lie::rend::Shader::Type::Pixel;
				}
				else {
					LIE_DLOG_ERROR("ResourceLoaderShaderFileList", "Malformed .shader file:" << file.getFullPath() << " - requested unknown shader type: " << tokens[i]);
					return nullptr;
				}
				lie::rend::Shader* sh = new lie::rend::Shader(type);
				std::string path = file.getFolder().getFullPath() + "\\" + tokens[i + 1];
				std::ifstream shFin(path);
				if (!shFin.is_open()){
					LIE_DLOG_ERROR("ResourceLoaderShaderFileList", "Failed to open shader file: " << path);
					return nullptr;
				}
				sh->loadAndCompile(shFin);
				if (!sh->isOkay()){
					LIE_DLOG_ERROR("ResourceLoaderShaderFileList", "Failed to compile the shader " << tokens[i + 1]
						<< " in the ShaderProgram in the .shader file:" << file.getFullPath()
						<< " - err: " << sh->getErrors());
				}
				progBuilder.addShader(*sh);
			}
			lie::rend::ShaderProgram* prog = progBuilder.build();
			if (prog == nullptr){
				LIE_DLOG_ERROR("ResourceLoaderShaderFileList", "Failed to link ShaderProgram in .shader file:"
					<< file.getFullPath() << " - err: " << progBuilder.getErrors());
				return nullptr;
			}
			else {
				LIE_DLOG_INFO("ResourceLoaderShaderFileList", "Loaded ShaderProgram from:" << file.getFullPath());
				for (int i = 0; i < prog->getParameterCount(); ++i){
					LIE_DLOG_INFO("quicktest", "Found parameter: " << prog->getParameterName(i) << ", type: " << (int)prog->getParameterType(i));
				}
				for (int i = 0; i < prog->getTextureChannelCount(); ++i){
					LIE_DLOG_INFO("ResourceLoaderShaderFileList", "Found texture channel: " << prog->getTextureChannelName(i));
				}
			}
			for (int i = 0; i < shaders.size(); ++i){
				delete shaders[i];
			}
			//:TODO: all newed objects leaked if a return nullptr occurs
			return prog;
		}
	}
}