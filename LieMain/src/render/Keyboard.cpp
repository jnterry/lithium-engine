#include <lie/core\Config.hpp>
#include <lie/render\Keyboard.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "win32/KeyboardImpl.win.hpp"
//#elif defined(LIE_OS_LINUX)
//#elif defined(LIE_OS_MACOS)
#else
    #error No keyboard implementation for this platform!
#endif

namespace lie{
    namespace rend{

        bool Keyboard::isPressed(Key key){
            return lie::rend::detail::KeyboardImpl::isPressed(key);
        }

    }
}
