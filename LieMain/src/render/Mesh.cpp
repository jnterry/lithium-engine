#include <lie/render\Mesh.hpp>

namespace lie{
    namespace rend{

        ////////////////////////////////////////////////////////////////////
        //Mesh::AttributeData functions
        Mesh::AttributeData::AttributeData(lie::core::SharedHandle<GpuBuffer> newGpuBuffer, unsigned char newComponentCount, lie::core::PrimativeType newType,
                                            bool newNormalized, unsigned short newStride, unsigned int newBufferOffset) :
                                            gpuBuffer(newGpuBuffer), componentCount(newComponentCount), type(newType),
                                            normalized(newNormalized), stride(newStride), bufferOffset(newBufferOffset)
                                            {/*empty body*/}

        Mesh::AttributeData::AttributeData() :  gpuBuffer(nullptr), componentCount(0), type(lie::core::PrimativeType::Float),
                                                normalized(false), stride(0), bufferOffset(0)
                                                {/*empty body*/}

        void Mesh::AttributeData::set( lie::core::SharedHandle<GpuBuffer> newGpuBuffer, unsigned char newComponentCount, lie::core::PrimativeType newType,
                                    bool newNormalized, unsigned short newStride, unsigned int newBufferOffset){
            this->gpuBuffer = newGpuBuffer;
            this->componentCount = newComponentCount;
            this->type = newType;
            this->normalized = newNormalized;
            this->stride = newStride;
            this->bufferOffset = newBufferOffset;
        }

        ////////////////////////////////////////////////////////////////////
        //Mesh functions
        Mesh::Mesh(unsigned char attributeCount) : mAttributeCount(attributeCount),
        mIndexBuffer(nullptr), mAttributes(new AttributeData[this->mAttributeCount]), mIndexBufferOffset(0){
            //empty body
        }
        Mesh::~Mesh(){
            delete[] this->mAttributes;
        }

        const Mesh::AttributeData& Mesh::getAttribute(AttributeId attributeId) const{
            return this->mAttributes[attributeId];
        }

        Mesh::AttributeData& Mesh::getAttribute(AttributeId attributeId){
            return this->mAttributes[attributeId];
        }

        unsigned char Mesh::getAttributeCount() const{
            return this->mAttributeCount;
        }

        void Mesh::setVertexCount(unsigned int vcount){
            this->mVertexCount = vcount;
        }

        unsigned int Mesh::getVertexCount() const{
            return this->mVertexCount;
        }

        bool Mesh::isIndexed() const{
            return this->mIndexBuffer != nullptr;
        }

        void Mesh::setIndexBufferParams(lie::core::SharedHandle<GpuBuffer> buf, unsigned int byteOffset){
            this->mIndexBuffer = buf;
            this->mIndexBufferOffset = byteOffset;
        }

        void Mesh::setIndexBuffer(lie::core::SharedHandle<GpuBuffer> buf){
            this->mIndexBuffer = buf;
        }

        void Mesh::setIndexBufferByteOffset(unsigned int byteOffset){
            this->mIndexBufferOffset = byteOffset;
        }

        const lie::core::SharedHandle<GpuBuffer> Mesh::getIndexBuffer() const{
            return this->mIndexBuffer;
        }

        unsigned int Mesh::getIndexBufferOffset() const{
            return this->mIndexBufferOffset;
        }



    }
}
