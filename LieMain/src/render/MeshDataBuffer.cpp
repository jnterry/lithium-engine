#include <lie/render\MeshDataBuffer.hpp>
#include <lie/core\Logger.hpp>
#include <lie/core\Convert.hpp>

namespace lie{
    namespace rend{
        MeshDataBuffer::MeshDataBuffer(const MeshVertexLayout& vertexDesc) : mVertexLayout(vertexDesc){

        }

        MeshDataBuffer::~MeshDataBuffer(){
        }

        const MeshVertexLayout& MeshDataBuffer::getVertexLayout() const{
            return this->mVertexLayout;
        }

        void MeshDataBuffer::appendVerticies(const void* vertexData, size_t vertexCount){
            this->mData.push_back(vertexData, vertexCount*this->mVertexLayout.getSize());
        }

        void MeshDataBuffer::setAttribData(unsigned char attribId, const void* data, size_t vertexCount, size_t startVertex){
            if(this->getVertexCount() < startVertex + vertexCount){
                //then increase vertex count
                this->setVertexCount(startVertex+vertexCount);
            }

            size_t vertexSize = this->mVertexLayout.getSize(); //size in bytes of a single vertex

            //current index into internal byte array
            //if we start at the vertex 0 then it is simply the offset for the attribute we are changing
            size_t internalByteIndex = startVertex*vertexSize + this->mVertexLayout.getAttribOffset(attribId);


            size_t dataIndex = 0; //current index into the passed in "data" arrray
            size_t attribSize = this->mVertexLayout.getAttrib(attribId).getSize(); //the size of the attribute that we are setting

            for(int i = 0; i < vertexCount; ++i){
                //copy "attribSize" bytes from the data array at dataIndex into the internal data buffer at index "internalByteIndex"
                memcpy(&this->mData[internalByteIndex],&(((unsigned char*)(data))[dataIndex]),attribSize);

                //The internal byte index is increased by a vertex size so the index is now the same attribute in the next vertex along
                internalByteIndex += vertexSize;

                //data index is increased by attribSize so we are now looking at the next set of data in the array
                //eg float positions[] = {
                //  0,0,0, //first 3 float are for vertex 1
                //  1,0,0, //next 3 float are for vertex 2
                //  0,1,0
                //}
                //in this case attribSize is 3*sizeof(float)
                dataIndex += attribSize;
            }
        }

        void MeshDataBuffer::clearAttribData(unsigned char attribId){
            size_t dataIndex = this->mVertexLayout.getAttribOffset(attribId); //start at the requested attrib in the first vertex
            size_t vertSize = this->mVertexLayout.getSize();
            size_t attribSize = this->mVertexLayout.getAttrib(attribId).getSize();
            for(int i = 0; i < this->getVertexCount(); ++i){
                memset(&this->mData[dataIndex], 0, attribSize);
                dataIndex += vertSize;
            }
        }

        void MeshDataBuffer::clearVerticies(){
            this->mData.clear();
        }

        void MeshDataBuffer::setVertexCount(size_t vertexCount){
            this->mData.resize(vertexCount*this->mVertexLayout.getSize());
        }

        size_t MeshDataBuffer::getVertexCount() const{
            return this->mData.size() / this->mVertexLayout.getSize();
        }


        size_t MeshDataBuffer::getFaceCount() const{
            return this->getDrawnVertexCount()/3;
        }

        bool MeshDataBuffer::hasIndexBuffer() const{
            return (this->mIndexBuffer.size() != 0);
        }

        bool MeshDataBuffer::validateIndexBuffer() const{
            //check all indexes refer to an actual vertex,
            //if we have 100 verticies (index 0->99) then anything 100 or
            //greater is invalid
            for(int i = 0; i < this->mIndexBuffer.size(); ++i){
                if(this->mIndexBuffer[i] >= this->getVertexCount()){
                    return false;
                }
            }
            return true;
        }

        void MeshDataBuffer::clearIndexBuffer(){
            this->mIndexBuffer.clear();
        }

        void MeshDataBuffer::appendIndicies(IndexType* data, size_t indexCount){
            this->mIndexBuffer.insert<unsigned int*>(this->mIndexBuffer.end(), data, &data[indexCount]);
        }

        size_t MeshDataBuffer::getIndexCount() const{
            return this->mIndexBuffer.size();
        }


        Mesh* MeshDataBuffer::buildMesh(Device& rsys) const{
            //create the mesh
            Mesh* mesh = rsys.createMesh(this->mVertexLayout.getAttribCount());
            mesh->setVertexCount(this->getDrawnVertexCount());

            if(this->hasIndexBuffer()){
                if(!this->validateIndexBuffer()){
                    //index buffer invalid :(
                    delete mesh;
                    LIE_DLOG_ERROR("MeshDataBuffer", "Tried to build mesh but its index buffer was invalid!");
                    return nullptr;
                }

                lie::core::SharedHandle<GpuBuffer> indBuf(new GpuBuffer(lie::rend::GpuBuffer::UsageType::Index));
                indBuf->bufferData(this->mIndexBuffer.data(), this->mIndexBuffer.size()*sizeof(IndexType));
                mesh->setIndexBuffer(indBuf);
            }

            //upload vertex data
            lie::core::SharedHandle<GpuBuffer> buf(new GpuBuffer(lie::rend::GpuBuffer::UsageType::Geometry));
            buf->bufferData(this->mData.data(), this->mData.size());

            //set up mesh attributes
            size_t currentOffset = 0;
            for(int i = 0; i < this->mVertexLayout.getAttribCount(); ++i){
                mesh->getAttribute(i).set(buf, //which gpu buffer?
                                         this->mVertexLayout.getAttrib(i).componentCount,
                                         this->mVertexLayout.getAttrib(i).type,
                                         false, //normalize?
                                         this->mVertexLayout.getSize(), //stide -> needs to be size of vertex
                                         currentOffset); //starts at 0, added to by attrib size
                //starting offset into gpu buffer for attrib i is determined by the size of all the attributes that come before it
                currentOffset += this->mVertexLayout.getAttrib(i).getSize();
            }

            return mesh;
        }

        size_t MeshDataBuffer::getDrawnVertexCount() const{
            //if we have 4 verticies/indicies we only draw 3 in 1 face
            //do count - count%3
            if(this->hasIndexBuffer()){
                return (this->getIndexCount()) - (this->getIndexCount()%3);
            } else {
                return this->getVertexCount()  - (this->getVertexCount()%3);
            }
        }
    }
}
