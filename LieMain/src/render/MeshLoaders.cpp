#include <lie/render/MeshLoaders.hpp>
#include "extlib/obj-loader/tiny_obj_loader.h"
#include <fstream>
#include <algorithm>

bool _objLoadSubMesh(std::string filename, tinyobj::mesh_t mesh, lie::rend::MeshDataBuffer& mdb, int positionAttrib, int normalAttrib, int texCordAttrib){
    LIE_DLOG_TRACE("MeshLoaders", "The obj " << filename << " contains " << mesh.positions.size() << " positions, "
                                << mesh.normals.size() << " normals, " << mesh.texcoords.size() << " texcords, " << mesh.indices.size() << " indicies");

    unsigned int vertCount = std::max(mesh.positions.size(), std::max(mesh.normals.size(), mesh.texcoords.size()));
    mdb.setVertexCount(vertCount);

    ////////////////////////////////////////////
    //copy position data
    if(positionAttrib >= 0){
        if(mdb.getVertexLayout().getAttrib(positionAttrib).componentCount != 3){
            LIE_DLOG_ERROR("MeshLoaders", "Tried to load obj " << filename << " into a MeshDataBuffer, but the attribute used for storing"
                           << " position had " << mdb.getVertexLayout().getAttrib(positionAttrib).componentCount << " components, obj expects"
                           << " 3");
            //return false;
        } else {
            mdb.setAttribData(positionAttrib, mesh.positions.data(), mesh.positions.size()/3);
        }
    }

    if(normalAttrib >= 0){
        mdb.setAttribData(normalAttrib, mesh.normals.data(), mesh.normals.size()/3);
    }

    if(texCordAttrib >= 0){
        if(mdb.getVertexLayout().getAttrib(texCordAttrib).componentCount != 2){
            LIE_DLOG_ERROR("MeshLoaders", "Tried to load obj " << filename << " into a MeshDataBuffer, but the attribute used for storing"
                           << " texture coordiantes had " << mdb.getVertexLayout().getAttrib(texCordAttrib).componentCount << " components, obj expects"
                           << " 2");
            //return false;
        } else {
            mdb.setAttribData(texCordAttrib, mesh.texcoords.data(), mesh.texcoords.size()/2);
        }
    }

    mdb.appendIndicies(mesh.indices.data(), mesh.indices.size());

    return true;
}

namespace lie{
    namespace rend{
        namespace mesh{

            bool loadObj(lie::core::File file, lie::rend::MeshDataBuffer& mdb, int positionAttrib, int normalAttrib, int texCordAttrib){

                std::ifstream fs(file.getFullPath());
                if(!fs.is_open()){
                    LIE_DLOG_ERROR("MeshLoaders", "Tried to load the obj file " << file.getFullPath() << " but failed to open file.");
                    return false;
                }
                std::vector<tinyobj::shape_t> loadedShapes;
                tinyobj::MaterialFileReader matRead("");

                std::string loadResult = tinyobj::LoadObj(loadedShapes, fs, matRead);
                if(loadResult == ""){
                    //then load successeed
                    LIE_DLOG_TRACE("MeshLoaders", file.getFullPath() << " load stage completed successfully, loaded " << loadedShapes.size() << " sub meshes");
                    if(loadedShapes.size() > 0){
                        return _objLoadSubMesh(file.getFullPath(), loadedShapes[0].mesh, mdb, positionAttrib, normalAttrib, texCordAttrib);
                    } else {
                        LIE_DLOG_ERROR("MeshLoaders", "Loaded the obj file " << file.getFullPath() << ", but it contained no meshes");
                        return false;
                    }
                } else {
                    LIE_DLOG_ERROR("MeshLoaders", "Failed to load the obj file " << file.getFullPath() << ", return string: " << loadResult);
                    return false;
                }
            }
        }
    }
}
