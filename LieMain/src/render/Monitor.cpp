#include <lie/render\Monitor.hpp>
#include <lie/core\Config.hpp>


#if defined(LIE_OS_WINDOWS)
    #include "win32/MonitorImpl.win.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxMonitor.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXMonitor.hpp"
#else
    #error Need to supply implementation to get supported video modes and desktop mode!
#endif

namespace lie{
    namespace rend{

        std::vector<Monitor> Monitor::getAllMonitors(){
			return std::vector<Monitor>(); //:TODO:
        }

        Monitor Monitor::getPrimaryMonitor(){
			return Monitor(); //:TODO:
        }

        Monitor Monitor::getMonitorCount(){
			return Monitor(); //:TODO:
		}

        std::vector<VideoMode> Monitor::getFullscreenModes(){
            return std::vector<VideoMode>(); //:TODO:
        }

        VideoMode Monitor::getDesktopMode(){
			return VideoMode(); //:TODO:
        }
        double Monitor::getDPI(){
            return 0;
        }

        double Monitor::getPhysicalWidth(){
            return 0;
        }

        double Monitor::getPhysicalHeight(){
            return 0;
        }

        std::string Monitor::getName(){
            return "TODO IMPL";
        }
    }
}
