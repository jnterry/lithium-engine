#include <lie/render/MaterialType.hpp>
#include <lie/render/Material.hpp>
#include <lie/core/Logger.hpp>

namespace lie{
	namespace rend{

		MaterialType::MaterialType(unsigned char attribCount, const lie::core::ByteBuffer& defaults)
			: mAttributeCount(attribCount), mDefaultMat(*this){
			this->mDefaultMat.mParamData.push_back(defaults);
			//empty body
		}

		MaterialType::~MaterialType(){
			delete[] this->mAttributes;
		}

		Material* MaterialType::createMaterial() const{
			Material* m = new Material(*this);
			m->mParamData.push_back(this->mDefaultMat.mParamData);
			return m;
		}

		bool MaterialType::hasAttribute(std::string name) const{
			for (int i = 0; i < mAttributeCount; ++i){
				if (this->mAttributes[i].name == name){
					return true;
				}
			}
			return false;
		}

		int MaterialType::getAttributeIndex(std::string name) const{
			for (int i = 0; i < mAttributeCount; ++i){
				if (this->mAttributes[i].name == name){
					return i;
				}
			}
			return -1;
		}

		unsigned char MaterialType::getTextureCount() const{
			return this->mTextureCount;
		}

		int MaterialType::getAttributeByteOffset(int index) const{
			return this->mAttributes[index].byteOffset;
		}

		MaterialType::AttributeMeta::AttributeMeta(std::string newName, ShaderParameterType newType, unsigned short newByteOffset)
			: name(newName), type(newType), byteOffset(newByteOffset) {
		}

		void MaterialType::AttributeMeta::setTo(const AttributeMeta& other){
			this->name = other.name;
			this->type = other.type;
			this->byteOffset = other.byteOffset;
		}
	}
}