#include <lie/render\BlendMode.hpp>

bool ordDependTable[] = {
    false, //Add
    false, //Multiply
    true, //None
    true, //Minus
    true, //RMinus
    true, //Divide
    true, //RDivide
    false, //Difference
    false, //Min
    false}; //Max

namespace lie{
    namespace rend{
        bool isOrderDependent(BlendMode mode){
            //avoid branching for small increase in memory consumption
            return ordDependTable[(char)mode];
        }
    }
}
