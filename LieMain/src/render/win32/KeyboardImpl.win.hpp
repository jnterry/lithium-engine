#include <lie/render/Keyboard.hpp>
#include <lie/core/Logger.hpp>
#include <windows.h>
#include <bitset>
#include <iostream>

namespace lie{
    namespace rend{
        namespace detail{
            struct KeyboardImpl{
                static bool isPressed(lie::rend::Keyboard::Key key){
                    int keyCode;
                    switch(key){
                    case lie::rend::Keyboard::Key::Unknown:
                        return false;
                        break;
                    case lie::rend::Keyboard::Key::A:
                        keyCode = 65;
                        break;
                    case lie::rend::Keyboard::Key::B:
                        keyCode = 66;
                        break;
                    case lie::rend::Keyboard::Key::C:
                        keyCode = 67;
                        break;
                    case lie::rend::Keyboard::Key::D:
                        keyCode = 68;
                        break;
                    case lie::rend::Keyboard::Key::E:
                        keyCode = 69;
                        break;
                    case lie::rend::Keyboard::Key::F:
                        keyCode = 70;
                        break;
                    case lie::rend::Keyboard::Key::G:
                        keyCode = 71;
                        break;
                    case lie::rend::Keyboard::Key::H:
                        keyCode = 72;
                        break;
                    case lie::rend::Keyboard::Key::I:
                        keyCode = 73;
                        break;
                    case lie::rend::Keyboard::Key::J:
                        keyCode = 74;
                        break;
                    case lie::rend::Keyboard::Key::K:
                        keyCode = 75;
                        break;
                    case lie::rend::Keyboard::Key::L:
                        keyCode = 76;
                        break;
                    case lie::rend::Keyboard::Key::M:
                        keyCode = 77;
                        break;
                    case lie::rend::Keyboard::Key::N:
                        keyCode = 78;
                        break;
                    case lie::rend::Keyboard::Key::O:
                        keyCode = 79;
                        break;
                    case lie::rend::Keyboard::Key::P:
                        keyCode = 80;
                        break;
                    case lie::rend::Keyboard::Key::Q:
                        keyCode = 81;
                        break;
                    case lie::rend::Keyboard::Key::R:
                        keyCode = 82;
                        break;
                    case lie::rend::Keyboard::Key::S:
                        keyCode = 83;
                        break;
                    case lie::rend::Keyboard::Key::T:
                        keyCode = 84;
                        break;
                    case lie::rend::Keyboard::Key::U:
                        keyCode = 85;
                        break;
                    case lie::rend::Keyboard::Key::V:
                        keyCode = 86;
                        break;
                    case lie::rend::Keyboard::Key::W:
                        keyCode = 87;
                        break;
                    case lie::rend::Keyboard::Key::X:
                        keyCode = 88;
                        break;
                    case lie::rend::Keyboard::Key::Y:
                        keyCode = 89;
                        break;
                    case lie::rend::Keyboard::Key::Z:
                        keyCode = 90;
                        break;
                    case lie::rend::Keyboard::Key::Num0:
                        keyCode = 48;
                        break;
                    case lie::rend::Keyboard::Key::Num1:
                        keyCode = 49;
                        break;
                    case lie::rend::Keyboard::Key::Num2:
                        keyCode = 50;
                        break;
                    case lie::rend::Keyboard::Key::Num3:
                        keyCode = 51;
                        break;
                        break;
                    case lie::rend::Keyboard::Key::Num4:
                        keyCode = 52;
                        break;
                    case lie::rend::Keyboard::Key::Num5:
                        keyCode = 53;
                        break;
                    case lie::rend::Keyboard::Key::Num6:
                        keyCode = 54;
                        break;
                    case lie::rend::Keyboard::Key::Num7:
                        keyCode = 55;
                        break;
                    case lie::rend::Keyboard::Key::Num8:
                        keyCode = 56;
                        break;
                    case lie::rend::Keyboard::Key::Num9:
                        keyCode = 57;
                        break;
                    case lie::rend::Keyboard::Key::Escape:
                        keyCode = VK_ESCAPE;
                        break;
                    case lie::rend::Keyboard::Key::LControl:
                        keyCode = VK_LCONTROL;
                        break;
                    case lie::rend::Keyboard::Key::LShift:
                        keyCode = VK_LSHIFT;
                        break;
                    case lie::rend::Keyboard::Key::LAlt:
                        keyCode = VK_LMENU; //this is correct, weird windows naming
                        break;
                    case lie::rend::Keyboard::Key::LSystem:
                        keyCode = VK_LWIN;
                        break;
                    case lie::rend::Keyboard::Key::RControl:
                        keyCode = VK_RCONTROL;
                        break;
                    case lie::rend::Keyboard::Key::RShift:
                        keyCode = VK_RSHIFT;
                        break;
                    case lie::rend::Keyboard::Key::RAlt:
                        keyCode = VK_RMENU;
                        break;
                    case lie::rend::Keyboard::Key::RSystem:
                        keyCode = VK_RWIN;
                        break;
                    case lie::rend::Keyboard::Key::Menu:
                        //keyCode = ;
                        return false; // ? VK_MENU is alt?
                        break;
                    case lie::rend::Keyboard::Key::LBracketSq:
                        keyCode = 91;
                        break;
                    case lie::rend::Keyboard::Key::RBracketSq:
                        keyCode = 93;
                        break;
                    case lie::rend::Keyboard::Key::SemiColon:
                        keyCode = 59;
                        break;
                    case lie::rend::Keyboard::Key::Comma:
                        keyCode = 44;
                        break;
                    case lie::rend::Keyboard::Key::Period:
                        keyCode = 46;
                        break;
                    case lie::rend::Keyboard::Key::Quote:
                        keyCode = 34;
                        break;
                    case lie::rend::Keyboard::Key::Slash:
                        keyCode = 47;
                        break;
                    case lie::rend::Keyboard::Key::BackSlash:
                        keyCode = 92;
                        break;
                    case lie::rend::Keyboard::Key::Tilde:
                        keyCode = 126;
                        break;
                    case lie::rend::Keyboard::Key::Equal:
                        keyCode = 61;
                        break;
                    case lie::rend::Keyboard::Key::Dash:
                        keyCode = 45;
                        break;
                    case lie::rend::Keyboard::Key::Space:
                        keyCode = VK_SPACE;
                        break;
                    case lie::rend::Keyboard::Key::Return:
                        keyCode = VK_RETURN;
                        break;
                    case lie::rend::Keyboard::Key::BackSpace:
                        keyCode = VK_BACK;
                        break;
                    case lie::rend::Keyboard::Key::Tab:
                        keyCode = VK_TAB;
                        break;
                    case lie::rend::Keyboard::Key::PageUp:
                        keyCode = VK_PRIOR;
                        break;
                    case lie::rend::Keyboard::Key::PageDown:
                        keyCode = VK_NEXT;
                        break;
                    case lie::rend::Keyboard::Key::End:
                        keyCode = VK_END;
                        break;
                    case lie::rend::Keyboard::Key::Home:
                        keyCode = VK_HOME;
                        break;
                    case lie::rend::Keyboard::Key::Insert:
                        keyCode = VK_INSERT;
                        break;
                    case lie::rend::Keyboard::Key::Delete:
                        keyCode = VK_DELETE;
                        break;
                    case lie::rend::Keyboard::Key::Add:
                        keyCode = VK_ADD;
                        break;
                    case lie::rend::Keyboard::Key::Subtract:
                        keyCode = VK_SUBTRACT;
                        break;
                    case lie::rend::Keyboard::Key::Multiply:
                        keyCode = VK_MULTIPLY;
                        break;
                    case lie::rend::Keyboard::Key::Divide:
                        keyCode = VK_DIVIDE;
                        break;
                    case lie::rend::Keyboard::Key::Left:
                        keyCode = VK_LEFT;
                        break;
                    case lie::rend::Keyboard::Key::Right:
                        keyCode = VK_RIGHT;
                        break;
                    case lie::rend::Keyboard::Key::Up:
                        keyCode = VK_UP;
                        break;
                    case lie::rend::Keyboard::Key::Down:
                        keyCode = VK_DOWN;
                        break;
                    case lie::rend::Keyboard::Key::Numpad0:
                        keyCode = VK_NUMPAD0;
                        break;
                    case lie::rend::Keyboard::Key::Numpad1:
                        keyCode = VK_NUMPAD1;
                        break;
                    case lie::rend::Keyboard::Key::Numpad2:
                        keyCode = VK_NUMPAD2;
                        break;
                    case lie::rend::Keyboard::Key::Numpad3:
                        keyCode = VK_NUMPAD3;
                        break;
                    case lie::rend::Keyboard::Key::Numpad4:
                        keyCode = VK_NUMPAD4;
                        break;
                    case lie::rend::Keyboard::Key::Numpad5:
                        keyCode = VK_NUMPAD5;
                        break;
                    case lie::rend::Keyboard::Key::Numpad6:
                        keyCode = VK_NUMPAD6;
                        break;
                    case lie::rend::Keyboard::Key::Numpad7:
                        keyCode = VK_NUMPAD7;
                        break;
                    case lie::rend::Keyboard::Key::Numpad8:
                        keyCode = VK_NUMPAD8;
                        break;
                    case lie::rend::Keyboard::Key::Numpad9:
                        keyCode = VK_NUMPAD9;
                        break;
                    case lie::rend::Keyboard::Key::F1:
                        keyCode = VK_F1;
                        break;
                    case lie::rend::Keyboard::Key::F2:
                        keyCode = VK_F2;
                        break;
                    case lie::rend::Keyboard::Key::F3:
                        keyCode = VK_F3;
                        break;
                    case lie::rend::Keyboard::Key::F4:
                        keyCode = VK_F4;
                        break;
                    case lie::rend::Keyboard::Key::F5:
                        keyCode = VK_F5;
                        break;
                    case lie::rend::Keyboard::Key::F6:
                        keyCode = VK_F6;
                        break;
                    case lie::rend::Keyboard::Key::F7:
                        keyCode = VK_F7;
                        break;
                    case lie::rend::Keyboard::Key::F8:
                        keyCode = VK_F8;
                        break;
                    case lie::rend::Keyboard::Key::F9:
                        keyCode = VK_F9;
                        break;
                    case lie::rend::Keyboard::Key::F10:
                        keyCode = VK_F10;
                        break;
                    case lie::rend::Keyboard::Key::F11:
                        keyCode = VK_F11;
                        break;
                    case lie::rend::Keyboard::Key::F12:
                        keyCode = VK_F12;
                        break;
                    case lie::rend::Keyboard::Key::F13:
                        keyCode = VK_F13;
                        break;
                    case lie::rend::Keyboard::Key::F14:
                        keyCode = VK_F14;
                        break;
                    case lie::rend::Keyboard::Key::F15:
                        keyCode = VK_F15;
                        break;
                    case lie::rend::Keyboard::Key::Pause:
                        keyCode = VK_PAUSE;
                        break;
                    default:
                        LIE_DLOG_REPORT("Win32KeyboardImpl", "Key state requested for key without case, keycode: " << key);
                        return false;
                    };
                    int result = GetKeyState(keyCode);
                    //std::bitset<32> bits = result;
                    //std::cout << "Key result: " << bits << std::endl;
                    return ((result & (1<<31))  != 0);

                }

            };

        }
    }
}
