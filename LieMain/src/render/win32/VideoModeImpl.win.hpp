#ifndef LIE_RENDER_WIN32VIDEOMODE_HPP
#define LIE_RENDER_WIN32VIDEOMODE_HPP

#include <lie/render\VideoMode.hpp>
#include <windows.h>
#include <vector>
#include <algorithm>

//implementation copied from sfml

namespace lie{
	namespace rend{
		namespace detail{

		    struct VideoModeImpl{
                static lie::rend::VideoMode getDesktopMode(){
                    DEVMODE win32Mode;
                    win32Mode.dmSize = sizeof(win32Mode);
                    EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &win32Mode);

                    return VideoMode(win32Mode.dmPelsWidth, win32Mode.dmPelsHeight, win32Mode.dmBitsPerPel);
                }

                static std::vector<VideoMode> getFullscreenModes(){
                    std::vector<VideoMode> modes;

                    // Enumerate all available video modes for the primary display adapter
                    DEVMODE win32Mode;
                    win32Mode.dmSize = sizeof(win32Mode);
                    for (int count = 0; EnumDisplaySettings(NULL, count, &win32Mode); ++count)
                    {
                        // Convert to sf::VideoMode
                        VideoMode mode(win32Mode.dmPelsWidth, win32Mode.dmPelsHeight, win32Mode.dmBitsPerPel);

                        // Add it only if it is not already in the array
                        if (std::find(modes.begin(), modes.end(), mode) == modes.end())
                            modes.push_back(mode);
                    }

                    return modes;
                }
		    };
		}
	}
}


#endif
