#include <lie/render/MeshUtils.hpp>

namespace lie{
    namespace rend{
        namespace mesh{



            /*void calculateSmoothNormals(MeshDataBuffer& mdb, unsigned char positionAttrib, unsigned char normalAttrib){

            }*/

            void generateNormalMesh(MeshDataBuffer& inMesh, unsigned char inPos, unsigned char inNorm,
                                     MeshDataBuffer& outMesh, unsigned char outPos);



        }
    }
}
