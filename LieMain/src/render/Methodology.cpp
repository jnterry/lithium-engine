#include <lie/render/Methodology.hpp>
#include <lie/render/Technique.hpp>
#include <lie/render/MaterialType.hpp>
#include <lie/render/MaterialTypeBuilder.hpp>
#include <lie/render/Device.hpp>
#include <lie/render/ShaderProgramBuilder.hpp>

namespace lie{
	namespace rend{
		Methodology::Methodology(const lie::rend::MaterialType& matType, lie::rend::Technique& tech)
			: mMatType(matType), technique(tech){
			//empty body
		}

		lie::rend::Methodology* Methodology::createDefault2D(Device& device){
			const MaterialType* mat = device.createMaterialType(0).build();
			ShaderProgram& sprog = lie::rend::ShaderProgramBuilder::createDefault2dShaderProgram();
			Pass* pass = new Pass(sprog);
			pass->setParameterSource("mvpMatrix", Pass::ParameterSource::MVP_MATRIX);
			pass->setParameterSource("ambientLightColor", Pass::ParameterSource::AMBIENT_LIGHT_COLOR);
			return new Methodology(*mat, *(new Technique(pass)));
		}

		const lie::rend::MaterialType& Methodology::getMaterialType(){
			return this->mMatType;
		}

	}
}