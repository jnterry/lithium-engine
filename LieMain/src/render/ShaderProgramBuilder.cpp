/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ShaderProgramBuilder.cpp
/// Contains render api agnositc portion of the implementation for
/// the ShaderProgramBuilder class
/// 
/// \ingroup 
/////////////////////////////////////////////////

#include <lie/render/ShaderProgramBuilder.hpp>
#include <lie/core/StringUtils.hpp>
#include <lie/render/Shader.hpp>
#include <iostream>


namespace lie{
	namespace rend{

		ShaderProgramBuilder::ShaderProgramBuilder()
			: mState(State::NOT_BUILT), mSProgImpl(nullptr){
			//empty body
		}

		void ShaderProgramBuilder::addShader(const Shader& shader){
			this->mShaders.push_back(&shader);
		}

		std::string ShaderProgramBuilder::getErrors() const{
			switch (this->mState){
			case State::BUILD_FAILED:{
				lie::core::StringBuilder s;
				s << "ShaderProgram link error: " << this->_getErrorsAPI() << '\n';
				for (const Shader* shad : this->mShaders){
					if (!shad->isOkay()){
						s << shad->getTypeString() << " Shader Error:\n";
						s << shad->getErrors() << "\n";
					}
				}
				return s;
			}
			case State::BUILD_SUCCESSFUL:
				return "";
			case State::NOT_BUILT:
				return "ShaderProgram not yet built!";
			}
		}

		ShaderProgram& ShaderProgramBuilder::createDefault2dShaderProgram(){
			ShaderProgramBuilder builder;
			builder.addShader(createDefault2dVertexShader());
			builder.addShader(createDefault2dPixelShader());
			ShaderProgram* result = builder.build();
			std::cout << builder.getErrors() << std::endl;
			return *result;
		}
	}
}

