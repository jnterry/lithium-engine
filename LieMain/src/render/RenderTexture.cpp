/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file RenderTexture.cpp
/// Contain render api agnostic implementations for RenderTexture
/// 
/// \ingroup render
/////////////////////////////////////////////////

#include <lie/render/RenderTexture.hpp>
#include <lie/render/Texture.hpp>
#include <lie/render/VideoMode.hpp>

namespace lie{
	namespace rend{

		RenderTexture::RenderTexture(lie::math::Vector2i size, int colorBufferCount, Texture* colorBuffers,
									Texture* depthBuffer, Texture* stencilBuffer, detail::RenderTextureImpl* impl)
			: mVideoMode(VideoMode(size,32)), mColorBufferCount(colorBufferCount), mColorBuffers(colorBuffers),
			mDepthBuffer(depthBuffer), mStencilBuffer(stencilBuffer), mImpl(impl){
			//empty body
		}

		RenderTexture::~RenderTexture(){
			this->_apiDestroy();
			if (this->mDepthBuffer != nullptr){
				delete this->mDepthBuffer;
			}
			if (this->mStencilBuffer != nullptr){
				delete this->mStencilBuffer;
			}
			delete[] this->mColorBuffers;
			delete this->mImpl;
		}

		bool RenderTexture::hasDepthBuffer(){
			return this->mDepthBuffer != nullptr;
		}

		Texture* RenderTexture::getDepthBuffer(){
			return this->mDepthBuffer;
		}

		bool RenderTexture::hasStencilBuffer(){
			return this->mStencilBuffer != nullptr;
		}

		Texture* RenderTexture::getStencilBuffer(){
			return this->mStencilBuffer;
		}

		int RenderTexture::getColorBufferCount(){
			return this->mColorBufferCount;
		}

		Texture* RenderTexture::getColorBuffer(int index){
			return &(this->mColorBuffers[index]);
		}

		const VideoMode& RenderTexture::getVideoMode() const{
			return this->mVideoMode;
		}
	}
}