/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Texture.cpp
/// \author Jamie Terry
/// \brief Contains implementation of render api independent Texture functions
///
/// \ingroup rend
/////////////////////////////////////////////////

#include <lie/render\Texture.hpp>
#include <lie/core\Logger.hpp>
#include "extlib\stb\stb_image.h"

namespace lie{
    namespace rend{

        int Texture::getWidth(){
            return this->mSize.x;
        }

        int Texture::getHeight(){
            return this->mSize.y;
        }

        const lie::math::Vector2i& Texture::getSize(){
            return this->mSize;
        }

		void Texture::setSize(int width, int height){
			this->mSize.x = width;
			this->mSize.y = height;
			this->_storeOnGpu(nullptr);
		}

		void Texture::setFormat(PixelFormat pixelFormat){
			this->mPixelFormat = pixelFormat;
			this->_storeOnGpu(nullptr);
		}

        bool Texture::loadFromFile(lie::core::File& file){
            int numComponents;
            unsigned char* imgData = stbi_load(file.getFullPath().c_str(), &this->mSize.x, &this->mSize.y, &numComponents, 4);

            if(imgData == NULL){
                LIE_DLOG_ERROR("render", "Failed to load a texture from the file: " << file.getFullPath());
                return false;
            }

            this->_storeOnGpu(imgData);

            stbi_image_free(imgData); //delete the cpu data, it is now on the gpu
            return true;
        }
    }
}
