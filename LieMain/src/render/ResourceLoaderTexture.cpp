#include <lie/core/File.hpp>
#include <lie/render/Texture.hpp>

namespace lie{
	namespace rend{

		lie::rend::Texture* resourceLoaderTextureJpg(lie::core::File& file, unsigned int byteOffset){
			lie::rend::Texture* tex = new lie::rend::Texture;
			if (!tex->loadFromFile(file)){
				LIE_DLOG_ERROR("quicktest", "Failed to load texture: " << file.getFullName());
				return nullptr;
			}
			else {
				LIE_DLOG_TRACE("quicktest", "Loaded texture: " << file.getFullName());
			}
			return tex;
		}

	}
}