#include <lie/render\Camera3d.hpp>
#include <lie/math/Vector3.hpp>
#include <lie/math/Vector3-impl.hpp>

namespace lie{
    namespace rend{

            Camera3d::Camera3d()
            :fov(lie::math::deg(45.0f)), nearCutOff(0.1f), farCutOff(200.0f){
                //empty body
            }

            Camera3d& Camera3d::lookAt(const lie::math::Vector3f& point){
                return *this;
            }

            lie::math::Matrix4x4f Camera3d::getTransformation(const lie::math::Vector2i& viewportSize) const{
				return this->getViewMatrix() * this->getProjectionMatrix(viewportSize);
            }

			lie::math::Matrix4x4f Camera3d::getProjectionMatrix(const lie::math::Vector2i& viewportSize) const{
				return lie::math::Matrix4x4f::createPerspectiveProjection(this->fov, viewportSize.x, viewportSize.y, this->nearCutOff, this->farCutOff);
			}

			lie::math::Matrix4x4f Camera3d::getViewMatrix() const{
				lie::math::Vector3f upDir(0, 1, 0);
				lie::math::Vector3f lookDir(0, 0, -1);
				upDir.rotate(this->transform.rotation);
				lookDir.rotate(this->transform.rotation);

				lie::math::Vector3f s = lookDir.getCrossProduct(upDir);
				lie::math::Vector3f u = s.getCrossProduct(lookDir);

				lie::math::Matrix4x4f result({ s.x, u.x, -lookDir.x, 0,
					s.y, u.y, -lookDir.y, 0,
					s.z, u.z, -lookDir.z, 0,
					0, 0, 0, 1 });

				//lie::math::Matrix4x4f result;

				//translate the whole world by the -ve of the cameras position
				//result *= lie::math::Matrix4x4f::createTranslation(this->mPosition*-1);
				result *= lie::math::Matrix4x4f::createTranslation(this->transform.position*-1);

				return result;
			}


    }
}
