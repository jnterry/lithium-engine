#include <lie/render\Camera2d.hpp>

namespace lie{
    namespace rend{

            Camera2d::Camera2d(lie::math::Vector2i viewRegionSize)
            : mSize(viewRegionSize),
            mCenter(lie::math::Vector2f(0,0)),
            mRotation(lie::math::deg(0)){

            }

            Camera2d& Camera2d::setCenter(float x, float y){
                this->mCenter.set(x,y);
                return *this;
            }

            Camera2d& Camera2d::setCenter(const lie::math::Vector2f& center){
                this->mCenter = center;
                return *this;
            }

            Camera2d& Camera2d::setSize(float width, float height){
                this->mSize.set(width, height);
                return *this;
            }

            Camera2d& Camera2d::setRotation(lie::math::Angle<float> angle){
                this->mRotation = angle;
                return *this;
            }

            Camera2d& Camera2d::move(float xOffset, float yOffset){
                this->mCenter.x += xOffset;
                this->mCenter.y += yOffset;
                return *this;
            }

            Camera2d& Camera2d::move(const lie::math::Vector2f& offset){
                this->mCenter += offset;
                return *this;
            }

			Camera2d& Camera2d::rotate(lie::math::Angle<float> angle){
                this->mRotation += angle;
                return *this;
            }

            Camera2d& Camera2d::zoom(float scaleFactor){
                this->mSize *= scaleFactor;
                return *this;
            }


            lie::math::Vector2f Camera2d::getCenter() const{
                return this->mCenter;
            }

            lie::math::Vector2f Camera2d::getSize() const{
                return this->mSize;
            }

            float Camera2d::getHeight() const{
                return this->mSize.y;
            }

            float Camera2d::getWidth() const{
                return this->mSize.x;
            }

            lie::math::Matrix3x3f Camera2d::getTransformation(const math::Vector2i& viewportSize) const{
				//if this camera is turned 20 degrees clockwise rotate the world
				//20 degrees anticlockwise to simulate the turn
				lie::math::Matrix3x3f result = lie::math::Matrix3x3f::createRotation(-this->mRotation);

                //we need to map the world coordinates to values between -1 and 1
                //the left most items should be x of -1 and the right most at 1
                //first translate in reverse direction compared to camera
                result *= lie::math::Matrix3x3f::createTranslation(this->mCenter*-1);

				//scale the world to the correct scale depending on the view ports size vs the cameras size
				//eg, if the camera's size if twice view ports size then the world needs to be made twice as big
				// :TODO:

                //now scale world coordinates to the camera's size
                //eg, for an x of 1024 px anything with a position relative to the camera of
                // -512 x to 512 x will be mapped to -1 x to 1 x
                //going from larger to smaller, so scale factor is 1/half-size
                result *= lie::math::Matrix3x3f::createScale(1/(this->mSize.x/2.0), 1/(this->mSize.y/2.0));

                

                return result;
            }



    }
}
