#include <lie/render/Renderer2d.hpp>
#include <lie/render/Pass.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/math/Matrix3x3.hpp>
#include <lie/render/Material.hpp>
#include <lie/render/Texture.hpp>

namespace{
	lie::math::Matrix3x3f calcParralaxProjectionMatrix(lie::math::Vector2f cameraPosition, lie::math::Vector2f parrallaxFactors){
		return lie::math::Matrix3x3f::Identity;
	}
}

namespace lie{
    namespace rend{
            Renderer2d::Renderer2d(unsigned short layerCount)
				: mCamera(Camera2d(lie::math::Vector2f(1024, 768))), mNumLayers(layerCount),
				  mParrallaxFactors(new lie::math::Vector2f[layerCount]){
				for (int i = 0; i < layerCount; ++i){
					this->mParrallaxFactors[i].setAll(1);
				}
            }

			Renderer2d::~Renderer2d(){
				delete[] this->mParrallaxFactors;
			}

            void Renderer2d::setCamera(Camera2d& cam){
                this->mCamera = cam;
            }

            Camera2d& Renderer2d::getCamera(){
                return this->mCamera;
            }

			unsigned short lie::rend::Renderer2d::getLayerCount() const{
				return this->mNumLayers;
			}

			void lie::rend::Renderer2d::setLayerCount(unsigned short count){
				lie::math::Vector2f* oldParrallaxFactors = this->mParrallaxFactors;
				this->mParrallaxFactors = new lie::math::Vector2f[count];

				//copy old factors
				for (int i = 0; i < count && i < this->mNumLayers; ++i){
					this->mParrallaxFactors[i] = oldParrallaxFactors[i];
				}

				//set new factors to 1
				for (int i = this->mNumLayers; i < count; ++i){
					this->mParrallaxFactors[i].set(1, 1);
				}

				this->mNumLayers = count;
				delete[] oldParrallaxFactors;
			}

			lie::math::Vector2f lie::rend::Renderer2d::getLayerParrallaxFactor(unsigned short layer) const{
				return this->mParrallaxFactors[layer];
			}

			float lie::rend::Renderer2d::getLayerParrallaxFactorX(unsigned short layer) const{
				return this->mParrallaxFactors[layer].x;
			}

			float lie::rend::Renderer2d::getLayerParrallaxFactorY(unsigned short layer) const{
				return this->mParrallaxFactors[layer].y;
			}

			void lie::rend::Renderer2d::setLayerParrallaxFactor(unsigned short layer, lie::math::Vector2f factors){
				this->mParrallaxFactors[layer] = factors;
			}

			void lie::rend::Renderer2d::setLayerParrallaxFactorX(unsigned short layer, float factorX){
				this->mParrallaxFactors[layer].x = factorX;
			}

			void lie::rend::Renderer2d::setLayerParrallaxFactorY(unsigned short layer, float factorY){
				this->mParrallaxFactors[layer].y = factorY;
			}

			void Renderer2d::usePass(Pass& pass, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer){
				//:TODO: this code is extremal similar to Renderer3d.use pass, somehow merge the two?
				pass.use();
				ShaderProgram& sprog = pass.getShaderProgram();

				for (int i = 0; i < sprog.getTextureChannelCount(); ++i){
					switch (pass.getTextureSource(i)){
					case Pass::TextureSource::MATERIAL:{
						int matTextIndex = pass.getTextureSourceArg(i);
						mat.getTexture(matTextIndex)->bind(i);
						sprog.setTextureChannelBinding(i, i);
						break;
					}
					case Pass::TextureSource::UNKNOWN:
						break;
					}
				}

				for (int i = 0; i < sprog.getParameterCount(); ++i){
					int sourceArg = pass.getParameterSourceArg(i);
					ShaderParameterType type = sprog.getParameterType(i);
					switch (pass.getParameterSource(i)){
					case Pass::ParameterSource::MATERIAL_ATTRIBUTE:
						switch (type){
						case ShaderParameterType::Mat4x4f:
							sprog.setParameter(i, mat.getAttributeMat4x4f(sourceArg));
							break;
						case ShaderParameterType::Mat3x3f:
							sprog.setParameter(i, mat.getAttributeMat3x3f(sourceArg));
							break;
						case ShaderParameterType::Vec4f:
							sprog.setParameter(i, mat.getAttributeVec4f(sourceArg));
							break;
						case ShaderParameterType::Vec3f:
							sprog.setParameter(i, mat.getAttributeVec3f(sourceArg));
							break;
						case ShaderParameterType::Vec2f:
							sprog.setParameter(i, mat.getAttributeVec2f(sourceArg));
							break;
						case ShaderParameterType::Int32:
							sprog.setParameter(i, mat.getAttributeInt32(sourceArg));
							break;
						case ShaderParameterType::Float:
							sprog.setParameter(i, mat.getAttributeFloat(sourceArg));
							break;
						}
						break;
					case Pass::ParameterSource::MVP_MATRIX:
						sprog.setParameter(i, modelMatrix * this->mCamera.getTransformation(this->mViewport.getSize()));
						break;
					case Pass::ParameterSource::MODEL_MATRIX:
						sprog.setParameter(i, modelMatrix);
						break;
					case Pass::ParameterSource::CAMERA_POSITION:
						sprog.setParameter(i, this->mCamera.getCenter());
						break;
					case Pass::ParameterSource::VIEW_MATRIX:
						sprog.setParameter(i, this->mCamera.getTransformation(this->mViewport.getSize()));
						break;
					case Pass::ParameterSource::PROJECTION_MATRIX:
						//:TOOD:
						sprog.setParameter(i, lie::math::Matrix3x3f::Identity);
						break;
					case Pass::ParameterSource::CAMERA_UP_DIRECTION:
						LIE_DLOG_FATAL("Renderer3d", "CAM UP DIR NOT IMPLEMENTED");
						break;
					case Pass::ParameterSource::AMBIENT_LIGHT_COLOR:
						sprog.setParameter(i, this->getAmbientLightColor());
						break;
					case Pass::ParameterSource::TIME:
						switch (type){
						case ShaderParameterType::Float:
							sprog.setParameter(i, (float)this->mTimer.getElapsedTime().asMilliseconds());
							break;
						case ShaderParameterType::Int32:
							sprog.setParameter(i, (int)this->mTimer.getElapsedTime().asMilliseconds());
							break;
						default:
							LIE_DLOG_FATAL("Renderer3dImmediate", "Tried to pass time to unknown parameter type");
							break;
						}
						break;
					}
				}
			}
    }
}
