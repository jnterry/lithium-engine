#include <lie/render/Renderer.hpp>
#include <lie/render/ShaderProgram.hpp>

namespace lie{
	namespace rend{

		Renderer::Renderer()
			: mAmbientLightColor(Color::White), mViewport(math::Aabb2i(0, 0, 1024, 768)){
			//empty body
		}

		void Renderer::begin(RenderTarget& target){
			this->mFrameTimer.restart();
			this->onBegin(target);
		}

		void Renderer::end(){
			this->onEnd();
			this->mLastFrameTime = this->mFrameTimer.getElapsedTime();
		}

		void Renderer::setViewport(const lie::math::Aabb2i& aabb){
			this->mViewport = aabb;
		}

		lie::math::Aabb2i& Renderer::getViewport(){
			return this->mViewport;
		}

		const lie::math::Aabb2i& Renderer::getViewport() const{
			return this->mViewport;
		}

		void Renderer::setAmbientLightColor(lie::rend::Color color){
			this->mAmbientLightColor = color;
		}

		lie::rend::Color& Renderer::getAmbientLightColor(){
			return this->mAmbientLightColor;
		}

		const lie::rend::Color& Renderer::getAmbientLightColor() const{
			return this->mAmbientLightColor;
		}

		void Renderer::resetTimer(){
			this->mTimer.restart();
		}

		lie::core::Time Renderer::getLastFrameTime() const{
			return this->mLastFrameTime;
		}

		lie::core::Time Renderer::getFrameTime() const{
			return this->mFrameTimer.getElapsedTime();
		}

		void Renderer::useFixedFunctionPipeine(){
			ShaderProgram::useNone();
		}
	}
}