#include <lie/render\Shader.hpp>
#include <lie/core/Logger.hpp>
#include "OpenGL.hpp"

namespace lie{
    namespace rend{

        Shader::Shader(Type shaderType){
            mType = shaderType;
            switch(shaderType){
            case Type::Vertex:
                this->mRef = lieCheckGlReturn(glCreateShader(GL_VERTEX_SHADER));
                return;
            case Type::Pixel:
                this->mRef = lieCheckGlReturn(glCreateShader(GL_FRAGMENT_SHADER));
                return;
            case Type::Geometry:
                this->mRef = lieCheckGlReturn(glCreateShader(GL_GEOMETRY_SHADER));
                return;
            default:
                return;
            }
        }

        Shader::~Shader(){
            lieCheckGl(glDeleteShader(this->mRef));
        }

		bool Shader::loadAndCompile(const char* source){
			lieCheckGl(glShaderSource(this->mRef, 1, &source, NULL));
			lieCheckGl(glCompileShader(this->mRef));
			return this->isOkay();
		}

        bool Shader::isOkay() const{
            GLint compileStatus; //var to store status in
            lieCheckGl(glGetShaderiv(this->mRef, GL_COMPILE_STATUS, &compileStatus)); //get staus
            return compileStatus == GL_TRUE; //if compile status is GL_TRUE then there are no problems, else, there are problems
        }

         std::string Shader::getErrors() const{
            //get length of info log
            GLint infoLength;
            lieCheckGl(glGetShaderiv(this->mRef, GL_INFO_LOG_LENGTH, &infoLength));

            //make char array of that length (+1 for NULL terminater), fill it with log
            GLchar* logData = new GLchar[infoLength+1];
            lieCheckGl(glGetShaderInfoLog(this->mRef, infoLength, NULL, logData));

            //copy char string into std::string, delete the char array, return the string
            std::string result = logData;
            delete[] logData;
            return result;
         }

         

    }
}

