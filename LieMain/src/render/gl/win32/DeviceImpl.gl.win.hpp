#ifndef LIE_RENDER_WIN32DEVICE_HPP
#define LIE_RENDER_WIN32DEVICE_HPP

#include <lie/core\Convert.hpp>
#include <lie/core\Logger.hpp>
#include <lie/render\Device.hpp>
#include <windows.h>
#include "RenderContextImpl.gl.win.hpp"
#include "../../../core/win32/ErrorToString.win.hpp"


namespace lie{
	namespace rend{
		namespace detail{

		    struct DeviceImpl{
                HDC mDeviceContext; //the windows device context owned by this render system
                HWND mDummyWindow; //we must create a window before we can create any contexts

                DeviceImpl(){
                    //create the dummy window that this render system will use to create contexts
                    //http://msdn.microsoft.com/en-us/library/windows/desktop/ms632679(v=vs.85).aspx
                    this->mDummyWindow = CreateWindowA(
                            "STATIC", //class name
                            "WIN32 DUMMY WINDOW", //window name
                            WS_POPUP | WS_DISABLED, //window style
                            0, //x cord
                            0, //y cord
                            1, //width (pixels)
                            1, //height (pixels)
                            NULL, //parent window
                            NULL, //menu
                            GetModuleHandle(NULL), //application instance
                            NULL);
                    mDeviceContext = GetDC(mDummyWindow);

                    if(mDeviceContext == NULL){
                        LIE_DLOG_ERROR("Win32DeviceImpl","Device context recieved from dummy window is NULL");
                    }

                    //set the pixel format of the window so we dont invalid pixel format errors
                    PIXELFORMATDESCRIPTOR pfd = {
                        sizeof(PIXELFORMATDESCRIPTOR),   // size of this pfd
                        1,                     // version number
                        PFD_DRAW_TO_WINDOW |   // support window
                        PFD_SUPPORT_OPENGL |   // support OpenGL
                        PFD_DOUBLEBUFFER,      // double buffered
                        PFD_TYPE_RGBA,         // RGBA type
                        24,                    // 24-bit color depth
                        0, 0, 0, 0, 0, 0,      // color bits ignored
                        0,                     // no alpha buffer
                        0,                     // shift bit ignored
                        0,                     // no accumulation buffer
                        0, 0, 0, 0,            // accum bits ignored
                        32,                    // z-buffer
                        0,                     // no stencil buffer
                        0,                     // no auxiliary buffer
                        PFD_MAIN_PLANE,        // main layer
                        0,                     // reserved
                        0, 0, 0                // layer masks ignored
                    };
                    // get the best available match of pixel format for the device context
                    int  iPixelFormat = ChoosePixelFormat(this->mDeviceContext, &pfd);

                    // make that the pixel format of the device context
                    SetPixelFormat(this->mDeviceContext, iPixelFormat, &pfd);



                    if(mDeviceContext){
                        //then created window succsessfully
                        LIE_DLOG_TRACE("Win32DeviceImpl","Device context created from dummy window");
                    } else {
                        LIE_DLOG_ERROR("Win32DeviceImpl","Failed to create device context from dummy window!")
                    }
                }

                lie::rend::RenderContext* createContext(RenderContext* shared){
                    RenderContext* c = new RenderContext();
                    LIE_DLOG_TRACE("Win32DeviceImpl","Creating new RenderContext");
                    c->mImpl->mDeviceContext = this->mDeviceContext;
                    c->mImpl->mHandle = wglCreateContext(GetDC(this->mDummyWindow));

                    if(!c->mImpl->mHandle){
                        //then an error occurred and we could not create the render context, log the error
                        DWORD error = GetLastError();
                        LIE_DLOG_ERROR("Win32DeviceImpl","Failed to create RenderContext (HGLRC) from DeviceContext! Msg: "
                                       << lie::core::detail::win32ErrorToString(error) << "(code: " << error << ")");
                    } else {
                        LIE_DLOG_TRACE("Win32DeviceImpl","Succsessfully created RenderContext");
                    }

                    if(shared != nullptr){
                        //then share new context with passed in one
                        if(!wglShareLists(shared->mImpl->mHandle, c->mImpl->mHandle)){
                            DWORD error = GetLastError();
                            LIE_DLOG_ERROR("Win32DeviceImpl","Failed to shared new context Msg: "
                                       << lie::core::detail::win32ErrorToString(error) << "(code: " << error << ")");
                        } else {
                            LIE_DLOG_TRACE("Win32DeviceImpl","Succsessfully shared new RenderContext");
                        }
                    }

                    return c;
                }
                void shareWith(RenderContext context){

                }
		    };

		}
	}
}


#endif //LIE_RENDER_WIN32DEVICE_HPP
