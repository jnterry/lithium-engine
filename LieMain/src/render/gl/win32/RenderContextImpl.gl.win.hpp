#ifndef LIE_RENDER_WIN32RENDERCONTEXTIMPL_HPP
#define LIE_RENDER_WIN32RENDERCONTEXTIMPL_HPP

#include <windows.h>

namespace lie{
    namespace rend{
        namespace detail{

            //:TODO: Resource leak -> wglDeleteContext() is never called!
            //could do in destructor but we don't want to delete it if we copy the instance then delete the old one
            //reference count?

            struct RenderContextImpl{
                HGLRC mHandle;//the windows "handle to opengl rendering context"
                HDC mDeviceContext; //the device context the render context was constructed from
                void makeCurrent(){
                    wglMakeCurrent(this->mDeviceContext, this->mHandle);
                }
            };

        }
    }
}


#endif // LIE_RENDER_WIN32RENDERCONTEXTIMPL_HPP
