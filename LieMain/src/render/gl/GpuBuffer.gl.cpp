#include <lie/render\GpuBuffer.hpp>
#include "GpuBufferImpl.gl.hpp"
#include "OpenGL.hpp"
namespace lie{
    namespace rend{

        GpuBuffer::GpuBuffer(UsageType usage) : mUsageType(usage){
            mImpl = new lie::rend::detail::GpuBufferImpl;
            glGenBuffers(1, &this->mImpl->glId);
        }

        GpuBuffer::~GpuBuffer(){
            glDeleteBuffers(1, &this->mImpl->glId);
            delete mImpl;//clean up internal data struct
        }

        void GpuBuffer::bufferData(const void* data, unsigned int dataSize){
            GLenum bufType;
            switch(this->mUsageType){
            case UsageType::Geometry:
                bufType = GL_ARRAY_BUFFER;
                break;
            case UsageType::Index:
                bufType = GL_ELEMENT_ARRAY_BUFFER;
                break;
            }
            lieCheckGl(glBindBuffer(bufType, this->mImpl->glId));
            //:TODO: allow user to specify usage hints with a defaulted parameter
            lieCheckGl(glBufferData(bufType, dataSize, data, GL_STATIC_DRAW));
            lieCheckGl(glBindBuffer(bufType, 0));
        }

    }
}
