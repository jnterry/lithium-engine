/////////////////////////////////////////////////
/// \file PassGL.cpp
/// \brief OpenGL implementations of API dependent Pass functions
/////////////////////////////////////////////////


#include <lie/render\Pass.hpp>
#include "OpenGL.hpp"

namespace {
    int faceModeToGlMode(lie::rend::FaceMode mode){
        switch(mode){
        case lie::rend::FaceMode::PointCloud:
            return GL_POINT;
        case lie::rend::FaceMode::Wireframe:
            return GL_LINE;
        case lie::rend::FaceMode::Filled:
            return GL_FILL;
        case lie::rend::FaceMode::Cull:
            return GL_FILL; //culling set separately
        }
    }
}


namespace lie{
    namespace rend{

        void Pass::_useApiSpecific(){
            /////////////////////////////////////////////////////////////////////
            //set up face mode
            if(this->mFrontFaceMode == FaceMode::Cull){
                if(this->mBackFaceMode == FaceMode::Cull){
                    //then cull both front and back
                    glEnable(GL_CULL_FACE);
                    glCullFace(GL_FRONT_AND_BACK);
                } else {
                    //only cull front
                    glEnable(GL_CULL_FACE);
                    glCullFace(GL_FRONT);
                }
            } else {
                //dont cull front
                if(this->mBackFaceMode == FaceMode::Cull){
                    //do cull back
                    glEnable(GL_CULL_FACE);
                    glCullFace(GL_BACK);
                } else {
                    //dont cull anything
                    glDisable(GL_CULL_FACE);
                }
            }
            glPolygonMode(GL_FRONT, faceModeToGlMode(this->mFrontFaceMode));
            glPolygonMode(GL_BACK, faceModeToGlMode(this->mBackFaceMode));

            /////////////////////////////////////////////////////////////////////
            //set up line/point size
            glLineWidth(this->mLineWidth);
            glPointSize(this->mPointSize);

            /////////////////////////////////////////////////////////////////////
            //Depth testing
            if(this->mDepthTestMode == lie::rend::DepthTestMode::None){
                glDisable(GL_DEPTH_TEST);
            } else {
                glEnable(GL_DEPTH_TEST);
                switch(this->mDepthTestMode){
                case lie::rend::DepthTestMode::LessThan:
                    glDepthFunc(GL_LESS);
                    break;
                case lie::rend::DepthTestMode::GreaterThan:
                    glDepthFunc(GL_GREATER);
                    break;
                }
            }

        }

    }
}
