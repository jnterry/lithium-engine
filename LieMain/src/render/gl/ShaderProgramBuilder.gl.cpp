/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ShaderProgramBuilderGl.hpp
/// Contains the render api specific implementation for the ShaderProgramBuilder class,
/// this file contains the opengl implementation
/// 
/// \ingroup 
/////////////////////////////////////////////////

#include <vector>

#include <lie/render/ShaderProgramBuilder.hpp>
#include <lie/render/Shader.hpp>
#include <lie/render/ShaderProgram.hpp>
#include "ShaderProgramImpl.gl.hpp"
#include "OpenGL.hpp"

namespace lie{
	namespace rend{
		ShaderProgram* ShaderProgramBuilder::build(){
			if (this->mState != State::NOT_BUILT){
				return nullptr;
			}

			this->mSProgImpl = new detail::ShaderProgramImpl;
			this->mSProgImpl->mRef = lieCheckGlReturn(glCreateProgram());

			for (const Shader* shader : this->mShaders){
				glAttachShader(this->mSProgImpl->mRef, shader->mRef);
			}
			glLinkProgram(this->mSProgImpl->mRef);
			for (const Shader* shader : this->mShaders){
				glDetachShader(this->mSProgImpl->mRef, shader->mRef);
			}

			GLint status;
			glGetProgramiv(this->mSProgImpl->mRef, GL_LINK_STATUS, &status);
			if (status != GL_TRUE){
				this->mState = State::BUILD_FAILED;
				return nullptr;
			} else {
				glGetProgramiv(this->mSProgImpl->mRef, GL_ACTIVE_UNIFORMS, &this->mSProgImpl->mParamCount);

				std::vector<detail::ShaderProgramImpl::ParamData> params;
				std::vector<detail::ShaderProgramImpl::TextureChannelData> textureChannels;
				

				GLsizei length;
				GLint size;
				GLenum glType;
				for (int i = 0; i < this->mSProgImpl->mParamCount; ++i){
					GLchar* name = new GLchar[128];
					glGetActiveUniform(this->mSProgImpl->mRef, i, 128, &length, &size, &glType, name);
					std::string uniName = std::string(name);
					int uniLocation = lieCheckGlReturn(glGetUniformLocation(this->mSProgImpl->mRef, uniName.c_str()));
					switch (glType){
					case GL_FLOAT_MAT4:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Mat4x4f);
						break;
					case GL_FLOAT_MAT3:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Mat3x3f);
						break;
					case GL_FLOAT_VEC4:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Vec4f);
						break;
					case GL_FLOAT_VEC3:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Vec3f);
						break;
					case GL_FLOAT_VEC2:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Vec2f);
						break;
					case GL_FLOAT:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Float);
						break;
					case GL_INT:
						params.emplace_back(uniName, uniLocation, ShaderParameterType::Int32);
						break;
					case GL_SAMPLER_2D:
						textureChannels.emplace_back(uniName, uniLocation);
						break;
					default:
						LIE_DLOG_REPORT("ShaderProgramBuilderGL", "Found uniform of unknown type, name: " << uniName << ", location: " << uniLocation << ", glType: " << glType);
						break;
					}
				}

				this->mSProgImpl->mParamCount = params.size();
				this->mSProgImpl->mParamData = new detail::ShaderProgramImpl::ParamData[params.size()];
				for (int i = 0; i < params.size(); ++i){
					this->mSProgImpl->mParamData[i].name = params[i].name;
					this->mSProgImpl->mParamData[i].uniformLocation = params[i].uniformLocation;
					this->mSProgImpl->mParamData[i].type = params[i].type;
				}
				
				this->mSProgImpl->mTextureChannelCount = textureChannels.size();
				this->mSProgImpl->mTextureData = new detail::ShaderProgramImpl::TextureChannelData[textureChannels.size()];
				for (int i = 0; i < textureChannels.size(); ++i){
					this->mSProgImpl->mTextureData[i].name = textureChannels[i].name;
					this->mSProgImpl->mTextureData[i].uniformLocation = textureChannels[i].uniformLocation;
				}

				this->mState = State::BUILD_SUCCESSFUL;
				return new ShaderProgram(this->mSProgImpl);
			}
		}

		std::string ShaderProgramBuilder::_getErrorsAPI() const{
			//get length of info log
			GLint infoLength;
			glGetProgramiv(this->mSProgImpl->mRef, GL_INFO_LOG_LENGTH, &infoLength);

			//make char array of that length (+1 for NULL terminator), fill it with log
			GLchar* logData = new GLchar[infoLength + 1];
			glGetProgramInfoLog(this->mSProgImpl->mRef, infoLength, NULL, logData);

			//copy char string into std::string, delete the char array, return the string
			std::string result = logData;
			delete[] logData;
			return result;
		}

		Shader& ShaderProgramBuilder::createDefault2dVertexShader(){
			Shader* s = new Shader(lie::rend::Shader::Type::Vertex);
			std::string source = "\
				#version 330 core\n\
				layout(location = 0) in vec2 vertPosition;\n\
				layout(location = 1) in vec4 vertColor;\n\
				smooth out vec4 color;\n\
				uniform mat3 mvpMatrix = mat3(1, 0, 0, 0, 1, 0, 0, 0, 1);\n\
				void main(){\n\
					gl_Position = vec4((mvpMatrix * vec3(vertPosition, 1)), 1);\n\
					gl_Position.zw = vec2(0,1);\n\
					color = vertColor;\n\
				}";
			s->loadAndCompile(source);
			return *s;
		}

		Shader& ShaderProgramBuilder::createDefault2dPixelShader(){
			Shader* s = new Shader(lie::rend::Shader::Type::Pixel);
			std::string source = "\
				#version 330 core\n\
				uniform sampler2D diffuse;\n\
				smooth in vec4 color;\n\
				smooth out vec4 outputColor;\n\
				uniform vec3 ambientLightColor = vec3(0.6, 0.6, 0.6);\n\
				void main(){\n\
					outputColor = vec4(1,1,0,1);\n\
				}";
			s->loadAndCompile(source);
			return *s;
		}
	}
}