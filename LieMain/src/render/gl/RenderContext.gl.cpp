#include <lie/render\RenderContext.hpp>
#include <lie/core\Config.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "win32/RenderContextImpl.gl.win.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxRenderContextImpl.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXRenderContextImpl.hpp"
#else
    #error
#endif

namespace lie{
    namespace rend{
        void RenderContext::makeCurrent(){
            this->mImpl->makeCurrent();
        }

        RenderContext::RenderContext() : mImpl(nullptr){
            this->mImpl = new lie::rend::detail::RenderContextImpl();
        }

        RenderContext::~RenderContext(){
			if (this->mImpl != nullptr){
				delete this->mImpl;
			}
        }

    }
}
