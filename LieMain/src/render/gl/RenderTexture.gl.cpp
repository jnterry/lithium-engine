/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Contains opengl specific implementations for functions in 
/// lie::rend::RenderTexture
/// 
/// \ingroup render
/////////////////////////////////////////////////

#include <lie/render/RenderTexture.hpp>
#include <lie/render/BufferTypes.hpp>
#include "RenderTextureImpl.gl.hpp"
#include "OpenGL.hpp"

namespace lie{
	namespace rend{

		void RenderTexture::_apiDestroy(){
			glDeleteFramebuffers(1, &this->mImpl->mRef);
		}

		void RenderTexture::makeCurrent(){
			glBindFramebuffer(GL_FRAMEBUFFER, this->mImpl->mRef);
		}
	}
}
