/////////////////////////////////////////////////
//Part of Lithium Engine - By Jamie Terry
/////////////////////////////////////////////////
/// \file Texture.cpp
/// \author Jamie Terry
/// \brief Contains implementation of render api dependent Texture functions
/// This file holds the opengl implementation
/////////////////////////////////////////////////

#include "OpenGl.hpp"
#include "TextureImpl.gl.hpp"
#include <lie/render\Texture.hpp>


namespace lie{
    namespace rend{

		Texture::Texture(int width, int height, PixelFormat format)
			: mImpl(new lie::rend::detail::TextureImpl), mSize(width, height), mPixelFormat(format){
			lieCheckGl(glGenTextures(1, &this->mImpl->mTextureId));
			this->_storeOnGpu(nullptr);
		}

        Texture::~Texture(){
            glDeleteTextures(1, &this->mImpl->mTextureId);
            delete this->mImpl;
        }

        void Texture::_storeOnGpu(unsigned char* data){
            lieCheckGl(glBindTexture(GL_TEXTURE_2D, this->mImpl->mTextureId));

            //:TODO: Make this configurable?
            lieCheckGl(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
            lieCheckGl(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
            lieCheckGl(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
            lieCheckGl(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

			GLint internalFormat;
			GLenum format, type;
			switch (this->mPixelFormat){
			case PixelFormat::RGBA_8U:
				internalFormat = GL_RGBA;
				format = GL_RGBA;
				type = GL_UNSIGNED_BYTE;
				break;
			case PixelFormat::RGB_8U:
				internalFormat = GL_RGB;
				format = GL_RGB;
				type = GL_UNSIGNED_BYTE;
				break;
			case PixelFormat::DEPTH_24U:
				internalFormat = GL_DEPTH_COMPONENT24;
				format = GL_DEPTH_COMPONENT;
				type = GL_UNSIGNED_BYTE;
				break;
			default:
				internalFormat = GL_RGBA;
				format = GL_RGBA;
				type = GL_UNSIGNED_BYTE;
				break;
			}

			lieCheckGl(glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, this->mSize.x, this->mSize.y, 0, format, type,
									(data == nullptr ? 0 : data)));
        }

        void Texture::bind(UnitId unit){
            lieCheckGl(glActiveTexture(GL_TEXTURE0 + unit));
            lieCheckGl(glBindTexture(GL_TEXTURE_2D, this->mImpl->mTextureId));
        }

        void Texture::clearBound(UnitId unit){
            lieCheckGl(glActiveTexture(GL_TEXTURE0 + unit));
            lieCheckGl(glBindTexture(GL_TEXTURE_2D, 0));
        }


    }
}

