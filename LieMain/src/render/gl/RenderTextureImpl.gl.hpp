/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file RenderTextureImpl.gl.hpp
/// Contains deceleration of lie::rend::detail::RenderTextureImpl
/// for opengl
/// 
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_DETAIL_RENDERTEXTUREIMPL_HPP
#define LIE_RENDER_DETAIL_RENDERTEXTUREIMPL_HPP

#include "OpenGL.hpp"

namespace lie{
	namespace rend{
		namespace detail{
			class RenderTextureImpl{
			public:
				///< OpenGl handle to the FrameBuffer object
				GLuint mRef;
			};
		}
	}
}

#endif //LIE_RENDER_DETAIL_RENDERTEXTUREIMPL_HPP