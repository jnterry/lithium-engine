#include <lie/render/Renderer3dImmediate.hpp>
#include <lie/render/Mesh.hpp>
#include <lie/core\Config.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/render/Technique.hpp>
#include "OpenGL.hpp"
#include "GpuBufferImpl.gl.hpp"
#include <lie/render/Material.hpp>
#include <lie/render/MaterialType.hpp>
#include <lie/render/Texture.hpp>
#include <lie/render/RenderTarget.hpp>
#include <iostream>

namespace {
    GLenum primativeTypeToGlType(lie::core::PrimativeType type){
        switch(type){
        case lie::core::PrimativeType::Int8:
            return GL_BYTE;
        case lie::core::PrimativeType::uInt8:
            return GL_UNSIGNED_BYTE;
        case lie::core::PrimativeType::Int16:
            return GL_SHORT;
        case lie::core::PrimativeType::uInt16:
            return GL_UNSIGNED_SHORT;
        case lie::core::PrimativeType::Int32:
            return GL_INT;
        case lie::core::PrimativeType::uInt32:
            return GL_UNSIGNED_INT;
        case lie::core::PrimativeType::Float:
            return GL_FLOAT;
        case lie::core::PrimativeType::Double:
            return GL_DOUBLE;
        }
    }
}

namespace lie{
    namespace rend{

        void Renderer3dImmediate::onBegin(RenderTarget& target){
			target.makeCurrent();
            glViewport(this->mViewport.getLeft(), this->mViewport.getBottom(), this->mViewport.getWidth(), this->mViewport.getHeight());
            this->mCamera.getTransformation(this->mViewport.getSize());
        }

        void Renderer3dImmediate::enableFog(bool enabled){
            this->mIsFogEnabled = enabled;
        }

        bool Renderer3dImmediate::isFogEnabled(){
            return this->mIsFogEnabled;
        }

        void Renderer3dImmediate::setFogColor(lie::rend::Color color){
            this->mFogColor = color;
        }

        lie::rend::Color Renderer3dImmediate::getFogColor(){
            return this->mFogColor;
        }

		void Renderer3dImmediate::draw(const lie::rend::Methodology& meth, const lie::rend::Material& mat, const lie::math::Matrix4x4f& modelMatrix, const lie::rend::Mesh& mesh){
			this->usePass(meth.technique.pass, mat, modelMatrix);

            for(int i = 0; i < mesh.getAttributeCount(); ++i){
                #ifdef LIE_DEBUG
                    if(mesh.getAttribute(i).gpuBuffer == nullptr){
                        LIE_DLOG_FATAL("RendererImmeiate", "Tried to render mesh with uninitiated attribute " << i << " (gpuBuffer set to nullptr), call mesh.getAttribute(i).set(...)");
                    }
                #endif // LIE_DEBUG
                lieCheckGl(glBindBuffer(GL_ARRAY_BUFFER, mesh.getAttribute(i).gpuBuffer->mImpl->glId));
                lieCheckGl(glEnableVertexAttribArray(i));
                lieCheckGl(glVertexAttribPointer(
                    i,
                    mesh.getAttribute(i).componentCount,
                    primativeTypeToGlType( mesh.getAttribute(i).type),
                     mesh.getAttribute(i).normalized,
                     mesh.getAttribute(i).stride,
                    (void*)( mesh.getAttribute(i).bufferOffset)
                ));
            }
            if(mesh.isIndexed()){
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.getIndexBuffer()->mImpl->glId);

                //get an "opengl numeric argument invalid" error here?
                //can be caused by the mesh vertex count being 0
                lieCheckGl(glDrawElements(GL_TRIANGLES, mesh.getVertexCount(), GL_UNSIGNED_INT, (GLvoid*)mesh.getIndexBufferOffset()));
            } else {
                //draw in direct mode
                lieCheckGl(glDrawArrays(GL_TRIANGLES, 0, mesh.getVertexCount()));
            }


            for(int i = 0; i < mesh.getAttributeCount(); ++i){
                lieCheckGl(glDisableVertexAttribArray(i));
            }
            lieCheckGl(glBindBuffer(GL_ARRAY_BUFFER, 0)); //clear the bound buffer
        }

    }
}
