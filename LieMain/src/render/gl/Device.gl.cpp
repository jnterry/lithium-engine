#include <lie/render\Device.hpp>
#include "OpenGL.hpp"
#include <lie/core\Logger.hpp>
#include <lie/core\Config.hpp>
#include <lie/render/MaterialTypeBuilder.hpp>
#include <lie/render/RenderTexture.hpp>
#include <lie/render/Texture.hpp>
#include "RenderTextureImpl.gl.hpp"
#include "TextureImpl.gl.hpp"
#include <lie/render/PixelFormat.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "win32/DeviceImpl.gl.win.hpp"
//#elif defined(LIE_OS_LINUX)
//#elif defined(LIE_OS_MACOS)
#else
    #error Need to supply implementation for this platform!
#endif

namespace{
    bool initedGlew = false;
}

namespace lie{
    namespace rend{

		Device* Device::createDevice(){
			Device* rsys = new Device();

			//now that we have a valid opengl context and it is active on this thread we can initiate glew
			//to load all the needed open gl extensions
			if (!initedGlew){
				glewExperimental = GL_TRUE;

				GLenum glewInitStatus = glewInit();

				if (glewInitStatus != GLEW_OK){
					LIE_DLOG_ERROR("DeviceGL", "Failed to init glew! Code: " << glewInitStatus
						<< ", Msg: " << glewGetErrorString(glewInitStatus));
					delete rsys;
					return nullptr;
				}
				else {
					LIE_DLOG_INFO("DeviceGL", "Succsesfully initiated glew");
					LIE_DLOG_INFO("DeviceGL", "New GL Render System Created. API: " << rsys->getApiNameFull());
					initedGlew = true;
					return rsys;
				}
			}
		}

        Device::Device(){
            //create the implementation structure for this render system, the correct one will be made based off which #include we used
            this->mImpl = new lie::rend::detail::DeviceImpl;

            //create the default context for this render system and add it to the list of contexts
            //??? this->mContexts.push_back(this->mImpl->createContext()); results in an error, doing it in two stages works... WHAT?
            RenderContext* c = this->mImpl->createContext(nullptr); //create new context that is shared with null
            this->mContexts.push_back(c);

            //make the default context the current one
			this->mContexts[0]->makeCurrent();
        }

        Device::~Device(){

        }


        std::string Device::getApiName() const{
            return "OpenGL";
        }

        std::string Device::getApiNameFull() const{
            //why oh why does opengl return a const unsigned char* rather than just a const char*?
			const unsigned char* name = glGetString(GL_VERSION);
			if (name != nullptr && name != 0){
				return std::string("OpenGl - ").append(reinterpret_cast<const char*>(glGetString(GL_VERSION)));
			} else {
				return "OpenGl - ERROR";
			}
        }

        int Device::getApiMajorVersion() const{
            int result;
            glGetIntegerv(GL_MAJOR_VERSION, &result);
            return result;
        }

        int Device::getApiMinorVersion() const{
            int result;
            glGetIntegerv(GL_MINOR_VERSION, &result);
            return result;
        }

        int Device::querryFeature(Feature feat) const{
			int num = 0;
            switch(feat){
			case Feature::MaxMeshAttributes:
				glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &num);
				return num;
            case Feature::MaxTextureUnits:
                return 31;
			case Feature::MaxRenderTextureColorBuffers:
				glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &num);
				return num;
			case Feature::MaxRenderTextureWidth:
				glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &num);
				return num;
			case Feature::MaxRenderTextureHeight:
				glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &num);
				return num;
			case Feature::MaxPixelShaderColorOutputs:
				glGetIntegerv(GL_MAX_DRAW_BUFFERS, &num);
				return num;
			case Feature::MaxTextureDimension:
				glGetIntegerv(GL_MAX_TEXTURE_SIZE, &num);
				return num;
			case Feature::MaxTexturePixelCount:
				glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &num);
				return num;
			case Feature::MaxVertexAttributes:
				glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &num);
				return num;
            default:
                return 0;
            }
        }

        RenderContext* Device::createContext(){
            //create new context that is shared with the default context
            return this->mImpl->createContext(this->mContexts[0]);
        }

        RenderContext* Device::getDefaultContext(){
            return this->mContexts[0];
        }

        Window* Device::createWindow(VideoMode videomode, std::string title, int style){
            Window* win = new Window(videomode, title, style);
            //this->mImpl->shareWith(win.getRenderContext());
			win->makeCurrent();
            return win;
        }

        Mesh* Device::createMesh(unsigned char attributeCount) throw(lie::core::ExcepOutOfRange<unsigned char>){
			int hardwareMax = this->querryFeature(Feature::MaxMeshAttributes);
			if(attributeCount > hardwareMax){
				throw lie::core::ExcepOutOfRange<unsigned char>(__FUNCTION__, "attribute count", attributeCount, 0, hardwareMax,
					"Attempted to make a mesh with " + lie::core::toString(attributeCount) + " attributes but this exceeds the hardware limit");
            }
            return new lie::rend::Mesh(attributeCount);
        }

		MaterialTypeBuilder Device::createMaterialType(unsigned char textureCount){
			return MaterialTypeBuilder(textureCount, *this);
		}

		RenderTexture* Device::createRenderTexture(int width, int height, int numColorBuffers, bool depthBuffer, bool stencilBuffer){
			///////////////////////////////////////////////////////////////
			//Check the hardware supports a framebuffer with the requested attributes
			if (this->querryFeature(Feature::MaxRenderTextureColorBuffers) < numColorBuffers){
				LIE_DLOG_ERROR("DeviceGL", "Attempted to create a RenderTexture with " << numColorBuffers
											<< " color buffers, but the maximum supported number is "
											<< this->querryFeature(Feature::MaxRenderTextureColorBuffers));
				return nullptr;
			}
			if (this->querryFeature(Feature::MaxRenderTextureWidth) < width){
				LIE_DLOG_ERROR("DeviceGL", "Attempted to create a RenderTexture with a width of " << width
											<< "px, but the maximum supported width is "
											<< this->querryFeature(Feature::MaxRenderTextureWidth));
				return nullptr;
			}
			if (this->querryFeature(Feature::MaxRenderTextureHeight) < height){
				LIE_DLOG_ERROR("DeviceGL", "Attempted to create a RenderTexture with a height of " << height
											<< "px, but the maximum supported height is "
											<< this->querryFeature(Feature::MaxRenderTextureHeight));
				return nullptr;
			}

			///////////////////////////////////////////////////////////////
			//Create the textures that will be used as buffers
			detail::RenderTextureImpl* rtImpl = new detail::RenderTextureImpl();
			Texture* colorBuffersPtr = new Texture[numColorBuffers];
			for (int i = 0; i < numColorBuffers; ++i){
				colorBuffersPtr[i].setSize(width, height);
			}
			Texture* depthBufferPtr = nullptr;
			Texture* stencilBufferPtr = nullptr;

			///////////////////////////////////////////////////////////////
			//Create the framebuffer
			lieCheckGl(glGenFramebuffers(1, &rtImpl->mRef));
			lieCheckGl(glBindFramebuffer(GL_FRAMEBUFFER, rtImpl->mRef));

			///////////////////////////////////////////////////////////////
			//Attach the textures to the framebuffer
			for (int i = 0; i < numColorBuffers; ++i){
				lieCheckGl(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, colorBuffersPtr[i].mImpl->mTextureId, 0));
			}
			if (depthBuffer){
				depthBufferPtr = new Texture(width, height, PixelFormat::DEPTH_24U);
				lieCheckGl(glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthBufferPtr->mImpl->mTextureId, 0));
			}
			if (stencilBuffer){
				stencilBufferPtr = new Texture(width, height);
				lieCheckGl(glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencilBufferPtr->mImpl->mTextureId, 0));
			}

			///////////////////////////////////////////////////////////////
			//Specify which of the color buffers should be written to, this code means all color buffers can
			// be written to by shaders
			GLuint* bufferDrawTargets = new GLuint[numColorBuffers];
			for (int i = 0; i < numColorBuffers; ++i){
				bufferDrawTargets[i] = GL_COLOR_ATTACHMENT0 + i;
			}
			lieCheckGl(glDrawBuffers(numColorBuffers, bufferDrawTargets));
			delete bufferDrawTargets;

			///////////////////////////////////////////////////////////////
			//check if everything worked
			GLenum status = lieCheckGlReturn(glCheckFramebufferStatus(GL_FRAMEBUFFER));
			if (status != GL_FRAMEBUFFER_COMPLETE){
				std::string statusMsg;
				switch (status){
				case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
					statusMsg = "Incomplete Attachment";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
					statusMsg = "Missing Attachment";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
					statusMsg = "Incomplete Draw Buffer";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
					statusMsg = "Incomplete Read Buffer";
					break;
				default:
					"Unknown";
					break;
				}
				LIE_DLOG_ERROR("DeviceGL", "Attepted to create framebuffer with the resolution " << width
					<< "x" << height << " with " << numColorBuffers << " color buffers, " <<
					(depthBuffer ? "a depth buffer" : "no depth buffer") << " and " <<
					(stencilBuffer ? "a stencil buffer" : "no stencil buffer")
					<< " but an OpenGL error occured. Framebuffer Status: \"" << statusMsg 
					<< "\", code: " << status);
				delete[] colorBuffersPtr;
				if (depthBufferPtr != nullptr){
					delete depthBufferPtr;
				}
				if (stencilBufferPtr != nullptr){
					delete stencilBufferPtr;
				}

				lieCheckGl(glBindFramebuffer(GL_FRAMEBUFFER, 0)); //unbind the framebuffer
				return nullptr;
			}
			else {
				LIE_DLOG_TRACE("DeviceGL", "Successfully created framebuffer with the resolution " << width
					<< "x" << height << " with " << numColorBuffers << " color buffers, " <<
					(depthBuffer ? "a depth buffer" : "no depth buffer") << " and " <<
					(stencilBuffer ? "a stencil buffer" : "no stencil buffer")
					<< ". Framebuffer Status: " << status);

				lieCheckGl(glBindFramebuffer(GL_FRAMEBUFFER, 0)); //unbind the framebuffer
				return new RenderTexture(lie::math::Vector2i(width, height), numColorBuffers, colorBuffersPtr, depthBufferPtr, stencilBufferPtr, rtImpl);
			}
		}
    }
}
