#include "OpenGL.hpp"

namespace lie{
    namespace rend{
        namespace detail{

            void checkGl(int line, std::string file){
                GLenum lieDetailGlErrCode = glGetError();
                if(lieDetailGlErrCode != GL_NO_ERROR){
                    switch(lieDetailGlErrCode){
                    case GL_INVALID_ENUM:{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line)
                            << "Internal OpenGL Error occured: Invalid value passed as enum argument. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_INVALID_VALUE:{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Numeric argument invalid. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_INVALID_OPERATION:{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Operation invalid in current state. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_STACK_OVERFLOW:{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Requested command would cause a stack overflow. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_STACK_UNDERFLOW:{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Requested command would cause a stack underflow. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_OUT_OF_MEMORY :{
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Out of memory. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    case GL_INVALID_FRAMEBUFFER_OPERATION_EXT:{ \
                        lie::core::DLog.write(lie::core::RTimeMsg(lie::core::RTimeMsg::Error, file, file, line) <<
                            "Internal OpenGL Error occured: Framebuffer Error. (code " << lieDetailGlErrCode << ")");
                        break;
                    }
                    }

                }
            }

        }
    }
}
