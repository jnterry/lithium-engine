#include <lie/render/DistanceMap.hpp>
#include <lie/render/Device.hpp>
#include <lie/render/Texture.hpp>
#include <lie/render/RenderTexture.hpp>
#include <lie/render/Shader.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/render/Material.hpp>
#include "OpenGL.hpp"

namespace{
	lie::rend::ShaderProgram* distMapShader = nullptr;
	bool createDistMapShader(){
		lie::rend::Shader vertShader(lie::rend::Shader::Type::Vertex);
		vertShader.loadAndCompile(
			"\
			#version 330 core\n\
			layout(location = 0) in vec2 vertPos; \n\
			out vec2 pos; \n\
			void main(){\n\
				gl_Position = vertPos; \n\
				pos = vertPos; \n\
			}\n"); //end vertex shader

		lie::rend::Shader pixelShader(lie::rend::Shader::Type::Pixel);
		return true;
	}
}

namespace lie{
	namespace rend{

		Texture* createDistanceMap(Texture& original, Device& device, float threshold){
			if (distMapShader == nullptr && !createDistMapShader()){
				LIE_DLOG_ERROR("DistanceMap.gl", "Attepmted to create distance map shader but failed to create the distance map shader");
				return nullptr;
			}


			RenderTexture* result = device.createRenderTexture(original.getWidth(), original.getHeight(), 1, false, false);
			if (result == nullptr){
				LIE_DLOG_ERROR("DistanceMap.gl", "Attepmted to create distance map shader but failed to create the RenderTexture");
			}

			return result->getColorBuffer(0);
		}
	}
}