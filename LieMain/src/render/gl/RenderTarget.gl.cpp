#include <lie/render/RenderTarget.hpp>
#include "OpenGL.hpp"

namespace lie{
	namespace rend{
		void RenderTarget::clear(char buffers){
			this->makeCurrent();
			GLint buffs = 0;
			//glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
			if (buffers & lie::rend::BufferTypes::Color){ buffs |= GL_COLOR_BUFFER_BIT; }
			if (buffers & lie::rend::BufferTypes::Depth){ buffs |= GL_DEPTH_BUFFER_BIT; }
			if (buffers & lie::rend::BufferTypes::Stencil){ buffs |= GL_STENCIL_BUFFER_BIT; }
			glClear(buffs);
		}
	}
}