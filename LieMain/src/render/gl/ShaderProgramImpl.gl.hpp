/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Contains the opengl specific implementation for ShaderProgram
/// 
/// \ingroup 
/////////////////////////////////////////////////

#ifndef LIE_RENDER_DETAIL_SHADERPROGRAMIMPL_HPP
#define LIE_RENDER_DETAIL_SHADERPROGRAMIMPL_HPP

#include "OpenGL.hpp"

namespace lie{
	namespace rend{
		namespace detail{
			class ShaderProgramImpl{
			public:
				~ShaderProgramImpl();

				GLint mRef;

				//:TODO:, struct of arrays rather than array of structs may be quicker if
				//getting locations in the rendering loop!
				struct ParamData{
					ParamData() : ParamData("", 0, ShaderParameterType::Unknown){}
					ShaderProgramImpl::ParamData::ParamData(std::string newName, int uniLocation, ShaderParameterType newType);
					int uniformLocation;
					std::string name;
					ShaderParameterType type;
				};

				struct TextureChannelData{
					TextureChannelData() : TextureChannelData("", 0){}
					TextureChannelData(std::string newName, int uniLocation);
					int uniformLocation;
					std::string name;
				};

				///< The number of parameters the shader program has
				int mParamCount;

				///< Pointer to array of data about the parameters
				ParamData* mParamData;

				///< The number of texture channels in this ShaderProgram
				int mTextureChannelCount;

				///< Pointer to array of data about the texture channels
				TextureChannelData* mTextureData;
			};
		}
	}
}

#endif //LIE_RENDER_DETAIL_SHADERPROGRAMIMPL_HPP