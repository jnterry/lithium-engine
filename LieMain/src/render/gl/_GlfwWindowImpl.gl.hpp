/////////////////////////////////////////////////
/// \brief This implementation of window is based on the glfw libary
/// It is written for a quick working version however is not overly efficient
/// and does not provide the flexibility desired.
/// It should however work on most OSs and so can be used where an OS specific
/// implementation has not yet been written
/////////////////////////////////////////////////
#ifndef LIE_RENDER_GLFWWINDOWIMPL_HPP
#define LIE_RENDER_GLFWWINDOWIMPL_HPP

#include "OpenGL.hpp"
#include <GLFW/glfw3.h>
#include <lie/render\Window.hpp>
#include <map>
#include <lie/render\Event.hpp>
#include <lie/math\Vector2.hpp>
#include <lie/core/Logger.hpp>
#include <lie/core/NonCopyable.hpp>


namespace lie{
    namespace rend{
        namespace detail{
            ////////////////////////////////////////////////////////////////
            //these are in the namespace rather than being static vars in the class as there is no cpp file for this impl
            //instead it is included in the file "Wjndow.cpp" when it wishes to use this implementation of the Window
            //as it is only included in Window.cpp these do not clutter the namespace elsewhere
            bool initedGLFW;

            //urgh, map lookup on every event, another reason to ditch glfw and do it myself...
            std::map<GLFWwindow*, lie::rend::Window*> windowMap;

            ////////////////////////////////////////////////////////////////
            //Class Definition
            class WindowImpl : lie::core::NonCopyable{
            private:
                GLFWwindow* mWin;
                lie::rend::Window* mLWin;

                ////////////////////////////////////////////////////////////////
                //glfw event callbacks
                static void glfwEventHandlerTextInput(GLFWwindow* mWin, unsigned int codepoint){
                    lie::rend::Event e;
                    e.type = lie::rend::Event::TextEntered;
                    e.text.unicode = codepoint;
                    windowMap[mWin]->pushEvent(e);
                }

                static void glfwEventHandlerMouseEntered(GLFWwindow* mWin, int entered){
                    lie::rend::Event e;
                    e.type = (entered == GL_TRUE ? lie::rend::Event::MouseEntered : lie::rend::Event::MouseLeft);
                    windowMap[mWin]->pushEvent(e);
                }

                static void glfwEventHandlerMouseMove(GLFWwindow* mWin, double newx, double newy){
                    lie::rend::Event e;
                    e.type = lie::rend::Event::MouseMoved;
                    e.mouse.x = newx;
                    e.mouse.y = newy;
                    e.mouse.button = lie::rend::Mouse::getButton();
                    windowMap[mWin]->pushEvent(e);
                }

                static void glfwEventHandlerClosed(GLFWwindow* mWin){
                    lie::rend::Event e;
                    e.type = lie::rend::Event::Closed;
                    windowMap[mWin]->pushEvent(e);
                }

                static void glfwEventHandlerKey(GLFWwindow* mWin, int key, int scancode, int action, int modkeys){
                    lie::rend::Event e;
                    if(action == GLFW_PRESS || action == GLFW_REPEAT){
                        e.type = lie::rend::Event::KeyPressed;
                    }else{
                        e.type = lie::rend::Event::KeyReleased;
                    }

                    switch(key){
                    case GLFW_KEY_UNKNOWN:{e.key.keycode = lie::rend::Keyboard::Unknown;break;}

                    //letters
                    case GLFW_KEY_A:{e.key.keycode = lie::rend::Keyboard::A;break;}
                    case GLFW_KEY_B:{e.key.keycode = lie::rend::Keyboard::B;break;}
                    case GLFW_KEY_C:{e.key.keycode = lie::rend::Keyboard::C;break;}
                    case GLFW_KEY_D:{e.key.keycode = lie::rend::Keyboard::D;break;}
                    case GLFW_KEY_E:{e.key.keycode = lie::rend::Keyboard::E;break;}
                    case GLFW_KEY_F:{e.key.keycode = lie::rend::Keyboard::F;break;}
                    case GLFW_KEY_G:{e.key.keycode = lie::rend::Keyboard::G;break;}
                    case GLFW_KEY_H:{e.key.keycode = lie::rend::Keyboard::H;break;}
                    case GLFW_KEY_I:{e.key.keycode = lie::rend::Keyboard::I;break;}
                    case GLFW_KEY_J:{e.key.keycode = lie::rend::Keyboard::J;break;}
                    case GLFW_KEY_K:{e.key.keycode = lie::rend::Keyboard::K;break;}
                    case GLFW_KEY_L:{e.key.keycode = lie::rend::Keyboard::L;break;}
                    case GLFW_KEY_M:{e.key.keycode = lie::rend::Keyboard::M;break;}
                    case GLFW_KEY_N:{e.key.keycode = lie::rend::Keyboard::N;break;}
                    case GLFW_KEY_O:{e.key.keycode = lie::rend::Keyboard::O;break;}
                    case GLFW_KEY_P:{e.key.keycode = lie::rend::Keyboard::P;break;}
                    case GLFW_KEY_Q:{e.key.keycode = lie::rend::Keyboard::Q;break;}
                    case GLFW_KEY_R:{e.key.keycode = lie::rend::Keyboard::R;break;}
                    case GLFW_KEY_S:{e.key.keycode = lie::rend::Keyboard::S;break;}
                    case GLFW_KEY_T:{e.key.keycode = lie::rend::Keyboard::T;break;}
                    case GLFW_KEY_U:{e.key.keycode = lie::rend::Keyboard::U;break;}
                    case GLFW_KEY_V:{e.key.keycode = lie::rend::Keyboard::V;break;}
                    case GLFW_KEY_W:{e.key.keycode = lie::rend::Keyboard::W;break;}
                    case GLFW_KEY_X:{e.key.keycode = lie::rend::Keyboard::X;break;}
                    case GLFW_KEY_Y:{e.key.keycode = lie::rend::Keyboard::Y;break;}
                    case GLFW_KEY_Z:{e.key.keycode = lie::rend::Keyboard::Z;break;}

                    //numbers
                    case GLFW_KEY_0:{e.key.keycode = lie::rend::Keyboard::Num0;break;}
                    case GLFW_KEY_1:{e.key.keycode = lie::rend::Keyboard::Num1;break;}
                    case GLFW_KEY_2:{e.key.keycode = lie::rend::Keyboard::Num2;break;}
                    case GLFW_KEY_3:{e.key.keycode = lie::rend::Keyboard::Num3;break;}
                    case GLFW_KEY_4:{e.key.keycode = lie::rend::Keyboard::Num4;break;}
                    case GLFW_KEY_5:{e.key.keycode = lie::rend::Keyboard::Num5;break;}
                    case GLFW_KEY_6:{e.key.keycode = lie::rend::Keyboard::Num6;break;}
                    case GLFW_KEY_7:{e.key.keycode = lie::rend::Keyboard::Num7;break;}
                    case GLFW_KEY_8:{e.key.keycode = lie::rend::Keyboard::Num8;break;}
                    case GLFW_KEY_9:{e.key.keycode = lie::rend::Keyboard::Num9;break;}
                    case GLFW_KEY_KP_0:{e.key.keycode = lie::rend::Keyboard::Numpad0;break;}
                    case GLFW_KEY_KP_1:{e.key.keycode = lie::rend::Keyboard::Numpad1;break;}
                    case GLFW_KEY_KP_2:{e.key.keycode = lie::rend::Keyboard::Numpad2;break;}
                    case GLFW_KEY_KP_3:{e.key.keycode = lie::rend::Keyboard::Numpad3;break;}
                    case GLFW_KEY_KP_4:{e.key.keycode = lie::rend::Keyboard::Numpad4;break;}
                    case GLFW_KEY_KP_5:{e.key.keycode = lie::rend::Keyboard::Numpad5;break;}
                    case GLFW_KEY_KP_6:{e.key.keycode = lie::rend::Keyboard::Numpad6;break;}
                    case GLFW_KEY_KP_7:{e.key.keycode = lie::rend::Keyboard::Numpad7;break;}
                    case GLFW_KEY_KP_8:{e.key.keycode = lie::rend::Keyboard::Numpad8;break;}
                    case GLFW_KEY_KP_9:{e.key.keycode = lie::rend::Keyboard::Numpad9;break;}

                    //:TODO: Special keys, punctuation etc

                    default:{e.key.keycode = lie::rend::Keyboard::Unknown;break;}
                    }
                    e.key.shift = (modkeys & GLFW_MOD_SHIFT);
                    e.key.ctrl = (modkeys & GLFW_MOD_CONTROL);
                    e.key.alt = (modkeys & GLFW_MOD_ALT);
                    e.key.system = (modkeys & GLFW_MOD_SUPER);
                    windowMap[mWin]->pushEvent(e);
                  }

                static void glfwEventHandlerMouseButton(GLFWwindow* mWin, int button, int action, int modkeys){
                    lie::rend::Event e;
                    e.type = lie::rend::Event::MouseButtonPressed;
                    double mx, my;
                    glfwGetCursorPos(mWin, &mx, &my);
                    e.mouse.x = mx;
                    e.mouse.y = my;
                    switch(button){
                    case GLFW_MOUSE_BUTTON_LEFT:
                        e.mouse.button = lie::rend::Mouse::Button::Left;
                        break;
                    case GLFW_MOUSE_BUTTON_RIGHT:
                        e.mouse.button = lie::rend::Mouse::Button::Right;
                         break;
                    case GLFW_MOUSE_BUTTON_MIDDLE:
                        e.mouse.button = lie::rend::Mouse::Button::Middle;
                         break;
                    case GLFW_MOUSE_BUTTON_4: //but 1,2,3 are left,right,middle respectively
                        e.mouse.button = lie::rend::Mouse::Button::XButton1;
                         break;
                    case GLFW_MOUSE_BUTTON_5:
                        e.mouse.button = lie::rend::Mouse::Button::XButton2;
                        break;
                    }
                    windowMap[mWin]->pushEvent(e);
                }

                static void glfwEventHandlerMouseWheel(GLFWwindow* mWin, double deltax, double deltay){

                }

                static void glfwErrorCallback(int error, const char* msg){
                    LIE_DLOG_ERROR("GlfwWindowImpl", "GLFW error " << error << " occured, msg: " << msg);
                }
            public:

                const VideoMode& getVideoMode() const{
                    lie::rend::VideoMode mode(0,0,32);
                    glfwGetWindowSize(this->mWin, &mode.size.x, &mode.size.y);
                    return mode;
                }

                void create(lie::rend::VideoMode videomode, std::string caption){
                    //if already created, close it
                    if(this->isOpen()){this->close();}

                    if(!initedGLFW){
                        //register the error callback so if an error occurs with init it is logged
                        glfwSetErrorCallback(&lie::rend::detail::WindowImpl::glfwErrorCallback);

                        LIE_DLOG_TRACE("GlfwWindowImpl", "Attempting to init glfw version: " << glfwGetVersionString());
                        if(!glfwInit()){
                            LIE_DLOG_FATAL("GlfwWindowImpl", "Failed to init glfw, cannot create window.");
                        } else {
                            LIE_DLOG_TRACE("GlfwWindowImpl", "Successfully inited glfw");
                            initedGLFW = true;
                        }
                    }
                    this->mWin = glfwCreateWindow(videomode.size.x, videomode.size.y, caption.c_str(), NULL, NULL);

                    //no fps cap, good for benchmarking
                    //vsync <- this comment is for searching only
                    glfwSwapInterval(0);

                    //add event callbacks, these push the events to the window's queue
                    glfwSetKeyCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerKey);
                    glfwSetMouseButtonCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerMouseButton);
                    glfwSetScrollCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerMouseWheel);
                    glfwSetCharCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerTextInput);
                    glfwSetCursorPosCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerMouseMove);
                    glfwSetCursorEnterCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerMouseEntered);
                    glfwSetWindowCloseCallback(this->mWin, &lie::rend::detail::WindowImpl::glfwEventHandlerClosed);

                    windowMap[this->mWin] = this->mLWin;
                }

                void  close(){
                    glfwDestroyWindow(this->mWin);
                    this->mWin = nullptr;
                    windowMap.erase(this->mWin);
                }

                void  minimise(){

                }

                void  restore(){
                    glfwRestoreWindow(this->mWin);
                }

                void  maximise(){
					//:TODO:
                }

                bool  isMinimised(){
                    return glfwGetWindowAttrib(this->mWin, GLFW_ICONIFIED);
                }

                bool  isMaximised(){
					return false; //:TODO:
                }

                bool isOpen(){
                    //the window is "open" when the pointer to it is not null
                    //(in glfw terms it has been "glfwCreateWindow()-ed but not yet glfwDestroyWindow()-ed)
                    return (this->mWin != nullptr);
                }

                void setPosition(int x, int y, Monitor monitor){
                    glfwSetWindowPos(this->mWin, x, y);
                }

                int  getXPosition(){
                    int xpos, ypos;
                    glfwGetWindowPos(this->mWin, &xpos, &ypos);
                    return xpos;
                }

                int  getYPosition(){
                    int xpos, ypos;
                    glfwGetWindowPos(this->mWin, &xpos, &ypos);
                    return ypos;
                }

                Monitor getCurrentMonitor(){
					return Monitor(); //:TODO:
                }

                void makeCurrent(){
                    if(this->mWin != nullptr){
						glBindFramebuffer(GL_FRAMEBUFFER, 0); //unbind any bound framebuffer
                        glfwMakeContextCurrent(this->mWin);
                    }
                }

                void display(){
                    if(this->mWin != nullptr){
                        glfwSwapBuffers(this->mWin);
                    }
                }

                bool hasFocus(){
                    return glfwGetWindowAttrib(this->mWin, GLFW_FOCUSED);
                }

                WindowImpl(lie::rend::Window* lwin) : mLWin(lwin), mWin(nullptr){
                    //empty body
                }

                ~WindowImpl(){
                    this->close();
                }

                void waitForEvent(){
                    updateEvents();
                }

                //this function must be called regularly so that the os does not think the window is non responsive
                void updateEvents(){
                    glfwPollEvents();//glfw callbacks will add events to the queue
                }
            };//end of WindowImpl class
        }//end of detail::
    }//end of rend::
}//end of lie::


#endif // LIE_RENDER_GLFWWINDOWIMPL_HPP
