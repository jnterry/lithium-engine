/////////////////////////////////////////////////
/// \file lie/render/opengl.h
/// \brief Convenience file - includes all opengl headers
/// Also contains the macro lieCheckGl which, in debug mode, checks
/// for opengl errors and logs them
/// \ingroup render
/////////////////////////////////////////////////

#ifndef LIE_RENDER_OPENGL_H
#define LIE_RENDER_OPENGL_H

#include <lie/core/Config.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <lie/core/Logger.hpp>

#ifdef LIE_DEBUG
    //in debug we want to check for gl errors and log them if they occur

    //use this for a gl function that returns something
    #define lieCheckGlReturn(call) (lie::rend::detail::checkGl((call), __LINE__, __FILE__))

    //use this for a gl function that does not return anything
    #define lieCheckGl(call) ((call), lie::rend::detail::checkGl(__LINE__, __FILE__))
#else
    //not in debug mode, we dont want any extra overhead!
    #define lieCheckGl(call) (call)
    #define lieCheckGlReturn(call) (call)
#endif

namespace lie{
    namespace rend{
        namespace detail{

            void checkGl(int line, std::string file);

            template<typename T>
            T checkGl(T in, int line, std::string file){
                checkGl(line, file);
                return in;
            }
        }//end of detail::
    }//end of rend::
}//end of lie::

#endif // LIE_RENDER_OPENGL_H


