#ifndef LIE_RENDER_DETAIL_GPUBUFFERIMPL_HPP
#define LIE_RENDER_DETIAL_GPUBUFFERIMPL_HPP

#include "OpenGL.hpp"

namespace lie{
    namespace rend{
        namespace detail{

            /////////////////////////////////////////////////
            /// \brief Opengl specific data structure for a GPU buffer
            /// Should not be externally exposed to client code!
            /////////////////////////////////////////////////
            struct GpuBufferImpl{
                GLuint glId;
                GpuBufferImpl():glId(0){/*empty body*/}
            };

        }
    }
}

#endif // LIE_RENDER_DETAIL_GPUBUFFERIMPL_HPP
