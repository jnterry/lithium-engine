/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Contains the opengl specific implementation for ShaderProgram
/// 
/// \ingroup 
/////////////////////////////////////////////////

#include <lie/render/ShaderProgram.hpp>
#include "ShaderProgramImpl.gl.hpp"
#include "OpenGL.hpp"

namespace lie{
	namespace rend{
		namespace detail{
			ShaderProgramImpl::ParamData::ParamData(std::string newName, int uniLocation, ShaderParameterType newType)
				: uniformLocation(uniLocation), name(newName), type(newType){
				//empty body
			}

			ShaderProgramImpl::~ShaderProgramImpl(){
				delete[] this->mParamData;
				delete[] this->mTextureData;
			}

			ShaderProgramImpl::TextureChannelData::TextureChannelData(std::string newName, int uniLocation)
				: name(newName), uniformLocation(uniLocation){
				//empty body
			}
		}

		ShaderProgram::~ShaderProgram(){
			glDeleteProgram(this->mImpl->mRef);
			delete this->mImpl;
		}

		void ShaderProgram::use() const{
			glUseProgram(this->mImpl->mRef);
		}

		void ShaderProgram::useNone(){
			glUseProgram(0);
		}

		bool ShaderProgram::operator==(const ShaderProgram& other) const{
			return this->mImpl->mRef == other.mImpl->mRef;
		}

		ShaderProgram::ShaderProgram(detail::ShaderProgramImpl* impl)
			: mImpl(impl){
			//empty body
		}

		int ShaderProgram::getParameterCount() const{
			return this->mImpl->mParamCount;
		}

		int ShaderProgram::getParameterLocation(int index) const{
			return this->mImpl->mParamData[index].uniformLocation;
		}

		std::string ShaderProgram::getParameterName(int index) const{
			return this->mImpl->mParamData[index].name;
		}

		ShaderParameterType ShaderProgram::getParameterType(int index) const{
			return this->mImpl->mParamData[index].type;
		}

		int ShaderProgram::getParameterIndex(std::string name) const{
			for (int i = 0; i < this->mImpl->mParamCount; ++i){
				if (this->getParameterName(i) == name){ return i; }
			}
			return -1;
		}

		int ShaderProgram::getTextureChannelCount() const {
			return this->mImpl->mTextureChannelCount;
		}

		std::string ShaderProgram::getTextureChannelName(int index) const{
			return this->mImpl->mTextureData[index].name;
		}

		int ShaderProgram::getTextureChannelIndex(std::string name) const{
			for (int i = 0; i < this->mImpl->mTextureChannelCount; ++i){
				if (this->getTextureChannelName(i) == name){ return i; }
			}
			return -1;
		}

		void ShaderProgram::setTextureChannelBinding(int channel, int boundUnit){
			glUniform1i(this->mImpl->mTextureData[channel].uniformLocation, boundUnit);
		}

		//
		//SET UNIFORM OVERLOADS
		//
		bool ShaderProgram::setParameter(int index, lie::math::Matrix4x4f value){
			//if (this->getParameterType(index) != ShaderParameterType::Mat4x4f){
				//return false;
			//} else {
				glUniformMatrix4fv(this->getParameterLocation(index), 1, GL_FALSE, &value[0][0]);
				return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, lie::math::Matrix3x3f value){
			//if (this->getParameterType(index) != ShaderParameterType::Mat3x3f){
				//return false;
			//} else {
				glUniformMatrix3fv(this->getParameterLocation(index), 1, GL_FALSE, &value[0][0]);
				return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, float value){
			//if (this->getParameterType(index) != ShaderParameterType::Float){
				//return false;
			//} else {
				glUniform1f(this->getParameterLocation(index), value);
				return true;
			//}
		}
		bool ShaderProgram::setParameter(int index, int value){
			//if (this->getParameterType(index) != ShaderParameterType::Int32){
				//return false;
			//} else {
				glUniform1i(this->getParameterLocation(index), value);
				return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, lie::math::Vector2f value){
			//if (this->getParameterType(index) != ShaderParameterType::Float){
			//return false;
			//} else {
			glUniform2f(this->getParameterLocation(index), value.x, value.y);
			return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, lie::math::Vector3f value){
			//if (this->getParameterType(index) != ShaderParameterType::Float){
				//return false;
			//} else {
				glUniform3f(this->getParameterLocation(index), value.x, value.y, value.z);
				return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, lie::math::Vector4f value){
			//if (this->getParameterType(index) != ShaderParameterType::Float){
			//return false;
			//} else {
			glUniform4f(this->getParameterLocation(index), value.x, value.y, value.z, value.w);
			return true;
			//}
		}

		bool ShaderProgram::setParameter(int index, Color value){
			//if (this->getParameterType(index) != ShaderParameterType::Vec4f){
				//return false;
			//} else {
				glUniform4f(this->getParameterLocation(index), value.r / 255.0f, value.g / 255.0f, value.b / 255.0f, value.a / 255.0f);
				return true;
			//}
		}
			
	}
}