#include <lie/render\Renderer2dImmediate.hpp>
#include <lie/render/Methodology.hpp>
#include <lie/render/Technique.hpp>
#include <lie/render/Pass.hpp>
#include "OpenGL.hpp"


namespace lie{
    namespace rend{

        Renderer2dImmediate::Renderer2dImmediate(unsigned short layerCount)
        : Renderer2d(layerCount){
            //empty body
        }

		void Renderer2dImmediate::onBegin(RenderTarget& target){
			target.makeCurrent();
            glViewport(mViewport.getLeft(), this->mViewport.getBottom(), this->mViewport.getWidth(), this->mViewport.getHeight());
        }

		void Renderer2dImmediate::draw(const Methodology& meth, const Material& mat, const math::Matrix3x3f& modelMatrix, unsigned short layer, lie::math::Shape2f& shape){
			this->usePass(*meth.technique.pass, mat, modelMatrix, layer);
			glBegin(GL_TRIANGLES);
			for (int i = 0; i < shape.size() -1; ++i){
				glVertex2f(shape.at(i).x, shape.at(i).y);
				glVertex2f(shape.at(0).x, shape.at(0).y);
				glVertex2f(shape.at(i+1).x, shape.at(i+1).y);
			}
			glEnd();
		}

        

    }
}
