#ifndef LIE_RENDER_GL_TEXTUREIMPL_HPP
#define LIE_RENDER_GL_TEXTUREIMPL_HPP

#include "OpenGl.hpp"

namespace lie{
    namespace rend{
        namespace detail{
            struct TextureImpl{
                ///GLuint id of the texture
                GLuint mTextureId;
            };
        }
    }
}

#endif // LIE_RENDER_GL_TEXTUREIMPL_HPP
