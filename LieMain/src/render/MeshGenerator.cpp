#include <lie/render/MeshGenerator.hpp>
#include <lie/render/MeshDataBuffer.hpp>
#include <lie/render/MeshLoaders.hpp>
#include <lie/math/Vector3.hpp>
#include <lie/math/Constants.hpp>


namespace {
	lie::rend::MeshVertexLayout defaultVertexLayout = lie::rend::MeshVertexLayout({
		lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float, 3),  //pos
		lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float, 3),  //norm
		lie::rend::MeshVertexLayout::Attribute(lie::core::PrimativeType::Float, 2) });//uv
}

namespace lie{
	namespace rend{

		const MeshVertexLayout& MeshGenerator::getDefaultVertexLayout(){
			return defaultVertexLayout;
		}

		MeshGenerator::MeshGenerator(lie::rend::Device& device)
			: MeshGenerator(device, getDefaultVertexLayout(), 0, 1, 2){
			//empty body
		}

		MeshGenerator::MeshGenerator(lie::rend::Device& device, const lie::rend::MeshVertexLayout& vertexLayout,
			unsigned char positionAttribute, unsigned char normalAttribute, unsigned char uvAttribute)
			: mDevice(device), mVertexLayout(vertexLayout), mPosAttrib(positionAttribute),
			mNormalAttrib(normalAttribute), mUvAttrib(uvAttribute){
			//empty body
		}

		Mesh* MeshGenerator::genRegularPolygon(float radius, unsigned int numSides, bool twoSided, Position position){
			lie::math::Vector3f center(0, 0, 0);
			switch (position){
			case Position::PXPYPZ:
				center.x = radius;
				center.y = radius;
				break;
			}

			int numFaces = numSides - 2;
			int numVerticies = numSides;
			if (twoSided){
				numFaces *= 2;
				numVerticies *= 2;
			}	

			float* positions = new float[3 * numVerticies];
			float* normals = new float[3 * numVerticies];
			float* uvs = new float[2 * numVerticies];
			lie::rend::MeshDataBuffer::IndexType* indicies = new lie::rend::MeshDataBuffer::IndexType[3 * numFaces];

			float deltaAngle = (2 * lie::math::PI_f) / (float)numSides;
			float curAngle = 0;
			for (int i = 0; i < numSides; ++i, curAngle += deltaAngle){
				float s = sin(curAngle);
				float c = cos(curAngle);
				positions[i * 3 + 0] = radius * s;
				positions[i * 3 + 1] = radius * c;
				positions[i * 3 + 2] = 0.0f;
				normals[i * 3 + 0] = 0.0f;
				normals[i * 3 + 1] = 0.0f;
				normals[i * 3 + 2] = -1.0f;
				//uvs between 0 and 1, convert sin and cos from -1->1 to 0->1
				uvs[i * 2 + 0] = (s + 1) / 2.0;
				uvs[i * 2 + 1] = (c + 1) / 2.0;
				if (twoSided){
					//need reversed normals
					positions[numSides * 3 + i * 3 + 0] = radius * s;
					positions[numSides * 3 + i * 3 + 1] = radius * c;
					positions[numSides * 3 + i * 3 + 2] = 0.0f;
					normals[numSides * 3 + i * 3 + 0] = 0.0f;
					normals[numSides * 3 + i * 3 + 1] = 0.0f;
					normals[numSides * 3 + i * 3 + 2] = 1.0f;
					//uvs between 0 and 1, convert sin and cos from -1->1 to 0->1
					uvs[numSides * 2 + i * 2 + 0] = (s + 1) / 2.0;
					uvs[numSides * 2 + i * 2 + 1] = (c + 1) / 2.0;
				}
			}

			for (int i = 0; i < numFaces; ++i){
				indicies[i * 3 + 0] = 0;
				indicies[i * 3 + 1] = i + 1;
				indicies[i * 3 + 2] = i + 2;
			}
			if (twoSided){
				for (int i = 0; i < numFaces; ++i){
					indicies[numSides*3 + i * 3 + 0] = numSides;
					indicies[numSides * 3 + i * 3 + 1] = numSides + i + 1;
					indicies[numSides * 3 + i * 3 + 2] = numSides + i + 2;
				}
			}

			lie::rend::MeshDataBuffer mdb(this->mVertexLayout);
			mdb.setVertexCount(numVerticies);
			mdb.setAttribData(0, positions, numVerticies);
			mdb.setAttribData(1, normals, numVerticies);
			mdb.setAttribData(2, uvs, numVerticies);
			mdb.appendIndicies(indicies, numFaces * 3);
			delete[] positions;
			delete[] normals;
			delete[] uvs;
			delete[] indicies;
			return mdb.buildMesh(this->mDevice);
		}

		Mesh* MeshGenerator::genCube(float sideLength, Position position){
			float hsl = sideLength / 2.0f; //half side length

			lie::math::Vector3f center(0,0,0);
			switch (position){
			case Position::PXPYPZ:
				center.setAll(hsl);
				break;
			}

			lie::core::ByteBuffer positions;
			lie::core::ByteBuffer normals;
			lie::core::ByteBuffer uvs;

			lie::math::Vector3f corner0 = center + lie::math::Vector3f(-hsl, -hsl, -hsl);
			lie::math::Vector3f corner1 = center + lie::math::Vector3f(-hsl,  hsl, -hsl);
			lie::math::Vector3f corner2 = center + lie::math::Vector3f(-hsl,  hsl,  hsl);
			lie::math::Vector3f corner3 = center + lie::math::Vector3f(-hsl, -hsl,  hsl);
			lie::math::Vector3f corner4 = center + lie::math::Vector3f( hsl, -hsl, -hsl);
			lie::math::Vector3f corner5 = center + lie::math::Vector3f( hsl, hsl, -hsl);
			lie::math::Vector3f corner6 = center + lie::math::Vector3f( hsl, hsl, hsl);
			lie::math::Vector3f corner7 = center + lie::math::Vector3f( hsl, -hsl, hsl);

			lie::rend::MeshDataBuffer::IndexType indicies[] = {
				2, 1, 0, //face 1
				2, 0, 3,
				4, 5, 6, //face 2
				4, 6, 7,
				5, 6, 7, //face 3
				5, 7, 4,
				2, 3, 7, //face 4
				7, 2, 6,
				3, 0, 4, //face 5
				4, 3, 7,
				2, 1, 5, //face 6
				2, 5, 6,
			};


			//face 1
			positions.push_back(corner0);
			positions.push_back(corner1);
			positions.push_back(corner2);
			positions.push_back(corner3);
			normals.push_back(lie::math::Vector3f(0, 0, -1));
			normals.push_back(lie::math::Vector3f(0, 0, -1));
			normals.push_back(lie::math::Vector3f(0, 0, -1));
			normals.push_back(lie::math::Vector3f(0, 0, -1));
			uvs.push_back(lie::math::Vector2f(0, 0));
			uvs.push_back(lie::math::Vector2f(0, 1));
			uvs.push_back(lie::math::Vector2f(1, 1));
			uvs.push_back(lie::math::Vector2f(1, 0));

			//face 2
			positions.push_back(corner0);
			positions.push_back(corner1);
			positions.push_back(corner5);
			positions.push_back(corner4);
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			uvs.push_back(lie::math::Vector2f(0, 0));
			uvs.push_back(lie::math::Vector2f(0, 1));
			uvs.push_back(lie::math::Vector2f(1, 1));
			uvs.push_back(lie::math::Vector2f(1, 0));

			//face 3
			positions.push_back(corner5);
			positions.push_back(corner6);
			positions.push_back(corner4);
			positions.push_back(corner7);
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			normals.push_back(lie::math::Vector3f(-1, 0, 0));
			uvs.push_back(lie::math::Vector2f(0, 0));
			uvs.push_back(lie::math::Vector2f(0, 1));
			uvs.push_back(lie::math::Vector2f(1, 1));
			uvs.push_back(lie::math::Vector2f(1, 0));
				

			lie::rend::MeshDataBuffer mdb(this->mVertexLayout);
			mdb.setVertexCount(8);
			mdb.setAttribData(0, positions.data(), 8);
			mdb.setAttribData(1, normals.data(), 8);
			mdb.setAttribData(2, uvs.data(), 8);
			mdb.appendIndicies(indicies, 12);
			return mdb.buildMesh(this->mDevice);
		}
	}
}