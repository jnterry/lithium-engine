#include <lie/render\VideoMode.hpp>
#include <lie/core\Config.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "win32/VideoModeImpl.win.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxVideoMode.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXVideoMode.hpp"
#else
    #error Need to supply implementation to get supported video modes and desktop mode!
#endif

namespace lie{
    namespace rend{


        VideoMode::VideoMode()
            : size(0,0), bitsPerPixel(0){/*empty body*/}

        VideoMode::VideoMode(const math::Vector2i& newSize, int newBitsPerPixel)
            : size(newSize), bitsPerPixel(newBitsPerPixel){/*empty body*/}

        VideoMode::VideoMode(const VideoMode& other)
            : size(other.size), bitsPerPixel(other.bitsPerPixel){/*empty body*/}

        //check that it is in the list of supported full screen modes
        bool VideoMode::isValidFullscreen() const {
            std::vector<VideoMode> modes = getFullscreenModes();
            return std::find(modes.begin(), modes.end(), *this) != modes.end();
        }

        bool VideoMode::operator==(const VideoMode& other) const {
            return (this->size == other.size &&
                    this->bitsPerPixel == other.bitsPerPixel);
        }

        bool VideoMode::operator!=(const VideoMode& other) const {return !this->operator!=(other);}

        bool VideoMode::operator<(const VideoMode& other) const{
            if(this->bitsPerPixel == other.bitsPerPixel){
                if(this->size.x == other.size.x){
                    return this->size.y < other.size.y;
                } else {
                    return this->size.x < other.size.x;
                }
            } else {
                return this->bitsPerPixel < other.bitsPerPixel;
            }
        }

        bool VideoMode::operator>(const VideoMode& other) const {return (other.operator<(*this));}

        bool VideoMode::operator<=(const VideoMode& other) const {return !(this->operator>(other));}

        bool VideoMode::operator>=(const VideoMode& other) const {return !(this->operator<(other));}

        VideoMode VideoMode::getDesktopMode(){
            return lie::rend::detail::VideoModeImpl::getDesktopMode();
        }

        std::vector<lie::rend::VideoMode> VideoMode::getFullscreenModes(){
            return lie::rend::detail::VideoModeImpl::getFullscreenModes();
        }
    }
}
