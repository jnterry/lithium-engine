#include <lie/render/MaterialTypeBuilder.hpp>

namespace lie{
	namespace rend{

		MaterialTypeBuilder::MaterialTypeBuilder(unsigned char textureCount, Device& dev)
		: mTextureCount(textureCount), mDevice(dev){
			//empty body
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, const lie::math::Matrix4x4f& defaultValue) {
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Mat4x4f, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, const lie::math::Matrix3x3f& defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Mat3x3f, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, const lie::math::Vector4f& defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Vec4f, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, const lie::math::Vector3f& defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Vec3f, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, const lie::math::Vector2f& defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Vec2f, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, float defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Float, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		MaterialTypeBuilder& MaterialTypeBuilder::addAttribute(std::string name, int defaultValue){
			this->mAttribMetas.emplace_back(name, ShaderParameterType::Int32, this->mDefaults.size());
			this->mDefaults.push_back(defaultValue);
			return *this;
		}

		const MaterialType* MaterialTypeBuilder::build(){
			//:TODO: memory leak if client doesn't delete, which they shouldn't 
			// (although technically possible on a ref)... this should be owned by the Device
			MaterialType* mt = new MaterialType(this->mAttribMetas.size(), this->mDefaults);
			mt->mAttributes = new MaterialType::AttributeMeta[this->mAttribMetas.size()]();
			mt->mTextureCount = this->mTextureCount;
			for (int i = 0; i < this->mAttribMetas.size(); ++i){
				mt->mAttributes[i].setTo(this->mAttribMetas[i]);
			}
			return mt;
		}

	}
}