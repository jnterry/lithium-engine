/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Shader.cpp
/// \author Jamie Terry
/// \date 2015/06/20
/// \brief Contains implementations of render api agnositic function in 
/// lie::render::Shader class
/// 
/// \ingroup render
/////////////////////////////////////////////////

#include <lie/render/Shader.hpp>
#include <fstream>
#include <string>
#include <lie/core/Logger.hpp>

namespace lie{
	namespace rend{
		lie::rend::Shader::Type Shader::getType() const{
			return mType;
		}

		bool Shader::loadAndCompile(std::istream& inputStream){
			//load data from start of inputStream from end into string data
			std::string source((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			return this->loadAndCompile(source);
		}

		bool Shader::loadAndCompile(std::string& source){
			const char* dataPointer = source.c_str();
			return this->loadAndCompile(source.c_str());
		}

		bool Shader::loadAndCompile(const lie::core::File& file){
			std::ifstream fin;
			fin.open(file.getFullPath(), std::ios::in);
			if (!fin.is_open()){
				LIE_DLOG_ERROR("lie::rend::Shader", "Tried to make new shader object by opening file \"" << file.getFullPath() <<
					"\" however it does not exist. If this shader is the default one, expect nothing to be rendered!");
				return false;
			}
			return this->loadAndCompile(fin);
		}

		std::string Shader::getTypeString() const{
			switch (this->mType){
			case Type::Vertex:
				return "Vertex";
			case Type::Pixel:
				return "Pixel";
			case Type::Geometry:
				return "Geometry";
			default:
				return "UNKNOWN TYPE";
			}
		}
	}
}