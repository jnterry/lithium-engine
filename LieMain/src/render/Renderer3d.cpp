#include <lie/render/Renderer3d.hpp>
#include <lie/render/Pass.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/render/Material.hpp>
#include <lie/render/MaterialType.hpp>
#include <lie/render/Texture.hpp>

namespace lie{
    namespace rend{

		Camera3d& Renderer3d::getCamera(){
			return this->mCamera;
		}

		void Renderer3d::setCamera(Camera3d cam){
			this->mCamera = cam;
		}

		void Renderer3d::usePass(Pass& pass, const Material& mat, const math::Matrix4x4f& modelMatrix){
			pass.use();
			ShaderProgram& sprog = pass.getShaderProgram();

			for (int i = 0; i < sprog.getTextureChannelCount(); ++i){
				switch (pass.getTextureSource(i)){
				case Pass::TextureSource::MATERIAL:{
					int matTextIndex = pass.getTextureSourceArg(i);
					mat.getTexture(matTextIndex)->bind(i);
					sprog.setTextureChannelBinding(i, i);
					break;
				}
				case Pass::TextureSource::UNKNOWN:
					break;
				}
			}

			for (int i = 0; i < sprog.getParameterCount(); ++i){
				int sourceArg = pass.getParameterSourceArg(i);
				ShaderParameterType type = sprog.getParameterType(i);
				switch (pass.getParameterSource(i)){
				case Pass::ParameterSource::MATERIAL_ATTRIBUTE:
					switch (type){
					case ShaderParameterType::Mat4x4f:
						sprog.setParameter(i, mat.getAttributeMat4x4f(sourceArg));
						break;
					case ShaderParameterType::Mat3x3f:
						sprog.setParameter(i, mat.getAttributeMat3x3f(sourceArg));
						break;
					case ShaderParameterType::Vec4f:
						sprog.setParameter(i, mat.getAttributeVec4f(sourceArg));
						break;
					case ShaderParameterType::Vec3f:
						sprog.setParameter(i, mat.getAttributeVec3f(sourceArg));
						break;
					case ShaderParameterType::Vec2f:
						sprog.setParameter(i, mat.getAttributeVec2f(sourceArg));
						break;
					case ShaderParameterType::Int32:
						sprog.setParameter(i, mat.getAttributeInt32(sourceArg));
						break;
					case ShaderParameterType::Float:
						sprog.setParameter(i, mat.getAttributeFloat(sourceArg));
						break;
					}
					break;
				case Pass::ParameterSource::MVP_MATRIX:
					sprog.setParameter(i, modelMatrix * this->mCamera.getTransformation(this->mViewport.getSize()));
					break;
				case Pass::ParameterSource::MODEL_MATRIX:
					sprog.setParameter(i, modelMatrix);
					break;
				case Pass::ParameterSource::CAMERA_POSITION:
					sprog.setParameter(i, this->mCamera.transform.position);
					break;
				case Pass::ParameterSource::VIEW_MATRIX:
					sprog.setParameter(i, this->mCamera.getViewMatrix());
					break;
				case Pass::ParameterSource::PROJECTION_MATRIX:
					sprog.setParameter(i, this->mCamera.getProjectionMatrix(this->mViewport.getSize()));
					break;
				case Pass::ParameterSource::CAMERA_LOOK_DIRECTION_3D:
					LIE_DLOG_FATAL("Renderer3d", "CAM LOOK DIR NOT IMPLEMENTED");
					break;
				case Pass::ParameterSource::CAMERA_UP_DIRECTION:
					LIE_DLOG_FATAL("Renderer3d", "CAM UP DIR NOT IMPLEMENTED");
					break;
				case Pass::ParameterSource::CAMERA_NEAR_CUT_OFF_3D:
					sprog.setParameter(i, this->mCamera.nearCutOff);
					break;
				case Pass::ParameterSource::CAMERA_FAR_CUT_OFF_3D:
					sprog.setParameter(i, this->mCamera.farCutOff);
					break;
				case Pass::ParameterSource::AMBIENT_LIGHT_COLOR:
					sprog.setParameter(i, this->getAmbientLightColor());
					break;
				case Pass::ParameterSource::TIME:
					switch (type){
					case ShaderParameterType::Float:
						sprog.setParameter(i, (float)this->mTimer.getElapsedTime().asMilliseconds());
						break;
					case ShaderParameterType::Int32:
						sprog.setParameter(i, (int)this->mTimer.getElapsedTime().asMilliseconds());
						break;
					default:
						LIE_DLOG_FATAL("Renderer3dImmediate", "Tried to pass time to unknown parameter type");
						break;
					}
					break;
				}
			}
		}

    }
}
