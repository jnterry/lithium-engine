#include <lie/core\Config.hpp>
#include <lie/render\Window.hpp>
#include <lie/core\Logger.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "gl/_GlfwWindowImpl.gl.hpp"
#elif defined(LIE_OS_LINUX)
    #include "_GlfwWindowImpl.hpp"
#elif defined(LIE_OS_MACOS)
    #include "_GlfwWindowImpl.hpp"
#else
    #error No window implementation for this platform!
#endif

namespace lie{
    namespace rend{

        //////////////////////////////////////////////////////////////////////////////////////////////
        //FUNCTIONS FROM RENDER TARGET
        //Windows expose the same interface as a render target (despite not being dervied from them)
        //These are the functions that are common to both a window and render target
        void Window::makeCurrent(){
            this->mImpl->makeCurrent();
        }

        void Window::display(){
            this->mImpl->display();
        }

        const VideoMode& Window::getVideoMode() const{
            return this->mImpl->getVideoMode();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        //FUNCTIONS FOR WINDOW
        //Window specific functions (ie: not from render target)

        std::string  Window::getCaption(){
            return this->mCaption;
        }

        void Window::setCaption(std::string cap){
            this->mCaption = cap;
        }

        int Window::getStyle(){
            return this->mStyle;
        }

        void  Window::setStyle(int style){
            this->mStyle = style;
        }

        void  Window::create(VideoMode videomode){
           this->mImpl->create(videomode, this->mCaption);
        }

        void  Window::close(){
            this->mImpl->close();
        }

        void  Window::minimise(){
            this->mImpl->minimise();
        }

        void  Window::restore(){
            this->mImpl->restore();
        }

        void  Window::maximise(){
            this->mImpl->maximise();
        }

        bool  Window::isMinimised(){
            return this->mImpl->isMinimised();
        }

        bool Window::isMaximised(){
            return this->mImpl->isMaximised();
        }

        bool Window::isOpen(){
            return this->mImpl->isOpen();
        }

        void Window::setPosition(int x, int y, Monitor monitor){
            this->mImpl->setPosition(x,y,monitor);
        }

        int Window::getXPosition(){
            return this->mImpl->getXPosition();
        }

        int  Window::getYPosition(){
            return this->mImpl->getYPosition();
        }

        Monitor Window::getCurrentMonitor(){
            return this->mImpl->getCurrentMonitor();
        }

        bool Window::hasFocus(){
           return this->mImpl->hasFocus();
        }

        Window::Window(lie::rend::VideoMode mode, std::string caption, int style) :
            mStyle(style), mCaption(caption){
            mImpl = new lie::rend::detail::WindowImpl(this);
            this->create(mode);
        }

        Window::~Window(){
            if(mImpl){
                this->close();
                delete this->mImpl;
            }
        }

        bool Window::hasEvent(){
            return this->mEvents.size() != 0;
        }

        bool Window::pollEvent(Event& event){
            this->mImpl->updateEvents();//update with OS so event queue actually contains events
            if(this->hasEvent()){
                event = this->mEvents.front();
                this->popEvent();
                return true;
            } else {
                //then no event in queue
                return false;
            }
        }

        void Window::waitForEvent(Event& event){
            this->mImpl->waitForEvent();
            //there MUST now be an event in the queue, else impl would not yet have returned
            pollEvent(event);
        }

        void Window::popEvent(){
            this->mEvents.pop();
        }

        void Window::pushEvent(Event e){
            this->mEvents.push(e);
        }

        Event getNextEvent();
    }
}
