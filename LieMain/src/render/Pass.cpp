/////////////////////////////////////////////////
/// \file Pass.cpp
/// \brief API independent Pass functions
/////////////////////////////////////////////////

#include <lie/render/Pass.hpp>
#include <lie/render/ShaderProgram.hpp>
#include <lie/render/Renderer3d.hpp>
#include <lie/render/Camera3d.hpp>
#include <lie/render/Material.hpp>
#include <lie/render/MaterialType.hpp>

namespace lie{
    namespace rend{


        Pass::Pass(lie::rend::ShaderProgram& shaderProgram) :
            mFrontFaceMode(lie::rend::FaceMode::Filled), mBackFaceMode(lie::rend::FaceMode::Cull),
            mDepthTestMode(lie::rend::DepthTestMode::LessThan), mLineWidth(1), mPointSize(1),
            mShaderProgram(shaderProgram){
			this->mParameterSources = new ParameterMeta[shaderProgram.getParameterCount()];
			this->mTextureSources = new TextureMeta[shaderProgram.getTextureChannelCount()];
        }

		void Pass::use(){
            this->_useApiSpecific();

            /////////////////////////////////////////////////////////////////////
            //Shader Program
            this->mShaderProgram.use();
        }


        FaceMode Pass::getBackFaceMode(){
            return mBackFaceMode;
        }

        FaceMode Pass::getFrontFaceMode(){
            return mFrontFaceMode;
        }

        void Pass::setBackFaceMode(FaceMode mode){
            mBackFaceMode = mode;
        }

        void Pass::setFrontFaceMode(FaceMode mode){
            mFrontFaceMode = mode;
        }

        void Pass::setFaceMode(FaceMode mode){
            mFrontFaceMode = mode;
            mBackFaceMode = mode;
        }

        void Pass::setFaceMode(FaceMode front, FaceMode back){
            mFrontFaceMode = front;
            mBackFaceMode = back;
        }

        void Pass::setLineWidth(unsigned char val){
            this->mLineWidth = val;
        }

        void Pass::setPointSize(unsigned char val){
            this->mPointSize = val;
        }

        unsigned char Pass::getLineWidth(){
                return this->mLineWidth;
        }

        unsigned char Pass::getPointSize(){
            return this->mPointSize;
        }

        void Pass::setDepthTestMode(lie::rend::DepthTestMode dtmode){
            this->mDepthTestMode = dtmode;
        }

        bool Pass::operator==(const Pass& other) const{
            return  this->mBackFaceMode == other.mBackFaceMode &&
                    this->mFrontFaceMode == other.mFrontFaceMode &&
                    this->mLineWidth == other.mLineWidth &&
                    this->mPointSize == other.mPointSize &&
                    this->mShaderProgram == other.mShaderProgram;
        }

        lie::rend::ShaderProgram& Pass::getShaderProgram(){
            return this->mShaderProgram;
        }

		void Pass::setParameterSource(int paramIndex, ParameterSource source, int arg){
			this->mParameterSources[paramIndex].source = source;
			this->mParameterSources[paramIndex].arg = arg;
		}

		bool Pass::setParameterSource(std::string paramName, ParameterSource source, int arg){
			int i = this->mShaderProgram.getParameterIndex(paramName);
			if (i == -1){
				LIE_DLOG_WARN("Pass", "Attempted to set parameter source for the parameter \"" << paramName 
					<< "\" but the shader does not contain a parameter with that name");
				return false;
			} else {
				this->setParameterSource(i, source, arg);
				return true;
			}
		}

		void Pass::setTextureSource(int channel, TextureSource source, int arg){
			this->mTextureSources[channel].source = source;
			this->mTextureSources[channel].arg = arg;
		}

		bool Pass::setTextureSource(std::string channelName, TextureSource source, int arg){
			int channel = this->mShaderProgram.getTextureChannelIndex(channelName);
			if (channel != -1){
				this->setTextureSource(channel, source, arg);
				return true;
			} else {
				return false;
			}
		}

		Pass::TextureSource Pass::getTextureSource(int channel) const{
			return this->mTextureSources[channel].source;
		}

		int Pass::getTextureSourceArg(int channel) const{
			return this->mTextureSources[channel].arg;
		}

		Pass::ParameterSource Pass::getParameterSource(int paramIndex) const{
			return this->mParameterSources[paramIndex].source;
		}

		int Pass::getParameterSourceArg(int paramIndex) const{
			return this->mParameterSources[paramIndex].arg;
		}

		Pass::TextureMeta::TextureMeta()
			: source(TextureSource::UNKNOWN), arg(0){
			//empty body
		}

		Pass::ParameterMeta::ParameterMeta()
			: source(ParameterSource::UNKNOWN), arg(0){
			//empty body
		}
    }
}
