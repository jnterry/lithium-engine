#include <lie/render\MeshVertexLayout.hpp>

namespace lie{
    namespace rend{
            MeshVertexLayout::Attribute::Attribute(lie::core::PrimativeType newType, unsigned char newComponentCount)
            : type(newType), componentCount(newComponentCount){
                //empty
            }

            size_t MeshVertexLayout::Attribute::getSize() const{
                return lie::core::primativeSize(type)*componentCount;
            }

            MeshVertexLayout::MeshVertexLayout(){
                //empty
            }

            MeshVertexLayout::MeshVertexLayout(std::initializer_list<Attribute> attribs)
            : mAttribs(attribs){
                //emtpy
            }

            void MeshVertexLayout::addAttrib(const Attribute& attribute){
                this->mAttribs.push_back(attribute);
            }

            size_t MeshVertexLayout::getSize() const{
                size_t size = 0;
                for(Attribute p : this->mAttribs){
                    size += p.getSize();
                }
                return size;
            }

            size_t MeshVertexLayout::getAttribCount() const{
                return this->mAttribs.size();
            }

            MeshVertexLayout::Attribute& MeshVertexLayout::getAttrib(size_t index){
                return this->mAttribs[index];
            }

            const MeshVertexLayout::Attribute& MeshVertexLayout::getAttrib(size_t index) const{
                return this->mAttribs[index];
            }

            size_t MeshVertexLayout::getAttribOffset(size_t index) const{
                //could use recursion here
                size_t result = 0;
                for(int i = 0; i < index; ++i){
                    result += this->mAttribs[i].getSize();
                }
                return result;
            }

    }
}
