#include <lie/render/Material.hpp>
#include <lie/render/MaterialType.hpp>

namespace lie{
	namespace rend{
		Material::Material(const MaterialType& matType) 
			: mMatType(matType), mTextures(new Texture*[matType.getTextureCount()]){
			for (int i = 0; i < matType.getTextureCount(); ++i){
				this->mTextures[i] = nullptr;
			}
		}

		Material::~Material(){
			delete[] this->mTextures;
		}

		const MaterialType& Material::getMaterialType() const{
			return this->mMatType;
		}

		void Material::setTexture(int index, Texture* tex){
			this->mTextures[index] = tex;
		}

		Texture* Material::getTexture(int index) const{
			return this->mTextures[index];
		}

		void Material::setAttribute(int index, const lie::math::Matrix4x4f& value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<lie::math::Matrix4x4f>(loc) = value;
		}

		void Material::setAttribute(int index, const lie::math::Matrix3x3f& value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<lie::math::Matrix3x3f>(loc) = value;
		}

		void Material::setAttribute(int index, const lie::math::Vector4f& value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<lie::math::Vector4f>(loc) = value;
		}

		void Material::setAttribute(int index, const lie::math::Vector3f& value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<lie::math::Vector3f>(loc) = value;
		}

		void Material::setAttribute(int index, const lie::math::Vector2f& value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<lie::math::Vector2f>(loc) = value;
		}

		void Material::setAttribute(int index, float value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<float>(loc) = value;
		}

		void Material::setAttribute(int index, int value){
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			this->mParamData.at<int>(loc) = value;
		}


		const lie::math::Matrix4x4f& Material::getAttributeMat4x4f(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<lie::math::Matrix4x4f>(loc);
		}

		const lie::math::Matrix3x3f& Material::getAttributeMat3x3f(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<lie::math::Matrix3x3f>(loc);
		}

		const lie::math::Vector4f& Material::getAttributeVec4f(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<lie::math::Vector4f>(loc);
		}

		const lie::math::Vector3f& Material::getAttributeVec3f(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<lie::math::Vector3f>(loc);
		}

		const lie::math::Vector2f& Material::getAttributeVec2f(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<lie::math::Vector2f>(loc);
		}

		float Material::getAttributeFloat(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<float>(loc);
		}

		int Material::getAttributeInt32(int index) const{
			size_t loc = this->mMatType.getAttributeByteOffset(index);
			return this->mParamData.at<int>(loc);
		}
	}
}