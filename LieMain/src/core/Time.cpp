#include <lie/core/Time.hpp>
#include <lie/core/Logger.hpp>
#include <lie/core/Convert.hpp>
#include <math.h>
#include <sstream>
#include <iostream>
#include <chrono>

namespace lie{
	namespace core{

		//this is platform indecent implementation, but resolution might not be great depending 
		// on std implementation
		Time Time::now(){
			//the resolution of this is std implementation dependent
			return Time::Nanoseconds((std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count()));
		}

        bool Time::useAmericanDateFormat = false;
        const Time Time::Zero = Time();


        Time::Time() : mNanoseconds(0)
        { /*Empty Body, done in init list*/	}

        ////////////////////////////////////////////////////////////////////////
		Time Time::Nanoseconds(long long number){
			Time t;
			t.setAsNanoseconds(number);
			return t;
		}

        Time Time::Microseconds(double number){
            Time t;
            t.setAsMicroseconds(number);
            return t;
        }

        Time Time::Milliseconds(double number){
            Time t;
            t.setAsMilliseconds(number);
            return t;
        }
        Time Time::Seconds(double number){
            Time t;
            t.setAsSeconds(number);
            return t;
        }
        Time Time::Minutes(double number){
            Time t;
            t.setAsMinutes(number);
            return t;
        }
        Time Time::Hours(double number){
            Time t;
            t.setAsHours(number);
            return t;
        }
        Time Time::Days(double number){
            Time t;
            t.setAsDays(number);
            return t;
        }
        Time Time::Weeks(double number){
            Time t;
            t.setAsWeeks(number);
            return t;
        }
        Time Time::Years(double number){
            Time t;
            t.setAsYears(number);
            return t;
        }

        ////////////////////////////////////////////////////////////////////////
        //Functions to return the day/minute/seconds etc represented by this
        //object
        //Note: These functions NEVER return fractions, eg if time was 90 seconds
        //get minute would return 1, NOT 1.5. To get fractions use the asMinute()
        //asSecond() etc functions
        short Time::getDayOfMonth() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            return t->tm_mday;
        }
        short Time::getHour24() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            return t->tm_hour;
        }
        short Time::getHour12() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            //Get 24 hour time
            int hour24 = t->tm_hour;

            if (hour24 == 0){
                return 12;
            } else if (hour24 > 12){
                return hour24 -= 12;
            } else {
                return hour24;
            }
        }
        bool Time::isAm() const{
            return this->getHour24() < 12;
        }
        std::string Time::getAmPm() const{
            if(this->isAm())
                return "AM";
            else
                return "PM";
        }
        short Time::getMonth() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);

            //The month returned by t->tm_mon is number of months
            //since January, therefore it is between 0 and 11
            return t->tm_mon + 1;

        }
        short Time::getMinute() const{

            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            return t->tm_min;
        }
        short Time::getSecond() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            return t->tm_sec;
        }
        short Time::getYear() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesn't like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            //Returns years since 1900, therefore must add 1990 to get actual year
            return t->tm_year + 1900;
        }
        short Time::getDayOfWeek() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesnt like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            //Returns 0 Sunday, 1 Monday ... 6 Saturday,
            //We want 1 Sunday, 2 Monday ... 7 Saturday,
            return t->tm_wday + 1;
        }
        short Time::getDayOfYear() const{
            time_t ti;
            if(this->asSeconds() < 0){
                ti = 0; //Preventing a crash...
                        //C++ time.h doesnt like -ve times, but this can represent one
            } else {
                ti = this->asSeconds();
            }
            tm* t = localtime(&ti);
            return t->tm_yday;
        }
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        //Functions returning a string containing the name of the month or year
        //represented by this instantce
        std::string Time::getDayName() const{
            return this->getDayName(this->getDayOfWeek());
        }
        std::string Time::getMonthName() const{
            return this->getMonthName(this->getMonth());
        }
        std::string Time::getDayNameShort() const{
            return this->getDayNameShort(this->getDayOfWeek());
        }
        std::string Time::getMonthNameShort() const{
            return this->getMonthNameShort(this->getMonth());
        }
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        //Returns a string containing the name of the month that is passed in,
        //Month numbers are Jan = 1, Feb = 2 ....  Dec = 12
        //Therefore the enum Time::Months::XXXXXXXXX
        //can be used
        std::string Time::getMonthName(short monthNumber){
            switch(monthNumber){
            case Time::Months::January:
                return "January";
                break;
            case Time::Months::Febuary:
                return "Febuary";
                break;
            case Time::Months::March:
                return "March";
                break;
            case Time::Months::April:
                return "April";
                break;
            case Time::Months::May:
                return "May";
                break;
            case Time::Months::June:
                return "June";
                break;
            case Time::Months::July:
                return "July";
                break;
            case Time::Months::August:
                return "August";
                break;
            case Time::Months::September:
                return "September";
                break;
            case Time::Months::October:
                return "October";
                break;
            case Time::Months::November:
                return "November";
                break;
            case Time::Months::December:
                return "December";
                break;
            default:
                //LIE_DLOG_WARN("The name for month " + numberToString(monthNumber) + "was requested but it does not exist, returning name 'ERROR MONTH'", "Time");
                return "ERROR MONTH";
                break;
            }
        }
        std::string Time::getMonthNameShort(short monthNumber){
            switch(monthNumber){
            case Time::Months::January:
                return "Jan";
                break;
            case Time::Months::Febuary:
                return "Feb";
                break;
            case Time::Months::March:
                return "Mar";
                break;
            case Time::Months::April:
                return "Apr";
                break;
            case Time::Months::May:
                return "May";
                break;
            case Time::Months::June:
                return "Jun";
                break;
            case Time::Months::July:
                return "Jul";
                break;
            case Time::Months::August:
                return "Aug";
                break;
            case Time::Months::September:
                return "Sep";
                break;
            case Time::Months::October:
                return "Oct";
                break;
            case Time::Months::November:
                return "Nov";
                break;
            case Time::Months::December:
                return "Dec";
                break;
            default:
                //LOG_WARN("The short name for month " + numberToString(monthNumber) + "was requested but it does not exist, returning name 'ERROR MONTH'", "Time");
                return "ERROR MONTH";
                break;
            }
        }
        std::string Time::getDayName(short dayNumber){
        switch(dayNumber){
            case Time::Days::Sunday:
                return "Sunday";
                break;
            case Time::Days::Monday:
                return "Monday";
                break;
            case Time::Days::Tuesday:
                return "Tuesday";
                break;
            case Time::Days::Wednesday:
                return "Wednesday";
                break;
            case Time::Days::Thursday:
                return "Thursday";
                break;
            case Time::Days::Friday:
                return "Friday";
                break;
            case Time::Days::Saturday:
                return "Saturday";
                break;
            default:
                //LOG_WARN("The name for day " + numberToString(dayNumber) + "was requested but it does not exist, returning name 'ERROR DAY'", "Time");
                return "ERROR DAY";
                break;
            }
        }
        std::string Time::getDayNameShort(short dayNumber){
            switch(dayNumber){
            case Time::Days::Sunday:
                return "Sun";
                break;
            case Time::Days::Monday:
                return "Mon";
                break;
            case Time::Days::Tuesday:
                return "Tue";
                break;
            case Time::Days::Wednesday:
                return "Wed";
                break;
            case Time::Days::Thursday:
                return "Thu";
                break;
            case Time::Days::Friday:
                return "Fri";
                break;
            case Time::Days::Saturday:
                return "Sat";
                break;
            default:
                //LOG_WARN("The short name for day " + numberToString(dayNumber) + "was requested but it does not exist, returning name 'ERROR DAY'", "Time");
                return "ERROR DAY";
                break;
            }
        }
        ////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////
        /// \brief
        ///  returns a string containing the date represented by this
        ///  time object
        ///  Format will depend on whether Time::useAmericanDateFormat
        ///  is set to true or false
        ///  The componenets of the date will be seperated by the string passed
        ///  into the function, its defualt is /
        ///  \return
        /// std::string containing date
        ////////////////////////////////////////////////////////////
        std::string Time::getDate(std::string seperatorToken) const{
            if(Time::useAmericanDateFormat)
                return this->getDateMMDDYY();
            else
                return this->getDateDDMMYY();
        }


        ////////////////////////////////////////////////////////////////////////
        //Functions to get a string containing the date in various formats
        //Note, Time::useAmericanDateFormat is completely igonred and the
        //format in the function name is used instead
        std::string Time::getDateDDMMYYYY(std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            ss << this->getDayOfMonth() << seperatorToken;

            ss.fill('0');
            ss.width(2);

             ss << this->getMonth() << seperatorToken << this->getYear();

            return ss.str();
        }
        std::string Time::getDateMMDDYYYY(std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            ss << this->getMonth() << seperatorToken;

            ss.fill('0');
            ss.width(2);

             ss << this->getDayOfMonth() << seperatorToken << this->getYear();

            return ss.str();
        }
        std::string Time::getDateDDMMYY(std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            ss << this->getDayOfMonth() << seperatorToken;

            ss.fill('0');
            ss.width(2);

            ss << this->getMonth() << seperatorToken << core::toString(this->getYear()).substr(2);

            return ss.str();
        }
        std::string Time::getDateMMDDYY(std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            ss << this->getMonth() << seperatorToken;

            ss.fill('0');
            ss.width(2);

             ss << this->getDayOfMonth() << seperatorToken << core::toString(this->getYear()).substr(2);

             return ss.str();
        }
        std::string Time::getDateYYYYMMDD(std::string seperatorToken) const{
            std::stringstream ss;
            ss << this->getYear() << seperatorToken;

            ss.fill('0');
            ss.width(2);
            ss << this->getMonth() << seperatorToken;

            ss.fill('0');
            ss.width(2);
             ss << this->getDayOfMonth();

            return ss.str();
        }
        ///////////////////////////////////////////////////////////////////////

        std::string Time::getTimeHHMMSS(bool use24Hour, std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            if(use24Hour)
                ss << this->getHour24() << seperatorToken;
            else
                ss << this->getHour12() << seperatorToken;

            ss.fill('0');
            ss.width(2);
            ss << this->getMinute() << seperatorToken;

            ss.fill('0');
            ss.width(2);
            ss << this->getSecond();

            return ss.str();
        }
        std::string Time::getTimeHHMM(bool use24Hour, std::string seperatorToken) const{
            std::stringstream ss;

            ss.fill('0');
            ss.width(2);
            if(use24Hour)
                ss << this->getHour24() << seperatorToken;
            else
                ss << this->getHour12() << seperatorToken;

            ss.fill('0');
            ss.width(2);
            ss << this->getMinute();

            return ss.str();
        }


        ////////////////////////////////////////////////////////////////////////
        // Functions to return the value stored in this object as a number of
        // microseconds, milliseconds, seconds, etc
        // Note: These return the exact number, eg 7.345 weeks. The getXXXX()
        // functions return the number of seconds past the mintute, minutes past the
        // hour, hour of day etc and so NEVER return a fraction
		long long Time::asNanoseconds() const{
		return this->mNanoseconds;
		}
        double Time::asMicroseconds() const{
            return this->mNanoseconds / 1e+3;
        }
        double Time::asMilliseconds() const{
			return this->mNanoseconds / 1e+6;
        }
        double Time::asSeconds() const{
			return this->mNanoseconds / 1e+9;
        }
        double Time::asMinutes() const{
            return this->asSeconds()/60.0;
        }
        double Time::asHours() const{
			return this->asSeconds()/3600.0;
        }
        double Time::asDays() const{
            return this->asSeconds()/86400.0;
        }
        double Time::asWeeks() const{
			return this->asSeconds()/ 604800.0;
        }
        double Time::asYears() const{
            return this->asSeconds()/3.1536e+7;
        }
        ///////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////
        //Functions used to set the time based on a number of seconds, days, milliseconds,
        //weeks etc
        //
		void Time::setAsNanoseconds(long long number){
			this->mNanoseconds = number;
		}
        void Time::setAsMicroseconds(long double number){
			this->mNanoseconds = number * 1e+3;
        }
        void Time::setAsMilliseconds(long double number){
			this->mNanoseconds = number * 1e+6;
        }
        void Time::setAsSeconds(long double number){
            this->mNanoseconds = number * 1e+9;
        }
        void Time::setAsMinutes(long double number){
			this->setAsSeconds(number * 60);
        }
        void Time::setAsHours(long double number){
			this->setAsSeconds(number * 3600);
        }
        void Time::setAsDays(long double number){
			this->setAsSeconds(number * 86400.0);
        }
        void Time::setAsWeeks(long double number){
			this->setAsSeconds(number * 604800.0);
        }
        void Time::setAsYears(long double number){
			this->setAsSeconds(number * 3.1536e+7);
        }
        ////////////////////////////////////////////////////////////////////


        Time& Time::operator=(const Time& rhs){
            if(this != &rhs)
                this->mNanoseconds = rhs.mNanoseconds;

            return *this;
        }
        Time& Time::operator+=(const Time& rhs){
            this->mNanoseconds += rhs.mNanoseconds;
            return *this;
        }
        Time& Time::operator-=(const Time& rhs){
            this->mNanoseconds -= rhs.mNanoseconds;
            return *this;
        }
        Time& Time::operator*=(const time_t rhs){
            this->mNanoseconds *= rhs;
            return *this;
        }
        Time& Time::operator/=(const time_t rhs){
            this->mNanoseconds /= rhs;
            return *this;
        }
        Time Time::operator+(const Time& rhs){
            Time t = *this;
            t += rhs;
            return t;
        }
        Time Time::operator-(const Time& rhs){
            Time t = *this;
            t -= rhs;
            return t;
        }
        Time Time::operator*(const time_t rhs){
            Time t = *this;
            t *= rhs;
            return t;
        }
        Time Time::operator/(const time_t rhs){
            Time t = *this;
            t /= rhs;
            return t;
        }
        bool Time::operator==(const Time& rhs){
            return this->mNanoseconds == rhs.mNanoseconds;
        }
        bool Time::operator!=(const Time& rhs){
            return this->mNanoseconds != rhs.mNanoseconds;
        }
        bool Time::operator<(const Time& rhs){
            return this->mNanoseconds < rhs.mNanoseconds;
        }
        bool Time::operator>(const Time& rhs){
            return this->mNanoseconds > rhs.mNanoseconds;
        }
        bool Time::operator<=(const Time& rhs){
            return this->mNanoseconds <= rhs.mNanoseconds;
        }
        bool Time::operator>=(const Time& rhs){
            return this->mNanoseconds >= rhs.mNanoseconds;
        }

    }//end of core::
}//end of
