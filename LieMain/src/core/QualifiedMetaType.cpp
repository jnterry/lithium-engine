#include <lie\core\QualifiedMetaType.hpp>
#include <lie\core\MetaType.hpp>

namespace{
    //this is for the sole reason that sizeof(someReference) returns the size of the object pointed to by the ref
    //sizeof(RefWrapper) will be the actual size of a reference, this depends on the compilers implementation of references
    //for most compilers it will simply be a pointer,
    class RefWrapper{
    private://we dont want anyone making instances of this class
        RefWrapper(int& i):value(i){}//this is only here to remove compiler warnings about uninitalised variables
        int& value;
    };
}

namespace lie{
    namespace core{

        QualifiedMetaType::QualifiedMetaType(const MetaType& type, unsigned char ptrLevel, bool isRef)
        : mBaseType(type), mQualifiers((127 & ptrLevel) | (int)isRef << 7){
            //empty body
        }

        const MetaType& QualifiedMetaType::getBaseType() const{
            return mBaseType;
        }

        unsigned char QualifiedMetaType::getPtrIndirection() const {
            //ignore top bit, it stores if it is a reference
            return (127 & mQualifiers);
        }

        bool QualifiedMetaType::isReference() const{
            return (128 & mQualifiers) == 128;
        }


        size_t QualifiedMetaType::getStaticSize() const{
            if(isReference()){
                return sizeof(RefWrapper);
            } else if(getPtrIndirection()==0){
                //then return size of actual type
                return this->mBaseType.getStaticSize();
            } else {
                //then it is a ptr, ptr to ptr, etc
                return sizeof(void*);
            }
        }

		bool QualifiedMetaType::operator==(const QualifiedMetaType& other) const{
			return	this->mBaseType == other.mBaseType &&
					this->mQualifiers == other.mQualifiers;
		}

    }
}

