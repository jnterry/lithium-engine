/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file StringUtils.cpp
/// \author Jamie Terry
/// \date 2015/08/06
/// \brief Contains the implementation of functions declared in StringUtils.hpp
/// 
/// \ingroup core
/////////////////////////////////////////////////

#include <lie\core\StringUtils.hpp>
#include <lie\core\Convert.hpp>

namespace lie{
	namespace core{

		StringBuilder::StringBuilder()
			:mIndentLevel(0), mIndentString("    "){
			//empty body
		}

		StringBuilder& StringBuilder::operator<<(const char* val){
			int i = 0;
			for (char c = val[0]; c != '\0'; c = val[++i]){
				mStream << c;
				if (c == '\n'){
					for (int j = 0; j < this->mIndentLevel; ++j){
						mStream << this->mIndentString;
					}
				}
			}
			return *this;
		}
			
		StringBuilder::operator std::string() const{
			return this->str();
		}

		std::string StringBuilder::str() const{
			return mStream.str();
		}

		bool StringBuilder::incrementIndentLevel(){
			if (this->mIndentLevel < 255){
				++this->mIndentLevel;
				return true;
			} else {
				return false;
			}
		}

		bool StringBuilder::decrementIndentLevel(){
			if (this->mIndentLevel > 0){
				--this->mIndentLevel;
				return true;
			} else {
				return false;
			}
		}

		void StringBuilder::setIndentLevel(unsigned char level){
			this->mIndentLevel = level;
		}

		unsigned char StringBuilder::getIndentLevel(){
			return this->mIndentLevel;
		}

		void StringBuilder::setIndentString(std::string str){
			this->mIndentString = str;
		}

		std::string StringBuilder::getIndentString(){
			return this->mIndentString;
		}

		bool equalsIgnoreCase(std::string left, std::string right){
			if (left.length() != right.length()){ return false; }
			for (int i = 0; i < left.length(); ++i){
				if (!equalsIgnoreCase(left[i], right[i])){
					return false;
				}
			}
			return true;
		}

		bool equalsIgnoreCase(char l, char r){
			if (l == r){
				return true;
			} else {
				if (('A' <= l && l <= 'Z')){
					//then l is an upper case char, see if its lower case equivalent == r
					if ((l + ('a' - 'A')) != r){
						return false;
					}
				} else if (('A' <= r && r <= 'Z')){
					//then r is an upper case char, see if its lower case equivalent == l
					if ((r + ('a' - 'A')) != l){
						return false;
					}
				} else {
					//both l and r are not upper case letters, therefore no chance case matters,
					//they just aren't equal
					return false;
				}
				return true;
			}
		}

		int stringSplit(std::vector<std::string>& result, std::string str, char seperator){
			int count = 0;
			if (str.empty()){ return 0; }
			if (str.length() == 1 && str[0] == seperator){ return 0; }

			int previousSeperator = 0;
			if (str[0] == seperator){
				previousSeperator = 1;
			}
			do{
				int nextSeperator = str.find(seperator, previousSeperator);
				if (nextSeperator == std::string::npos){
					nextSeperator = str.length();
				}
				result.push_back(str.substr(previousSeperator, nextSeperator - previousSeperator));
				previousSeperator = nextSeperator + 1;
				++count;
			} while (previousSeperator < str.length());
			return count;
		}
	}
}
