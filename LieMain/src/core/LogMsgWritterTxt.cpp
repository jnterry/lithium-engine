#include <lie/core\LogMsgWritterTxt.hpp>

namespace lie{
    namespace core{

        void LogMsgWritterTxt::onWrite(const RTimeMsg& msg){
            this->mFile << "[" << msg.getTime().getTimeHHMMSS() << "] ";
            switch(msg.getLevel()){
            case lie::core::RTimeMsg::Trace:
                this->mFile << " Trace - ";
                break;
            case lie::core::RTimeMsg::Info:
                this->mFile << "  Info - ";
                break;
            case lie::core::RTimeMsg::Report:
                this->mFile << "Report - ";
                break;
            case lie::core::RTimeMsg::Warn:
                this->mFile << "  Warn - ";
                break;
            case lie::core::RTimeMsg::Error:
                this->mFile << " Error - ";
                break;
            case lie::core::RTimeMsg::Fatal:
                this->mFile << " Fatal - ";
                break;
            default:
                this->mFile << " Lev" << (int)msg.getLevel() << " - ";
                break;
            }

            this->mFile << msg.getMessage();

            this->mFile << " (" << msg.getModule() << ", ln: " << msg.getLineNumber() << ")" << std::endl;
        }

        LogMsgWritterTxt::LogMsgWritterTxt(std::string filename){
            this->mFile.open(filename);
            if(this->mFile.is_open()){
                this->mFile << "File opened on " << lie::core::Time::now().getDate() << " at "
                << lie::core::Time::now().getTimeHHMMSS() << ", ready to recieve log messages." << std::endl << std::endl;
            } /*else {
                :TODO: do something, failed to open log file, cant really log error ;)
            }*/
        }

        LogMsgWritterTxt::~LogMsgWritterTxt(){
            this->mFile << "[" << lie::core::Time::now().getTimeHHMMSS() <<  "] - CLOSING FILE" << std::endl;
            this->mFile.close();
        }


    }
}
