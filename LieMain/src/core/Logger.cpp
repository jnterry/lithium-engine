#include <lie/core\Logger.hpp>
namespace lie{
    namespace core{

        Logger::Logger(){
                //ctor
        }

        Logger::~Logger(){
            //log the fact this logging service is being deleted
            this->write(LIE_RTMSG_INFO("lie::core::Logger") << "Logging serivce shutting down.");

            //writters will be automaticly deleted if needed as they are stored with shared pointers
        }

        void Logger::write(const RTimeMsg& msg){
            for(unsigned int i = 0; i < this->mWritters.size(); ++i){
                this->mWritters[i]->write(msg);
            }
        }

        void Logger::addWritter(std::shared_ptr<LogMsgWritterBase> writter){
            this->mWritters.push_back(writter);
        }


        Logger DLog = Logger();


    }
}

