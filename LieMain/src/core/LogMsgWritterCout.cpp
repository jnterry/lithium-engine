#include <iostream>
#include <lie/core\LogMsgWritterCout.hpp>

namespace lie{
    namespace core{

        void LogMsgWritterCout::onWrite(const RTimeMsg& msg){
            std::cout << "[" << msg.getTime().getTimeHHMMSS() << "] ";
            switch(msg.getLevel()){
            case lie::core::RTimeMsg::Trace:
                std::cout << " Trace - ";
                break;
            case lie::core::RTimeMsg::Info:
                std::cout << "  Info - ";
                break;
            case lie::core::RTimeMsg::Report:
                std::cout << "Report - ";
                break;
            case lie::core::RTimeMsg::Warn:
                std::cout << "  Warn - ";
                break;
            case lie::core::RTimeMsg::Error:
                std::cout << " Error - ";
                break;
            case lie::core::RTimeMsg::Fatal:
                std::cout << " Fatal - ";
                break;
            default:
                std::cout << " Lev" << (int)msg.getLevel() << " - ";
                break;
            }

            std::cout << msg.getMessage();

            std::cout << " (" << msg.getModule() << ", ln: " << msg.getLineNumber() << ")" << std::endl;
        }

    }
}
