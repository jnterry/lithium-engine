#include <lie/core\LogMsgWritterBase.hpp>

namespace lie{
    namespace core{

            void LogMsgWritterBase::write(const RTimeMsg& msg){
                 if(this->mFilter != nullptr){
                    if(!mFilter(msg)){
                        return;
                    }
                 }
                 //if reached here, then msg either satisifes filter or the filter is null, write the message
                 this->onWrite(msg);
            }

            void LogMsgWritterBase::setFilter(RTimeMsgFilter filter){
                this->mFilter = filter;
            }

    }
}
