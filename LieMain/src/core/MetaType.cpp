#include <lie\core\MetaType.hpp>
#include <lie\core\MetaTypeField.hpp>
#include <lie\core\TypeInfo.hpp>

#include <stdint.h>
#include <cstdint>

namespace {
    static const lie::core::MetaType::Flag_t primativeFlagValue = lie::core::MetaType::Concrete |
                                                                    lie::core::MetaType::StaticMem;
}

namespace lie{
    namespace core{

        /////////////////////////////////
        //Make MetaTypes for all the primitive types
        //There are some issues here...
        //depending on the compiler std::uInt8_t may or may not be the same as unsigned char,
        //std::int8_t may or may not be the same as char and/or signed char, etc
        //this makes it VERY difficult to ensure that all the standard primitive types
        //(char, int, short, long, etc) have a MetaType defined for them
        //for now they dont, instead only the std::(u)intN_t types have MetaTypes defined
        //these are more important as fixed width types are needed for serialization, and anything
        //that could be serialized should use these types anyway in order to be portable (ie: file made
        //on one machine needs to read on another  with different os/32-64 bit-ness, etc
        template<> const MetaType TypeInfo<std::uint8_t>::type = MetaType("uInt8", sizeof(std::uint8_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::int8_t>::type = MetaType("Int8", sizeof(std::int8_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::uint16_t>::type = MetaType("uInt16", sizeof(std::uint16_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::int16_t>::type = MetaType("Int16", sizeof(std::int16_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::uint32_t>::type = MetaType("uInt32", sizeof(std::uint32_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::int32_t>::type = MetaType("Int32", sizeof(std::int32_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::uint64_t>::type = MetaType("uInt64", sizeof(std::uint64_t), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<std::int64_t>::type = MetaType("Int64", sizeof(std::int64_t), {}, primativeFlagValue);

        /*template<> const MetaType TypeInfo<unsigned char>::type = MetaType("unsigned char", sizeof(unsigned char), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<char>::type = MetaType("char", sizeof(char), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<unsigned short>::type = MetaType("unsigned short", sizeof(unsigned short), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<short>::type = MetaType("short", sizeof(short), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<unsigned int>::type = MetaType("unsigned int", sizeof(unsigned int), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<int>::type = MetaType("int", sizeof(int), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<unsigned long>::type = MetaType("unsigned long", sizeof(unsigned long), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<long>::type = MetaType("long", sizeof(long), {}, primativeFlagValue);*/


        template<> const MetaType TypeInfo<float>::type = MetaType("float", sizeof(float), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<double>::type = MetaType("double", sizeof(double), {}, primativeFlagValue);
        template<> const MetaType TypeInfo<bool>::type = MetaType("bool", sizeof(bool), {}, primativeFlagValue);

        MetaType::MetaType(std::string name, size_t staticSize, std::initializer_list<MetaTypeField> fields, Flag_t flags) :
            mName(name), mFields(fields), mStaticSize(staticSize), mFlags(flags){

        }

        MetaType::~MetaType(){

        }

        std::string MetaType::getName() const{
            return this->mName;
        }

        bool MetaType::is(Flag_t flags) const{
            return ((unsigned char)flags & (unsigned char)this->mFlags) == (unsigned char)flags;
        }

        bool MetaType::isAtomic() const{
            return  this->is(primativeFlagValue) &&
                    this->getFieldCount() == 0 &&
                    this->getStaticSize() != 0;

        }

        size_t MetaType::getStaticSize() const{
            return this->mStaticSize;
        }

        unsigned char MetaType::getFieldCount() const{
            return this->mFields.size();
        }

        const MetaTypeField& MetaType::getField(FieldIndex_t fieldIndex) const{
            return this->mFields[fieldIndex];
        }

        bool MetaType::operator==(const MetaType& other) const{
            return this == &other;
        }
    }
}
