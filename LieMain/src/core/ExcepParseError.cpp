#include <lie/core/ExcepParseError.hpp>
#include <lie/core/StringUtils.hpp>
#include <string>

namespace {
	std::string generateMessage(std::string badString, std::string formatDesciption,
		std::string formatRegex, std::string details){
		lie::core::StringBuilder sb;
		sb << "Attempted to parse the string \"" << badString << "\"\n"
			<< "This does not meet the required format.\n";
		if (!details.empty()){
			sb << details << "\n";
		}
		if (!formatDesciption.empty()){
			sb << "The required format is: \"" << formatDesciption << "\"";
			if (!formatRegex.empty()){
				sb << " , regex: " << formatRegex;
			}
		}
		else if (!formatRegex.empty()){
			sb << "The string must match the regex:\"" << formatRegex << "\"";
		}
		return sb;
	}
}

namespace lie{
	namespace core{
			ExcepParseError::ExcepParseError(std::string function, std::string badString,
				std::string formatDesciption, std::string formatRegex, std::string details,
				Exception* cause, lie::core::Logger& logger)
				: mBadString(badString), mFormatDesciption(formatDesciption),
				mFormatRegex(formatRegex), Exception("lie::core::ExceptionParseError", function,
					generateMessage(badString, formatDesciption, formatRegex, details), cause, logger){
				//empty body
			}


			std::string ExcepParseError::type() const{
				return "lie::core::ExcepParseError";
			}
	}
}