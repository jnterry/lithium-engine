#include <lie/core/RTimeMsg.hpp>
#include <lie/core/Convert.hpp>

namespace lie{
    namespace core{

        RTimeMsg::RTimeMsg(unsigned char level, std::string module, std::string file, unsigned int line) :
            mModule(module), mFileName(file), mLineNumber(line), mTime(lie::core::Time::now()), mLevel(level)
            {/*EmptyBody*/}

        RTimeMsg::RTimeMsg(const RTimeMsg& other) :
            mModule(other.mModule), mFileName(other.mFileName),
            mLineNumber(other.mLineNumber), mTime(other.mTime),
            mLevel(other.mLevel), mMessage(other.mMessage.str())
            {/*EmptyBody*/}

        const std::string& RTimeMsg::getModule() const {return mModule;}

        const std::string& RTimeMsg::getFileName() const {return mFileName;}

        const unsigned int RTimeMsg::getLineNumber() const {return mLineNumber;}

        const lie::core::Time& RTimeMsg::getTime() const {return mTime;}

        const unsigned char RTimeMsg::getLevel() const {return mLevel;}

        std::string RTimeMsg::getMessage() const {return mMessage.str();}

        std::string RTimeMsg::getLevelString() const {
            switch(this->mLevel){
            case Trace:
                return "Trace";
            case Info:
                return " Info";
            case Report:
                return "Report";
            case Warn:
                return "Warn";
            case Error:
                return "Error";
            case Fatal:
                return "Fatal";
            default:
                return lie::core::toString(this->mLevel);
            }
        }

        std::string RTimeMsg::getLevelStringFormatted() const{
            switch(this->mLevel){
            case Trace:
                return "Trace";
            case Info:
                return " Info";
            case Report:
                return "Reprt";
            case Warn:
                return " Warn";
            case Error:
                return "Error";
            case Fatal:
                return "Fatal";
            default:
                //:TODO: -> format to have leading 0s so number is always 3 digit and thus lines up
                //with other strings
                return "# " + lie::core::toString(this->mLevel);
            }
        }

    }
}
