#include <lie\core\MetaTypeField.hpp>
#include <lie\core\MetaType.hpp>


namespace lie{
    namespace core{
            MetaTypeField::MetaTypeField(QualifiedMetaType type, std::string name, size_t offset, unsigned int count) :
                mType(type), mCount(count), mName(name), mOffset(offset){
                /*empty body*/
            }

            size_t MetaTypeField::getOffset() const{
                return this->mOffset;
            }

            std::string MetaTypeField::getName() const{
                return this->mName;
            }

            size_t MetaTypeField::getSize() const{
                return this->mType.getStaticSize() * this->mCount;
            }

            const QualifiedMetaType MetaTypeField::getType() const{
                return this->mType;
            }

			bool MetaTypeField::operator=(const MetaTypeField& other) const{
				return	this->mType == other.mType &&
						this->mName == other.mName &&
						this->mOffset == other.mOffset;
			}
    }
}

