#include <lie/core\Folder.hpp>
#include <lie/core\Config.hpp>
#include <lie/core\Chars.hpp>

#if defined(LIE_OS_WINDOWS)
    #include "win32/FolderImpl.win.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxFolderImpl.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXFolderImpl.hpp"
#else
    #error No implementation for this os is avalible!
#endif

#include <lie/core\File.hpp>

namespace lie{
    namespace core{

        Folder::Folder() : mVolumeName(""){
            /*empty body*/
        }

        Folder::Folder(std::string path){
            //assume path is in windows style
            int lastSplit = 0;
            bool findVol = false;//if true then loop should interpret first string as the volume
            if(path[0]=='\\' && path[1]=='\\' && path[2]=='?' && path[3]=='\\'){
                /*see http://msdn.microsoft.com/en-us/library/windows/desktop/aa365248(v=vs.85).aspx
                then we have a path like: \\?\Volume{26a21bda-a627-11d7-9931-806e6f6e6963}\
                rather than the usual C:\, D:\, etc*/
                lastSplit=4;
                findVol = true;
            } else if(lie::core::chars::isCapitalLetter(path[0]) && path[1]==':' && path[2] =='\\'){
                //then we have a drive letter absolute path
                //eg: C:, D:, etc
                this->mVolumeName = std::string(&path[0], 2);
                lastSplit = 3;
            }
            for(unsigned int i = lastSplit; i < path.length(); ++i){
                if(path[i]=='\\'){
                    if(findVol){
                        this->mVolumeName = std::string(&path[lastSplit], i - lastSplit);
                        findVol = false;
                    } else {
                        this->push(std::string(&path[lastSplit], i - lastSplit));
                    }
                    ++i;//so we skip the '\' char, we dont want it in the next element's string
                    lastSplit = i;
                }
            }
            if(path[path.length()-1] != '\\'){
                //then the path did not end with a "\", eg: C:\folder\test
                //we need to add the last part ("test") as it was not accounted for in the loop
                //start from last split and get the length of the remianing string -1, as we dont want the "\" char
                this->push(std::string(&path[lastSplit], path.length() - lastSplit));
            }
        }

        Folder::Folder(const Folder& otherdir): mDirList(otherdir.mDirList), mVolumeName(otherdir.mVolumeName)
        {/*empty body*/}

        std::string Folder::getVolume() const{
            return this->mVolumeName;
        }

        void Folder::setVolume(std::string volume){
            this->mVolumeName = volume;
        }

        Folder& Folder::push(std::string dir){
            this->mDirList.push_back(dir);
            return *this;
        }

        Folder& Folder::pop(){
            if(this->mDirList.size() > 0){
                this->mDirList.pop_back();
            }
            return *this;
        }

        bool Folder::isRoot() const{
            return (this->mDirList.size() == 0);
        }

        Folder Folder::getParent() const{
            Folder parent(*this);
            parent.pop();
            return parent;
        }

        std::string Folder::getFullPath(lie::core::PathStyle style) const{
            switch(style){
            case PathStyle::Windows:{
                std::string spath;
                spath.append(this->mVolumeName);
                for(unsigned int i = 0; i < this->mDirList.size(); ++i){
                    spath.append("\\" + this->mDirList[i]);
                }
                return spath;
            }
            case PathStyle::Unix:
                return "lie::core::Folder::getFullPath() -> unix path style not implemented!";
            default:
                return "lie::core::Folder::getFullPath() -> path style not implemented for style: " + (int)style;
            }//end of switch(style)
        }

        std::string Folder::getName() const{
            if(this->mDirList.size() > 0){
                return this->mDirList[this->mDirList.size()-1];
            } else {
                return "";
            }
        }

        bool Folder::operator==(const Folder& other) const{
            return (this->mVolumeName == other.mVolumeName &&
                    this->mDirList == other.mDirList);
        }


        /////////////////////////////////////////////////////////
        //PLATFORM SPECFIC FUNCTIONS BELOW
        Folder Folder::getRoot(){
            return lie::core::detail::FolderImpl::getRoot();
        }

        Folder Folder::getExecutableFolder(){
            return lie::core::detail::FolderImpl::getExecutableFolder();
        }

        bool Folder::exists() const{
            return lie::core::detail::FolderImpl::exists(this);
        }

        bool Folder::create(bool createParents) const{
            return lie::core::detail::FolderImpl::create(this, createParents);
        }

        std::vector<Folder> Folder::getSubfolders() const throw(lie::core::ExcepFileIO){
            return lie::core::detail::FolderImpl::getSubfolders(this);
        }

        std::vector<File> Folder::getFiles() const throw(lie::core::ExcepFileIO){
            return lie::core::detail::FolderImpl::getFiles(this);
        }


    }
}
