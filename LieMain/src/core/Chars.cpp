#include <lie/core/Chars.hpp>
namespace lie{
    namespace core{

        namespace chars{
            bool isCapitalLetter(char c){
                return  c=='A' || c=='J' || c=='S' ||
                        c=='B' || c=='K' || c=='T' ||
                        c=='C' || c=='L' || c=='U' ||
                        c=='D' || c=='M' || c=='V' ||
                        c=='E' || c=='N' || c=='W' ||
                        c=='F' || c=='O' || c=='X' ||
                        c=='G' || c=='P' || c=='Y' ||
                        c=='H' || c=='Q' || c=='Z' ||
                        c=='I' || c=='R';
            }
            bool isLowercaseLetter(char c){
                return  c=='a' || c=='j' || c=='s' ||
                        c=='b' || c=='k' || c=='t' ||
                        c=='c' || c=='l' || c=='u' ||
                        c=='d' || c=='m' || c=='v' ||
                        c=='e' || c=='n' || c=='w' ||
                        c=='f' || c=='o' || c=='x' ||
                        c=='g' || c=='p' || c=='y' ||
                        c=='h' || c=='q' || c=='z' ||
                        c=='i' || c=='r';
            }

            bool isLetter(char c){
                return isCapitalLetter(c) || isLowercaseLetter(c);
            }
            bool isNumeric(char c){
                return  c=='1' || c=='4' || c=='7' ||
                        c=='2' || c=='5' || c=='8' ||
                        c=='3' || c=='6' || c=='9' ||
                        c=='0';
            }
        }
    }
}
