/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file Exception.cpp
/// \author Jamie Terry
/// \date 2015/08/06
/// \brief Contains implementation for the  lie::core::Exception class
/// 
/// \ingroup core
/////////////////////////////////////////////////

#include <lie\core\Exception.hpp>

namespace lie{
	namespace core{

		Exception::Exception(std::string typeName, std::string funcName, std::string msg,
			Exception* cause, lie::core::Logger& log)
			: mType(typeName), mFunction(funcName), mMessage(msg), mCause(nullptr){

			if (cause != nullptr){
				try{
					mCause = new Exception(*cause);
				} catch (...){
					//must be some sort of out of memory issue
					log.write(lie::core::RTimeMsg(lie::core::RTimeMsg::StandardLevels::Error, "lie::core::Exception", __FILE__, __LINE__) <<
						"Exception was thrown while copying cause exception, setting mCause to nullptr.");
					mCause = nullptr;
				}
			}

			lie::core::StringBuilder sb;
			sb << "    Type: " << type() << "\n";
			sb << "Function: " << mFunction << "\n";
			sb << " Message:\n" << mMessage;
			if (mCause != nullptr){
				sb << "\n";
				sb << "   Cause:\n";
				sb.incrementIndentLevel();
				sb << mCause->what() << "\n";
				sb.decrementIndentLevel();
			}
			mWhat = sb.str();

			log.write(lie::core::RTimeMsg(lie::core::RTimeMsg::StandardLevels::Error, "", "", 0) << mWhat);
		}

		Exception::Exception(std::string funcName, std::string msg,
			Exception* cause, lie::core::Logger& log)
			: Exception("lie::core:Exception", funcName, msg, cause, log){
			//empty body
		}

		Exception::Exception(Exception& other)
			: mType(other.mType), mMessage(other.mMessage), mWhat(other.mWhat),
			mCause(other.mCause), mFunction(other.mFunction){
			
			//prevent other's destructor deleting the memory pointed at, this new
			//instance now owns that memory
			other.mCause = nullptr; 
		}

		Exception::~Exception(){
			if (mCause != nullptr){
				delete mCause;
			}
		}

		std::string Exception::type() const throw(){
			return this->mType;
		}

		std::string Exception::message() const throw(){
			return this->mMessage;
		}

		std::string Exception::function() const throw(){
			return this->mFunction;
		}

		const Exception* Exception::cause() const throw(){
			return this->mCause;
		}

		const char* Exception::what() const throw(){
			return mWhat.c_str();
		}

	}
}
