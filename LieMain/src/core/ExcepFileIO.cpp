/////////////////////////////////////////////////
///  Part of Lithium Engine - By Jamie Terry  ///
/////////////////////////////////////////////////
/// \file ExcepFileIO.cpp
/// \author Jamie Terry
/// \date 2015/08/06
/// \brief Contains implementation of the lie::core::ExcepFileIO class
/// 
/// \ingroup core
/////////////////////////////////////////////////

#include <lie\core\ExcepFileIO.hpp>

namespace lie{
	namespace core{
		ExcepFileIO::ExcepFileIO(std::string function, std::string path, std::string details,
			Exception* cause, lie::core::Logger& logger)
		: mPath(path), Exception("lie::core::ExcepFileIO", function, "Error occurred operating on \"" + path + "\"\nError:" + details, cause, logger){
			//empty body
		}

		std::string ExcepFileIO::getPath(){
			return this->mPath;
		}
	}
}