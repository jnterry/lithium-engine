#include <lie/core\LogMsgWritterHtml.hpp>

namespace lie{
    namespace core{

        void LogMsgWritterHtml::onWrite(const RTimeMsg& msg){
            switch(msg.getLevel()){
            case lie::core::RTimeMsg::Trace:
                this->mFile << "<tr bgcolor=#CBCBCF>" << std::endl;
                break;
            case lie::core::RTimeMsg::Info:
                this->mFile << "<tr bgcolor=#D1FFFF>" << std::endl;
                break;
            case lie::core::RTimeMsg::Report:
                this->mFile << "<tr bgcolor=#F2FF00>" << std::endl;
                break;
            case lie::core::RTimeMsg::Warn:
                this->mFile << "<tr bgcolor=#FFBF00>" << std::endl;
                break;
            case lie::core::RTimeMsg::Error:
                this->mFile << "<tr bgcolor=#FF6363>" << std::endl;
                break;
            case lie::core::RTimeMsg::Fatal:
                this->mFile << "<tr bgcolor=#910000>" << std::endl;
                break;
            default:
                this->mFile << "<tr>" << std::endl;
                break;
            }
            this->mFile << "<td>" << msg.getTime().getTimeHHMMSS() << "</td>" << std::endl;

            switch(msg.getLevel()){
            case lie::core::RTimeMsg::Trace:
                this->mFile << "<td>" << "Trace" << "</td>" << std::endl;
                break;
            case lie::core::RTimeMsg::Info:
                this->mFile << "<td>" << "Info" << "</td>" << std::endl;
                break;
            case lie::core::RTimeMsg::Report:
                this->mFile << "<td>" << "Report" << "</td>" << std::endl;
                break;
            case lie::core::RTimeMsg::Warn:
                this->mFile << "<td>" << "Warn" << "</td>" << std::endl;
                break;
            case lie::core::RTimeMsg::Error:
                this->mFile << "<td>" << "Error" << "</td>" << std::endl;
                break;
            case lie::core::RTimeMsg::Fatal:
                this->mFile << "<td>" << "Fatal" << "</td>" << std::endl;
                break;
            default:
                this->mFile << "<td>" << "Level " << (int)msg.getLevel() << "</td>" << std::endl;
                break;
            }

            this->mFile << "<td>" << std::endl;
            if(msg.getMessage().find('\n') != std::string::npos){ //if there are new lines, use pre to preserve them
                this->mFile << "<pre>" << msg.getMessage() << "</pre>" << std::endl;
            } else {
                this->mFile << msg.getMessage() << std::endl;
            }
            this->mFile << "</td>" << std::endl;

            this->mFile << "<td>" << msg.getModule() << "</td>" << std::endl;
            this->mFile << "<td>" << msg.getLineNumber() << "</td>" << std::endl;

            this->mFile << "</tr>" << std::endl;
        }

        LogMsgWritterHtml::LogMsgWritterHtml(std::string filename){
            this->mFile.open(filename);
            if(this->mFile.is_open()){

                this->mFile << "<!DOCTYPE html>" << std::endl;
                this->mFile << "<html>" << std::endl;
                this->mFile << "<body>" << std::endl;
                this->mFile << "<font face =\"arial\">" << std::endl;

                this->mFile << "<h1>Log File</h1>" << std::endl;
                this->mFile << "File opened on " << lie::core::Time::now().getDate() << " at "
                << lie::core::Time::now().getTimeHHMMSS() << ", ready to recieve log messages." << std::endl << std::endl;

                this->mFile << "<table bordercolor=#000000 border=\"1\">" << std::endl;
                this->mFile << "<tr>" << std::endl;
                this->mFile << "<td>Time</td>" << std::endl;
                this->mFile << "<td>Type</td>" << std::endl;
                this->mFile << "<td>Message</td>" << std::endl;
                this->mFile << "<td>Module</td>" << std::endl;
                this->mFile << "<td>Line</td>" << std::endl;
                this->mFile << "</tr>" << std::endl;
            } /*else {
                :TODO: do something, failed to open log file, cant really log error ;)
            }*/
        }

        LogMsgWritterHtml::~LogMsgWritterHtml(){
            this->mFile << "</table>" << std::endl;
            this->mFile << "[" << lie::core::Time::now().getTimeHHMMSS() <<  "] - CLOSING FILE" << std::endl;
            this->mFile << "</body>" << std::endl;
            this->mFile << "</html>" << std::endl;
            this->mFile.close();
        }

    }
}
