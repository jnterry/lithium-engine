#include <lie/core/ByteBuffer.hpp>
namespace lie{
	namespace core{
		void ByteBuffer::push_back(const ByteBuffer& other){
			this->mBytes.insert(this->mBytes.end(), other.mBytes.begin(), other.mBytes.end());
		}
	}
}