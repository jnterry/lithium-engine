#include <lie/core/Convert.hpp>
#include <lie/core/Logger.hpp>
#include <lie/core/ExcepParseError.hpp>
#include <sstream>
#include <cctype>
#include <stdlib.h>
#include <sstream>
#include <lie/core\Types.hpp>
#include <string>
#include <regex>


namespace lie{
    namespace core{
        //:TODO:
        //0_o these work when in Convert.cpp, Chars.cpp, etc but I get undefined reference errors
        //if I put them in Types.cpp, I made a PrimativeTypes.cpp, still undefined reference
        //removed and re-added Types.cpp to the project, still undefined references, other functions
        //in Types.cpp work fine
        //wtf is going on here.............
        size_t primativeSize(PrimativeType type){
            switch(type){
            case PrimativeType::uInt8:
                return sizeof(unsigned char);
            case PrimativeType::Int8:
                return sizeof(char);
            case PrimativeType::uInt16:
                return sizeof(unsigned short);
            case PrimativeType::Int16:
                return sizeof(short);
            case PrimativeType::uInt32:
                return sizeof(unsigned int);
            case PrimativeType::Int32:
                return sizeof(int);
            case PrimativeType::Float:
                return sizeof(float);
            case PrimativeType::Double:
                return sizeof(double);
            default:
                return 0;
            }
        }

        std::string primativeString(PrimativeType type){
            switch(type){
            case PrimativeType::uInt8:
                return "uInt8";
            case PrimativeType::Int8:
                return "Int8";
            case PrimativeType::uInt16:
                return "uInt16";
            case PrimativeType::Int16:
                return "Int16";
            case PrimativeType::uInt32:
                return "uInt32";
            case PrimativeType::Int32:
                return "Int32";
            case PrimativeType::Float:
                return "Float";
            case PrimativeType::Double:
                return "Double";
            default:
                return "Unknown-Type";
            }
        }

		int parseInt(std::string str) throw(lie::core::ExcepParseError){
			if (std::regex_match(str, std::regex("^\s*[\+-]?[0-9]+$"))){
				return atoi(str.c_str());
			} else {
				throw lie::core::ExcepParseError(__FUNCTION__, str, "Integer", "^\s*[\+-]?[0-9]+$", "");
			}
		}

		double parseDouble(std::string str) throw(lie::core::ExcepParseError){
			std::stringstream stream(str);
			double result;
			if(!(stream >> result)){
				throw lie::core::ExcepParseError(__FUNCTION__, str, "Decimal Number", "", "");
			}
			return result;
		}

		float parseFloat(std::string str) throw(lie::core::ExcepParseError){
			std::stringstream stream(str);
			float result;
			if(!(stream >> result)){
				throw lie::core::ExcepParseError(__FUNCTION__, str, "Decimal Number", "", "");
			}
			return result;
		}

		bool compareFloats(float f1, float f2, int maxOffset){
			//convet floats into ints and compare them in that way
			//each int maps onto a float, therefore adding 1 to a float converted into an int
			//will find the next float along, therefore the + - maxOffset says if f2 is within the 20
			// possilbe floats above or below f1, say there are equal
			//see http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm#_Toc135149455
			//for more info
			return ((*(int*)&f1)+maxOffset >= *(int*)&f2 && (*(int*)&f1)-maxOffset <= *(int*)&f2);
		}

		bool compareDoubles(double d1, double d2, int maxOffset){
			//convet floats into ints and compare them in that way
			//each int maps onto a float, therefore adding 1 to a float converted into an int
			//will find the next float along, therefore the + - maxOffset says if f2 is within the 20
			// possilbe floats above or below f1, say there are equal
			//see http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm#_Toc135149455
			//for more info
			return ((*(long*)&d1)+maxOffset >= *(long*)&d2 && (*(long*)&d1)-maxOffset <= *(long*)&d2);
		}
	}
}
