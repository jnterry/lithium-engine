#include <lie/core/Types.hpp>

namespace lie{
    namespace core{

        //primativeString and primativeSize defined in Convert.cpp, in here makes undefined reference linker error 0_o
        //:TODO:



        template<> PrimativeType getPrimativeType<unsigned char>(){
            return PrimativeType::uInt8;
        }

        template<> PrimativeType getPrimativeType<char>(){
            return PrimativeType::Int8;
        }

        template<> PrimativeType getPrimativeType<unsigned short>(){
            return PrimativeType::uInt16;
        }

        template<> PrimativeType getPrimativeType<short>(){
            return PrimativeType::Int16;
        }

        template<> PrimativeType getPrimativeType<unsigned int>(){
            return PrimativeType::uInt32;
        }

        template<> PrimativeType getPrimativeType<int>(){
            return PrimativeType::Int32;
        }

        template<> PrimativeType getPrimativeType<float>(){
            return PrimativeType::Float;
        }

        template<> PrimativeType getPrimativeType<double>(){
            return PrimativeType::Double;
        }

    }
}
