#include "ErrorToString.win.hpp"

namespace lie{
    namespace core{
        namespace detail{
            std::string win32ErrorToString(DWORD errCode){
                //http://www.codeproject.com/Tips/479880/GetLastError-as-std-string
                std::string errStr = "???";
                LPVOID lpMsgBuf;

                DWORD bufLen = FormatMessage(
                    FORMAT_MESSAGE_ALLOCATE_BUFFER |
                    FORMAT_MESSAGE_FROM_SYSTEM |
                    FORMAT_MESSAGE_IGNORE_INSERTS,
                    NULL,
                    errCode,
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                    (LPTSTR) &lpMsgBuf,
                    0, NULL );

                if (bufLen){
                    LPCSTR lpMsgStr = (LPCSTR)lpMsgBuf;
                    errStr = std::string(lpMsgStr, lpMsgStr+bufLen);

                    LocalFree(lpMsgBuf);
                }
                return errStr;
            }
        }
    }
}
