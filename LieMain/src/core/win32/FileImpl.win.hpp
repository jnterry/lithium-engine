#include <lie/core\File.hpp>
#include <windows.h>
namespace lie{
    namespace core{
        namespace detail{

            class FileImpl{
            public:
                static File getExecutable(){
                    CHAR cpath[MAX_PATH];
                    GetModuleFileNameA(GetModuleHandle(NULL), cpath, MAX_PATH);
                    return File(std::string(cpath));
                }

                static bool exists(File* file){
                    DWORD dwAttrib = GetFileAttributes(file->getFullPath(lie::core::PathStyle::Windows).c_str());
                    return (dwAttrib != INVALID_FILE_ATTRIBUTES) && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
                }

                static bool create(File* file, bool createParents){
                    if(file->exists()){return false;}
                    if(!file->getFolder().exists()){
                        if(createParents){
                            if(!file->getFolder().create(true)){
                                //failed to make parents
                                return false;
                            }
                        } else {
                            //parent folder(s) do not exist and we were asked not to make them, return false
                            return false;
                        }
                    }

                    //if still executing, then parents definately exist
                    HANDLE h = CreateFile(
                            file->getFullPath(lie::core::PathStyle::Windows).c_str(),
                            GENERIC_READ | GENERIC_WRITE,
                            0,//share mode -> do not allow multiple programs to open/read/write to the file simultaniously
                            NULL, //security settings
                            CREATE_NEW,
                            FILE_ATTRIBUTE_NORMAL,
                            NULL);
                    return (INVALID_HANDLE_VALUE != h);
                }

            };

        }
    }
}
