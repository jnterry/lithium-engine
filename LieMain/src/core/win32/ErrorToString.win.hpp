#ifndef LIE_RENDER_WIN32ERRORTOSTRING_HPP
#define LIE_RENDER_WIN32ERRORTOSTRING_HPP

#include <windows.h>
#include <string>

namespace lie{
    namespace core{
        namespace detail{

            /////////////////////////////////////////////////
            /// \brief Utility function which takes a windows api error code and returns the string
            /// describing it
            /////////////////////////////////////////////////
            std::string win32ErrorToString(DWORD errCode);

        }
    }
}

#endif // LIE_RENDER_WIN32ERRORTOSTRING_HPP
