#ifndef LIE_CORE_DETAIL_FOLDERIMPL_HPP
#define LIE_CORE_DETAIL_FOLDERIMPL_HPP

#include <lie/core\Folder.hpp>
#include <lie/core\File.hpp>
#include <lie/core\Logger.hpp>
#include <lie/core\Convert.hpp>
#include "ErrorToString.win.hpp"
#include <windows.h>
#include <iostream>
#include <string.h>


namespace lie{
    namespace core{
        namespace detail{
            class FolderImpl{
            public:
                static Folder getRoot(){
                    Folder dir;
                    return dir;
                }

                static Folder getExecutableFolder(){
                    CHAR cpath[MAX_PATH];
                    GetModuleFileNameA(GetModuleHandle(NULL), cpath, MAX_PATH);
                    return Folder(std::string(cpath)).pop(); //pop off the exe file name
                }

                static bool exists(const Folder* folder){
                    DWORD dwAttrib = GetFileAttributes(folder->getFullPath(lie::core::PathStyle::Windows).c_str());
                    return (dwAttrib != INVALID_FILE_ATTRIBUTES) && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
                }

                static bool create(const Folder* folder, bool createParents){
                    SetLastError(0);
                    if(folder->exists()){
                        return false;
                    } else {
                        if(!folder->getParent().exists()){
                            //then parents do not exist, CreateDirectory() does not create parent directories
                            //so if requested lets create the parents recursively
                             if(createParents){
                                if(!folder->getParent().create(true)){
                                    //failed to create parents
                                    return false;
                                }
                            } else {
                                //then parent does not exists, and we have been told not to make a new one
                                return false;
                            }
                        }
                        //if still executing then parents exist
                        return CreateDirectory(folder->getFullPath().c_str(), NULL);
                    }
                }

                static std::vector<Folder> getSubfolders(const Folder* folder) throw (lie::core::ExcepFileIO){
                    std::vector<Folder> results;
                    if(exists(folder)){
                        std::string searchQuerry = folder->getFullPath(lie::core::PathStyle::Windows)+"\\*";
                        if(searchQuerry.length() > MAX_PATH){
                            throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of sub folders but the path name is too long, \
								this is a windows limitation!");
                        }
                        WIN32_FIND_DATA ffd;
                        HANDLE fileh = FindFirstFile(searchQuerry.c_str(), &ffd);
                        if(INVALID_HANDLE_VALUE == fileh){
                            DWORD err = GetLastError();
							throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of sub folders \
								but FindFirstFile returned \"INVALID_HANDLE_VALUE\". Win error code: " +
								lie::core::toString(err) + ", msg: " + lie::core::detail::win32ErrorToString(err));
                        }
                        do{
                            if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
                                std::string filename(ffd.cFileName);
                                //windows list every folder as having a ".." and "." sub folder to indicate their parent, we want to ignore these
                                if(filename != ".." && filename != "."){
                                    results.push_back(Folder(folder->getFullPath(lie::core::PathStyle::Windows) + "\\" + filename));
                                }
                            }
                        }while(FindNextFile(fileh, &ffd) != 0);
                        DWORD err = GetLastError();
                        if(err != ERROR_NO_MORE_FILES){
							throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of sub folders \
								but a windows error occurred, code: " + lie::core::toString(err) + ", msg: " +
								lie::core::detail::win32ErrorToString(err));
																								
                        }
                        return results;
                    } else {
                        //folder does not exist so cant have any sub folders
                        return results;
                    }
                }

                static std::vector<File> getFiles(const Folder* folder) throw(lie::core::ExcepFileIO){
                    std::vector<File> results;
                    if(exists(folder)){
                        std::string searchQuerry = folder->getFullPath(lie::core::PathStyle::Windows)+"\\*";
                        if(searchQuerry.length() > MAX_PATH){
							throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of files but the path name is too long, \
								this is a windows limitation!");
						}
                        WIN32_FIND_DATA ffd;
                        HANDLE fileh = FindFirstFile(searchQuerry.c_str(), &ffd);
                        if(INVALID_HANDLE_VALUE == fileh){
                            DWORD err = GetLastError();
							throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of files \
								but FindFirstFile returned \"INVALID_HANDLE_VALUE\". Win error code: " +
								lie::core::toString(err) + ", msg: " + lie::core::detail::win32ErrorToString(err));
                        }
                        do{
                            if(!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)){
                                results.push_back(File(folder->getFullPath(lie::core::PathStyle::Windows) + "\\" + ffd.cFileName));
                            }
                        }while(FindNextFile(fileh, &ffd) != 0);
                        DWORD err = GetLastError();
                        if(err != ERROR_NO_MORE_FILES){
                            DWORD err = GetLastError();
							throw lie::core::ExcepFileIO(__FUNCTION__, folder->getFullPath(),
								"Win32: Tried to get list of files \
								but a windows error occurred, code: " + lie::core::toString(err) + ", msg: " +
								lie::core::detail::win32ErrorToString(err));
                        }
                        return results;
                    } else {
                        //folder does not exist so cant contain any files
                        return results;
                    }
                }

            };
        }
    }
}

#endif // LIE_CORE_DETAIL_FOLDERIMPL_HPP
