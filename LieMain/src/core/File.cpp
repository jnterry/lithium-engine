#include <lie/core\File.hpp>

#include <lie/core\Config.hpp>
#if defined(LIE_OS_WINDOWS)
    #include "win32/FileImpl.win.hpp"
//#elif defined(LIE_OS_LINUX)
   // #include "_LinuxFileImpl.hpp"
//#elif defined(LIE_OS_MACOS)
    //#include "_OSXFileImpl.hpp"
#else
    #error No implementation for this os is avalible!
#endif

namespace lie{
    namespace core{

        File::File(const Folder& dir, std::string fullname) : mDir(dir){
            this->setFullName(fullname);
        }

        File::File(const Folder& dir, std::string filename, std::string extension) : mDir(dir), mName(filename), mExtension(extension)
        {/*empty body*/}

        File::File(const std::string& path) : mDir(path){
            //the Folder now includes the file as initiated above in init list
            //set the file name to the last element of the dir
            this->setFullName(this->mDir.getName());
            this->mDir.pop(); //then pop the file name off the dir
        }

        std::string File::getFullName() const{
            if(this->mExtension == ""){
                //we dont want the '.' if the file has no extension
                return (this->mName);
            } else {
                return (this->mName + "." + this->mExtension);
            }
        }

        std::string File::getFullPath(lie::core::PathStyle style) const{
            switch(style){
            case lie::core::PathStyle::Windows:
                return this->mDir.getFullPath(style) + "\\" + this->getFullName();
            case lie::core::PathStyle::Unix:
                return this->mDir.getFullPath(style) + "\\" + this->getFullName();
            default:
                return "File::getFullPath() -> No implementation for style: " + (int)style;
            }

        }

        std::string File::getName() const{
            return this->mName;
        }

        std::string File::getExtension() const{
            return this->mExtension;
        }

        void File::setName(std::string name){
            this->mName = name;
        }

        void File::setExtension(std::string ext){
            this->mExtension = ext;
        }

        void File::setFullName(std::string name){
            unsigned int dotindex = name.find_last_of('.');
            if(dotindex == std::string::npos){
                //then there is not last dot, assume the file has no extension
                this->mName = name;
                this->mExtension = "";
            } else {
                //then there is a . char, split at it to find the extension
                this->mName = name.substr(0, dotindex);
                this->mExtension = name.substr(dotindex+1);
            }

        }

        Folder& File::getFolder(){
            return this->mDir;
        }

        void File::setFolder(Folder dir){
            this->mDir = dir;
        }

        bool File::operator==(const File& other) const{
            return (this->mDir == other.mDir &&
                    this->mName == other.mName &&
                    this->mExtension == other.mExtension);
        }


        ///////////////////////////////////////////////////////////////////////
        //PLATFORM SPECIFIC FUNCTIONS BELOW
        File File::getExecutable(){
            return lie::core::detail::FileImpl::getExecutable();
        }

        bool File::exists(){
            return lie::core::detail::FileImpl::exists(this);
        }

        bool File::create(bool createParents){
            return lie::core::detail::FileImpl::create(this, createParents);
        }
    }
}
