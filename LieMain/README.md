# What goes here? #

This is the root directory containing the Lithium Engine source code

## include ##
This folder container the header files defining the public interface of Lithium engine, your project should include these

## src ##
This folder contains source and header files related to the implementation of Lithium Engine, none of these need to be included in your project