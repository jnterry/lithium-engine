/*
    This examples shows various methods that can be used to create instances of the Mesh class.
    All data is hardcoded into the application rather than loaded from a file.
*/

//:TODO: finish this


#include <lie/core.hpp>
#include <lie/math.hpp>
#include <lie/render.hpp>

int main(int argc, char** argv) {
    ////////////////////////////////////
    //set up logs
    lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterHtml("Log.html")));
    lie::core::DLog.addWritter(lie::core::LogMsgWritterPtr(new lie::core::LogMsgWritterCout()));

    ////////////////////////////////////
    //init render system, create window
    lie::rend::RenderSystem rsys;
    lie::rend::Window app = rsys.createWindow(lie::rend::VideoMode(1024,768,32), "Mesh Creation", lie::rend::Window::Default);
    app.makeCurrent();

    ////////////////////////////////////
    //create the shader program
    lie::rend::Shader vertShader(lie::rend::Shader::Type::Vertex);
    vertShader.loadAndCompile(lie::core::File(lie::core::Folder::getExecutableFolder(), "vertex.glsl"));
    if(vertShader.isOkay()){
        LIE_DLOG_TRACE("MeshCreation", "Vertex Shader Okay");
    } else {
        LIE_DLOG_ERROR("MeshCreation", "Failed to load Vertex Shader, err: " << vertShader.getErrors());
    }
    lie::rend::Shader pixelShader(lie::rend::Shader::Type::Pixel);
    pixelShader.loadAndCompile(lie::core::File(lie::core::Folder::getExecutableFolder(), "fragment.glsl"));
    if(pixelShader.isOkay()){
        LIE_DLOG_TRACE("MeshCreation", "Pixel Shader Okay");
    } else {
        LIE_DLOG_ERROR("MeshCreation", "Failed to load Pixel Shader, err: " << pixelShader.getErrors());
    }
    lie::rend::ShaderProgram shaderProg;
    shaderProg.attachShader(vertShader);
    shaderProg.attachShader(pixelShader);
    shaderProg.link();
    if(shaderProg.isOkay()){
        LIE_DLOG_TRACE("MeshCreation", "Shader Program Okay");
    } else {
        LIE_DLOG_ERROR("MeshCreation", "Failed to link shader program, err: " << shaderProg.getErrors());
    }

    ////////////////////////////////////
    //Manual mesh creation - using two buffers, one for color and one for location
    //This mesh is the muticolored triange
    float mesh1Pos[] = {
        -1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 1.732051f, 0.0f,
    };

    float mesh1Col[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
    };
    lie::rend::Mesh* mesh1 = rsys.createMesh(2); //it has 2 attributes, position and color
    lie::rend::GpuBuffer mesh1PosBuf(lie::rend::GpuBuffer::UsageType::Geometry); //create gpu buffer for position
    mesh1PosBuf.bufferData(mesh1Pos, sizeof(mesh1Pos)); //put data in gpu buffer
    mesh1->getAttribute(0).set(&mesh1PosBuf, 3, lie::core::PrimativeType::Float, false, 0, 0); //set the mesh to use the new buffer
    lie::rend::GpuBuffer mesh1ColBuf(lie::rend::GpuBuffer::UsageType::Geometry);
    mesh1PosBuf.bufferData(mesh1Col, sizeof(mesh1Col));
    mesh1->getAttribute(1).set(&mesh1PosBuf, 3, lie::core::PrimativeType::Float, false, 0, 0);
    mesh1->setVertexCount(3);


    lie::rend::Renderer3dImmediate rend3d;
    ////////////////////////////////////
    //Enter main loop
    LIE_DLOG_TRACE("MeshCreation", "Entering main loop program");
    while(app.isOpen()){
        lie::rend::Event e;
        while(app.pollEvent(e)){
            switch(e.type){
            case lie::rend::Event::Closed:
                app.close();
            }//end of event switch
        }//end of event loop

    }//end of main loop
    LIE_DLOG_TRACE("MeshCreation", "Ending program");
}
