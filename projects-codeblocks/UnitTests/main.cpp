#include <iostream>
#include <lie-test\all.hpp>
#include <lie\core\Timer.hpp>

int main(int argc, char** argv) {

    lie::test::TestGroup rootTester("Root");

    std::cout << "Running unit tests..." << std::endl;
    lie::core::Timer utTimer;
    lie::Test_All(&rootTester);
    lie::core::Time utTime = utTimer.getElapsedTime();
    std::cout << "Done in " << utTime.asSeconds() << " seconds" << std::endl;

    std::cout << "Saving report..." << std::endl;
    lie::core::Timer reportTimer;
    if(rootTester.saveReport("Unit Test Report.html")){
        lie::core::Time reportTime = reportTimer.getElapsedTime();
        std::cout << "Report saved successfully in " << reportTime.asSeconds() << " seconds" << std::endl;
    } else {
        std::cout << "Failed to save report" << std::endl;
    }
}
